package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 更新一个月最后一天的价格
 * 
 * @author wzc
 * 
 */
public class UpdateLastDayPrice implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新最后一天的价格");
		new HuaminHotelDb().updatelastdayprice();
		System.out.println("最后一天的价格更新结束");
	}

}
