package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 更新合同附属信息
 * 
 * @author wzc
 * 
 */
public class UpdateContract implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新合同附属信息……");
		new HuaminHotelDb().updateContractInfo();
		System.out.println("更新合同附属信息over……");
	}

}
