package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 更新一个月价格
 * 
 * @author wzc
 * 
 */
public class UpdateOneMonthPrice implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新一个月的价格");
		new HuaminHotelDb().updateOneMonthPrice();
		System.out.println("结束更新一个月的价格");
	}

}
