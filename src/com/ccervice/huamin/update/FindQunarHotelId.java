package com.ccervice.huamin.update;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc 根据本地酒店查找去哪酒店id
 * 
 */
public class FindQunarHotelId {

	/**
	 * 更新QunarHotelId
	 * 
	 * @throws Exception
	 */
	public void updateQunarHotelId() throws Exception {
		List<City> citys = Server.getInstance().getHotelService().findAllCity(
						"where C_QUNARCODE IS NOT NULL ",
						"order by id asc", -1, 0);
		int l=citys.size();
		System.out.println("城市数量："+citys.size());
		for (City city : citys) {
			System.out.println("城市名称："+city.getName()+",城市数量"+l--);
			List<Hotel> hotelstemp = Server.getInstance().getHotelService()
					.findAllHotel("where c_sourcetype=1 and c_cityid=101 and (c_qunarid is null or c_qunarid='' or c_qunarid='null')  and c_cityid="
									+ city.getId(), "ORDER BY ID ASC", -1, 0);
			System.out.println("酒店数量："+hotelstemp.size());
			int m=hotelstemp.size();
			for (Hotel hotel : hotelstemp) {
				System.out.println("酒店名称："+hotel.getName()+",酒店数量"+m--);
				String hotelname = hotel.getName().replaceAll("TF", "").trim();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, 1);
				String strUrl = "http://hotel.qunar.com/render/renderAPI.jsp?"
						+ "showAllCondition=1"
						+ "&attrs=L0F4L3C1,ZO1FcGJH,J6TkcChI,HCEm2cI6,08F7hM4i,8dksuR_,YRHLp-jc,pl6clDL0,HFn32cI6,vf_x4Gjt,2XkzJryU,vNfnYBK6,TDoolO-H,pk4QaDyF,x0oSHP6u,z4VVfNJo,5_VrVbqO,VAuXapLv,U1ur4rJN,px3FxFdF,pk4QaDyF,HGYGeXFY,6X7_yoo3,0Ie44fNU,dDjWmcqr,MMObDrW4,ownT_WG6,yYdMIL83,Y0LTFGFh,8F2RFLSO"
						+ "&showBrandInfo=2"
						+ "&showNonPrice=1"
						+ "&showFullRoom=1"
						+ "&showPromotion=1"
						+ "&showTopHotel=1"
						+ "&showGroupShop=1"
						+ "&output=json1.1"
						// +"&v=0.05416518063093645"
						+ "&cityurl=" + city.getQunarcode() + "&q="
						+ URLEncoder.encode(hotelname, "utf-8") + "&fromDate="
						+ sdf.format(new Date()) + "&toDate="
						+ sdf.format(cal.getTime()) + "&requestor=RT_HSLIST"
						// +"&filterid=3a7d82ac-a754-447e-a366-009e0b1984d8_A"
						+ "&requestTime=" + System.currentTimeMillis()
						+ "&needFP=1" + "&__jscallback=XQScript_11";
				System.out.println(strUrl);
				String json = PHUtil.submitPost(strUrl, "").toString();
				String tmp = json.substring(json.indexOf("{"), json.length());
				JSONObject datas = JSONObject.fromObject(tmp.subSequence(0, tmp
						.lastIndexOf(")")));
				JSONArray hotels = datas.getJSONArray("hotels");
				boolean flag = true;
				for (int i = 0; i < hotels.size(); i++) {
					JSONObject hotelt = hotels.getJSONObject(i);
					JSONObject attrs = hotelt.getJSONObject("attrs");
					JSONObject selected = hotelt.getJSONObject("selected");
					if (selected.containsKey("fts2")) {
						if (flag) {
							flag = false;
							if (!(hotelt.getString("id")).equals(hotel
									.getQunarId())) {
								System.out.println(attrs.getString("hotelName")
										+ ":" + hotelt.getString("id") + " ID:"
										+ hotel.getId() + " Sourcetype:"
										+ hotel.getSourcetype() + " 原QunarId:"
										+ hotel.getQunarId() + " 更改后QunarId:"
										+ hotelt.getString("id"));
								hotel.setQunarId(hotelt.getString("id"));
								Server.getInstance().getHotelService()
										.updateHotelIgnoreNull(hotel);
							}

						} else {
							System.out.println(attrs.getString("hotelName")
									+ ":" + hotelt.getString("id") + " ID:"
									+ hotel.getId() + " Sourcetype:"
									+ hotel.getSourcetype()
									+ " fts2出现多次 将更改QunarId为空");
							hotel.setQunarId("");
							Server.getInstance().getHotelService()
									.updateHotelIgnoreNull(hotel);
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		new FindQunarHotelId().updateQunarHotelId();
	}
}
