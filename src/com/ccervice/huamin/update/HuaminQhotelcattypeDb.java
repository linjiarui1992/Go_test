package com.ccervice.huamin.update;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.inter.server.Server;

/**
 * 
 * 
 * @Caless:HuaminQhotelcattypeDb.java
 * @ClassDesc:录入华闽酒店相关的房型床型数据
 * @Author:胡灿
 * @Date:2012-12-15 上午11:07:01
 * @Company: 航天华有(北京)科技有限公司
 * @Copyright: Copyright (c) 2012
 * @version: 1.0
 */
public class HuaminQhotelcattypeDb {
	
	
	public HuaminQhotelcattypeDb() {
		
	}
	/**
	 * 查询华闽酒店相关的房型床型数据
	 * 
	 * @param requestBean
	 */
	public void getQhotelcattypetemp(Long hotelid) {
		Hotel hotel = Server.getInstance().getHotelService().findHotel(hotelid);
		if(hotel!=null && hotel.getHotelcode()!=null && !"".equals(hotel.getHotelcode().trim())){
			String url = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotelcattype&p_lang=SIM";
			url += "&p_hotel=" + hotel.getHotelcode();
			System.out.println("[查询华闽酒店相关的房型床型数据]url:" + url);
			String str = Util.getStr(url);
			System.out.println(str);
			parsexml_cancel(str,hotelid,hotel.getHotelcode());
		}
	}
	/**
	 * 查询华闽酒店相关的房型床型数据
	 * 
	 * @param requestBean
	 */
	@SuppressWarnings("unchecked")
	public void getQhotelcattype() {
		List<Hotel> hotelList = Server.getInstance().getHotelService().findAllHotel("where C_SOURCETYPE=3  and C_HOTELCODE is not null ", "order by c_cityid ", -1,0);
		int i = hotelList.size();
		for (Hotel hotel : hotelList) {
			System.out.println("剩余酒店数量：" + i--+",当前酒店："+hotel.getName());
			String url = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotelcattype&p_lang=SIM";

			// 1 P_COMPANY 公司賬號 为必须 已写死在url中
			// 2 P_ID 網頁用戶名稱 为必须 已写死在url中
			// 3 P_PASS 網頁密碼 为必须 已写死在url中
			// 4 P_LANG ENG-英文 (預設) CHN-中文繁體 SIM-中文簡體 为必须 已写死在url中

			// 5 P_COUNTRY 國家代碼
//			if (requestBean.getCountry() != null
//					&& !"".equals(requestBean.getCountry())) {
//				url += "&p_country=" + requestBean.getCountry();
//			}
//
//			// 6 P_AREA 地區代碼
//			if (requestBean.getArea() != null
//					&& !"".equals(requestBean.getArea())) {
//				url += "&p_area=" + requestBean.getArea();
//			}
//
//			// 7 P_CITY 城市代碼
//			if (requestBean.getCity() != null
//					&& !"".equals(requestBean.getCity())) {
//				url += "&p_city=" + requestBean.getCity();
//			}
//
//			// 8 P_CAT 房型代碼
//			if (requestBean.getCat() != null
//					&& !"".equals(requestBean.getCat())) {
//				url += "&p_cat=" + requestBean.getCat();
//			}
//			// 9 P_CATNAME 房型名稱
//			if (requestBean.getCatname() != null
//					&& !"".equals(requestBean.getCatname())) {
//				url += "&p_catname=" + requestBean.getCatname();
//			}
//			// 10 P_GRADE 酒店星級 5, 4, 3, 2, 1
//			if (requestBean.getGrade() != null
//					&& !"".equals(requestBean.getGrade())) {
//				url += "&p_grade=" + requestBean.getGrade();
//			}
			// 11 P_HOTEL 酒店代碼 TODO
			// if(requestBean.getHotel()!=null&&!"".equals(requestBean.getHotel())){
			// url+="&p_hotel="+requestBean.getHotel();
			// }
			if(hotel.getHotelcode()==null || "".equals(hotel.getHotelcode().trim())){
				continue;
			}
			url += "&p_hotel=" + hotel.getHotelcode();

			// 12 P_HOTELNAME 酒店名稱
//			if (requestBean.getHotelname() != null
//					&& !"".equals(requestBean.getHotelname())) {
//				url += "&p_hotelname=" + requestBean.getHotelname();
//			}
//			// 13 P_CREATED 日-月-年eg.18-Oct-12
//			if (requestBean.getCreated() != null
//					&& !"".equals(requestBean.getCreated())) {
//				url += "&p_created=" + requestBean.getCreated();
//			}
//			// 14 P_MODIFY 日-月-年eg.18-Oct-12
//			if (requestBean.getModify() != null
//					&& !"".equals(requestBean.getModify())) {
//				url += "&p_modify=" + requestBean.getModify();
//			}

			System.out.println("[查询华闽酒店相关的房型床型数据]url:" + url);
			String str = Util.getStr(url);
			parsexml_cancel(str,hotel.getId(),hotel.getHotelcode());
		}
	}

	/**
	 * 录入华闽酒店相关的房型床型数据
	 * 
	 * @param str
	 */
	@SuppressWarnings("unchecked")
	private void parsexml_cancel(String xmlstr,long hotelid,String hmHotelCode) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(xmlstr!=null && xmlstr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
			xmlstr = xmlstr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		}
		SAXBuilder build = new SAXBuilder();
		Document document;
		try {
			document = build.build(new StringReader(xmlstr));
			Element root = document.getRootElement();
			Element result = root.getChild("XML_RESULT");
			Element hotels = result.getChild("HOTELS");
			if (result.getChildren().size() > 2) {
				List<Element> hotel = hotels.getChildren("HOTEL");
				for (Element hotelbean : hotel) {
					String hotelcode = hotelbean.getChildText("HOTEL");// 酒店代碼
					if(hotelcode==null || !hotelcode.equals(hmHotelCode)){
						continue;
					}
					Element rooms = hotelbean.getChild("ROOMS");
					List<Element> room = rooms.getChildren("ROOM");
					for (Element roombean : room) {
						String cat = roombean.getChildText("CAT");
						String catname = roombean.getChildText("CATNAME");
						String type = roombean.getChildText("TYPE");
						String typename = roombean.getChildText("TYPENAME");
						String wifi=roombean.getChildText("WIFI");//无线
						String boardband=roombean.getChildText("BOARDBAND");//宽带上网
						String bedtypeid = null;
						if (type != null && !"".equals(type)) {
							List<Bedtype> bedtypeList = Server.getInstance()
									.getHotelService().findAllBedtype(
											"where C_TYPE='" + type + "'", "",
											-1, 0);
							Bedtype bedtype;
							boolean falgBedtype = true;
							if (bedtypeList != null && bedtypeList.size() > 0) {
								bedtype = bedtypeList.get(0);
								falgBedtype = false;
								bedtypeid = bedtype.getId() + "";
							} else {
								bedtype = new Bedtype();
								bedtype.setId(0L);
								bedtype.setType(type);
							}
							if (typename != null && !"".equals(typename)) {
								bedtype.setTypename(typename);
							}
							// bedtype.setMaxguest(0L);//最多入住人数 默认为0
							if (falgBedtype) {
								if(bedtype!=null&&bedtype.getType()!=null){
									if(bedtype.getType().equals("D")){
										bedtype.setTypename("大床");
									}
									if(bedtype.getType().equals("S")){
										bedtype.setTypename("单床");
									}
									if(bedtype.getType().equals("T")){
										bedtype.setTypename("双床");
									}
								}
								Bedtype bt = Server.getInstance().getHotelService().createBedtype(bedtype);
								System.out.println("[录入华闽酒店相关的房型床型数据]向C_BEDTYPE表添加一条数据,添加成功,ID:"+ bt.getId());
								bedtypeid = bt.getId() + "";
							} else {
								if(bedtype!=null&&bedtype.getType()!=null){
									if(bedtype.getType().equals("D")){
										bedtype.setTypename("大床");
									}
									if(bedtype.getType().equals("S")){
										bedtype.setTypename("单床");
									}
									if(bedtype.getType().equals("T")){
										bedtype.setTypename("双床");
									}
								}
								Server.getInstance().getHotelService().updateBedtypeIgnoreNull(bedtype);
							}
						}

						if (cat != null && !"".equals(cat)) {
							List<Roomtype> roomtypeList = Server.getInstance().getHotelService().findAllRoomtype(
											"where C_ROOMCODE='" + cat+ "' and  C_BED='"+ bedtypeid+ "' and   C_HOTELID="+ hotelid, "", -1, 0);
							Roomtype rt;
							boolean falgRoomtype = true;
							if (roomtypeList != null && roomtypeList.size() > 0) {
								rt = roomtypeList.get(0);
								rt.setState(1);
								falgRoomtype = false;
							} else {
								rt = new Roomtype();
								rt.setId(0L);
								rt.setRoomcode(cat);
								rt.setState(1);
								rt.setLanguage(0);
							}
							if (catname != null && !"".equals(catname)) {
								rt.setName(catname);
							}
							if(wifi!=null&&wifi.length()>0&&!"".equals(wifi)){
								rt.setWidedesc(wifi);
							}
							if(boardband!=null&&!"".equals(boardband)){
								if(rt.getWidedesc()!=null){
									rt.setWidedesc(wifi+","+boardband);
								}else{
									rt.setWidedesc(boardband);
								}
							}
							rt.setHotelid(Long.valueOf(hotelid));
							if (bedtypeid != null) {
								rt.setBed(Integer.valueOf(bedtypeid));
							}
							rt.setLastupdatetime(sdf.format(new Date(System.currentTimeMillis())));
							if (falgRoomtype) {
								Roomtype bt = Server.getInstance().getHotelService().createRoomtype(rt);
							} else {
								Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(rt);
							}

						}
					}
				}
			}
			Element resultcode = result.getChild("RETURN_CODE");
			String resultco = resultcode.getText();
			String errormessage = result.getChildText("ERROR_MESSAGE");
			if (errormessage != null || !"".equals(errormessage)) {
				WriteLog.write("查询华闽酒店相关的房型床型数据", "错误信息:" + resultco + ","
						+ errormessage);
			}
		} catch (Exception e) {
			WriteLog.write("查询华闽酒店相关的房型床型数据", "返回数据格式有问题,错误数据:" + xmlstr);
			e.printStackTrace();
		}

	}

	/**
	 * 日期格式转换 如 "2012-11-08"转换后"08-Nov-12"
	 * 
	 * @param datestr
	 * @return
	 */
	public String getdatestr(String datestr) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Locale l = new Locale("en");
		Date date;
		try {
			date = sdf.parse(datestr);
			String day = String.format("%td", date);
			String month = String.format(l, "%tb", date);
			String year = String.format("%ty", date);
			datestr = day + "-" + month + "-" + year;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(datestr);
		return datestr;
	}

	/**
	 * 获取指定日期的前一天
	 * 
	 * @return
	 */
	public static Date CatchYesterday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.roll(Calendar.DATE, -1);
		Date newdate = cal.getTime();
		return newdate;
	}
}
