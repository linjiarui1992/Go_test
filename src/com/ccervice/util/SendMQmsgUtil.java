package com.ccervice.util;

import java.util.List;

import javax.jms.JMSException;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

/**
 * <p>发送MQ消息</P>
 * @author zhangqifei
 * @time 2016年8月11日 下午2:38:54
 */
public class SendMQmsgUtil {

	 /**
     * 根据配置文件判断使用什么技术类型
     * @param sendBody
     * @param name
     * @throws JMSException
     */
    public static void sendGetUrlMQmsg(String sendBody) throws JMSException{
    	String type=PropertyUtil.getValue("type", "train.properties");
        String QUEUE_NAME = getSysconfigString("QueueMQ_trainorder_waitorder_orderid");
    	int temp=Integer.parseInt(type);
    	if(temp==0){
    		sendAvtiveMQmsg(sendBody,QUEUE_NAME);
    	}else if(temp==1){
    		sendALiMQmsg(sendBody,QUEUE_NAME);
    	}else{
    		return;
    	}
    }
    
    /**
     * 发送avtivemq
     * @param sendBody
     * @param mqName
     * @throws JMSException
     */
    public static void sendAvtiveMQmsg(String sendBody, String mqName) throws JMSException {
        String url = getSysconfigString("activeMQ_url");
        sendAvtiveMQmsg(sendBody, url, mqName);
    }

    /**
     * 发送avtivemq
     * @param sendBody
     * @param mqUrl
     * @param mqName
     * @throws JMSException
     */
    public static void sendAvtiveMQmsg(String sendBody, String mqUrl, String mqName) throws JMSException {
        WriteLog.write("TongchengSupplyMethodMqMSGUtil_sendActiveMQmsg","mqname--->" + mqName + ":mqUrl--->" + mqUrl + ":sendBody--->" + sendBody);
        ActiveMQUtil.sendMessage(mqUrl, mqName, sendBody);
    }
    
    /**
     * 发送阿里云mq
     * @param sendBody
     * @param topicName
     * @throws JMSException 
     */
    public static void sendALiMQmsg(String sendBody,String topicName) throws JMSException{
    	String producerId=PropertyUtil.getValue("ProducerId", "train.properties");
    	String accessKey=PropertyUtil.getValue("AccessKey", "train.properties");
    	String secretKey=PropertyUtil.getValue("SecretKey", "train.properties");
    	String tagName=PropertyUtil.getValue("tagName", "train.properties");
    	String keyOrderId=PropertyUtil.getValue("keyOrderId", "train.properties");
    	sendALiMQmsg(sendBody,topicName,producerId,accessKey,secretKey,tagName,keyOrderId);
    }
    
    /**
     * 发送阿里云mq
     * @param sendBody
     * @param topicName
     * @param producerId
     * @param accessKey
     * @param secretKey
     * @throws JMSException 
     */
    public static void sendALiMQmsg(String sendBody,String topicName,String producerId,
    		String accessKey,String secretKey,String tagName,String keyOrderId) throws JMSException{
    	WriteLog.write("TongchengSupplyMethodMqMSGUtil_sendALiMQmsg", "topicName--->" + topicName + ":producerId--->" + producerId
                + ":accessKey--->" + accessKey+ ":secretKey--->" + secretKey+ ":sendBody--->" + sendBody+ ":tagName--->" + tagName+ ":keyOrderId--->" + keyOrderId);
    	ALiMQUtil.sendMessage(sendBody, topicName, producerId, accessKey, secretKey, tagName, keyOrderId);
    }
    
    /**
     * 根据sysconfig的name获得value
     * 内存
     * @param name
     * @return
     */
    public static String getSysconfigString(String name) {
        String result = "-1";
        try {
            if (Server.getInstance().getDateHashMap().get(name) == null) {
                List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                        .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
                if (sysoconfigs.size() > 0) {
                    result = sysoconfigs.get(0).getValue();
                    Server.getInstance().getDateHashMap().put(name, result);
                }
            }
            else {
                result = Server.getInstance().getDateHashMap().get(name);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
	
   
}
