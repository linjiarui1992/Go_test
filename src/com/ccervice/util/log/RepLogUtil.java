/**
 * 
 */
package com.ccervice.util.log;

import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.compareprice.PropertyUtil;

/**
 * 此工具类是发送rep的异常信息到指定的url
 * 该url可以记录rep的信息包括错误但不限于错误的其他信息
 * @time 2015年10月30日 上午10:29:03
 * @author chendong
 */
public class RepLogUtil extends Thread {

    public static void main(String[] args) {

        //        RepLogUtil reputil = new RepLogUtil("L299", "内网ip", "外网ip", "记录内容", 0);
        RepLogUtil reputil = new RepLogUtil("L300", "10.251.247.161", "121.43.155.68", "1", 0);
        reputil.start();

    }

    String RepNo;

    String RepNip;

    String RepWip;

    String Contentstr;

    int Type;

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        super.run();
        getString();
    }

    /**
     * 
     * @param repNo         rep编号
     * @param repNip           rep内网ip
     * @param repWip           rep外网ip
     * @param contentstr         rep出错原因
     * @param type           1:下单,2
     * @time 2015-10-30 15:16:40
     * @author zhaohongbo
     */
    public RepLogUtil(String repNo, String repNip, String repWip, String contentstr, int type) {
        super();
        this.RepNo = repNo;
        this.RepNip = repNip;
        this.RepWip = repWip;
        this.Contentstr = contentstr;
        this.Type = type;
    }

    /**
     * 根据url获取对应的rep信息
     * @time 2015-10-30 10:42:44
     * @author zhaohongbo
     * */
    public void getString() {
        String repLogSendUrl = PropertyUtil.getValue("repLogSendUrl", "train.log.properties");
        String paramContent = "RepNo=" + RepNo + "&RepNip=" + RepNip + "&RepWip=" + RepWip + "&Contentstr="
                + Contentstr + "&Type=" + Type;
        System.out.println(paramContent);
        String result = SendPostandGet.submitPost(repLogSendUrl, paramContent).toString();
        System.out.println(result);
    }
}
