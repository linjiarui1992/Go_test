package com.ccervice.util;

import java.sql.Timestamp;

import com.ccservice.t12306.method.reg.Reg12306DBHelper;

public class RegistrationLong {
    public static void main(String[] args) {
        RegistrationLong registrationLong = new RegistrationLong("ADSL001");
        registrationLong.ErrorPrint("err01", "name1");
        try {

        }
        catch (Exception e) {
        }
    }

    public String adslNo;

    public RegistrationLong(String adslNo) {
        super();
        this.adslNo = adslNo;
    }

    /**插入异常信息
     * 2015年11月3日 18:39:01
     * yangtao
     * 
     * name配置文件里的域名
     * err 异常信息
     * time 返回结果信息时间
     * ty帐号
     * adname adsl名字
     * 
     * 待修改
     * */
    public void ErrorPrint(String regResult, String name) {
        try {
            String sql = null;
            String typej = "";
            if (regResult.startsWith("true")) {
                typej = "成功";
            }
            else if (regResult.contains("获取注册账号失败") || "".equals(regResult) || regResult.contains("该手机号码已被注册")
                    || regResult.contains("没有可用号码") || regResult.contains("获取手机号失败") || regResult.contains("您可继续使用获取验证码短信功能")
                    || regResult.contains("您获取验证码短信次数过多") || regResult.contains("系统忙，请稍后再试") || regResult.contains("网络繁忙")
                    || regResult.contains("您填写的身份信息重复") || "telnet登录失败".contains(regResult) || "打码校验失败".contains(regResult)
                    || regResult.contains("失败后修改数据库:用户名已经被注册") || regResult.contains("获取手机号失败还原状态")
                    || regResult.contains("注册没开") || regResult.contains("短信验证后身份信息重复")) {
                typej = "失败";
            }
            else if (regResult.contains("验证码") || regResult.contains("成功了去激活")) {
                typej = "成功";
                //times = new Timestamp((System.currentTimeMillis()));
                // String sqls = "(select ID from Reg12306MonitorBean where RegistrationType='成功')";
            }
            else {
                typej = "失败";
            }
            Timestamp time = new Timestamp(System.currentTimeMillis());
            sql = "INSERT INTO [Reg12306MonitorBean]([RegistrationType],[RegistrationTime],[Describe],[Adsl]) VALUES('"
                    + typej + "','" + time + "','" + regResult + "','adslNo:" + adslNo + "')";
            //            System.out.println(sql);
            Reg12306DBHelper.executeSql(sql);
        }
        catch (Exception e) {
            //            e.printStackTrace();
        }
    }

    public String getAdslNo() {
        return adslNo;
    }

    public void setAdslNo(String adslNo) {
        this.adslNo = adslNo;
    }

}
