
/**
 * AirWsCoManageCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.baitour.www;

    /**
     *  AirWsCoManageCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AirWsCoManageCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AirWsCoManageCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AirWsCoManageCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getPatSpecialPriceDataStr method
            * override this method for handling normal response from getPatSpecialPriceDataStr operation
            */
           public void receiveResultgetPatSpecialPriceDataStr(
                    com.baitour.www.AirWsCoManageStub.GetPatSpecialPriceDataStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatSpecialPriceDataStr operation
           */
            public void receiveErrorgetPatSpecialPriceDataStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInvalidationProviderList method
            * override this method for handling normal response from getInvalidationProviderList operation
            */
           public void receiveResultgetInvalidationProviderList(
                    com.baitour.www.AirWsCoManageStub.GetInvalidationProviderListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviderList operation
           */
            public void receiveErrorgetInvalidationProviderList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticSpecialPriceList method
            * override this method for handling normal response from getDomesticSpecialPriceList operation
            */
           public void receiveResultgetDomesticSpecialPriceList(
                    com.baitour.www.AirWsCoManageStub.GetDomesticSpecialPriceListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticSpecialPriceList operation
           */
            public void receiveErrorgetDomesticSpecialPriceList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterDomesticCabinZRateByXml method
            * override this method for handling normal response from getAlterDomesticCabinZRateByXml operation
            */
           public void receiveResultgetAlterDomesticCabinZRateByXml(
                    com.baitour.www.AirWsCoManageStub.GetAlterDomesticCabinZRateByXmlResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterDomesticCabinZRateByXml operation
           */
            public void receiveErrorgetAlterDomesticCabinZRateByXml(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAlterDomesticCabinZRateByParam method
            * override this method for handling normal response from getAlterDomesticCabinZRateByParam operation
            */
           public void receiveResultgetAlterDomesticCabinZRateByParam(
                    com.baitour.www.AirWsCoManageStub.GetAlterDomesticCabinZRateByParamResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAlterDomesticCabinZRateByParam operation
           */
            public void receiveErrorgetAlterDomesticCabinZRateByParam(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticMatchNormalZRate method
            * override this method for handling normal response from getDomesticMatchNormalZRate operation
            */
           public void receiveResultgetDomesticMatchNormalZRate(
                    com.baitour.www.AirWsCoManageStub.GetDomesticMatchNormalZRateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticMatchNormalZRate operation
           */
            public void receiveErrorgetDomesticMatchNormalZRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInvalidationProviderListXML method
            * override this method for handling normal response from getInvalidationProviderListXML operation
            */
           public void receiveResultgetInvalidationProviderListXML(
                    com.baitour.www.AirWsCoManageStub.GetInvalidationProviderListXMLResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviderListXML operation
           */
            public void receiveErrorgetInvalidationProviderListXML(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticCabinZRate method
            * override this method for handling normal response from getDomesticCabinZRate operation
            */
           public void receiveResultgetDomesticCabinZRate(
                    com.baitour.www.AirWsCoManageStub.GetDomesticCabinZRateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticCabinZRate operation
           */
            public void receiveErrorgetDomesticCabinZRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticFlightData method
            * override this method for handling normal response from getDomesticFlightData operation
            */
           public void receiveResultgetDomesticFlightData(
                    com.baitour.www.AirWsCoManageStub.GetDomesticFlightDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticFlightData operation
           */
            public void receiveErrorgetDomesticFlightData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticMatchNormalZRateListStr method
            * override this method for handling normal response from getDomesticMatchNormalZRateListStr operation
            */
           public void receiveResultgetDomesticMatchNormalZRateListStr(
                    com.baitour.www.AirWsCoManageStub.GetDomesticMatchNormalZRateListStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticMatchNormalZRateListStr operation
           */
            public void receiveErrorgetDomesticMatchNormalZRateListStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticSpecialPriceListStr method
            * override this method for handling normal response from getDomesticSpecialPriceListStr operation
            */
           public void receiveResultgetDomesticSpecialPriceListStr(
                    com.baitour.www.AirWsCoManageStub.GetDomesticSpecialPriceListStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticSpecialPriceListStr operation
           */
            public void receiveErrorgetDomesticSpecialPriceListStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticCabinZRateStr method
            * override this method for handling normal response from getDomesticCabinZRateStr operation
            */
           public void receiveResultgetDomesticCabinZRateStr(
                    com.baitour.www.AirWsCoManageStub.GetDomesticCabinZRateStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticCabinZRateStr operation
           */
            public void receiveErrorgetDomesticCabinZRateStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInvalidationProviderOfficeNoList method
            * override this method for handling normal response from getInvalidationProviderOfficeNoList operation
            */
           public void receiveResultgetInvalidationProviderOfficeNoList(
                    com.baitour.www.AirWsCoManageStub.GetInvalidationProviderOfficeNoListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviderOfficeNoList operation
           */
            public void receiveErrorgetInvalidationProviderOfficeNoList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticMatchNormalZRateList method
            * override this method for handling normal response from getDomesticMatchNormalZRateList operation
            */
           public void receiveResultgetDomesticMatchNormalZRateList(
                    com.baitour.www.AirWsCoManageStub.GetDomesticMatchNormalZRateListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticMatchNormalZRateList operation
           */
            public void receiveErrorgetDomesticMatchNormalZRateList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatSpecialPriceData method
            * override this method for handling normal response from getPatSpecialPriceData operation
            */
           public void receiveResultgetPatSpecialPriceData(
                    com.baitour.www.AirWsCoManageStub.GetPatSpecialPriceDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatSpecialPriceData operation
           */
            public void receiveErrorgetPatSpecialPriceData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticMatchNormalZRateStr method
            * override this method for handling normal response from getDomesticMatchNormalZRateStr operation
            */
           public void receiveResultgetDomesticMatchNormalZRateStr(
                    com.baitour.www.AirWsCoManageStub.GetDomesticMatchNormalZRateStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticMatchNormalZRateStr operation
           */
            public void receiveErrorgetDomesticMatchNormalZRateStr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getInvalidationProviderListSTR method
            * override this method for handling normal response from getInvalidationProviderListSTR operation
            */
           public void receiveResultgetInvalidationProviderListSTR(
                    com.baitour.www.AirWsCoManageStub.GetInvalidationProviderListSTRResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getInvalidationProviderListSTR operation
           */
            public void receiveErrorgetInvalidationProviderListSTR(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDomesticFlightDataStr method
            * override this method for handling normal response from getDomesticFlightDataStr operation
            */
           public void receiveResultgetDomesticFlightDataStr(
                    com.baitour.www.AirWsCoManageStub.GetDomesticFlightDataStrResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDomesticFlightDataStr operation
           */
            public void receiveErrorgetDomesticFlightDataStr(java.lang.Exception e) {
            }
                


    }
    