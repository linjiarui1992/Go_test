package com.yeexing.webservice.service;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plcid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orgCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dstCity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="airComp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cabin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="flightNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plcuser" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="airSeg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "plcid",
    "orgCity",
    "dstCity",
    "airComp",
    "cabin",
    "startDate",
    "flightNo",
    "plcuser",
    "airSeg",
    "sign"
})
@XmlRootElement(name = "AirpQueryStatusBusiness")
public class AirpQueryStatusBusiness {

    @XmlElement(required = true, nillable = true)
    protected String userName;
    @XmlElement(required = true, nillable = true)
    protected String plcid;
    @XmlElement(required = true, nillable = true)
    protected String orgCity;
    @XmlElement(required = true, nillable = true)
    protected String dstCity;
    @XmlElement(required = true, nillable = true)
    protected String airComp;
    @XmlElement(required = true, nillable = true)
    protected String cabin;
    @XmlElement(required = true, nillable = true)
    protected String startDate;
    @XmlElement(required = true, nillable = true)
    protected String flightNo;
    @XmlElement(required = true, nillable = true)
    protected String plcuser;
    @XmlElement(required = true, nillable = true)
    protected String airSeg;
    @XmlElement(required = true, nillable = true)
    protected String sign;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the plcid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlcid() {
        return plcid;
    }

    /**
     * Sets the value of the plcid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlcid(String value) {
        this.plcid = value;
    }

    /**
     * Gets the value of the orgCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgCity() {
        return orgCity;
    }

    /**
     * Sets the value of the orgCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgCity(String value) {
        this.orgCity = value;
    }

    /**
     * Gets the value of the dstCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDstCity() {
        return dstCity;
    }

    /**
     * Sets the value of the dstCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDstCity(String value) {
        this.dstCity = value;
    }

    /**
     * Gets the value of the airComp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirComp() {
        return airComp;
    }

    /**
     * Sets the value of the airComp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirComp(String value) {
        this.airComp = value;
    }

    /**
     * Gets the value of the cabin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabin() {
        return cabin;
    }

    /**
     * Sets the value of the cabin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabin(String value) {
        this.cabin = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the flightNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNo() {
        return flightNo;
    }

    /**
     * Sets the value of the flightNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNo(String value) {
        this.flightNo = value;
    }

    /**
     * Gets the value of the plcuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlcuser() {
        return plcuser;
    }

    /**
     * Sets the value of the plcuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlcuser(String value) {
        this.plcuser = value;
    }

    /**
     * Gets the value of the airSeg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirSeg() {
        return airSeg;
    }

    /**
     * Sets the value of the airSeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirSeg(String value) {
        this.airSeg = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

}
