package com.ccservice.qunar.train;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class MyThread12306AccountCheckStatusTongcheng extends Thread {
    private String repUrl = "http://121.40.162.18:9016/Reptile/traininit";

    private String checkurl = "http://121.40.162.18:9016/Reptile/CheckName.jsp";

    public void run() {
        while (true) {
            execute();
        }
    }

    public static void main(String[] args) {
        MyThread12306AccountCheckStatusTongcheng myThread12306AccountCheckStatus = new MyThread12306AccountCheckStatusTongcheng();
        while (true) {
            if (getNowTime()) {
                myThread12306AccountCheckStatus.execute();
            }
            else {
                try {
                    Thread.sleep(10000L);
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 说明:接口可调用时间:早7晚11
     * @param date
     * @return
     * @time 2014年8月30日 下午4:23:20
     * @author yinshubin
     */
    public static boolean getNowTime() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("07:00:00");
            Date dateAfter = df.parse("23:00:00");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
            WriteLog.write("QunarTrainIdVerification", e + "");
        }
        return false;//现在24小时,以后有需要再改为FALSE
    }

    public void execute() {
        List customerusers = getCustomeruserCheckMobile();
        if (customerusers.size() == 0) {
            System.out.println("没有待核验手机号的账号了,等会");
        }
        else {
            for (int i = 0; i < customerusers.size(); i++) {
                Customeruser customeruser = new Customeruser();
                Map map = (Map) customerusers.get(i);
                String loginname = map.get("C_LOGINNAME").toString();
                String password = map.get("C_LOGPASSWORD").toString();
                Long id = Long.parseLong(map.get("ID").toString());
                customeruser.setId(id);
                customeruser.setLoginname(loginname);
                customeruser.setLogpassword(password);
                int isExist = checkLoginName(loginname);
                if (isExist == 1) {//12306存在的话去核验
                    //                    String cookieString = JobTrainUtil.getCookie(repUrl, loginname, password);
                    //                    if (cookieString != null && cookieString.startsWith("JSESSIONID")) {
                    checkPhone(customeruser, "");
                    //                    }
                }
                else if (isExist == 0) {//12306不存在 修改状态
                    //#TODO
                    //12注册完邮箱账号拿完乘客等待去12306注册信息
                    //                        把这个账号的isenable改为12
                    ChangeStatusPhone2Ennused(customeruser, 12);
                }
                try {
                    if (isExist != 1) {
                        Thread.sleep(100L);
                    }
                    else {
                        Thread.sleep(1000L);
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取到n个准备核验的账号
     * @return
     * @time 2015年9月30日 下午3:10:00
     * @author chendong
     */
    private List getCustomeruserCheckMobile() {
        //这个存储过程还没写,isenable =-11的customeruser
        //        SELECT TOP 10 *
        //        FROM [B2B_DB_TC].[dbo].[T_CUSTOMERUSER] with(nolock) where c_type=4 
        //        and c_isenable=-11 order by id desc
        String procedureSqlString = "sp_CustomerUser_Job12306AccountCheckStatus_tongcheng";
        List customerusers = getsystemservice().findMapResultByProcedure(procedureSqlString);
        return customerusers;
    }

    private ISystemService getsystemservice() {
        //        String Job12306AccountCheckStatus_serviceurl = PropertyUtil.getValue("serviceurl",
        //                "train.checkStatus.properties");
        //        HessianProxyFactory factory = new HessianProxyFactory();
        //        ISystemService isystemservice = null;
        //        try {
        //            isystemservice = (ISystemService) factory.create(ISystemService.class,
        //                    Job12306AccountCheckStatus_serviceurl + ISystemService.class.getSimpleName());
        //        }
        //        catch (MalformedURLException e) {
        //            e.printStackTrace();
        //        }
        //        return isystemservice;
        try {
            return Server.getInstance().getSystemService();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 账号是否存在
     * 
     * @param loginname
     * @return true 存在   false不存在
     * @time 2015年9月30日 下午2:50:23
     * @author chendong 
     */
    private int checkLoginName(String loginname) {
        //        {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":true,"messages":[],"validateMessages":{}}
        int isexist = -1;
        try {
            String checkname = SendPostandGet.submitGet(checkurl + "?name=" + loginname);
            isexist = Integer.valueOf(checkname);
        }
        catch (Exception e) {
        }

        return isexist;
    }

    public void checkPhone(Customeruser customeruser, String cookie) {
        JSONObject reqobj = new JSONObject();
        reqobj.put("loginName", customeruser.getLoginname() == null ? "" : customeruser.getLoginname());
        reqobj.put("loginPwd", customeruser.getLogpassword() == null ? "" : customeruser.getLogpassword());
        reqobj.put("orderId", 1008611);//用于日志记录
        reqobj.put("cookie", customeruser.getCardnunber());//cookie
        reqobj.put("train_date", "2015-11-16");//乘车日期
        reqobj.put("from_station", "TYV");//出发站编码
        reqobj.put("to_station", "TDV");//到达站编码
        reqobj.put("from_station_name", "太原");//出发站名
        reqobj.put("to_station_name", "太原东");//到达站名
        reqobj.put("train_code", "2672");//车次
        reqobj.put("orderType", 0);//1：去哪儿订单
        reqobj.put("queryLink", PropertyUtil.getValue("queryTicketLink"));//查询车票链接，防止12306变数据，从cn_interface传入
        reqobj.put(
                "seatTypeOf12306",
                "P#tz_num#特等座@M#zy_num#一等座@O#ze_num#二等座@F#rw_num#动卧@E#qt_num#特等软座@A#qt_num#高级动卧@9#swz_num#商务座@8#ze_num#二等软座@7#zy_num#一等软座@6#gr_num#高级软卧@4#rw_num#软卧@3#yw_num#硬卧@2#rz_num#软座@1#yz_num#硬座@0#wz_num#无座");//12306座席
        reqobj.put(
                "passengers",
                "{\"oldPassengerStr\":\"杨鹏鸿,1,452226200007215714,1_\",\"prices\":\"189.5\",\"zwcodes\":\"1\",\"passengerTicketStr\":\"1,0,1,杨鹏鸿,1,452226200007215714,,N\"}");
        String jsonStr = reqobj.toJSONString();
        try {
            jsonStr = URLEncoder.encode(jsonStr, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //请求参数
        String param = "datatypeflag=" + "301" + "&jsonStr=" + jsonStr;
        //请求REP
        String retdata = SendPostandGet.submitPost(repUrl, param, "UTF-8").toString();
        System.out.println(retdata);
        //                if (retdata != null && !"-1".equals(retdata)) {
        //                    JSONObject jsonObject = JSONObject.parseObject(retdata);
        //                    
        //                }
        //        returnb returnBean = new JSONObject();
        if (retdata == null || !retdata.contains("success")) {
            return;
        }
        try {
            JSONObject repobj = JSONObject.parseObject(retdata);
            if (repobj.getBooleanValue("success")) {
                ChangeStatusPhone2Canused(customeruser);
                return;
            }
            else {
                String msg = repobj.getString("msg");
                /* 31身份信息未通过核验
                * 32注册的用户与其他用户重复
                * 33手机核验
                * 41备用1
                * 42备用2
                */
                if (msg.indexOf("您在12306网站注册时填写信息有误") > -1) {//31
                    ChangeStatusPhone2Ennused(customeruser, 31);
                }
                else if (msg.indexOf("您注册的信息与其他用户重复") > -1) {//32
                    ChangeStatusPhone2Ennused(customeruser, 32);
                }
                else if (msg.indexOf("手机核验") > -1) {//33
                    ChangeStatusPhone2Ennused(customeruser, 33);
                }
                else {
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //解析数据
    }

    private void ChangeStatusPhone2Ennused(Customeruser cus, int type) {
        String procedureSqlString = "sp_Customeruser_Update_Isenable_Enused @type=" + type + " ,@id=" + cus.getId();
        WriteLog.write("12306Check_ChangeStatusPhone2Ennused_Tongcheng", procedureSqlString);
        getsystemservice().findMapResultByProcedure(procedureSqlString);
    }

    private void ChangeStatusPhone2Canused(Customeruser cus) {
        WriteLog.write("12306Check_ChangeStatusPhone2Canused_Tongcheng", cus.getId() + "");
        //        String procedureSqlString = "sp_Customeruser_Update_Isenable_Canused @id=" + cus.getId();
        //        WriteLog.write("12306Check_ChangeStatusPhone2Canused_Tongcheng", procedureSqlString);
        //        getsystemservice().findMapResultByProcedure(procedureSqlString);
    }
}
