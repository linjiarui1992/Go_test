package com.ccservice.qunar.train;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

public class JobQunarOrderOfflineHtpw implements Job{
	
	private long qcyg_agentid;
	
	private String qunarPayurl;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		 String isqunaracquisition = null;
	        long syscfid = 0L;
	        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        WriteLog.write("JobQunarOffline_Test", sdFormat.format(new Date()));
	        String merchantCode=PropertyUtil.getValue("htpwmerchantCode", "train.properties");
	        String HMAC=PropertyUtil.getValue("htpwHMAC", "train.properties");
	        try {
	            String url = HttpClient
	                    .httpget(
	                            "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="+merchantCode+"&type=WAIT_TICKET&HMAC="+HMAC,
	                            "UTF-8");
	            WriteLog.write("JobQunarOrderOffline_json拉单json_htpw", "josn:" + url);
	            Classification(JSONObject.parseObject(url));
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
	}
	
	public void Classification(JSONObject jsonObject) {
        //        getCommon();
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrderOffline", jsonObject.toString());
            String orderNos = allOrderNos(data);
            WriteLog.write("JobQunarOrderOffline", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
                                                        isoldorder = true;
                            break;
//                                                        exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setOrderjson(info);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }
	
	// 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
    	System.out.println("StarthtpwQunarLadan");
        JobDetail jobDetail = new JobDetail("JobQunarOrderOfflineHtpw", "JobQunarOrderOfflineHtpwGroup",
        		JobQunarOrderOfflineHtpw.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobQunarOrderOfflineHtpw","JobQunarOrderOfflineHtpwGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
	
	public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;

        t1 = new MyThreadQunarOrderOfflineHtpw(qunarordermethod, this.qcyg_agentid, this.qunarPayurl);
        pool.execute(t1);

        pool.shutdown();
//        WriteLog.write("JobQunarOrderOffline", "qunar订单号:" + qunarordermethod.getQunarordernumber() + "线程关闭");
    }
	
	public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
	public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }

	public static void main(String[] args) {
		String url = "{\"data\":[{\"arrStation\":\"昆山南\",\"dptStation\":\"北京南\",\"extSeat\":[],\"extSeatMap\":[],\"isPaper\":1,\"orderDate\":\"2016-07-09 14:36:41\",\"orderNo\":\"htpww16070914364191c\",\"orderType\":0,\"paperBackup\":0,\"paperLowSeatCount\":0,\"paperType\":3,\"passengers\":[{\"certNo\":\"132433197006167218\",\"certType\":\"1\",\"name\":\"夏永民\",\"ticketType\":\"1\"},{\"certNo\":\"132433196904264015\",\"certType\":\"1\",\"name\":\"张大响\",\"ticketType\":\"1\"}],\"seat\":{\"3\":898.5},\"seatMap\":{\"一等座\":898.5},\"ticketPay\":1797,\"ticketToStationNo\":\"9496\",\"trainEndTime\":\"2016-07-11 14:48\",\"trainNo\":\"G115\",\"trainStartTime\":\"2016-07-11 09:22\",\"transportAddress\":\"北京北京市崇文区北京南站一层南进站口东侧150米报亭\",\"transportName\":\"闫鑫\",\"transportPhone\":\"13911288883\"}],\"ret\":true,\"total\":1}";
		JobQunarOrderOfflineHtpw jobQunarOrderOfflinehtpw = new JobQunarOrderOfflineHtpw();
		jobQunarOrderOfflinehtpw.Classification(JSONObject.parseObject(url));
	}

}
