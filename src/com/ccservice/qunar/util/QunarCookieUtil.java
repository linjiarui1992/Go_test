package com.ccservice.qunar.util;

import java.io.*;
import com.ccservice.compareprice.PropertyUtil;

/**
 * 获取去哪儿Cookie，与cn_interface保持一致
 * @author WH
 */

public class QunarCookieUtil {

    private static String dir = PropertyUtil.getValue("QunarCookiePath").replace("/", File.separator);

    /**读Cookie*/
    public static String getCookie(String ip) {
        StringBuffer buf = new StringBuffer();
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            String txt = dir + File.separator + "HotelCookie" + File.separator + ip + ".txt";
            File file = new File(txt);
            if (file.exists()) {
                fis = new FileInputStream(txt);
                isr = new InputStreamReader(fis);
                br = new BufferedReader(isr);
                String lineTxt = null;
                while ((lineTxt = br.readLine()) != null) {
                    buf.append(lineTxt);
                }
            }
        }
        catch (Exception e) {
            buf = new StringBuffer();
        }
        finally {
            try {
                if (br != null)
                    br.close();
            }
            catch (Exception e) {
            }
            try {
                if (isr != null)
                    isr.close();
            }
            catch (Exception e) {
            }
            try {
                if (fis != null)
                    fis.close();
            }
            catch (Exception e) {
            }
        }
        return buf.toString();
    }

    /**注释Cookie，表示抓取数据完毕，不再请求去哪儿获取Cookie*/
    public static void endCatch(String ip) {
        //注释Cookie
        String cookie = "//" + getCookie(ip);
        //重写Cookie
        FileWriter fw = null;
        BufferedWriter bw = null;
        try {
            String txt = dir + File.separator + "HotelCookie" + File.separator + ip + ".txt";
            File file = new File(txt);
            if (file.exists()) {
                fw = new FileWriter(file);
                bw = new BufferedWriter(fw);
                bw.write(cookie);
            }
        }
        catch (Exception e) {

        }
        finally {
            try {
                if (bw != null)
                    bw.flush();
                bw.close();
            }
            catch (Exception e) {
            }
            try {
                if (fw != null)
                    fw.flush();
                fw.close();
            }
            catch (Exception e) {
            }
        }
    }

}