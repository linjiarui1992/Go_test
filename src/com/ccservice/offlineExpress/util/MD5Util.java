package com.ccservice.offlineExpress.util;

import java.security.MessageDigest;

public class MD5Util {

    /**
     * @Description: 32位加密小写
     * @time 2017年8月11日 上午11:40:14
     * @author WH
     */
    public static String MD5Encode(String origin) {
        //结果
        String result = "";
        //加密
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //加密
            result = byteArrayToHexString(md5.digest(origin.getBytes()));
        }
        catch (Exception e) {

        }
        //返回
        return result;
    }

    /**
     * 带字符编码的MD5
     * @author WH
     * @time 2017年8月7日 下午5:12:48
     * @version 1.0
     * @param origin 明文 32位 小写
     * @param charset 字符编码，如UTF-8
     * @return 密文
     */
    public static String MD5Encode(String origin, String charset) {
        //结果
        String result = "";
        //加密
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            //加密
            result = byteArrayToHexString(md5.digest(origin.getBytes(charset)));
        }
        catch (Exception e) {

        }
        //返回
        return result;
    }

    /**
     * @author zcn
     * @time 2017年8月7日 下午5:12:48
     * @version 1.0
     * @param origin 明文 32位 大写
     * @return 密文
     */
    public final static String MD5(String origin) {
        try {
            byte[] btInput = origin.getBytes();
            //获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            //使用指定的字节更新摘要
            mdInst.update(btInput);
            //获得密文
            byte[] md = mdInst.digest();
            //把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigitsToLarge[byte0 >>> 4 & 0xf];
                str[k++] = hexDigitsToLarge[byte0 & 0xf];
            }
            return new String(str);
        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 带字符编码的MD5
     * @author ztj
     * @time 2017年8月7日 下午5:12:48
     * @version 1.0
     * @param origin 明文 32位 大写
     * @param charset 字符编码，如UTF-8
     * @return 密文
     */
    public final static String MD5(String origin, String charset) {
        try {
            byte[] btInput = origin.getBytes(charset);
            //获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            //使用指定的字节更新摘要
            mdInst.update(btInput);
            //获得密文
            byte[] md = mdInst.digest();
            //把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigitsToLarge[byte0 >>> 4 & 0xf];
                str[k++] = hexDigitsToLarge[byte0 & 0xf];
            }
            return new String(str);
        }
        catch (Exception e) {
            return "";
        }
    }

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        return hexDigits[n / 16] + hexDigits[n % 16];
    }

    private static String byteArrayToHexString(byte b[]) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            buf.append(byteToHexString(b[i]));
        }
        return buf.toString();
    }

    private static final String hexDigits[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f" };

    private static final char hexDigitsToLarge[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
            'D', 'E', 'F' };

}