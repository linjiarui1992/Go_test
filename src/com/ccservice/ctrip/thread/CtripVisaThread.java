package com.ccservice.ctrip.thread;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.visa.Visa;
import com.ccservice.b2b2c.base.visamaterial.Visamaterial;
import com.ccservice.b2b2c.base.visamateriallink.Visamateriallink;
import com.ccservice.ctrip.capturedata.ICripVisa;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CtripVisaThread implements Runnable {

    private String url = "http://localhost:8080/Reptile/service/";

    private HessianProxyFactory factory = new HessianProxyFactory();

    private String guoJiaMingCheng;

    private List detailInfoList = new ArrayList();;

    private Visa visa = new Visa();

    private Visamaterial visamaterial = new Visamaterial();

    private Visamateriallink visamateriallink = new Visamateriallink();

    public CtripVisaThread(String guoJiaMingCheng, List detailInfoList) {
        this.guoJiaMingCheng = guoJiaMingCheng;
        this.detailInfoList = detailInfoList;
    }

    @Override
    public void run() {
        try {
            if (detailInfoList != null && detailInfoList.size() > 0) {
                for (int i = 0; i < detailInfoList.size(); i++) {
                    JSONObject jsonObject = JSONObject.fromObject(detailInfoList.get(i).toString());
                    String detailInfoLink = jsonObject.getString("detailInfoLink");
                    if (detailInfoLink != null && !detailInfoLink.equals("")) {
                        String detailInfo = dataDnalysisDetailsPage(detailInfoLink);
                        if (detailInfo != null && !detailInfo.equals("") && !detailInfo.equals("FAIL")) {
                            createNativeVisa(detailInfo);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
        }
    }

    public String dataDnalysisDetailsPage(String detailInfoLink) {
        try {
            ICripVisa service = (ICripVisa) factory.create(ICripVisa.class, url + ICripVisa.class.getSimpleName());
            String detailInfo = service.dataDnalysisDetailsPage(detailInfoLink.trim());
            return detailInfo;
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "FAIL";
    }

    public void createNativeVisa(String detailInfo) {
        try {
            JSONObject josonObject = JSONObject.fromObject(detailInfo);
            String qianZhengMingCheng = josonObject.getString("qianZhengMingCheng");
            String guoQi = josonObject.getString("guoQi");
            String jiaGe = josonObject.getString("jiaGe");
            String qianZhengLeiXing = josonObject.getString("qianZhengLeiXing");
            String shouLiShiJian = josonObject.getString("shouLiShiJian");
            String ruJingCiShu = josonObject.getString("ruJingCiShu");
            String tingLiuTianShu = josonObject.getString("tingLiuTianShu");
            String youXiaoQi = josonObject.getString("youXiaoQi");
            String shouLiFanWei = josonObject.getString("shouLiFanWei");
            String yuDingXuZhi = josonObject.getString("yuDingXuZhi");
            visa.setVisaname(qianZhengMingCheng);
            visa.setWorkday(shouLiShiJian);
            String str = qianZhengMingCheng;
            String[] str1 = str.split("（");
            if (str1 != null && str1.length == 2) {
                visa.setWorkaddress(str1[1].replace("送签）", ""));
            }
            visa.setGuestarea(shouLiFanWei);
            if (qianZhengLeiXing.contains("商务")) {
                visa.setVisatypeid(58L);
            }
            else if (qianZhengLeiXing.contains("旅游")) {
                visa.setVisatypeid(60L);
            }
            visa.setValidity(youXiaoQi);
            String sql = "select ID as id from T_COUNTRY where C_ZHNAME like '%" + guoJiaMingCheng + "%';";
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list != null && list.size() > 0) {
                Map map = (Map) list.get(0);
                visa.setCountryid(Long.valueOf(map.get("id").toString()));
            }
            visa.setStaydays(tingLiuTianShu);
            visa.setInterview(5L);
            visa.setInvation(5L);
            visa.setPrice(jiaGe);
            visa.setCreateuserid(62L);
            visa.setCreatetime(new Timestamp(System.currentTimeMillis()));
            if (jiaGe.contains("实时")) {
                visa.setStatus(0L);
            }
            else {
                visa.setStatus(1L);
            }
            visa.setNotices(yuDingXuZhi);
            visa.setShowisnot(4L);
            visa.setComeintimes(ruJingCiShu);
            visa.setCountryImg(guoQi);
            visa.setHotVisa(0);
            visa.setLocalData(1);
            this.visa = Server.getInstance().getVisaService().createVisa(visa);
            JSONArray suoXuCaiLiaoArr = josonObject.getJSONArray("suoXuCaiLiao");
            if (suoXuCaiLiaoArr != null && suoXuCaiLiaoArr.size() > 0) {
                for (int i = 0; i < suoXuCaiLiaoArr.size(); i++) {
                    JSONObject suoXuCaiLiao = JSONObject.fromObject(suoXuCaiLiaoArr.get(i));
                    String caiLiaoMingCheng = suoXuCaiLiao.getString("caiLiaoMingCheng");
                    String caiLiaoMiaoShu = suoXuCaiLiao.getString("caiLiaoMiaoShu");
                    visamaterial.setMaterialname(caiLiaoMingCheng);
                    visamaterial.setMaterialdesc(caiLiaoMiaoShu);
                    this.visamaterial = Server.getInstance().getVisaService().createVisamaterial(visamaterial);
                    visamateriallink.setVisaid(this.visa.getId());
                    visamateriallink.setMaterialid(String.valueOf(this.visamaterial.getId()));
                    Server.getInstance().getVisaService().createVisamateriallink(visamateriallink);
                    this.visamaterial.setId(0);
                }
            }
            this.visa.setId(0);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
