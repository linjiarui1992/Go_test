package com.ccservice.jielv.hotel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.region.Region;

public class Util {
	public static String getStr(String urlstr) {
		String totalurl = urlstr;
		URL url = null;
		try {
			url = new URL(totalurl);
			try {
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setDoInput(true);
				conn.connect();
				InputStream inputStream = conn.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						inputStream));
				StringBuilder sb = new StringBuilder();
				String str = "";
				while ((str = br.readLine()) != null) {
					sb.append(str);
				}
				String strReturn = sb.toString().trim();
				conn.disconnect();
//				if (strReturn.contains("?")) {
//					strReturn = strReturn.replaceFirst("\\?", "");
//				}
				return strReturn;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	// 根据区域id查找本地区域id
	public static Long getregionidbycode(String code, String regionname) {
		List<Region> regions = Server.getInstance().getHotelService()
				.findAllRegion(
						" WHERE " + Region.COL_HUAMINCODE + "='" + code + "'",
						"", -1, 0);
		if (regions.size() > 0) {
			if (regions.size() > 1) {
				for (Region region : regions) {
					if (regionname.equals(region.getName())) {
						return region.getId();
					}
				}
			} else {
				return regions.get(0).getId();
			}
		}
		return 0l;
	}

	// 根据华敏城市code查找本地城市id
	public static long getcityidbycode(String huamincode) {
		List<City> cityes = Server.getInstance().getHotelService().findAllCity(
				" where c_type=1 and " + City.COL_HUAMINCODE + "='"
						+ huamincode + "'", "", -1, 0);
		if (cityes.size() > 0) {
			return cityes.get(0).getId();
		}
		return 0;
	}

}
