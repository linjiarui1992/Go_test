package com.ccservice.jielv.hotelupdate;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateJllastDayPriceJob5 implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("更新月数最后一天价格开始……");
		JLUpdateJob.updateJllastDayPrice();
		System.out.println("更新月数最后一天价格结束……");
	}

}
