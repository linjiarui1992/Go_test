package com.ccservice.grabticket;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.grabticket.util.DateUtils;
import com.ccservice.grabticket.util.MD5Util;
import com.ccservice.huamin.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.util.httpclient.HttpsClientUtils;
public class CancelGrabticketServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
    
    private TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
    
    private String search_url = "http://bespeak.hangtian123.net/trainorder_bespeak/CancelTicket";
    
    private Long outtime = 6000L;
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String orderId = request.getParameter("orderId");
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(Long.parseLong(orderId));
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------订单信息查询失败------");
        }
        
        //查询帐号
        String partnerid = PropertyUtil.getValue("search_partnerid", "grabticket.properties");
        //查询key值
        String key = PropertyUtil.getValue("search_key", "grabticket.properties");
        String reqtime = DateUtils.dateFormat(new Date(), "yyyyMMddHHmmss");
        //签名
        String sign = "";
        try {
            sign = MD5Util.MD5(partnerid + reqtime + MD5Util.MD5(key));
        } catch (Exception e) {
            WriteLog.write("线上线下抢票接口对接", "------取消订单签名失败------");
        }
        
        JSONObject _param = new JSONObject();
        String orderid = trainOrderOffline.getOrderNumber();
        _param.put("qorderid", orderid);//抢票订单号(唯一)
        _param.put("partnerid", partnerid);//帐号
        _param.put("reqtime", reqtime);//调用接口时间
        _param.put("sign", sign);//加密校验
        WriteLog.write("线上线下抢票接口对接", _param.toJSONString());
        JSONObject jsonObject = new JSONObject();
        try {
            Map<String, String> listdata = new HashMap<>();
            listdata.put("jsonStr", _param.toJSONString());
            String result = HttpsClientUtils.posthttpclientdata(search_url, listdata, outtime);
            WriteLog.write("线上线下抢票接口对接", result);
            JSONObject resultJson = JSONObject.parseObject(result);
            boolean isSuccess = resultJson.getBooleanValue("isSuccess");//取消订单是否成功
            if (isSuccess) {
                jsonObject.put("isSuccess", true);
                jsonObject.put("msg", "线下抢票订单取消成功");
                WriteLog.write("线上线下抢票接口对接", "------线下抢票订单取消成功------" + orderid);
            } else {
                jsonObject.put("isSuccess", false);
                jsonObject.put("msg", "线下抢票订单取消失败");
                String code = resultJson.getString("code");//下单结果代码
                String msg = resultJson.getString("msg");//下单结果信息
                WriteLog.write("线上线下抢票接口对接", "------线下抢票订单取消失败------" + code + ":" + msg);
            }
        } catch (Exception e) {
            jsonObject.put("isSuccess", false);
            jsonObject.put("msg", "线下抢票订单取消接口调用失败");
            WriteLog.write("线上线下抢票接口对接", "------线下抢票订单取消接口调用失败------");
        }
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(jsonObject.toJSONString().getBytes(Charset.forName("UTF-8")));
    }
}
