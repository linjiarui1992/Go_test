package com.ccservice.newelong.hoteldb;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import org.dom4j.Element;
import org.dom4j.Document;
import java.util.Iterator;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import net.sf.json.JSONObject;
import org.dom4j.io.SAXReader;
import org.dom4j.DocumentHelper;
import java.io.FileOutputStream;
import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.elong.inter.DownFile;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.mixdata.GetQunarMixKey;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;


/**
 * 酒店品牌
 * 艺龙2.0接口
 * 去哪儿接口
 * @author WH
 */
public class NewBrandCNDB {
	
	private static Map<Long,Boolean> chainmap = new HashMap<Long, Boolean>();
	private static String mixKey = "0e6d244df4242a2c0d13891739000700dly2ufLwZDmTwq";//默认一个MixKey
	
	public static void main(String[] args) throws Exception{
		long start = System.currentTimeMillis();
		System.out.println("Start Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
		//下载文件
		DownFile.download("http://api.elong.com/xml/v2.0/hotel/brand_cn.xml", "D:\\酒店数据\\brand_cn.xml");
		//解析艺龙酒店品牌
		elongbrand("D:\\酒店数据\\brand_cn.xml");
		//去哪儿酒店品牌
		qunarbrand();
		long end = System.currentTimeMillis();
		System.out.println("End Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}
	
	@SuppressWarnings("unchecked")
	public static void elongbrand(String filename) throws Exception {
		//获取XML
		SAXReader reader = new SAXReader();
		Document document = reader.read(new File(filename));
		String xml = document.asXML();
		//解析XML
		Document doc = DocumentHelper.parseText(xml.toString());
		Element root = doc.getRootElement();
		List<Element> HotelBrands = root.elements("HotelBrand");
		//遍历XML
		if(HotelBrands!=null && HotelBrands.size()>0){
			for(Element ele:HotelBrands){
				String BrandId = ele.attributeValue("BrandId");//艺龙品牌ID
				String ShortName = ele.attributeValue("ShortName");//简称
				String Letters = ele.attributeValue("Letters");//拼音简写
				if(UpdateElDataUtil.StringIsNull(BrandId) || UpdateElDataUtil.StringIsNull(ShortName)){
					continue;
				}
				String sql = "where C_BRANDID = '"+BrandId+"'";
				List<Chaininfo> locals = Server.getInstance().getHotelService().findAllChaininfo(sql, "", -1, 0);
				if(locals==null || locals.size()==0){
					Chaininfo chaininfo = new Chaininfo();
					chaininfo.setName(ShortName);
					chaininfo.setBindid(BrandId);
					chaininfo.setShortname(Letters);
					Server.getInstance().getHotelService().createChaininfo(chaininfo);
					System.out.println("从艺龙新增酒店品牌---" + ShortName);
				}else{
					boolean flag = false;
					Chaininfo chaininfo = locals.get(0);
					if(!ShortName.equals(chaininfo.getName())){
						flag = true;
						chaininfo.setName(ShortName);
					}
					if(!UpdateElDataUtil.StringIsNull(Letters) && !Letters.equalsIgnoreCase(chaininfo.getShortname())){
						flag = true;
						chaininfo.setShortname(Letters);
					}
					if(!UpdateElDataUtil.StringIsNull(chaininfo.getImagepic()) && UpdateElDataUtil.StringIsNull(chaininfo.getImagepic2())){
						flag = true;
						download(chaininfo.getImagepic(),chaininfo);
					}
					if(flag){
						Server.getInstance().getHotelService().updateChaininfoIgnoreNull(chaininfo);
						System.out.println("从艺龙更新酒店品牌---" + ShortName);
					}else{
						System.out.println("从艺龙匹配酒店品牌---" + ShortName);
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void qunarbrand() throws Exception{
		List<City> citys = Server.getInstance().getHotelService().findAllCity("where C_QUNARCODE is not null and C_SUPERIORCITY is null", "order by ID", -1, 0);
		for(City city:citys){
			if(!UpdateElDataUtil.StringIsNull(city.getQunarcode())){
				try {
					getqunar(city);
				} catch (Exception e) {
					System.out.println("去哪儿城市"+city.getName()+"品牌异常---" + e.getMessage());
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void getqunar(City city) throws Exception{
		String fromDate = UpdateElDataUtil.getCurrentDate();
		String toDate = UpdateElDataUtil.getAddDate(fromDate, 1);
		long currentTimeMillis = System.currentTimeMillis();
		//Address
		String strUrl = "http://hotel.qunar.com/render/renderAPIList.jsp?attrs=L0F4L3C1,ZO1FcGJH,J6TkcChI,HCEm2cI6,08F7hM4i," +
						"8dksuR_,YRHLp-jc,pl6clDL0,HFn32cI6,vf_x4Gjt,2XkzJryU,vNfnYBK6,TDoolO-H,pk4QaDyF,x0oSHP6u,z4VVfNJo,5_VrVbqO," +
						"VAuXapLv,U1ur4rJN,px3FxFdF,xaSZV4wU,ZZY89LZZ,HGYGeXFY,ownT_WG6,0Ie44fNU,yYdMIL83,MMObDrW4,dDjWmcqr,Y0LTFGFh," +
						"6X7_yoo3,8F2RFLSO,U3rHP23d&showAllCondition=0&showBrandInfo=1&showNonPrice=0&showFullRoom=0&showPromotion=0&" +
						"showTopHotel=0&showGroupShop=0&output=json1.1&v=0.7880550532970746&requestTime=" + currentTimeMillis +
						"&mixKey=" + mixKey + "&requestor=RT_HSLIST&cityurl=" + city.getQunarcode() + "&fromDate=" + fromDate + 
						"&toDate=" + toDate + "&limit=0%2C1&needFP=0&__jscallback=XQScript_7";
		//Request
		String json = PHUtil.submitPost(strUrl, "").toString();
		if(!UpdateElDataUtil.StringIsNull(json)){
			json = json.replace("XQScript_7(", "");
			json = json.substring(0, json.lastIndexOf(")"));
			JSONObject obj = JSONObject.fromObject(json);
			if(obj.containsKey("errmsg")){
				//mixKey失效 XQScript_7({"ret":false,"errcode":110,"errmsg":"invalid cookie"})
				if("invalid cookie".equals(obj.getString("errmsg"))){
					//获取mixKey
					mixKey = GetQunarMixKey.get(city.getQunarcode());
					//重新请求
					getqunar(city);
				}
				//访问频繁，需要验证码 XQScript_7({"ret":false,"errcode":110,"errmsg":"invalid vcd cookie"})
				else if("invalid vcd cookie".equals(obj.getString("errmsg"))){
					System.out.println("访问频繁，等待10秒...");
					//等10秒
					Thread.sleep(1000*10);
					//重新请求
					getqunar(city);
				}
			}
			//解析去哪儿返回信息
			else{
				JSONObject info = obj.getJSONObject("info");
				//品牌
				JSONObject brands = info.getJSONObject("brands");
				Iterator<String> qunarcodes = brands.keySet().iterator();
				while(qunarcodes.hasNext()){
					String QunarBrandId = qunarcodes.next();
					JSONObject brandinfo = brands.getJSONObject(QunarBrandId);
					String brandname = brandinfo.getString("name");
					int brandcount = brandinfo.getInt("count");
					String group = brandinfo.getString("group");
					String brandlogo = brandinfo.getString("logo");
					if(UpdateElDataUtil.StringIsNull(QunarBrandId) || UpdateElDataUtil.StringIsNull(brandname))continue;
					if("如家快捷".equals(brandname)) brandname = "如家";
					if("汉庭酒店".equals(brandname)) brandname = "汉庭";
					if("锦江酒店".equals(brandname)) brandname = "锦江";
					if("锐思特连锁".equals(brandname)) brandname = "锐思特";
					if("海航大酒店".equals(brandname)) brandname = "海航";
					if("金陵连锁".equals(brandname)) brandname = "金陵";
					if("丽笙Blu".equals(brandname)) brandname = "丽笙";
					if("国际青年旅舍".equals(brandname)) brandname = "国际青年旅舍";
					if("海航大酒店".equals(brandname)) brandname = "海航";
					if("泊捷时尚酒店".equals(brandname)) brandname = "泊捷时尚";
					if("南苑e家".equals(brandname)) brandname = "南苑E家";
					if("粤海酒店".equals(brandname)) brandname = "粤海";
					if("开元大酒店".equals(brandname)) brandname = "开元";
					if("开元名都".equals(brandname)) brandname = "开元";
					if("开元度假村".equals(brandname)) brandname = "开元";
					if("开元曼居".equals(brandname)) brandname = "开元";
					if("城市名人".equals(brandname)) brandname = "名人";
					if("花间堂".equals(brandname)) brandname = "TEMP";//存在多个，用ID查询
					//品牌名称匹配
					String sql = "where C_NAME='"+brandname+"'";
					List<Chaininfo> TempList = Server.getInstance().getHotelService().findAllChaininfo(sql, "", -1, 0);
					if(TempList!=null && TempList.size()>0){
						if(TempList.size()==1){
							Chaininfo QunarChaininfo = TempList.get(0);
							boolean flag = false;
							if(!UpdateElDataUtil.StringIsNull(group) && !group.equals(QunarChaininfo.getDescription())){
								flag = true;
								QunarChaininfo.setDescription(group);
							}
							if(chainmap.get(QunarChaininfo.getId())==null || UpdateElDataUtil.StringIsNull(QunarChaininfo.getTotal())){
								QunarChaininfo.setTotal("0");
							}
							if(brandcount>0){
								flag = true;
								int total = Integer.parseInt(QunarChaininfo.getTotal());
								QunarChaininfo.setTotal(Integer.toString(total + brandcount));
							}
							if(!UpdateElDataUtil.StringIsNull(brandlogo)){
								if(UpdateElDataUtil.StringIsNull(QunarChaininfo.getImagepic2())){
									flag = true;
									download(brandlogo,QunarChaininfo);
								}
							}
							if(!QunarBrandId.equalsIgnoreCase(QunarChaininfo.getFullname())){
								flag = true;
								QunarChaininfo.setFullname(QunarBrandId);
							}
							if(flag){
								Server.getInstance().getHotelService().updateChaininfoIgnoreNull(QunarChaininfo);
								System.out.println("从去哪儿更新酒店品牌---" + QunarBrandId + "---" + brandname);
							}else{
								System.out.println("从去哪儿匹配酒店品牌---" + QunarBrandId + "---" + brandname);
							}
							chainmap.put(QunarChaininfo.getId(), true);
						}else{
							System.out.println("去哪儿品牌"+brandname+"名称匹配本地有多个.");
						}
					}
					//品牌ID匹配
					else{
						sql = "where C_FULLNAME='"+QunarBrandId+"'";
						TempList = Server.getInstance().getHotelService().findAllChaininfo(sql, "", -1, 0);
						if(TempList!=null && TempList.size()>0){
							if(TempList.size()==1){
								Chaininfo QunarChaininfo = TempList.get(0);
								boolean flag = false;
								if(!UpdateElDataUtil.StringIsNull(group) && !group.equals(QunarChaininfo.getDescription())){
									flag = true;
									QunarChaininfo.setDescription(group);
								}
								if(chainmap.get(QunarChaininfo.getId())==null || UpdateElDataUtil.StringIsNull(QunarChaininfo.getTotal())){
									QunarChaininfo.setTotal("0");
								}
								if(brandcount>0){
									flag = true;
									int total = Integer.parseInt(QunarChaininfo.getTotal());
									QunarChaininfo.setTotal(Integer.toString(total + brandcount));
								}
								if(!UpdateElDataUtil.StringIsNull(brandlogo)){
									if(UpdateElDataUtil.StringIsNull(QunarChaininfo.getImagepic2())){
										flag = true;
										download(brandlogo,QunarChaininfo);
									}
								}
								if(flag){
									Server.getInstance().getHotelService().updateChaininfoIgnoreNull(QunarChaininfo);
									System.out.println("从去哪儿更新酒店品牌---" + QunarBrandId + "---" + brandname);
								}else{
									System.out.println("从去哪儿匹配酒店品牌---" + QunarBrandId + "---" + brandname);
								}
								chainmap.put(QunarChaininfo.getId(), true);
							}else{
								System.out.println("去哪儿品牌"+brandname+"ID匹配本地有多个.");
							}
						}
					}
				}
				System.out.println("去哪儿城市"+city.getName()+"品牌完成，等待1秒...");
				Thread.sleep(1000*1);
			}
		}
	}
	
	/**
	 * 下载图片
	 */
	private static void download(String urlString, Chaininfo chaininfo) throws Exception {
		//本地存储路径
		String filePath = "D:\\hotelimage\\chain";
		if(!new File(filePath).exists()){
			new File(filePath).mkdirs();
		}
		//库中存储图片名称
		String localImgName = "chain" + chaininfo.getId() + ".jpg";
		//本地存储路径
		String localPath = filePath + "\\" + localImgName;
		//判断图片是否存在
		if(!new File(localPath).exists()){
			System.out.println("开始下载图片----------"+localPath);
			URL url = new URL(urlString);
			URLConnection con = url.openConnection();
			InputStream is = con.getInputStream();
			byte[] bs = new byte[1024];
			int len;
			OutputStream os = new FileOutputStream(localPath);
			while ((len = is.read(bs)) != -1) {
				os.write(bs, 0, len);
			}
			os.close();
			is.close();
		}
		chaininfo.setImagepic2("/hotelimage/chain/" + localImgName);
	}
}
