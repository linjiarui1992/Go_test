package com.ccservice.compareprice;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.mixdata.RoomTypeMix;
/**
 * 
 * @author wzc
 * 更新每月的价格数据
 */
public class UpdatePriceJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新一个月的价格……");
		try {
			//new UpdatePrice().test();
			RoomTypeMix.mixRoomTypeOfiFirst();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("更新一个月的价格结束……");
	}

}
