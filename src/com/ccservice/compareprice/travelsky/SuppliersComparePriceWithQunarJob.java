package com.ccservice.compareprice.travelsky;

import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.text.SimpleDateFormat;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.HotelData;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.compareprice.jielv.CompareRoomNameUtil;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceDetail;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceResponse;

/**
 * 多供应同时比价上去哪儿、减少请求去哪儿次数、暂只有中航信
 * @author WH
 */

public class SuppliersComparePriceWithQunarJob implements Job {

    private static String onlineCity = PropertyUtil.getValue("onlineCity").trim();//上线城市

    private int catchdays = Integer.parseInt(PropertyUtil.getValue("catchdays").trim());//价格天数

    private int addPrice = Integer.parseInt(PropertyUtil.getValue("addPrice").trim());//加价

    private int pricescale = Integer.parseInt(PropertyUtil.getValue("pricescale").trim());//比例

    private String comparePriceTime = PropertyUtil.getValue("comparePriceTime").trim();//比价时间段

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    private String separate = "@.@";//与RequestSuppliersPrice需一致

    private String priceMsg = "加价错误.";

    public void execute(JobExecutionContext context) throws JobExecutionException {
        getHotelDatas();
    }

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        new SuppliersComparePriceWithQunarJob().getHotelDatas();
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start) / 1000 / 60 + "分钟。");
    }

    //比价时间判断
    private void timeWait() throws Exception {
        try {
            //TIME
            Date current = format.parse(format.format(new Date()));
            Date start = format.parse(comparePriceTime.split("-")[0]);
            Date end = format.parse(comparePriceTime.split("-")[1]);
            //WAIT
            if (current.before(start)) {
                long wait = start.getTime() - current.getTime();
                System.out.println("比价时间未到，等待" + wait / 1000 + "秒。");
                Thread.sleep(wait);
            }
            if (current.after(end)) {
                Date _24 = format.parse("24:00:00");
                Date _00 = format.parse("00:00:00");
                long wait = _24.getTime() - current.getTime() + start.getTime() - _00.getTime();
                System.out.println("比价时间未到，等待" + wait / 1000 + "秒。");
                Thread.sleep(wait);
            }
        }
        catch (Exception e) {
        }
    }

    @SuppressWarnings("unchecked")
    private void getHotelDatas() {
        while (true) {
            try {
                //删除小于当前日期的、更新最后时间
                String updateSql = "delete from T_HOTELGOODDATA where C_DATENUM < '"
                        + ElongHotelInterfaceUtil.getCurrentDate() + "'";
                Server.getInstance().getSystemService().findMapResultBySql(updateSql, null);
                if (addPrice < 0) {
                    throw new Exception(priceMsg);
                }
                String citysql = "where C_TRAVELSKYCODE != '' and C_TYPE = 1";
                if (!ElongHotelInterfaceUtil.StringIsNull(onlineCity)) {
                    citysql += " and ID in (" + onlineCity + ")";
                }
                List<City> cityes = Server.getInstance().getHotelService()
                        .findAllCity(citysql, "order by ID desc", -1, 0);
                for (City city : cityes) {
                    System.out.println("城市：" + city.getName());
                    try {
                        loadHotel(city.getId());
                    }
                    catch (Exception e) {
                        System.out.println("城市 [" + city.getName() + " ]比价去哪儿，" + ElongHotelInterfaceUtil.errormsg(e));
                    }
                }
            }
            catch (Exception e) {
                if (priceMsg.equals(e.getMessage())) {
                    System.out.println("加价错误，中断操作。");
                    break;
                }
                System.out.println("比价上去哪儿，" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
    }

    @SuppressWarnings({ "unchecked" })
    private void loadHotel(long cityid) throws Exception {
        String where = new String(PropertyUtil.getValue("jlHotelSql").getBytes("iso8859-1"), "utf-8");
        where += " and c_cityid = " + cityid;
        List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(where.trim(), "", -1, 0);
        //循环酒店
        for (Hotel h : hotels) {
            timeWait();//判断比价时间
            try {
                //过滤
                if (ElongHotelInterfaceUtil.StringIsNull(h.getQunarId())
                        || ElongHotelInterfaceUtil.StringIsNull(h.getHotelcode())) {
                    continue;
                }
                System.out.println("========" + h.getName() + "========");
                //时间
                String startDate = ElongHotelInterfaceUtil.getCurrentDate();
                String endDate = ElongHotelInterfaceUtil.getAddDate(startDate, catchdays);
                //加载中航信
                if (h.getSourcetype() == 12) {
                    Map<String, HXHotelPriceResponse> hxMap = new HashMap<String, HXHotelPriceResponse>();
                    try {
                        hxMap = RequestSuppliersPrice.loadHX(h, startDate, endDate);
                    }
                    catch (Exception e) {
                        hxMap = new HashMap<String, HXHotelPriceResponse>();
                    }
                    System.out.println("中航信有价格数量：" + hxMap.size());
                    //加载去哪儿
                    Map<String, List<HotelData>> quMap = new HashMap<String, List<HotelData>>();
                    if (hxMap.size() > 0) {
                        try {
                            quMap = RequestSuppliersPrice.loadQN(h, startDate, endDate);
                        }
                        catch (Exception e) {
                            quMap = new HashMap<String, List<HotelData>>();
                        }
                        System.out.println("去哪儿有价格数量：" + quMap.size());
                    }
                    //比价
                    compare(quMap, hxMap, h);
                }
            }
            catch (Exception e) {
                System.out.println("中航信酒店 [" + h.getName() + " ]比价去哪儿，" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
    }

    /**
     * 比价去哪儿
     */
    @SuppressWarnings("unchecked")
    private void compare(Map<String, List<HotelData>> quMap, Map<String, HXHotelPriceResponse> hxMap, Hotel hxHotel) {
        //优势数据
        List<HotelGoodData> hxGoodDatas = new ArrayList<HotelGoodData>();
        //循环去哪儿房型价格
        for (String quTotalRoomName : quMap.keySet()) {
            //去哪儿正式房型下价格
            List<HotelData> quList = quMap.get(quTotalRoomName);
            //去哪儿房型下代理价格
            out: for (HotelData qu : quList) {
                int quAgentPrice = qu.getSealprice().intValue();//去哪儿代理平均价格
                String quAgentRoomName = qu.getAgentRoom();//去哪儿代理房型
                //循环中航信
                for (String hxKey : hxMap.keySet()) {//key：roomTypeName + "@.@" + freeMeal
                    //航信房型
                    HXHotelPriceResponse hxData = hxMap.get(hxKey);
                    String hxRoomName = hxData.getRoomTypeName();
                    //同一房型
                    if (CompareRoomNameUtil.IsSame(hxRoomName, hxData.getBedType(), quTotalRoomName)
                            || CompareRoomNameUtil.IsSame(hxRoomName, hxData.getBedType(), quAgentRoomName)) {
                        //价格明细
                        Map<String, HXHotelPriceDetail> hxRates = hxData.getRates();
                        int hxBf = Integer.parseInt(hxKey.split(separate)[1]);
                        Double hxAvgBase = hxData.getTotalAmountPrice() / hxRates.size();
                        Double hxAvgPrice = ElongHotelInterfaceUtil.add(hxAvgBase, addPrice);
                        //比较价格、早餐
                        if (hxAvgPrice.intValue() <= quAgentPrice && equalBf(hxBf, qu.getRoomnameBF())) {
                            System.out.println("中航信优势房型：" + hxRoomName + "<---------->" + quTotalRoomName);
                            long addprice = getAddPirce(hxAvgBase, quAgentPrice);
                            for (String date : hxRates.keySet()) {
                                HXHotelPriceDetail detail = hxRates.get(date);
                                HotelGoodData hd = RequestSuppliersPrice.hxDataToLocal(hxData, detail, date);
                                if (ElongHotelInterfaceUtil.StringIsNull(hd.getJlkeyid())) {
                                    continue;
                                }
                                hd.setProfit(Long.valueOf(addprice));
                                hd.setShijiprice((long) ElongHotelInterfaceUtil.add(hd.getBaseprice().longValue(),
                                        addprice));
                                hd.setSealprice(qu.getSealprice().longValue());
                                hd.setAgentname(qu.getAgentNmae());
                                hd.setQunarName(quTotalRoomName);
                                hxGoodDatas.add(hd);
                            }
                            break out;//下一个去哪儿正式房型
                        }
                    }
                }
            }
        }
        //判断酒店是否利用时间差通过手工上去哪儿了、概率小
        hxHotel = Server.getInstance().getHotelService().findHotel(hxHotel.getId());
        if (hxHotel.getPush() != null && hxHotel.getPush().intValue() == 1) {
            return;
        }
        if (hxGoodDatas.size() > 0) {
            writeDataDB(hxGoodDatas, hxHotel.getId(), 12);
        }
        //无优势、删除原有数据、更新酒店push
        else {
            String sql = "select top 1 * from t_hotelgooddata where c_hotelid = " + hxHotel.getId();
            List<HotelGoodData> olds = hxHotel.getPush() == null ? Server.getInstance().getHotelService()
                    .findAllHotelGoodDataBySql(sql, -1, 0) : new ArrayList<HotelGoodData>();
            if (hxHotel.getPush() != null || olds.size() > 0) {
                String delsql = "delete from t_hotelgooddata where c_hotelid = " + hxHotel.getId()
                        + ";update t_hotel set c_push = null , c_goqunarupdatetime = '" + sdf.format(new Date())
                        + "' where id = " + hxHotel.getId();
                Server.getInstance().getSystemService().findMapResultBySql(delsql, null);
            }
        }
    }

    //优势数据写入数据库
    @SuppressWarnings("unchecked")
    private void writeDataDB(List<HotelGoodData> datas, long hotelid, int source) {
        //查询原有
        Map<String, HotelGoodData> oldMap = new HashMap<String, HotelGoodData>();
        List<HotelGoodData> olds = Server.getInstance().getHotelService()
                .findAllHotelGoodData("where c_hotelid = " + hotelid, "", -1, 0);
        if (olds != null && olds.size() > 0) {
            for (HotelGoodData old : olds) {
                oldMap.put(old.getJlkeyid(), old);
            }
        }
        //更新酒店标示
        int count = 0;
        //保存新数据
        String updatetime = sdf.format(new Date());
        for (HotelGoodData data : datas) {
            String keyid = data.getJlkeyid();
            try {
                //原先存在，更新
                if (oldMap.size() > 0 && oldMap.containsKey(keyid)) {
                    HotelGoodData old = oldMap.get(keyid);
                    if (RequestSuppliersPrice.NeedUpdate(old, data, source)) {
                        data.setId(old.getId());
                        data.setRoomflag(old.getRoomflag());
                        data.setUpdatetime(updatetime);
                        Server.getInstance().getHotelService().updateHotelGoodData(data);
                        count++;
                    }
                }
                //原先不存在，新增
                else {
                    data.setUpdatetime(updatetime);
                    Server.getInstance().getHotelService().createHotelGoodData(data);
                    count++;
                }
            }
            catch (Exception e) {
                System.out.println("酒店 [" + data.getHotelname() + " ]比价去哪儿，" + ElongHotelInterfaceUtil.errormsg(e));
            }
            finally {
                oldMap.remove(keyid);
            }
        }
        //删除
        if (oldMap.size() > 0) {
            String ids = "";
            for (String key : oldMap.keySet()) {
                HotelGoodData old = oldMap.get(key);
                ids += old.getId() + ",";
            }
            ids = ids.substring(0, ids.length() - 1);
            String delsql = "delete from T_HOTELGOODDATA where ID in (" + ids + ")";
            Server.getInstance().getSystemService().findMapResultBySql(delsql, null);
            count++;
        }
        //PUSH --> 2：程序上去哪儿 、 通过比价更新
        if (count > 0) {
            String sql = "update t_hotel set c_push = 2 , c_goqunarupdatetime = '" + sdf.format(new Date())
                    + "' where id = " + hotelid;
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
    }

    /**
     * 比例取价格，利润最大化
     */
    private long getAddPirce(double basePrice, double qunarPrice) {
        long addprice = 0;
        try {
            //差价
            Double price = ElongHotelInterfaceUtil.subtract(qunarPrice, basePrice);
            if (price > addPrice) {
                //100*30*0.01 = 30
                double temp = ElongHotelInterfaceUtil.multiply(price, pricescale * 0.01);
                //100-30=70
                price = ElongHotelInterfaceUtil.subtract(price, temp);
                if (price > addPrice) {
                    addprice = price.longValue();
                }
            }
        }
        catch (Exception e) {
        }
        if (addprice == 0) {
            addprice = addPrice;
        }
        return addprice;
    }

    //供应商早餐与去哪儿早餐比较
    private boolean equalBf(int supplierBf, String qunarBfStr) {
        //比较早餐
        boolean bfFlag = false;
        //去哪儿早餐
        long qunarBf = getQunarBf(qunarBfStr);
        //相同
        if (supplierBf == qunarBf) {
            bfFlag = true;
        }
        //供应商含早、双早、三早，去哪儿单早
        else if ((supplierBf == -1 || supplierBf == 2 || supplierBf == 3) && qunarBf == 1) {
            bfFlag = true;
        }
        //供应商三早，去哪儿双早
        else if (supplierBf == 3 && qunarBf == 2) {
            bfFlag = true;
        }
        return bfFlag;
    }

    //去哪儿早餐转换
    private long getQunarBf(String bfstr) {
        long bf = 0;
        if ("单早".equals(bfstr)) {
            bf = 1;
        }
        else if ("双早".equals(bfstr)) {
            bf = 2;
        }
        else if ("三早".equals(bfstr)) {
            bf = 3;
        }
        else if ("四早".equals(bfstr)) {
            bf = 4;
        }
        else if ("含早".equals(bfstr)) {
            bf = -1;
        }
        return bf;
    }
}