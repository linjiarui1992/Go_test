package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

/**
 * 
 * 
 * @Caless:ComProcPublic.java
 * @ClassDesc: 比价实体工具类  公共类
 * @Author:胡灿
 * @Date:2012-12-18 下午07:33:08
 * @Company: 航天华有(北京)科技有限公司
 * @Copyright: Copyright (c) 2012
 * @version: 1.0
 */
public class ComProcPublic extends Utils {
	public final static String YDX_ID = "wiotatts037";// 易定行id

	/**
	 * 比价代码程序
	 * huc
	 * @param dayqunardatas
	 *            去哪数据集合
	 * @param profit
	 *            最低利润点
	 * @param lower
	 *            比别人家低的价格
	 * @throws Exception
	 */
	public void comparequnar(Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas,
			List<HotelGoodData> hotelGoodDataList, Long profit, Long lower,String sorucetype)
	throws Exception {
		List<HotelGoodData> result0 = new ArrayList<HotelGoodData>();
		label: 	for (HotelGoodData hotelGoodData : hotelGoodDataList) {
			System.out.println("比价日期：" + hotelGoodData.getDatenum());
			Map<Long, Map<String, List<HotelData>>> tem = dayqunardatas.get(hotelGoodData.getDatenum());
			if (tem != null) {
					Map<String, List<HotelData>> rooms = tem.get(hotelGoodData.getHotelid());
					List<HotelData> hoteltdatas = rooms.get(hotelGoodData.getQunarName());
					if (hoteltdatas != null && hoteltdatas.size() > 0) {
						for (HotelData hotelData : hoteltdatas) {
							HotelGoodData good = new HotelGoodData();
							good.setHotelid(hotelGoodData.getHotelid());
							good.setRoomtypeid(hotelGoodData.getRoomtypeid());
							good.setRoomtypename(hotelGoodData.getRoomtypename());
							good.setHotelname(hotelGoodData.getHotelname());
							good.setContractid(hotelGoodData.getContractid());
							good.setContractver(hotelGoodData.getContractver());
							good.setBfcount(hotelGoodData.getBfcount());
							good.setProdid(hotelGoodData.getProdid());
							good.setBedtypeid(hotelGoodData.getBedtypeid());
							good.setMinday(hotelGoodData.getMinday());
							good.setCityid(hotelGoodData.getCityid());
							good.setBeforeday(hotelGoodData.getBeforeday());
							Long sp = hotelGoodData.getBaseprice()+ profit;
							if(hotelGoodData.getYuliunum()==-1){
								good.setYuliunum(0L);//TODO
							}else{
								good.setYuliunum(hotelGoodData.getYuliunum());//TODO
							}
							good.setDatenum(hotelGoodData.getDatenum());
							good.setBaseprice(hotelGoodData.getBaseprice());
							good.setSorucetype(hotelGoodData.getSorucetype());
							good.setJlkeyid(hotelGoodData.getJlkeyid());
							good.setJltime(hotelGoodData.getJltime());
							good.setJlft(hotelGoodData.getJlft());
							good.setJlf(hotelGoodData.getJlf());
							good.setJlroomtypeid(hotelGoodData.getJlroomtypeid());
							good.setRatetypeid(hotelGoodData.getRatetypeid());
							good.setAllotmenttype(hotelGoodData.getAllotmenttype());
							good.setRatetype(hotelGoodData.getRatetype());
							good.setQunarName(hotelGoodData.getQunarName());
							good.setAgentname(hotelData.getAgentName(hotelData.getAgentNo()));
							good.setSealprice(hotelData.getSealprice().longValue());
							good.setShijiprice(hotelData.getSealprice().longValue());
							good.setAllot(hotelGoodData.getAllot());
							good.setProfit(hotelData.getSealprice().longValue()- hotelGoodData.getBaseprice());
								// 如果利润为profit则不用再减，如果大于prodfit则比其他代理商少lower元
							if (good.getProfit() != profit&&good.getProfit() >= (profit + lower)) {
									good.setShijiprice(hotelData.getSealprice()- lower);
									good.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- lower);
							}
							//易订行过滤  关房过滤 休息中过滤
							if (hotelData.getAgentNo().equals("wiotatts037")||hotelData.getRoomstatus() == -1||hotelData.getRoomstatus() == -2) {// 
								continue;
							}
							if (hotelGoodData.getYuliunum()==-1) {
								good.setRoomflag("0");
								good.setRoomstatus(1l);
								result0.add(good);
								continue label;//满房 跳出最外层循环
							}
							Double profittemp = good.getProfit().doubleValue();
							if (hotelGoodData.getBfcount()==0) {
								if (sp > hotelData.getSealprice()) {
									good.setRoomstatus(1l);
									good.setRoomflag("2");
									result0.add(good);
									continue label;//没有优势 跳出最外层循环
								} else {
									if (hotelData.getType() == 0&& profittemp / 2 >= profit) {
										good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
										good.setProfit(hotelData.getSealprice()- hotelGoodData.getBaseprice()- (long) (profittemp / 2));
									} else if (hotelData.getType() == 0&& profittemp / 2 < profit) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue  label;// 跳出最外层循环
									}
									if (good.getProfit() < profit) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue label;// 跳出最外层循环
									}
									if (hotelData.getRoomnameBF().contains("早")
											&& !hotelData.getRoomnameBF().equals("无早")
											&& !hotelData.getRoomnameBF().equals("不含早")) {
										if (good.getProfit() > (profit + 10)) {
											good.setShijiprice(good.getShijiprice() - 10);
											good.setProfit(good.getProfit() - 10);
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											System.out.println("当前利润："+ good.getProfit());
											result0.add(good);
											continue  label;// 跳出最外层循环
										} else if (good.getProfit() > profit + 5) {
											good.setShijiprice(good.getShijiprice() - 5);
											good.setProfit(good.getProfit() - 5);
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											System.out.println("当前利润："+ good.getProfit());
											result0.add(good);
											continue  label;// 跳出最外层循环
										} else {
											good.setRoomstatus(0l);
											good.setShijiprice(good.getShijiprice() - lower);
											good.setProfit(good.getProfit() - lower);
											good.setRoomflag("1");
											System.out.println("当前利润："+ good.getProfit());
											result0.add(good);
											continue  label;// 跳出最外层循环
										}
									} else {
										good.setRoomstatus(0l);
										good.setRoomflag("1");
										result0.add(good);
										continue  label;// 跳出最外层循环
									}
								}
							} else if (hotelGoodData.getBfcount()==1) {
								if (sp > hotelData.getSealprice()) {
									good.setRoomstatus(1l);
									good.setRoomflag("2");
									result0.add(good);
									continue  label;// 跳出最外层循环
								} else {
									if (hotelData.getRoomnameBF().contains("早")
											&& !hotelData.getRoomnameBF().contains("无早")) {
										if (hotelData.getType() == 0&& (profittemp / 2) >= profit) {
											good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
											good.setProfit(good.getProfit()- (long) (profittemp / 2));
										} else if (hotelData.getType() == 0&& profittemp / 2 < profit) {
											good.setRoomstatus(1l);
											good.setRoomflag("2");
											result0.add(good);
											continue  label;// 跳出最外层循环
										}
										if (good.getProfit() < profit) {
											good.setRoomstatus(1l);
											good.setRoomflag("2");
											result0.add(good);
											continue  label;// 跳出最外层循环
										}
										if (hotelData.getRoomnameBF().contains("单早")|| hotelData.getRoomnameBF().contains("1份早餐")) {
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											System.out.println("当前利润："+ good.getProfit());
											result0.add(good);
											continue  label;// 跳出最外层循环
										} else if (hotelData.getRoomnameBF().equals("")|| hotelData.getRoomnameBF().contains("无早")|| hotelData.getRoomnameBF().contains("不含早")) {
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											result0.add(good);
											System.out.println("当前利润："+ good.getProfit());
											continue  label;// 跳出最外层循环
										} else {
											if (good.getProfit() > (profit + 10)) {
												good.setShijiprice(good.getShijiprice() - 10);
												good.setProfit(good.getProfit() - 10);
												good.setRoomstatus(0l);
												good.setRoomflag("1");
												System.out.println("当前利润："+ good.getProfit());
												result0.add(good);
												continue  label;// 跳出最外层循环
											} else if (good.getProfit() >= (profit + 5)) {
												good.setShijiprice(good.getShijiprice() - 5);
												good.setProfit(good.getProfit() - 5);
												good.setRoomstatus(0l);
												good.setRoomflag("1");
												System.out.println("当前利润："+ good.getProfit());
												result0.add(good);
												continue  label;// 跳出最外层循环
											} else {
												good.setRoomstatus(0l);
												good.setRoomflag("1");
												System.out.println("当前利润："+ good.getProfit());
												result0.add(good);
												continue  label;// 跳出最外层循环
											}
										}
									}
								}
							} else if (hotelGoodData.getBfcount()==2) {
								if (sp > hotelData.getSealprice()) {
									good.setRoomstatus(1l);
									good.setRoomflag("2");
									result0.add(good);
									continue  label;// 跳出最外层循环
								} else {
									if (hotelData.getType() == 0&& (profittemp / 2 >= profit)) {
										good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
										good.setProfit(good.getProfit()- (long) (profittemp / 2));
									} else if (hotelData.getType() == 0&& (profittemp / 2 < profit)) {
										good.setRoomstatus(1l);
										good.setRoomflag("1");
										result0.add(good);
										continue  label;// 跳出最外层循环
									}
									if (good.getProfit() < profit) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue  label;// 跳出最外层循环
									}
									
									if (hotelData.getRoomnameBF().contains("双早")|| hotelData.getRoomnameBF().equals("")
													|| hotelData.getRoomnameBF().equals("无早")
													|| hotelData.getRoomnameBF().equals("不含早")
													|| hotelData.getRoomnameBF().equals("单早")
													|| hotelData.getRoomnameBF().equals("1份早餐")
													|| hotelData.getRoomnameBF().contains("2份早餐")
													|| hotelData.getRoomnameBF().contains("含早")) {
										System.out.println("当前利润："+ good.getProfit());
										good.setRoomstatus(0l);
										good.setRoomflag("1");
										System.out.println("当前利润："+ good.getProfit());
										result0.add(good);
										continue  label;// 跳出最外层循环
									} else {
										if (good.getProfit() >= (profit + 10)) {
											good.setShijiprice(good.getShijiprice() - 10);
											good.setProfit(good.getProfit() - 10);
										} else if (good.getProfit() >= (profit + 5)) {
											good.setShijiprice(good.getShijiprice() - 5);
											good.setProfit(good.getProfit() - 5);
										}
										System.out.println("当前利润："+ good.getProfit());
										good.setRoomstatus(0l);
										good.setRoomflag("1");
										result0.add(good);
										continue label;// 跳出最外层循环
									}
								}
							} else if (hotelGoodData.getBfcount()==3) {
								if (sp > hotelData.getSealprice()) {
									good.setRoomstatus(1l);
									good.setRoomflag("2");
									result0.add(good);
									continue label;// 跳出最外层循环
								} else {
									if (hotelData.getType() == 0&& (profittemp / 2 >= profit)) {
										good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
										good.setProfit(good.getProfit()- (long) (profittemp / 2));
									} else if (hotelData.getType() == 0&& (profittemp / 2 < profit)) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue label;// 跳出最外层循环
									}
									if (hotelData.getRoomnameBF().contains("三早")
													|| hotelData.getRoomnameBF().contains("双早")
													|| hotelData.getRoomnameBF().contains("1份早餐")
													|| hotelData.getRoomnameBF().contains("2份早餐")
													|| hotelData.getRoomnameBF().contains("3份早餐")
													|| hotelData.getRoomnameBF().contains("")
													|| hotelData.getRoomnameBF().contains("无早")
													|| hotelData.getRoomnameBF().contains("不含早")
													|| hotelData.getRoomnameBF().contains("单早")
													|| hotelData.getRoomnameBF().contains("含早")) {
										good.setRoomstatus(0l);
										System.out.println("当前利润："+ good.getProfit());
										good.setRoomflag("1");
										result0.add(good);
										continue label;// 跳出最外层循环
									} else {
										if (good.getProfit() >= (profit + 10)) {
											good.setShijiprice(good.getShijiprice() - 10);
											good.setProfit(good.getProfit() - 10);
											System.out.println("当前利润："+ good.getProfit());
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											result0.add(good);
											continue label;// 跳出最外层循环
										} else if (good.getProfit() >= (profit + 5)) {
											good.setShijiprice(good.getShijiprice() - 5);
											good.setProfit(good.getProfit() - 5);
											System.out.println("当前利润："+ good.getProfit());
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											result0.add(good);
											continue label;// 跳出最外层循环
										} else {
											good.setRoomstatus(0l);
											System.out.println("当前利润："+ good.getProfit());
											good.setRoomflag("1");
											result0.add(good);
											continue label;// 跳出最外层循环
										}
									}
								}
							} else if (hotelGoodData.getBfcount()==4) {
								if (sp > hotelData.getSealprice()) {
									good.setRoomstatus(1l);
									good.setRoomflag("2");
									result0.add(good);
									continue label;// 跳出最外层循环
								} else {
									if (hotelData.getType() == 0&& (profittemp / 2 >= profit)) {
										good.setShijiprice(hotelData.getSealprice()- (long) (profittemp / 2));
										good.setProfit(good.getProfit()- (long) (profittemp / 2));
									} else if (hotelData.getType() == 0&& (profittemp / 2 < profit)) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue label;// 跳出最外层循环
									}
									if (good.getProfit() < profit) {
										good.setRoomstatus(1l);
										good.setRoomflag("2");
										result0.add(good);
										continue label;// 跳出最外层循环
									}
									if (hotelData.getRoomnameBF().contains("四早")
													|| hotelData.getRoomnameBF().contains("双早")
													|| hotelData.getRoomnameBF().contains("2份早餐")
													|| hotelData.getRoomnameBF().contains("4份早餐")
													|| hotelData.getRoomnameBF().contains("三早")
													|| hotelData.getRoomnameBF().contains("3份早餐")
													|| hotelData.getRoomnameBF().contains("")
													|| hotelData.getRoomnameBF().contains("无早")
													|| hotelData.getRoomnameBF().contains("不含早")
													|| hotelData.getRoomnameBF().contains("单早")
													|| hotelData.getRoomnameBF().contains("1份早餐")
													|| hotelData.getRoomnameBF().contains("含早")) {
										good.setRoomstatus(0l);
										good.setRoomflag("1");
										System.out.println("当前利润："+ good.getProfit());
										result0.add(good);
										continue label;// 跳出最外层循环
									} else {
										if (good.getProfit() >= (profit + 10)) {
											good.setShijiprice(good.getShijiprice() - 10);
											good.setProfit(good.getProfit() - 10);
											System.out.println("当前利润："+ good.getProfit());
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											result0.add(good);
											continue label;// 跳出最外层循环
										} else if (good.getProfit() >= (profit + 5)) {
											good.setShijiprice(good.getShijiprice() - 5);
											good.setProfit(good.getProfit() - 5);
											System.out.println("当前利润："+ good.getProfit());
											good.setRoomstatus(0l);
											good.setRoomflag("1");
											result0.add(good);
											continue label;// 跳出最外层循环
										} else {
											good.setRoomflag("1");
											good.setRoomstatus(0l);
											result0.add(good);
											continue label;// 跳出最外层循环
										}
									}
								}
							}
						}
					}
				}
			}
		writeDataDB(result0,sorucetype);
	}

	/**
	 * 写入势数据写入数据库
	 * 
	 * @param gooddata
	 */
	public void writeDataDB(List<HotelGoodData> gooddata,String sorucetype){
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if("3".equals(sorucetype)){//华闽
		Set<Long> hotelids=new HashSet<Long>();
		int version=0;
		for (int i = 0; i < gooddata.size() ; i++) {
			HotelGoodData good=gooddata.get(i);
			if(i==gooddata.size()-1){
				
				Server.getInstance().getSystemService().findMapResultBySql(
						"delete from T_HOTELGOODDATA where c_hotelid="+ good.getHotelid()+" and C_HIDECLOSE<"+version, null);
				System.out.println("删除版本低的……");
			}
			if(!hotelids.contains(good.getHotelid())){
				hotelids.add(good.getHotelid());
				List versionids=Server.getInstance().getSystemService().findMapResultBySql("select MAX(C_HIDECLOSE) as C_HIDECLOSE from T_HOTELGOODDATA where C_HOTELID="+good.getHotelid(), null);
				if(versionids!=null&&versionids.size()>0){
					if(((Map)versionids.get(0)).get("C_HIDECLOSE")!=null){
						System.out.println("版本提升……");
						version=Integer.parseInt(((Map)versionids.get(0)).get("C_HIDECLOSE").toString());
						version=version+1;
					}
				}
			}
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			Integer coloseopen=Integer.parseInt(PropertyUtil.getValue("openclose"));
			good.setOpenclose(coloseopen);
			good.setSorucetype("3");
			// 特殊处理
			if(good.getProfit()>0&&good.getProfit()<20){
				good.setRoomflag("2");
				 good.setRoomstatus(0l);//开房
				 good.setShijiprice(good.getBaseprice()+20);
				 good.setProfit(20l);
			}
			if(good.getProfit()>=20){
				good.setRoomflag("1");
			}
			if(good.getProfit()<=0){
				good.setRoomflag("-1");
				good.setShijiprice(good.getBaseprice()+20);
				good.setRoomstatus(0l);//开房
				good.setProfit(20l);
			}
			System.out.println(good);
			String str="[dbo].[sp_inserthmgooddata] @hotelid = "+
			good.getHotelid()+",@hotelname = N'"+
			good.getHotelname()+"',@roomtypeid = "+
			good.getRoomtypeid()+",@roomtypename = N'"+
			good.getRoomtypename()+"',@shijiprice = "+
			good.getShijiprice()+",@baseprice = "+
			good.getBaseprice()+",	@sealprice = "+
			good.getSealprice()+",@profit = "+
			good.getProfit()+",@roomstatus = "+
			good.getRoomstatus()+",@yuliunum = "+
			good.getYuliunum()+",@datenum = N'"+
			good.getDatenum()+"',@minday = "+
			good.getMinday()+",@openclose = "+
			good.getOpenclose()+",@beforeday = "+
			good.getBeforeday()+",@contractid = N'"+
			good.getContractid()+"',@contractver = N'"+
			good.getContractver()+"',@prodid = N'"+
			good.getProdid()+"',@bfcount = "+
			good.getBfcount()+",@sorucetype = N'"+
			good.getSorucetype()+"',@agentname = N'"+
			good.getAgentname()+"',@updatetime = N'"+
			good.getUpdatetime()+"',@roomflag=N'"+
			good.getRoomflag()+"',@cityid=N'"+
			good.getCityid()+"',@bedtypeid="+
			good.getBedtypeid()+",@version="+
			version+",@qunarname=N'"+
			good.getQunarName()+"',@allot="+
			good.getAllot();
			synchronized (this) {
				List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
			}
			}
		}else if("6".equals(sorucetype)){//捷旅
			Set<Long> hotelids=new HashSet<Long>();
			for (int i = 0; i < gooddata.size() ; i++) {
				HotelGoodData good=gooddata.get(i);
				good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
				good.setId(0l);
				if(hotelids.contains(good.getHotelid())){
				}else{
					hotelids.add(good.getHotelid());
					Server.getInstance().getSystemService().findMapResultBySql(
							"delete from T_HOTELGOODDATA where c_hotelid=" + good.getHotelid(), null);
					System.out.println("删除………………");
				}
				
				good.setOpenclose(1);
				good.setSorucetype("6");
				//特殊处理
				if(good.getProfit()!=null){
					if(good.getProfit()>0&&good.getProfit()<20){
						good.setRoomflag("2");
						//good.setRoomstatus(0l);//开房
						//good.setShijiprice(good.getBaseprice()+20);
					}
					if(good.getProfit()>=20){
						good.setRoomflag("1");
					}
				}else{
					System.out.println(good.getProfit());
				}
				
				if(good.getShijiprice()<20){
					good.setShijiprice(0l);
				}
				System.out.println(good);
				String str="[dbo].[sp_insertgooddata] @hotelid = "+
				good.getHotelid()+",@hotelname = N'"+
				good.getHotelname()+"',@roomtypeid = "+
				good.getRoomtypeid()+",@roomtypename = N'"+
				good.getRoomtypename()+"',@shijiprice = "+
				good.getShijiprice()+",@baseprice = "+
				good.getBaseprice()+",	@sealprice = "+
				good.getSealprice()+",@profit = "+
				good.getProfit()+",@roomstatus = "+
				good.getRoomstatus()+",@yuliunum = "+
				good.getYuliunum()+",@datenum = N'"+
				good.getDatenum()+"',	@minday = "+
				good.getMinday()+",@openclose = "+
				good.getOpenclose()+",@beforeday = "+
				good.getBeforeday()+",@contractid = N'"+
				good.getContractid()+"',@contractver = N'"+
				good.getContractver()+"',@prodid = N'"+
				good.getProdid()+"',@bfcount = "+
				good.getBfcount()+",@sorucetype = N'"+
				good.getSorucetype()+"',@agentname = N'"+
				good.getAgentname()+"',@updatetime = N'"+
				good.getUpdatetime()+"',@roomflag=N'"+
				good.getRoomflag()+"',@cityid=N'"+
				good.getCityid()+"',@jlkeyid=N'"+
				good.getJlkeyid()+"',@jltime=N'"+
				good.getJltime()+"',@jlft=N'"+
				good.getJlft()+"',@jlf=N'"+
				good.getJlf()+"',@jlroomtypeid=N'"+
				good.getJlroomtypeid()+"',@allotmenttype=N'"+
				good.getAllotmenttype()+"',@ratetype=N'"+
				good.getRatetype()+"',@ratetypeid=N'"+
				good.getRatetypeid()+"'@qunarname=N'"+
				good.getQunarName()+"'";
				synchronized (this) {
					List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
				}
			}
		}else{
			System.out.println("来源不存在........");
		}
		
	}
	
	
	
	// 加载去哪酒店数据
	public Map<String, Map<Long, Map<String, List<HotelData>>>> loadQunarData(Hotel hotel, Date start) {
		Map<String, Map<Long, Map<String, List<HotelData>>>> dayqunardatas=new HashMap<String, Map<Long,Map<String,List<HotelData>>>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		Map<Long, Map<String, List<HotelData>>> qunarDatas=new HashMap<Long, Map<String,List<HotelData>>>();
		// 返回去哪对应的数据集合
		 Map<String, List<HotelData>> roomDatas=new HashMap<String, List<HotelData>>();
		String citycode = "";
		citycode = hotel.getQunarId().substring(0,
				hotel.getQunarId().lastIndexOf("_"));
		String url = "http://hotel.qunar.com/price/detail.jsp?fromDate="
				+ sdf.format(start.getTime());
		url+= "&toDate=" + sdf.format(cal.getTime())+ "&cityurl=" + citycode + "&HotelSEQ="+ hotel.getQunarId().trim()+"&random="+Math.random();
		System.out.println(url);
		String str = HttpClient.httpget(url, "utf-8");
//		roomnames = new ArrayList<String>();
		if (str != null && !str.equals("")) {
			String key = str.trim();
			if (!key.equals("")) {
				String strstr = key.substring(key.indexOf("(") + 1, key.trim()
						.indexOf("/*"));
				JSONObject datas = JSONObject.fromObject(strstr + "}");
				JSONObject result = datas.getJSONObject("result");
				Iterator<String> strs = result.keys();
				HotelData hoteldata;
//				roomDatas = new HashMap<String, List<HotelData>>();
				while (strs.hasNext()) {
					hoteldata = new HotelData();
					String keys = strs.next();
					JSONArray arra = JSONArray.fromObject(result.get(keys));
					if (arra.get(0) != null) {
						hoteldata.setSealprice(Integer.parseInt(arra.get(0)
								.toString()));
					}
					hoteldata.setAgentNo(keys.substring(0, keys.trim().indexOf(
							"|")));
					hoteldata.setHotelid(hotel.getId() + "");
					hoteldata.setHotelname(hotel.getName());
					if (arra.get(2) != null) {
						String strtemp = arra.getString(2);
						hoteldata.setRoomnameBF(getString(strtemp));
					}
					// 开关房
					if (arra.get(9) != null) {
						String strtemp = arra.getString(9);
						hoteldata.setRoomstatus(Integer.parseInt(strtemp));
					}
					// 现预付
					if (arra.get(14) != null) {
						String strtemp = arra.getString(14);
						hoteldata.setType(Integer.parseInt(strtemp));
					}
					if (arra.get(4) != null) {
						String strtemp = arra.getString(4);
						if (YDX_ID.equals(hoteldata.getAgentNo())) {
							String roomtypeidtemp = strtemp.substring(
									strtemp.indexOf("roomId=") + 7,
									strtemp.indexOf("&cpcRoomType")).trim();
							String roomtypeid=roomtypeidtemp.substring(roomtypeidtemp.lastIndexOf("_")+1);
							hoteldata.setRoomtypeid(Long.parseLong(roomtypeid));
						}
					}
					if (arra.get(3) != null) {
						hoteldata.setRoomname(arra.getString(3));
//						if (!roomnames.contains(arra.getString(3))) {
//							roomnames.add(arra.getString(3));
//						}
						if (roomDatas.containsKey(arra.getString(3))) {
							List<HotelData> datastemp = roomDatas.get(arra
									.getString(3));
							datastemp.add(hoteldata);
							Collections.sort(datastemp);
							roomDatas.put(arra.getString(3), datastemp);
						} else {
							List<HotelData> hoteldatas = new ArrayList<HotelData>();
							hoteldatas.add(hoteldata);
							Collections.sort(hoteldatas);
							roomDatas.put(arra.getString(3), hoteldatas);
						}
					}
				}
				qunarDatas.put(hotel.getId(), roomDatas);
				JSONObject detailBasic = datas.getJSONObject("detailBasic");
				JSONObject vendors = detailBasic.getJSONObject("vendors");
				Iterator<String> names = vendors.keys();
				Map<String, String> agentnos = new HashMap<String, String>();
				Set<String> keys = agentnos.keySet();
				for (String k : keys) {
					WriteLog.write("去哪代理商", k + " - " + agentnos.get(k));
				}
			}
		}
		dayqunardatas.put(sdf.format(start.getTime()), qunarDatas);
		return dayqunardatas;

	}
}
