package com.ccservice.compareprice.jielv;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 *本地房型名称比较去哪儿房型名称 
 */
public class CompareRoomNameUtil {

    /**
     * @param localRoomName 本地房型、不可为空
     * @param localBed 本地床型、不可为空
     * @param qunarRoomName 去哪儿房型、不可为空
     * @return true / false
     */
    public static boolean IsSame(String localRoomName, int localBed, String qunarRoomName) {
        boolean flag = false;
        try {
            if (ElongHotelInterfaceUtil.StringIsNull(localRoomName)
                    || ElongHotelInterfaceUtil.StringIsNull(qunarRoomName)) {
                return false;
            }

            localRoomName = localRoomName.trim();
            qunarRoomName = qunarRoomName.trim();
            //名称一致
            if (localRoomName.equalsIgnoreCase(qunarRoomName)) {
                return true;
            }

            //最后为房或间，并一致
            String tempLocalName = localRoomName;
            String tempQunarName = qunarRoomName;
            if (localRoomName.endsWith("客房")) {
                tempLocalName = localRoomName.substring(0, localRoomName.length() - 2);
            }
            else if (localRoomName.endsWith("房") || localRoomName.endsWith("间")) {
                tempLocalName = localRoomName.substring(0, localRoomName.length() - 1);
            }
            if (qunarRoomName.endsWith("客房")) {
                tempQunarName = qunarRoomName.substring(0, qunarRoomName.length() - 2);
            }
            if (qunarRoomName.endsWith("房") || qunarRoomName.endsWith("间")) {
                tempQunarName = qunarRoomName.substring(0, qunarRoomName.length() - 1);
            }
            if (tempLocalName.equalsIgnoreCase(tempQunarName)) {
                return true;
            }

            //最后为床型，并一致
            String localBedName = getBedName(localBed);
            if (tempLocalName.endsWith(localBedName)) {
                tempLocalName = tempLocalName.substring(0, tempLocalName.length() - localBedName.length());
            }
            if (tempQunarName.endsWith(localBedName)) {
                tempQunarName = tempQunarName.substring(tempQunarName.length() - localBedName.length());
            }
            if (tempLocalName.equalsIgnoreCase(tempQunarName)) {
                return true;
            }
        }
        catch (Exception e) {
        }
        return flag;
    }

    private static String getBedName(int bed) {
        String name = "";
        if (bed == 0) {
            name = "大床";
        }
        else if (bed == 1) {
            name = "双床";
        }
        else if (bed == 2) {
            name = "大床/双床";
        }
        else if (bed == 3) {
            name = "三床";
        }
        else if (bed == 4) {
            name = "一单一双";
        }
        else if (bed == 5) {
            name = "单人床";
        }
        else if (bed == 6) {
            name = "上下铺";
        }
        else if (bed == 7) {
            name = "通铺";
        }
        else if (bed == 8) {
            name = "榻榻米";
        }
        else if (bed == 9) {
            name = "水床";
        }
        else if (bed == 10) {
            name = "圆床";
        }
        else if (bed == 11) {
            name = "拼床";
        }
        return name;
    }
}
