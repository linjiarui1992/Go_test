/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainOrderOffline
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 上午10:30:12 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOffline {
    private Long Id;//

    private String OrderNumber;//订单号 - 自己生成

    private String CreateTime;//

    private Integer AgentId;//代理ID - 代售点

    private Integer CreateUId;//创建人userid

    private String CreateUser;//创建人

    private String ContactUser;//乘客联系人

    private String ContactTel;//乘客联系电话

    private Double OrderPrice;//订单金额

    private Integer OrderStatus;//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理 - 

    private String TradeNo;//交易号

    private Double AgentProfit;//代理利润

    private Integer SupplyPayWay;//支付供应方式

    private Integer TicketCount;//该订单总票数

    private Integer RefundReason;//拒单原因-状态

    private String RefundReasonStr;//拒单原因

    private String OrderTimeout;//订单超时时间  - 由于是非空字段，可以在第一次插入的时候传入 - 1900-01-01 00:00:00.000 - 在前台做显示的大小的判定

    private Integer ChuPiaoAgentid;//出票人agentid - 出票点

    private String ChuPiaoTime;//出票时间

    private String Remark;//备注信息

    private String OrderNumberOnline;//采购的订单号

    /**
     * 支付信息
     */
    private Integer paystatus;//支付状态

    /**
     * 车票定制信息
     * 
        if("靠窗".equals(seatDetail)){
            papertype=1;
        }else if("连座".equals(seatDetail)){
            papertype=2;
        }else if("上铺".equals(seatDetail)){
            papertype=3;
        }else if("中铺".equals(seatDetail)){
            papertype=4;
        }else if("下铺".equals(seatDetail)){
            papertype=5;
        }else if("同包厢".equals(seatDetail)){
            papertype=6;
        }else if("中上铺".equals(seatDetail)){
            papertype=7;
        }else if("一起".equals(seatDetail)){
            papertype=8;
        }
     * 【？？？】
     * 
     * OrderPaperEnum
     * 
     * 主要存储了车票定制类型
     * 
     * 0 普通 - 1 团体 - 2 下铺票 - 3 靠窗 - 4 连坐 - 5 过道 - 6 上铺票 - 7 中铺票 - 8 同包厢
     * 
     * 不同的接口中的定制信息不一样
     * 
     */
    private Integer PaperType;//纸质票类型: 1 靠窗,2 连坐,3 上铺,4中铺,5 下铺 - 6-不接受无座 - 7-接受无座 - 

    private Integer PaperBackup;//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)

    private Integer paperLowSeatCount;//至少接受下铺/靠窗/连坐数量

    private String extSeat;//备选席别

    /**
     * 拒单相关操作信息
     */
    private Integer lockedStatus;//0表示未被锁定，1表示被锁定 - 2-锁单失败 3-锁单中 - 4-锁单超时 -【途牛】 状态5-闪送单的锁单超时 - 6-客服的闪送单的二次下单锁单操作

    private Integer operateid;//拒单操作的ID

    private String operatetime;//拒单操作时间

    private Integer refusereason;//拒单原因code

    private String refusereasonstr;//拒单原因的消息

    /**
     * 快递相关信息
     * 
     * 存储快递时效信息，用于提醒代售点判定该单是否会成功送达
     */
    private String expressDeliver;//快递信息描述,例如："如果2017-08-16 18:00:00正常发件。快递类型为:标准快递。快递预计到达时间:2017-08-17 18:00:00,2017-08-17 18:00:00。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。"

    private String telephone;//收件人电话

    private Integer questionDraw;//questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单

    private Integer questionMail;//questionMail=1 //快递发件失败 questionMail=2  快递发件成功

    private Integer isDelivery;//送票到站标识 ??? 0-不送票到站 1-送票到站 - 默认值是 0 

    private Integer offlineOrderType;//测试环境表有，线上表里面没有

    private String needDeliveryTime;//测试环境表有，线上表里面没有

    private String QuestionOrder;//问题订单标记

    private Boolean isLockCallback;//途牛的异步锁票回调变同步的标志位-只能被修改一次，专用字段增加

    private Integer isGrab;//同程抢票订单标识  0: 普通订单,1: 抢票订单 - 默认值是 0 

    private String lockTime;//20170919-新增需求 - 锁单时间

    //淘宝新增12306高铁动车选座字段
    private String customizeStr;//20171018-新增需求 - 新增定制服务汉字字段 - 暂时只有淘宝试用，相关存储过程暂时不修改

    //UU跑腿对接新增字段
    private Integer isRapidSend;//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

    //需要在闪送单的接单的回调接口中做相关的操作和处理
    private Boolean isRapidSendSuccess;//闪送订单是否下单成功 - 默认是false - false-闪送下单失败 - true-闪送下单成功 //bit - ((0)) - 闪送订单是否下单成功[回调的结果] - 默认是false - false-闪送下单失败 - true-闪送下单成功

    private Boolean isRapidSendCallback;//闪送订单下单之后是否有反馈 - 默认是false - false-尚未反馈 - true-反馈已接收 - 异步效果

    private Boolean isSecondDateCallback;//是否是二次日期替换之后的回调 - 默认是false - true-二次进单，且日期发生了变化【同程20171108出票回调日期更新新增需求】

    private Integer isSecondOrder;//是否是二次进单  0: 普通订单,1: 二次进单 - 默认值是 0 

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(String createTime) {
        CreateTime = createTime;
    }

    public Integer getAgentId() {
        return AgentId;
    }

    public void setAgentId(Integer agentId) {
        AgentId = agentId;
    }

    public Integer getCreateUId() {
        return CreateUId;
    }

    public void setCreateUId(Integer createUId) {
        CreateUId = createUId;
    }

    public String getCreateUser() {
        return CreateUser;
    }

    public void setCreateUser(String createUser) {
        CreateUser = createUser;
    }

    public String getContactUser() {
        return ContactUser;
    }

    public void setContactUser(String contactUser) {
        ContactUser = contactUser;
    }

    public String getContactTel() {
        return ContactTel;
    }

    public void setContactTel(String contactTel) {
        ContactTel = contactTel;
    }

    public Double getOrderPrice() {
        return OrderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        OrderPrice = orderPrice;
    }

    public Integer getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getTradeNo() {
        return TradeNo;
    }

    public void setTradeNo(String tradeNo) {
        TradeNo = tradeNo;
    }

    public Double getAgentProfit() {
        return AgentProfit;
    }

    public void setAgentProfit(Double agentProfit) {
        AgentProfit = agentProfit;
    }

    public Integer getSupplyPayWay() {
        return SupplyPayWay;
    }

    public void setSupplyPayWay(Integer supplyPayWay) {
        SupplyPayWay = supplyPayWay;
    }

    public Integer getTicketCount() {
        return TicketCount;
    }

    public void setTicketCount(Integer ticketCount) {
        TicketCount = ticketCount;
    }

    public Integer getRefundReason() {
        return RefundReason;
    }

    public void setRefundReason(Integer refundReason) {
        RefundReason = refundReason;
    }

    public String getRefundReasonStr() {
        return RefundReasonStr;
    }

    public void setRefundReasonStr(String refundReasonStr) {
        RefundReasonStr = refundReasonStr;
    }

    public String getOrderTimeout() {
        return OrderTimeout;
    }

    public void setOrderTimeout(String orderTimeout) {
        OrderTimeout = orderTimeout;
    }

    public Integer getChuPiaoAgentid() {
        return ChuPiaoAgentid;
    }

    public void setChuPiaoAgentid(Integer chuPiaoAgentid) {
        ChuPiaoAgentid = chuPiaoAgentid;
    }

    public String getChuPiaoTime() {
        return ChuPiaoTime;
    }

    public void setChuPiaoTime(String chuPiaoTime) {
        ChuPiaoTime = chuPiaoTime;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getOrderNumberOnline() {
        return OrderNumberOnline;
    }

    public void setOrderNumberOnline(String orderNumberOnline) {
        OrderNumberOnline = orderNumberOnline;
    }

    public Integer getPaystatus() {
        return paystatus;
    }

    public void setPaystatus(Integer paystatus) {
        this.paystatus = paystatus;
    }

    public Integer getPaperType() {
        return PaperType;
    }

    public void setPaperType(Integer paperType) {
        PaperType = paperType;
    }

    public Integer getPaperBackup() {
        return PaperBackup;
    }

    public void setPaperBackup(Integer paperBackup) {
        PaperBackup = paperBackup;
    }

    public Integer getPaperLowSeatCount() {
        return paperLowSeatCount;
    }

    public void setPaperLowSeatCount(Integer paperLowSeatCount) {
        this.paperLowSeatCount = paperLowSeatCount;
    }

    public String getExtSeat() {
        return extSeat;
    }

    public void setExtSeat(String extSeat) {
        this.extSeat = extSeat;
    }

    public Integer getLockedStatus() {
        return lockedStatus;
    }

    public void setLockedStatus(Integer lockedStatus) {
        this.lockedStatus = lockedStatus;
    }

    public Integer getOperateid() {
        return operateid;
    }

    public void setOperateid(Integer operateid) {
        this.operateid = operateid;
    }

    public String getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(String operatetime) {
        this.operatetime = operatetime;
    }

    public Integer getRefusereason() {
        return refusereason;
    }

    public void setRefusereason(Integer refusereason) {
        this.refusereason = refusereason;
    }

    public String getRefusereasonstr() {
        return refusereasonstr;
    }

    public void setRefusereasonstr(String refusereasonstr) {
        this.refusereasonstr = refusereasonstr;
    }

    public String getExpressDeliver() {
        return expressDeliver;
    }

    public void setExpressDeliver(String expressDeliver) {
        this.expressDeliver = expressDeliver;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getQuestionDraw() {
        return questionDraw;
    }

    public void setQuestionDraw(Integer questionDraw) {
        this.questionDraw = questionDraw;
    }

    public Integer getQuestionMail() {
        return questionMail;
    }

    public void setQuestionMail(Integer questionMail) {
        this.questionMail = questionMail;
    }

    public Integer getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(Integer isDelivery) {
        this.isDelivery = isDelivery;
    }

    public Integer getOfflineOrderType() {
        return offlineOrderType;
    }

    public void setOfflineOrderType(Integer offlineOrderType) {
        this.offlineOrderType = offlineOrderType;
    }

    public String getNeedDeliveryTime() {
        return needDeliveryTime;
    }

    public void setNeedDeliveryTime(String needDeliveryTime) {
        this.needDeliveryTime = needDeliveryTime;
    }

    public String getQuestionOrder() {
        return QuestionOrder;
    }

    public void setQuestionOrder(String questionOrder) {
        QuestionOrder = questionOrder;
    }

    public Boolean getIsLockCallback() {
        return isLockCallback;
    }

    public void setIsLockCallback(Boolean isLockCallback) {
        this.isLockCallback = isLockCallback;
    }

    public Integer getIsGrab() {
        return isGrab;
    }

    public void setIsGrab(Integer isGrab) {
        this.isGrab = isGrab;
    }

    public String getLockTime() {
        return lockTime;
    }

    public void setLockTime(String lockTime) {
        this.lockTime = lockTime;
    }

    public String getCustomizeStr() {
        return customizeStr;
    }

    public void setCustomizeStr(String customizeStr) {
        this.customizeStr = customizeStr;
    }

    public Integer getIsRapidSend() {
        return isRapidSend;
    }

    public void setIsRapidSend(Integer isRapidSend) {
        this.isRapidSend = isRapidSend;
    }

    public Boolean getIsRapidSendSuccess() {
        return isRapidSendSuccess;
    }

    public void setIsRapidSendSuccess(Boolean isRapidSendSuccess) {
        this.isRapidSendSuccess = isRapidSendSuccess;
    }

    public Boolean getIsRapidSendCallback() {
        return isRapidSendCallback;
    }

    public void setIsRapidSendCallback(Boolean isRapidSendCallback) {
        this.isRapidSendCallback = isRapidSendCallback;
    }

    public Boolean getIsSecondDateCallback() {
        return isSecondDateCallback;
    }

    public void setIsSecondDateCallback(Boolean isSecondDateCallback) {
        this.isSecondDateCallback = isSecondDateCallback;
    }

    public Integer getIsSecondOrder() {
        return isSecondOrder;
    }

    public void setIsSecondOrder(Integer isSecondOrder) {
        this.isSecondOrder = isSecondOrder;
    }

    @Override
    public String toString() {
        return "TrainOrderOffline [Id=" + Id + ", OrderNumber=" + OrderNumber + ", CreateTime=" + CreateTime
                + ", AgentId=" + AgentId + ", CreateUId=" + CreateUId + ", CreateUser=" + CreateUser + ", ContactUser="
                + ContactUser + ", ContactTel=" + ContactTel + ", OrderPrice=" + OrderPrice + ", OrderStatus="
                + OrderStatus + ", TradeNo=" + TradeNo + ", AgentProfit=" + AgentProfit + ", SupplyPayWay="
                + SupplyPayWay + ", TicketCount=" + TicketCount + ", RefundReason=" + RefundReason
                + ", RefundReasonStr=" + RefundReasonStr + ", OrderTimeout=" + OrderTimeout + ", ChuPiaoAgentid="
                + ChuPiaoAgentid + ", ChuPiaoTime=" + ChuPiaoTime + ", Remark=" + Remark + ", OrderNumberOnline="
                + OrderNumberOnline + ", paystatus=" + paystatus + ", PaperType=" + PaperType + ", PaperBackup="
                + PaperBackup + ", paperLowSeatCount=" + paperLowSeatCount + ", extSeat=" + extSeat + ", lockedStatus="
                + lockedStatus + ", operateid=" + operateid + ", operatetime=" + operatetime + ", refusereason="
                + refusereason + ", refusereasonstr=" + refusereasonstr + ", expressDeliver=" + expressDeliver
                + ", telephone=" + telephone + ", questionDraw=" + questionDraw + ", questionMail=" + questionMail
                + ", isDelivery=" + isDelivery + ", offlineOrderType=" + offlineOrderType + ", needDeliveryTime="
                + needDeliveryTime + ", QuestionOrder=" + QuestionOrder + ", isLockCallback=" + isLockCallback
                + ", isGrab=" + isGrab + ", lockTime=" + lockTime + ", customizeStr=" + customizeStr + ", isRapidSend="
                + isRapidSend + ", isRapidSendSuccess=" + isRapidSendSuccess + ", isRapidSendCallback="
                + isRapidSendCallback + ", isSecondDateCallback=" + isSecondDateCallback + ", isSecondOrder="
                + isSecondOrder + "]";
    }

}
