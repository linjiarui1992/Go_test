/**
 * 版权所有;//空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.T_CUSTOMERAGENT
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午3:47:28 
 * @version: v 1.0
 * @since 
 *
 */
public class T_CUSTOMERAGENT {
    private Double ID;//
    private String C_CODE;//
    private Double C_AGENTTYPE;//
    private String C_AGENTVSDATE;//
    private String C_AGENTVEDATE;//
    private String C_AGENTCOMPANYNAME;//
    private String C_AGENTSHORTNAME;//
    private Double C_ALLOWLEVELCOUNT;//
    private Double C_ALLOWPROXYCOUNT;//
    private Double C_AGENTCITYID;//
    private String C_AGENTTEL;//
    private String C_AGENTADDRESS;//
    private String C_AGENTPOSTCODE;//
    private String C_AGENTCONTACTNAME;//
    private String C_AGENTEMAIL;//
    private Double C_AGENTCHECKSTATUS;//
    private Double C_AGENTISENABLE;//
    private String C_MODIFYTIME;//
    private String C_MODIFYUSER;//
    private String C_CREATETIME;//
    private String C_CREATEUSER;//
    private String C_PARENTSTR;//
    private Double C_BROKENUM;//
    private Double C_CHILDBROKENUM;//
    private String C_ALIPAYACCOUNT;//
    private String C_TENPAYACCOUNT;//
    private String C_KUAIBILLACCOUNT;//
    private String C_MSNQQ;//
    private String C_WEBSITE;//
    private Double C_BIGTYPE;//
    private Double C_USERID;//
    private String C_MUCODE;//
    private String C_CACODE;//
    private String C_CZCODE;//
    private Double C_RUNTYPE;//
    private Double C_RUNVALUE;//
    private String C_AGENTPHONE;//
    private String C_AGENRFAX;//
    private String C_AGENTMOBILE;//
    private String C_AGENTOTHER;//
    private String C_INDUSTRY;//
    private String C_FINANCENAME;//
    private String C_FINANCEPHONE;//
    private String C_FINANCEFAX;//
    private String C_FINANCEMOBILE;//
    private String C_FINANCEEMAIL;//
    private Integer C_SELFCODE;//
    private String C_AIRPORTCODE;//
    private Double C_ISMODIFYRET;//
    private Integer C_SMSCOUNT;//
    private Double C_AGENTJIBIE;//
    private Double C_CITYID;//
    private Double C_PARENTID;//
    private Double C_ISALLOWMONTHPAY;//
    private String C_OUTTICKETMANTEL;//
    private String C_OUTTICKETMANMSNQQ;//
    private String C_BACKTICKETMANTEL;//
    private String C_BACKTICKETMANMSNQQ;//
    private String C_WORKTIMEBEGIN;//
    private String C_WORKTIMEEND;//
    private Double C_VMONEY;//
    private Integer C_OPENABLE;//
    private Integer C_B2COPENABLE;//
    private String C_CHINAPNRCOUNT;//
    private Double C_SMSMONEY;//
    private String C_SMSCOUNTER;//
    private String C_SMSPWD;//
    private Integer C_ISPARTNER;//
    private Integer C_ITINERARYCOUNT;//
    private String C_VMONEYPWD;//
    private Double C_MANAGERUID;//
    private Double C_SPECIALDATA;//
    private Double C_DNSAGENTID;//
    private Double C_DISABLEMONEY;//
    private Double C_GUARANMONEY;//
    private Integer C_HOTELSUPPLIERFLAG;//
    private Double C_HOTELSUPPLIERID;//
    private Double C_SMSMOVEMONEY;//
    private String C_SELLERNAME;//
    private String C_SELLERPHONE;//
    private Integer C_SMSTICKETFLAG;//
    private Double C_SMSMOUNTCLOCK;//
    private Double C_SMSCLOCKMOUNT;//
    private String C_SMSGETTELPHONE;//
    private Integer C_ISTENPAYPARTNER;//
    private Double TrainSingleFactorage;//
    private Integer ProvinceId;//
    private String ProvinceName;//
    private Integer CityId;//
    private String CityName;//
    private Integer RegionID;//
    private String RegionName;//
    private Integer TownID;//
    private String TownName;//
    private Integer AgentHeat;//
    private Integer FKBusinessIdentityID;//
    private String MonthPayNum;//
    private String PayType;//
    public Double getID() {
        return ID;
    }
    public void setID(Double iD) {
        ID = iD;
    }
    public String getC_CODE() {
        return C_CODE;
    }
    public void setC_CODE(String c_CODE) {
        C_CODE = c_CODE;
    }
    public Double getC_AGENTTYPE() {
        return C_AGENTTYPE;
    }
    public void setC_AGENTTYPE(Double c_AGENTTYPE) {
        C_AGENTTYPE = c_AGENTTYPE;
    }
    public String getC_AGENTVSDATE() {
        return C_AGENTVSDATE;
    }
    public void setC_AGENTVSDATE(String c_AGENTVSDATE) {
        C_AGENTVSDATE = c_AGENTVSDATE;
    }
    public String getC_AGENTVEDATE() {
        return C_AGENTVEDATE;
    }
    public void setC_AGENTVEDATE(String c_AGENTVEDATE) {
        C_AGENTVEDATE = c_AGENTVEDATE;
    }
    public String getC_AGENTCOMPANYNAME() {
        return C_AGENTCOMPANYNAME;
    }
    public void setC_AGENTCOMPANYNAME(String c_AGENTCOMPANYNAME) {
        C_AGENTCOMPANYNAME = c_AGENTCOMPANYNAME;
    }
    public String getC_AGENTSHORTNAME() {
        return C_AGENTSHORTNAME;
    }
    public void setC_AGENTSHORTNAME(String c_AGENTSHORTNAME) {
        C_AGENTSHORTNAME = c_AGENTSHORTNAME;
    }
    public Double getC_ALLOWLEVELCOUNT() {
        return C_ALLOWLEVELCOUNT;
    }
    public void setC_ALLOWLEVELCOUNT(Double c_ALLOWLEVELCOUNT) {
        C_ALLOWLEVELCOUNT = c_ALLOWLEVELCOUNT;
    }
    public Double getC_ALLOWPROXYCOUNT() {
        return C_ALLOWPROXYCOUNT;
    }
    public void setC_ALLOWPROXYCOUNT(Double c_ALLOWPROXYCOUNT) {
        C_ALLOWPROXYCOUNT = c_ALLOWPROXYCOUNT;
    }
    public Double getC_AGENTCITYID() {
        return C_AGENTCITYID;
    }
    public void setC_AGENTCITYID(Double c_AGENTCITYID) {
        C_AGENTCITYID = c_AGENTCITYID;
    }
    public String getC_AGENTTEL() {
        return C_AGENTTEL;
    }
    public void setC_AGENTTEL(String c_AGENTTEL) {
        C_AGENTTEL = c_AGENTTEL;
    }
    public String getC_AGENTADDRESS() {
        return C_AGENTADDRESS;
    }
    public void setC_AGENTADDRESS(String c_AGENTADDRESS) {
        C_AGENTADDRESS = c_AGENTADDRESS;
    }
    public String getC_AGENTPOSTCODE() {
        return C_AGENTPOSTCODE;
    }
    public void setC_AGENTPOSTCODE(String c_AGENTPOSTCODE) {
        C_AGENTPOSTCODE = c_AGENTPOSTCODE;
    }
    public String getC_AGENTCONTACTNAME() {
        return C_AGENTCONTACTNAME;
    }
    public void setC_AGENTCONTACTNAME(String c_AGENTCONTACTNAME) {
        C_AGENTCONTACTNAME = c_AGENTCONTACTNAME;
    }
    public String getC_AGENTEMAIL() {
        return C_AGENTEMAIL;
    }
    public void setC_AGENTEMAIL(String c_AGENTEMAIL) {
        C_AGENTEMAIL = c_AGENTEMAIL;
    }
    public Double getC_AGENTCHECKSTATUS() {
        return C_AGENTCHECKSTATUS;
    }
    public void setC_AGENTCHECKSTATUS(Double c_AGENTCHECKSTATUS) {
        C_AGENTCHECKSTATUS = c_AGENTCHECKSTATUS;
    }
    public Double getC_AGENTISENABLE() {
        return C_AGENTISENABLE;
    }
    public void setC_AGENTISENABLE(Double c_AGENTISENABLE) {
        C_AGENTISENABLE = c_AGENTISENABLE;
    }
    public String getC_MODIFYTIME() {
        return C_MODIFYTIME;
    }
    public void setC_MODIFYTIME(String c_MODIFYTIME) {
        C_MODIFYTIME = c_MODIFYTIME;
    }
    public String getC_MODIFYUSER() {
        return C_MODIFYUSER;
    }
    public void setC_MODIFYUSER(String c_MODIFYUSER) {
        C_MODIFYUSER = c_MODIFYUSER;
    }
    public String getC_CREATETIME() {
        return C_CREATETIME;
    }
    public void setC_CREATETIME(String c_CREATETIME) {
        C_CREATETIME = c_CREATETIME;
    }
    public String getC_CREATEUSER() {
        return C_CREATEUSER;
    }
    public void setC_CREATEUSER(String c_CREATEUSER) {
        C_CREATEUSER = c_CREATEUSER;
    }
    public String getC_PARENTSTR() {
        return C_PARENTSTR;
    }
    public void setC_PARENTSTR(String c_PARENTSTR) {
        C_PARENTSTR = c_PARENTSTR;
    }
    public Double getC_BROKENUM() {
        return C_BROKENUM;
    }
    public void setC_BROKENUM(Double c_BROKENUM) {
        C_BROKENUM = c_BROKENUM;
    }
    public Double getC_CHILDBROKENUM() {
        return C_CHILDBROKENUM;
    }
    public void setC_CHILDBROKENUM(Double c_CHILDBROKENUM) {
        C_CHILDBROKENUM = c_CHILDBROKENUM;
    }
    public String getC_ALIPAYACCOUNT() {
        return C_ALIPAYACCOUNT;
    }
    public void setC_ALIPAYACCOUNT(String c_ALIPAYACCOUNT) {
        C_ALIPAYACCOUNT = c_ALIPAYACCOUNT;
    }
    public String getC_TENPAYACCOUNT() {
        return C_TENPAYACCOUNT;
    }
    public void setC_TENPAYACCOUNT(String c_TENPAYACCOUNT) {
        C_TENPAYACCOUNT = c_TENPAYACCOUNT;
    }
    public String getC_KUAIBILLACCOUNT() {
        return C_KUAIBILLACCOUNT;
    }
    public void setC_KUAIBILLACCOUNT(String c_KUAIBILLACCOUNT) {
        C_KUAIBILLACCOUNT = c_KUAIBILLACCOUNT;
    }
    public String getC_MSNQQ() {
        return C_MSNQQ;
    }
    public void setC_MSNQQ(String c_MSNQQ) {
        C_MSNQQ = c_MSNQQ;
    }
    public String getC_WEBSITE() {
        return C_WEBSITE;
    }
    public void setC_WEBSITE(String c_WEBSITE) {
        C_WEBSITE = c_WEBSITE;
    }
    public Double getC_BIGTYPE() {
        return C_BIGTYPE;
    }
    public void setC_BIGTYPE(Double c_BIGTYPE) {
        C_BIGTYPE = c_BIGTYPE;
    }
    public Double getC_USERID() {
        return C_USERID;
    }
    public void setC_USERID(Double c_USERID) {
        C_USERID = c_USERID;
    }
    public String getC_MUCODE() {
        return C_MUCODE;
    }
    public void setC_MUCODE(String c_MUCODE) {
        C_MUCODE = c_MUCODE;
    }
    public String getC_CACODE() {
        return C_CACODE;
    }
    public void setC_CACODE(String c_CACODE) {
        C_CACODE = c_CACODE;
    }
    public String getC_CZCODE() {
        return C_CZCODE;
    }
    public void setC_CZCODE(String c_CZCODE) {
        C_CZCODE = c_CZCODE;
    }
    public Double getC_RUNTYPE() {
        return C_RUNTYPE;
    }
    public void setC_RUNTYPE(Double c_RUNTYPE) {
        C_RUNTYPE = c_RUNTYPE;
    }
    public Double getC_RUNVALUE() {
        return C_RUNVALUE;
    }
    public void setC_RUNVALUE(Double c_RUNVALUE) {
        C_RUNVALUE = c_RUNVALUE;
    }
    public String getC_AGENTPHONE() {
        return C_AGENTPHONE;
    }
    public void setC_AGENTPHONE(String c_AGENTPHONE) {
        C_AGENTPHONE = c_AGENTPHONE;
    }
    public String getC_AGENRFAX() {
        return C_AGENRFAX;
    }
    public void setC_AGENRFAX(String c_AGENRFAX) {
        C_AGENRFAX = c_AGENRFAX;
    }
    public String getC_AGENTMOBILE() {
        return C_AGENTMOBILE;
    }
    public void setC_AGENTMOBILE(String c_AGENTMOBILE) {
        C_AGENTMOBILE = c_AGENTMOBILE;
    }
    public String getC_AGENTOTHER() {
        return C_AGENTOTHER;
    }
    public void setC_AGENTOTHER(String c_AGENTOTHER) {
        C_AGENTOTHER = c_AGENTOTHER;
    }
    public String getC_INDUSTRY() {
        return C_INDUSTRY;
    }
    public void setC_INDUSTRY(String c_INDUSTRY) {
        C_INDUSTRY = c_INDUSTRY;
    }
    public String getC_FINANCENAME() {
        return C_FINANCENAME;
    }
    public void setC_FINANCENAME(String c_FINANCENAME) {
        C_FINANCENAME = c_FINANCENAME;
    }
    public String getC_FINANCEPHONE() {
        return C_FINANCEPHONE;
    }
    public void setC_FINANCEPHONE(String c_FINANCEPHONE) {
        C_FINANCEPHONE = c_FINANCEPHONE;
    }
    public String getC_FINANCEFAX() {
        return C_FINANCEFAX;
    }
    public void setC_FINANCEFAX(String c_FINANCEFAX) {
        C_FINANCEFAX = c_FINANCEFAX;
    }
    public String getC_FINANCEMOBILE() {
        return C_FINANCEMOBILE;
    }
    public void setC_FINANCEMOBILE(String c_FINANCEMOBILE) {
        C_FINANCEMOBILE = c_FINANCEMOBILE;
    }
    public String getC_FINANCEEMAIL() {
        return C_FINANCEEMAIL;
    }
    public void setC_FINANCEEMAIL(String c_FINANCEEMAIL) {
        C_FINANCEEMAIL = c_FINANCEEMAIL;
    }
    public Integer getC_SELFCODE() {
        return C_SELFCODE;
    }
    public void setC_SELFCODE(Integer c_SELFCODE) {
        C_SELFCODE = c_SELFCODE;
    }
    public String getC_AIRPORTCODE() {
        return C_AIRPORTCODE;
    }
    public void setC_AIRPORTCODE(String c_AIRPORTCODE) {
        C_AIRPORTCODE = c_AIRPORTCODE;
    }
    public Double getC_ISMODIFYRET() {
        return C_ISMODIFYRET;
    }
    public void setC_ISMODIFYRET(Double c_ISMODIFYRET) {
        C_ISMODIFYRET = c_ISMODIFYRET;
    }
    public Integer getC_SMSCOUNT() {
        return C_SMSCOUNT;
    }
    public void setC_SMSCOUNT(Integer c_SMSCOUNT) {
        C_SMSCOUNT = c_SMSCOUNT;
    }
    public Double getC_AGENTJIBIE() {
        return C_AGENTJIBIE;
    }
    public void setC_AGENTJIBIE(Double c_AGENTJIBIE) {
        C_AGENTJIBIE = c_AGENTJIBIE;
    }
    public Double getC_CITYID() {
        return C_CITYID;
    }
    public void setC_CITYID(Double c_CITYID) {
        C_CITYID = c_CITYID;
    }
    public Double getC_PARENTID() {
        return C_PARENTID;
    }
    public void setC_PARENTID(Double c_PARENTID) {
        C_PARENTID = c_PARENTID;
    }
    public Double getC_ISALLOWMONTHPAY() {
        return C_ISALLOWMONTHPAY;
    }
    public void setC_ISALLOWMONTHPAY(Double c_ISALLOWMONTHPAY) {
        C_ISALLOWMONTHPAY = c_ISALLOWMONTHPAY;
    }
    public String getC_OUTTICKETMANTEL() {
        return C_OUTTICKETMANTEL;
    }
    public void setC_OUTTICKETMANTEL(String c_OUTTICKETMANTEL) {
        C_OUTTICKETMANTEL = c_OUTTICKETMANTEL;
    }
    public String getC_OUTTICKETMANMSNQQ() {
        return C_OUTTICKETMANMSNQQ;
    }
    public void setC_OUTTICKETMANMSNQQ(String c_OUTTICKETMANMSNQQ) {
        C_OUTTICKETMANMSNQQ = c_OUTTICKETMANMSNQQ;
    }
    public String getC_BACKTICKETMANTEL() {
        return C_BACKTICKETMANTEL;
    }
    public void setC_BACKTICKETMANTEL(String c_BACKTICKETMANTEL) {
        C_BACKTICKETMANTEL = c_BACKTICKETMANTEL;
    }
    public String getC_BACKTICKETMANMSNQQ() {
        return C_BACKTICKETMANMSNQQ;
    }
    public void setC_BACKTICKETMANMSNQQ(String c_BACKTICKETMANMSNQQ) {
        C_BACKTICKETMANMSNQQ = c_BACKTICKETMANMSNQQ;
    }
    public String getC_WORKTIMEBEGIN() {
        return C_WORKTIMEBEGIN;
    }
    public void setC_WORKTIMEBEGIN(String c_WORKTIMEBEGIN) {
        C_WORKTIMEBEGIN = c_WORKTIMEBEGIN;
    }
    public String getC_WORKTIMEEND() {
        return C_WORKTIMEEND;
    }
    public void setC_WORKTIMEEND(String c_WORKTIMEEND) {
        C_WORKTIMEEND = c_WORKTIMEEND;
    }
    public Double getC_VMONEY() {
        return C_VMONEY;
    }
    public void setC_VMONEY(Double c_VMONEY) {
        C_VMONEY = c_VMONEY;
    }
    public Integer getC_OPENABLE() {
        return C_OPENABLE;
    }
    public void setC_OPENABLE(Integer c_OPENABLE) {
        C_OPENABLE = c_OPENABLE;
    }
    public Integer getC_B2COPENABLE() {
        return C_B2COPENABLE;
    }
    public void setC_B2COPENABLE(Integer c_B2COPENABLE) {
        C_B2COPENABLE = c_B2COPENABLE;
    }
    public String getC_CHINAPNRCOUNT() {
        return C_CHINAPNRCOUNT;
    }
    public void setC_CHINAPNRCOUNT(String c_CHINAPNRCOUNT) {
        C_CHINAPNRCOUNT = c_CHINAPNRCOUNT;
    }
    public Double getC_SMSMONEY() {
        return C_SMSMONEY;
    }
    public void setC_SMSMONEY(Double c_SMSMONEY) {
        C_SMSMONEY = c_SMSMONEY;
    }
    public String getC_SMSCOUNTER() {
        return C_SMSCOUNTER;
    }
    public void setC_SMSCOUNTER(String c_SMSCOUNTER) {
        C_SMSCOUNTER = c_SMSCOUNTER;
    }
    public String getC_SMSPWD() {
        return C_SMSPWD;
    }
    public void setC_SMSPWD(String c_SMSPWD) {
        C_SMSPWD = c_SMSPWD;
    }
    public Integer getC_ISPARTNER() {
        return C_ISPARTNER;
    }
    public void setC_ISPARTNER(Integer c_ISPARTNER) {
        C_ISPARTNER = c_ISPARTNER;
    }
    public Integer getC_ITINERARYCOUNT() {
        return C_ITINERARYCOUNT;
    }
    public void setC_ITINERARYCOUNT(Integer c_ITINERARYCOUNT) {
        C_ITINERARYCOUNT = c_ITINERARYCOUNT;
    }
    public String getC_VMONEYPWD() {
        return C_VMONEYPWD;
    }
    public void setC_VMONEYPWD(String c_VMONEYPWD) {
        C_VMONEYPWD = c_VMONEYPWD;
    }
    public Double getC_MANAGERUID() {
        return C_MANAGERUID;
    }
    public void setC_MANAGERUID(Double c_MANAGERUID) {
        C_MANAGERUID = c_MANAGERUID;
    }
    public Double getC_SPECIALDATA() {
        return C_SPECIALDATA;
    }
    public void setC_SPECIALDATA(Double c_SPECIALDATA) {
        C_SPECIALDATA = c_SPECIALDATA;
    }
    public Double getC_DNSAGENTID() {
        return C_DNSAGENTID;
    }
    public void setC_DNSAGENTID(Double c_DNSAGENTID) {
        C_DNSAGENTID = c_DNSAGENTID;
    }
    public Double getC_DISABLEMONEY() {
        return C_DISABLEMONEY;
    }
    public void setC_DISABLEMONEY(Double c_DISABLEMONEY) {
        C_DISABLEMONEY = c_DISABLEMONEY;
    }
    public Double getC_GUARANMONEY() {
        return C_GUARANMONEY;
    }
    public void setC_GUARANMONEY(Double c_GUARANMONEY) {
        C_GUARANMONEY = c_GUARANMONEY;
    }
    public Integer getC_HOTELSUPPLIERFLAG() {
        return C_HOTELSUPPLIERFLAG;
    }
    public void setC_HOTELSUPPLIERFLAG(Integer c_HOTELSUPPLIERFLAG) {
        C_HOTELSUPPLIERFLAG = c_HOTELSUPPLIERFLAG;
    }
    public Double getC_HOTELSUPPLIERID() {
        return C_HOTELSUPPLIERID;
    }
    public void setC_HOTELSUPPLIERID(Double c_HOTELSUPPLIERID) {
        C_HOTELSUPPLIERID = c_HOTELSUPPLIERID;
    }
    public Double getC_SMSMOVEMONEY() {
        return C_SMSMOVEMONEY;
    }
    public void setC_SMSMOVEMONEY(Double c_SMSMOVEMONEY) {
        C_SMSMOVEMONEY = c_SMSMOVEMONEY;
    }
    public String getC_SELLERNAME() {
        return C_SELLERNAME;
    }
    public void setC_SELLERNAME(String c_SELLERNAME) {
        C_SELLERNAME = c_SELLERNAME;
    }
    public String getC_SELLERPHONE() {
        return C_SELLERPHONE;
    }
    public void setC_SELLERPHONE(String c_SELLERPHONE) {
        C_SELLERPHONE = c_SELLERPHONE;
    }
    public Integer getC_SMSTICKETFLAG() {
        return C_SMSTICKETFLAG;
    }
    public void setC_SMSTICKETFLAG(Integer c_SMSTICKETFLAG) {
        C_SMSTICKETFLAG = c_SMSTICKETFLAG;
    }
    public Double getC_SMSMOUNTCLOCK() {
        return C_SMSMOUNTCLOCK;
    }
    public void setC_SMSMOUNTCLOCK(Double c_SMSMOUNTCLOCK) {
        C_SMSMOUNTCLOCK = c_SMSMOUNTCLOCK;
    }
    public Double getC_SMSCLOCKMOUNT() {
        return C_SMSCLOCKMOUNT;
    }
    public void setC_SMSCLOCKMOUNT(Double c_SMSCLOCKMOUNT) {
        C_SMSCLOCKMOUNT = c_SMSCLOCKMOUNT;
    }
    public String getC_SMSGETTELPHONE() {
        return C_SMSGETTELPHONE;
    }
    public void setC_SMSGETTELPHONE(String c_SMSGETTELPHONE) {
        C_SMSGETTELPHONE = c_SMSGETTELPHONE;
    }
    public Integer getC_ISTENPAYPARTNER() {
        return C_ISTENPAYPARTNER;
    }
    public void setC_ISTENPAYPARTNER(Integer c_ISTENPAYPARTNER) {
        C_ISTENPAYPARTNER = c_ISTENPAYPARTNER;
    }
    public Double getTrainSingleFactorage() {
        return TrainSingleFactorage;
    }
    public void setTrainSingleFactorage(Double trainSingleFactorage) {
        TrainSingleFactorage = trainSingleFactorage;
    }
    public Integer getProvinceId() {
        return ProvinceId;
    }
    public void setProvinceId(Integer provinceId) {
        ProvinceId = provinceId;
    }
    public String getProvinceName() {
        return ProvinceName;
    }
    public void setProvinceName(String provinceName) {
        ProvinceName = provinceName;
    }
    public Integer getCityId() {
        return CityId;
    }
    public void setCityId(Integer cityId) {
        CityId = cityId;
    }
    public String getCityName() {
        return CityName;
    }
    public void setCityName(String cityName) {
        CityName = cityName;
    }
    public Integer getRegionID() {
        return RegionID;
    }
    public void setRegionID(Integer regionID) {
        RegionID = regionID;
    }
    public String getRegionName() {
        return RegionName;
    }
    public void setRegionName(String regionName) {
        RegionName = regionName;
    }
    public Integer getTownID() {
        return TownID;
    }
    public void setTownID(Integer townID) {
        TownID = townID;
    }
    public String getTownName() {
        return TownName;
    }
    public void setTownName(String townName) {
        TownName = townName;
    }
    public Integer getAgentHeat() {
        return AgentHeat;
    }
    public void setAgentHeat(Integer agentHeat) {
        AgentHeat = agentHeat;
    }
    public Integer getFKBusinessIdentityID() {
        return FKBusinessIdentityID;
    }
    public void setFKBusinessIdentityID(Integer fKBusinessIdentityID) {
        FKBusinessIdentityID = fKBusinessIdentityID;
    }
    public String getMonthPayNum() {
        return MonthPayNum;
    }
    public void setMonthPayNum(String monthPayNum) {
        MonthPayNum = monthPayNum;
    }
    public String getPayType() {
        return PayType;
    }
    public void setPayType(String payType) {
        PayType = payType;
    }
    @Override
    public String toString() {
        return "T_CUSTOMERAGENT [ID=" + ID + ", C_CODE=" + C_CODE + ", C_AGENTTYPE=" + C_AGENTTYPE + ", C_AGENTVSDATE="
                + C_AGENTVSDATE + ", C_AGENTVEDATE=" + C_AGENTVEDATE + ", C_AGENTCOMPANYNAME=" + C_AGENTCOMPANYNAME
                + ", C_AGENTSHORTNAME=" + C_AGENTSHORTNAME + ", C_ALLOWLEVELCOUNT=" + C_ALLOWLEVELCOUNT
                + ", C_ALLOWPROXYCOUNT=" + C_ALLOWPROXYCOUNT + ", C_AGENTCITYID=" + C_AGENTCITYID + ", C_AGENTTEL="
                + C_AGENTTEL + ", C_AGENTADDRESS=" + C_AGENTADDRESS + ", C_AGENTPOSTCODE=" + C_AGENTPOSTCODE
                + ", C_AGENTCONTACTNAME=" + C_AGENTCONTACTNAME + ", C_AGENTEMAIL=" + C_AGENTEMAIL
                + ", C_AGENTCHECKSTATUS=" + C_AGENTCHECKSTATUS + ", C_AGENTISENABLE=" + C_AGENTISENABLE
                + ", C_MODIFYTIME=" + C_MODIFYTIME + ", C_MODIFYUSER=" + C_MODIFYUSER + ", C_CREATETIME=" + C_CREATETIME
                + ", C_CREATEUSER=" + C_CREATEUSER + ", C_PARENTSTR=" + C_PARENTSTR + ", C_BROKENUM=" + C_BROKENUM
                + ", C_CHILDBROKENUM=" + C_CHILDBROKENUM + ", C_ALIPAYACCOUNT=" + C_ALIPAYACCOUNT + ", C_TENPAYACCOUNT="
                + C_TENPAYACCOUNT + ", C_KUAIBILLACCOUNT=" + C_KUAIBILLACCOUNT + ", C_MSNQQ=" + C_MSNQQ + ", C_WEBSITE="
                + C_WEBSITE + ", C_BIGTYPE=" + C_BIGTYPE + ", C_USERID=" + C_USERID + ", C_MUCODE=" + C_MUCODE
                + ", C_CACODE=" + C_CACODE + ", C_CZCODE=" + C_CZCODE + ", C_RUNTYPE=" + C_RUNTYPE + ", C_RUNVALUE="
                + C_RUNVALUE + ", C_AGENTPHONE=" + C_AGENTPHONE + ", C_AGENRFAX=" + C_AGENRFAX + ", C_AGENTMOBILE="
                + C_AGENTMOBILE + ", C_AGENTOTHER=" + C_AGENTOTHER + ", C_INDUSTRY=" + C_INDUSTRY + ", C_FINANCENAME="
                + C_FINANCENAME + ", C_FINANCEPHONE=" + C_FINANCEPHONE + ", C_FINANCEFAX=" + C_FINANCEFAX
                + ", C_FINANCEMOBILE=" + C_FINANCEMOBILE + ", C_FINANCEEMAIL=" + C_FINANCEEMAIL + ", C_SELFCODE="
                + C_SELFCODE + ", C_AIRPORTCODE=" + C_AIRPORTCODE + ", C_ISMODIFYRET=" + C_ISMODIFYRET + ", C_SMSCOUNT="
                + C_SMSCOUNT + ", C_AGENTJIBIE=" + C_AGENTJIBIE + ", C_CITYID=" + C_CITYID + ", C_PARENTID="
                + C_PARENTID + ", C_ISALLOWMONTHPAY=" + C_ISALLOWMONTHPAY + ", C_OUTTICKETMANTEL=" + C_OUTTICKETMANTEL
                + ", C_OUTTICKETMANMSNQQ=" + C_OUTTICKETMANMSNQQ + ", C_BACKTICKETMANTEL=" + C_BACKTICKETMANTEL
                + ", C_BACKTICKETMANMSNQQ=" + C_BACKTICKETMANMSNQQ + ", C_WORKTIMEBEGIN=" + C_WORKTIMEBEGIN
                + ", C_WORKTIMEEND=" + C_WORKTIMEEND + ", C_VMONEY=" + C_VMONEY + ", C_OPENABLE=" + C_OPENABLE
                + ", C_B2COPENABLE=" + C_B2COPENABLE + ", C_CHINAPNRCOUNT=" + C_CHINAPNRCOUNT + ", C_SMSMONEY="
                + C_SMSMONEY + ", C_SMSCOUNTER=" + C_SMSCOUNTER + ", C_SMSPWD=" + C_SMSPWD + ", C_ISPARTNER="
                + C_ISPARTNER + ", C_ITINERARYCOUNT=" + C_ITINERARYCOUNT + ", C_VMONEYPWD=" + C_VMONEYPWD
                + ", C_MANAGERUID=" + C_MANAGERUID + ", C_SPECIALDATA=" + C_SPECIALDATA + ", C_DNSAGENTID="
                + C_DNSAGENTID + ", C_DISABLEMONEY=" + C_DISABLEMONEY + ", C_GUARANMONEY=" + C_GUARANMONEY
                + ", C_HOTELSUPPLIERFLAG=" + C_HOTELSUPPLIERFLAG + ", C_HOTELSUPPLIERID=" + C_HOTELSUPPLIERID
                + ", C_SMSMOVEMONEY=" + C_SMSMOVEMONEY + ", C_SELLERNAME=" + C_SELLERNAME + ", C_SELLERPHONE="
                + C_SELLERPHONE + ", C_SMSTICKETFLAG=" + C_SMSTICKETFLAG + ", C_SMSMOUNTCLOCK=" + C_SMSMOUNTCLOCK
                + ", C_SMSCLOCKMOUNT=" + C_SMSCLOCKMOUNT + ", C_SMSGETTELPHONE=" + C_SMSGETTELPHONE
                + ", C_ISTENPAYPARTNER=" + C_ISTENPAYPARTNER + ", TrainSingleFactorage=" + TrainSingleFactorage
                + ", ProvinceId=" + ProvinceId + ", ProvinceName=" + ProvinceName + ", CityId=" + CityId + ", CityName="
                + CityName + ", RegionID=" + RegionID + ", RegionName=" + RegionName + ", TownID=" + TownID
                + ", TownName=" + TownName + ", AgentHeat=" + AgentHeat + ", FKBusinessIdentityID="
                + FKBusinessIdentityID + ", MonthPayNum=" + MonthPayNum + ", PayType=" + PayType + "]";
    }
    
}
