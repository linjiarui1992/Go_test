/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineTomAgentAdd
 * @description: TODO - 送票到站地址表
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午6:19:12 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineTomAgentAdd {
    private Integer id;//
    private Integer agentId;//代售点ID
    private String deliveryAddress;//送票到站地址
    private String deliveryTime;//送票到站时间区间
    private String deliveryName;//接收人姓名
    private String deliveryMobile;//接收人电话
    private String trainStation;//始发站地址
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getAgentId() {
        return agentId;
    }
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }
    public String getDeliveryAddress() {
        return deliveryAddress;
    }
    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
    public String getDeliveryTime() {
        return deliveryTime;
    }
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
    public String getDeliveryName() {
        return deliveryName;
    }
    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }
    public String getDeliveryMobile() {
        return deliveryMobile;
    }
    public void setDeliveryMobile(String deliveryMobile) {
        this.deliveryMobile = deliveryMobile;
    }
    public String getTrainStation() {
        return trainStation;
    }
    public void setTrainStation(String trainStation) {
        this.trainStation = trainStation;
    }
    @Override
    public String toString() {
        return "TrainOfflineTomAgentAdd [id=" + id + ", agentId=" + agentId + ", deliveryAddress=" + deliveryAddress
                + ", deliveryTime=" + deliveryTime + ", deliveryName=" + deliveryName + ", deliveryMobile="
                + deliveryMobile + ", trainStation=" + trainStation + "]";
    }
    
}
