package com.ccservice.offline.domain;


/**
 * @className: com.ccservice.tuniu.train.domain.TrainOrderOffline
 * @description: TODO - 
 * @author: 郑州-技术-陈亚峰
 * @createTime: 2017年8月16日 上午20:06:12 
 * @version: v 1.0
 * @since 
 *
 */
public class MailAddress {
    private Integer ID;
    private String MAILNAME;//收件人
    private String MAILTEL;//收件电话
    private String POSTCODE;//邮编
    private String ADDRESS;//收件地址
    private String PROVINCEID;//省份
    private String COUNTRYID;//
    private String CITYID;//
    private String PROVINCENAME;//省份名称
    private String CITYNAME;//城市名称
    private Integer REGIONID;//
    private String REGIONNAME;//区域名称
    private String TOWNID;
    private String TOWNNAME;
    private String MAIL;
    private String NOTE;
    private Integer ORDERID;//平台订单号
    private Integer BUSSTYPE;
    private Integer STATE;//未知字段 - 暂且用在标识配送的状态 - 默认值 - 0-未配送 - 1-开始配送 - 2-已送达
    private String CREATETIME;
    private String ORDERNUMBER;
    private String DEPARTTIME;
    private Integer INSURCOUNT;
    private Integer AGENTID;
    private Integer PRINTSTATE;//邮寄状态 - 默认值是0 - 在相关的存储过程中已经不考虑该值的状态了 - 0 - 待邮寄 - 1 - 已邮寄  - 还原到正式环境的版本的处理
    private Integer accountstate;//账户状态：0：冻结。1：正常 - 用于防止多次结算问题 0：待结算 - 1：已结算 
    private String ExpressNum;//快递单号
    private String ExpressTime;
    private Integer ExpressAgent;//0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】

    private Double rapidSendPrice;//闪送订单的价格 - 在下单的时候做反馈

    public Integer getID() {
        return ID;
    }
    public void setID(Integer iD) {
        ID = iD;
    }
    public String getMAILNAME() {
        return MAILNAME;
    }
    public void setMAILNAME(String mAILNAME) {
        MAILNAME = mAILNAME;
    }
    public String getMAILTEL() {
        return MAILTEL;
    }
    public void setMAILTEL(String mAILTEL) {
        MAILTEL = mAILTEL;
    }
    public String getPOSTCODE() {
        return POSTCODE;
    }
    public void setPOSTCODE(String pOSTCODE) {
        POSTCODE = pOSTCODE;
    }
    public String getADDRESS() {
        return ADDRESS;
    }
    public void setADDRESS(String aDDRESS) {
        ADDRESS = aDDRESS;
    }
    public String getPROVINCEID() {
        return PROVINCEID;
    }
    public void setPROVINCEID(String pROVINCEID) {
        PROVINCEID = pROVINCEID;
    }
    public String getCOUNTRYID() {
        return COUNTRYID;
    }
    public void setCOUNTRYID(String cOUNTRYID) {
        COUNTRYID = cOUNTRYID;
    }
    public String getCITYID() {
        return CITYID;
    }
    public void setCITYID(String cITYID) {
        CITYID = cITYID;
    }
    public String getPROVINCENAME() {
        return PROVINCENAME;
    }
    public void setPROVINCENAME(String pROVINCENAME) {
        PROVINCENAME = pROVINCENAME;
    }
    public String getCITYNAME() {
        return CITYNAME;
    }
    public void setCITYNAME(String cITYNAME) {
        CITYNAME = cITYNAME;
    }
    public Integer getREGIONID() {
        return REGIONID;
    }
    public void setREGIONID(Integer rEGIONID) {
        REGIONID = rEGIONID;
    }
    public String getREGIONNAME() {
        return REGIONNAME;
    }
    public void setREGIONNAME(String rEGIONNAME) {
        REGIONNAME = rEGIONNAME;
    }
    public String getTOWNID() {
        return TOWNID;
    }
    public void setTOWNID(String tOWNID) {
        TOWNID = tOWNID;
    }
    public String getTOWNNAME() {
        return TOWNNAME;
    }
    public void setTOWNNAME(String tOWNNAME) {
        TOWNNAME = tOWNNAME;
    }
    public String getMAIL() {
        return MAIL;
    }
    public void setMAIL(String mAIL) {
        MAIL = mAIL;
    }
    public String getNOTE() {
        return NOTE;
    }
    public void setNOTE(String nOTE) {
        NOTE = nOTE;
    }
    public Integer getORDERID() {
        return ORDERID;
    }
    public void setORDERID(Integer oRDERID) {
        ORDERID = oRDERID;
    }
    public Integer getBUSSTYPE() {
        return BUSSTYPE;
    }
    public void setBUSSTYPE(Integer bUSSTYPE) {
        BUSSTYPE = bUSSTYPE;
    }
    public Integer getSTATE() {
        return STATE;
    }
    public void setSTATE(Integer sTATE) {
        STATE = sTATE;
    }
    public String getCREATETIME() {
        return CREATETIME;
    }
    public void setCREATETIME(String cREATETIME) {
        CREATETIME = cREATETIME;
    }
    public String getORDERNUMBER() {
        return ORDERNUMBER;
    }
    public void setORDERNUMBER(String oRDERNUMBER) {
        ORDERNUMBER = oRDERNUMBER;
    }
    public String getDEPARTTIME() {
        return DEPARTTIME;
    }
    public void setDEPARTTIME(String dEPARTTIME) {
        DEPARTTIME = dEPARTTIME;
    }
    public Integer getINSURCOUNT() {
        return INSURCOUNT;
    }
    public void setINSURCOUNT(Integer iNSURCOUNT) {
        INSURCOUNT = iNSURCOUNT;
    }
    public Integer getAGENTID() {
        return AGENTID;
    }
    public void setAGENTID(Integer aGENTID) {
        AGENTID = aGENTID;
    }
    public Integer getPRINTSTATE() {
        return PRINTSTATE;
    }
    public void setPRINTSTATE(Integer pRINTSTATE) {
        PRINTSTATE = pRINTSTATE;
    }
    public Integer getAccountstate() {
        return accountstate;
    }
    public void setAccountstate(Integer accountstate) {
        this.accountstate = accountstate;
    }
    public String getExpressNum() {
        return ExpressNum;
    }
    public void setExpressNum(String expressNum) {
        ExpressNum = expressNum;
    }
    public String getExpressTime() {
        return ExpressTime;
    }
    public void setExpressTime(String expressTime) {
        ExpressTime = expressTime;
    }
    public Integer getExpressAgent() {
        return ExpressAgent;
    }
    public void setExpressAgent(Integer expressAgent) {
        ExpressAgent = expressAgent;
    }
    public Double getRapidSendPrice() {
        return rapidSendPrice;
    }
    public void setRapidSendPrice(Double rapidSendPrice) {
        this.rapidSendPrice = rapidSendPrice;
    }
    
    @Override
    public String toString() {
        return "MailAddress [ID=" + ID + ", MAILNAME=" + MAILNAME + ", MAILTEL=" + MAILTEL + ", POSTCODE=" + POSTCODE
                + ", ADDRESS=" + ADDRESS + ", PROVINCEID=" + PROVINCEID + ", COUNTRYID=" + COUNTRYID + ", CITYID="
                + CITYID + ", PROVINCENAME=" + PROVINCENAME + ", CITYNAME=" + CITYNAME + ", REGIONID=" + REGIONID
                + ", REGIONNAME=" + REGIONNAME + ", TOWNID=" + TOWNID + ", TOWNNAME=" + TOWNNAME + ", MAIL=" + MAIL
                + ", NOTE=" + NOTE + ", ORDERID=" + ORDERID + ", BUSSTYPE=" + BUSSTYPE + ", STATE=" + STATE
                + ", CREATETIME=" + CREATETIME + ", ORDERNUMBER=" + ORDERNUMBER + ", DEPARTTIME=" + DEPARTTIME
                + ", INSURCOUNT=" + INSURCOUNT + ", AGENTID=" + AGENTID + ", PRINTSTATE=" + PRINTSTATE
                + ", accountstate=" + accountstate + ", ExpressNum=" + ExpressNum + ", ExpressTime=" + ExpressTime
                + ", ExpressAgent=" + ExpressAgent + ", rapidSendPrice=" + rapidSendPrice + "]";
    }
    
}
