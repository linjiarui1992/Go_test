/**
 * 版权所有;//空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineExpRec
 * @description: TODO - 代售点相关信息维护表
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月5日 上午10:53:28 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflinePrice {
    private Integer Id;//
    private double AgentId;//
    private double Express1;//省内快递费
    private double Express2;//省外快递费
    private double shouxuPrice;//手续费 - 出票服务费
    private String Alternative1;//所属省会 - 【？？？】 - 【最后用于结算时匹配省外或者省内】
    private String Alternative2;//营业时间 - 【？？？】 - 显示上下班时间 - 但是并不是维护在此处
    private String Alternative3;//是否支持电子票 - 【？？？】 - 0-不支持-否
    private float Express3;//配送到站 快递费 - 送站服务费 快递费 2016年10月25日17:27:19 chendong增加
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public double getAgentId() {
		return AgentId;
	}
	public void setAgentId(double agentId) {
		AgentId = agentId;
	}
	public double getExpress1() {
		return Express1;
	}
	public void setExpress1(double express1) {
		Express1 = express1;
	}
	public double getExpress2() {
		return Express2;
	}
	public void setExpress2(double express2) {
		Express2 = express2;
	}
	public double getShouxuPrice() {
		return shouxuPrice;
	}
	public void setShouxuPrice(double shouxuPrice) {
		this.shouxuPrice = shouxuPrice;
	}
	public String getAlternative1() {
		return Alternative1;
	}
	public void setAlternative1(String alternative1) {
		Alternative1 = alternative1;
	}
	public String getAlternative2() {
		return Alternative2;
	}
	public void setAlternative2(String alternative2) {
		Alternative2 = alternative2;
	}
	public String getAlternative3() {
		return Alternative3;
	}
	public void setAlternative3(String alternative3) {
		Alternative3 = alternative3;
	}
	public float getExpress3() {
		return Express3;
	}
	public void setExpress3(float express3) {
		Express3 = express3;
	}
	@Override
	public String toString() {
		return "TrainOfflinePrice [Id=" + Id + ", AgentId=" + AgentId + ", Express1=" + Express1 + ", Express2="
				+ Express2 + ", shouxuPrice=" + shouxuPrice + ", Alternative1=" + Alternative1 + ", Alternative2="
				+ Alternative2 + ", Alternative3=" + Alternative3 + ", Express3=" + Express3 + "]";
	}
    
}
