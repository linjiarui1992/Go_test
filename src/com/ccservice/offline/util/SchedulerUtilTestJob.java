/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.util;

import java.util.Random;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.offline.dao.TrainOrderOfflineDao;

/**
 * @className: com.ccservice.offline.util.SchedulerUtilTestJob
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年11月9日 下午4:31:51 
 * @version: v 1.0
 * @since 
 *
 */
public class SchedulerUtilTestJob implements Job {
    private static final String LOGNAME = "定时任务测试类";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    public void execute(JobExecutionContext context) throws JobExecutionException {
        //锁单超时，未完成出票动作 - 此处根据状态的判定，完成自动取消锁定
        String jobName = context.getJobDetail().getName();

        JobDataMap dataMap = context.getJobDetail().getJobDataMap();

        Long OrderId = dataMap.getLong("OrderId");
        Integer useridi = dataMap.getInt("useridi");
        String LockRapidSendWait = dataMap.getString("LockWait");
        
        System.out.println(TrainOrderOfflineUtil.getNowDateStr()+ r1 + ":"+LOGNAME+"-进入到自动取消闪送单的方法-OrderId-->"+OrderId+"-useridi-->"+useridi+"-LockRapidSendWait-->"+LockRapidSendWait);
    }
}
