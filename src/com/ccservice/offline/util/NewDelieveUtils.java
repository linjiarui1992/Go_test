package com.ccservice.offline.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.offlineExpress.util.RequestUtil;

public class NewDelieveUtils {

    private final String logName = "线下火车票_顺丰破解获取快递时效_平台进单";

    private int random = new Random().nextInt(9000000) + 1000000;

    public static void main(String[] args) {
        //        DelieveUtils utils = new DelieveUtils();
        //        System.out.println(utils.getDelieveStr("1", "499", "湖南-株洲-荷塘区-托里县环城宾馆对面清新水果店", 81));
    }

    /**
     * 根据快递代理商传入，获取快递时效。
     * 
     * @param expressAgent 快递代理商
     * @param agengId 代售点ID
     * @param address 邮寄地址
     * @param CreateUId 采购商
     * @return
     * @time 2017年11月10日 下午4:08:29
     * @author liujun
     */
    public String getDelieveStr(String expressAgent, String agengId, String address, int CreateUId) {
        String result = "";
        // UU跑腿
        if ("10".equals(expressAgent)) {
            result = getUUptDelieveStr(random, agengId, address);
        }
        else {
            JSONObject jsonObject = getDelieveTime(expressAgent, agengId, address, CreateUId);
            String realTime = jsonObject.getString("realTime");// 真实发车时间
            String deliverType = jsonObject.getString("deliverType");// 真实发车时间
            String deliverTime = jsonObject.getString("deliverTime");// 预计到达时间
            // EMS
            if ("1".equals(expressAgent)) {
                if (deliverTime == null || "".equals(deliverTime) || "该地址不在配送范围！！！".equals(deliverTime)) {
                    result = "【EMS快递】<br/>获取快递时效失败.";
                }
                else {
                    int sectionType = jsonObject.getIntValue("sectionType"); // 邮寄区间
                    // 同城
                    if (sectionType == 1) {
                        deliverTime = emsDeliverTimeAdd(deliverTime, "0.5");
                        WriteLog.write(logName, random + "EMS同城加时0.5天：" + deliverTime);
                    }
                    // 同省，或者北京，天津，上海
                    if (sectionType == 2) {
                        deliverTime = emsDeliverTimeAdd(deliverTime, "1");
                        WriteLog.write(logName, random + "EMS同省加时1天：" + deliverTime);
                    }
                    // 跨省+1.5天
                    if (sectionType == 3) {
                        deliverTime = emsDeliverTimeAdd(deliverTime, "1.5");
                        WriteLog.write(logName, random + "EMS跨省加时1.5天：" + deliverTime);
                    }
                    // 偏远省跨省+2天
                    if (sectionType == 4) {
                        deliverTime = emsDeliverTimeAdd(deliverTime, "2");
                        WriteLog.write(logName, random + "EMS偏远跨省加时2天：" + deliverTime);
                    }
                    else {
                        deliverTime = emsDeliverTimeAdd(deliverTime, "2");
                        WriteLog.write(logName, random + "EMS区间未匹配默认加时2天：" + deliverTime);
                    }
                    result = "【EMS快递】<br/>如果" + realTime + "正常发件。快递类型为:标准快递。快递预计到达时间:" + deliverTime
                            + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                }
                // 顺丰
            }
            else {
                if (deliverTime == null || "".equals(deliverTime)) {
                    result = "【顺丰快递】<br/>获取快递时效失败.";
                }
                else if ("该地址不在配送范围！！！".equals(deliverTime)) {

                    result = "【顺丰快递】<br/>" + deliverTime;
                }
                else {
                    // 平台加时
                    result = "【顺丰快递】<br/>如果" + realTime + "正常发件。快递类型为:" + deliverType + "。快递预计到达时间:" + deliverTime
                            + ":00" + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                }
            }
        }
        WriteLog.write(logName, random + "快递时效：" + result);
        return result;
    }

    /**
     * 获取UU跑腿快递时效
     * 
     * @param agengId 代售点ID
     * @param address 邮寄地址
     * @return
     * @time 2017年11月10日 上午11:31:06
     * @author liujun
     */
    private String getUUptDelieveStr(int random, String agentId, String address) {
        String delieveStr = "";
        String deliverTime = "";
        // 当前日期
        String isOnlineDate = TimeUtil.gettodaydate(1);
        String sql = "SELECT isOnline FROM T_AGENTISONLINETIME with(nolock) WHERE agentId = " + agentId
                + " AND isOnlineDate = '" + isOnlineDate + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        WriteLog.write(logName, "代售点上班信息：" + list);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            String isOnline = String.valueOf(map.get("isOnline"));
            String realTime = TimeUtil.gettodaydate(4);
            if (isOnline != null && !"".equals(isOnline)) {
                // 代售点为上班状态时，反馈的预计到达时间为当前下单时间+3小时
                if ("1".equals(isOnline)) {
                    deliverTime = addHour(realTime, 3);
                }
                // 当代售点为非上班状态时，反馈的预计到达时间为当前下单时间的第二天8点+3小时
                else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd");
                    Calendar date = Calendar.getInstance();
                    date.set(Calendar.DATE, date.get(Calendar.DATE) + 1);
                    realTime = sdf.format(date.getTime()) + " 08:00:00";
                    deliverTime = sdf.format(date.getTime()) + " 11:00:00";
                }
            }
            WriteLog.write(logName, "deliverTime：" + deliverTime + "realTime：" + realTime);
            delieveStr += "【UU跑腿】<br/>如果" + realTime + "正常发件。快递预计到达时间:" + deliverTime
                    + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
        }
        else {
            delieveStr = "获取快递时效失败";
        }
        if (deliverTime != null && !"".equals(deliverTime)) {
            deliverTime = deliverTime.split(",")[0];
        }
        return delieveStr;
    }

    /**
     * 获取快递时效
     * 
     */
    private JSONObject getDelieveTime(String expressAgent, String agengId, String toAddress, int CreateUId) {
        JSONObject result = new JSONObject();
        WriteLog.write(logName, random + "代售点ID：" + agengId + "--地址：" + toAddress);
        String time1 = "";
        String time2 = "";
        String fromAddress = "";
        // 根据采购商ID获取代购点地址以及发快递时间
        String sql = "SELECT c.C_AGENTADDRESS,t.time1,t.time2,t.time3,t.time4 from T_CUSTOMERAGENT c with(nolock) INNER JOIN TrainOrderAgentTimes t with(nolock) ON t.agentId = c.ID where c.ID="
                + agengId;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        WriteLog.write(logName, random + "--代售点信息结果--" + list);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            fromAddress = String.valueOf(map.get("C_AGENTADDRESS"));
            if ("0".equals(expressAgent)) {
                time1 = String.valueOf(map.get("time1"));
                time2 = String.valueOf(map.get("time2"));
            }
            else if ("1".equals(expressAgent)) {
                time1 = String.valueOf(map.get("time3"));
                time2 = String.valueOf(map.get("time4"));
            }
        }
        else {
            if ("0".equals(expressAgent)) {
                time1 = "10:00:00";
                time2 = "18:00:00";
            }
            else if ("1".equals(expressAgent)) {
                time1 = "11:00:00";
                time2 = "17:00:00";
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        // 获取真实的发快递时间
        String time = getRealTimes(dates, time1, time2);
        String reqUrl = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        //请求方法
        String method = "";
        //参数拼接
        JSONObject data = new JSONObject();
        boolean isAddTime = false;
        if (CreateUId == 56 || CreateUId == 81) {
            // 同城和淘宝拆分地址
            String[] address = toAddress.split("-");
            if (address.length > 3) {
                method = "getExpressDeliverTm";
                String toProvince = address[0];
                String toCity = address[1];
                String toCounty = address[2];
                data.put("toProvince", toProvince);
                // 地址转换
                String translationSql = "SELECT Translation_After FROM address_translation with(nolock) WHERE CreateUid = "
                        + CreateUId + " AND Translation_Pre = '" + toCity + "'";
                List translationList = Server.getInstance().getSystemService().findMapResultBySql(translationSql, null);
                WriteLog.write(logName, random + "地址转换查询结果：" + translationList);
                if (translationList.size() > 0) {
                    Map map = (Map) translationList.get(0);
                    toCity = String.valueOf(map.get("Translation_After"));
                }
                data.put("toCity", toCity);
                data.put("toCounty", toCounty);
                if (isCountyLevelCity(toCounty)) {
                    isAddTime = true;
                }
            }
            else {
                method = "getExpressDeliverTmByTotalAddress";
                data.put("toAddress", toAddress);
            }
        }
        else {
            if (CreateUId == 80) {
                toAddress = toAddress.split("#")[0];
            }
            method = "getExpressDeliverTmByTotalAddress";
            data.put("toAddress", toAddress);
        }
        data.put("formAddress", fromAddress);
        data.put("time", time);

        JSONObject requestParam = new JSONObject();
        requestParam.put("data", data);
        requestParam.put("method", method);
        requestParam.put("reqtime", time);
        requestParam.put("sign", MD5Util.MD5(method + time, "utf-8"));
        //请求参数
        JSONObject req = new JSONObject();
        req.put("expressName", "sf");
        req.put("requestParam", requestParam);
        //请求
        WriteLog.write(logName, random + "--请求--" + req);
        String reqResult = RequestUtil.post(reqUrl, req.toJSONString(), "utf-8", new HashMap<String, String>(), 60000);
        //请求结果
        WriteLog.write(logName, random + "--请求结果--" + reqResult);
        if (reqResult != null) {
            JSONObject reqJSONObject = JSONObject.parseObject(reqResult);
            result = JSONObject.parseObject(reqJSONObject.getString("result"));
        }

        // 判断区间
        int sectionType = getSectionType(fromAddress, toAddress);
        WriteLog.write(logName, random + "--城市区间判断结果--" + sectionType);
        result.put("sectionType", sectionType);
        result.put("isAddTime", isAddTime);
        result.put("realTime", time);
        return result;
    }

    /**
     * 判断县级市
     * 
     * @param address
     * @return
     * @time 2017年12月15日 下午5:04:48
     * @author liujun
     */
    private boolean isCountyLevelCity(String address) {
        boolean result = false;
        // 三级地址为县/县级市
        if (address.contains("市") || address.contains("县")) {
            result = true;
        }
        return result;
    }

    /**
     * 判断县级市
     * 
     * @param address
     * @return
     * @time 2017年12月15日 下午5:04:48
     * @author liujun
     */
    private int getSectionType(String fromAddress, String toAddress) {
        int result = 0;
        String fromProvince = fromAddress.split("-")[0];
        String fromCity = fromAddress.split("-")[1];
        // 包含城市，同城
        if (toAddress.contains(fromCity)) {
            result = 1;
        }
        else if (toAddress.contains(fromProvince)) {
            result = 2;
        }
        else {
            // 跨省
            String trans_provincial = PropertyUtil.getValue("trans_provincial", "train.properties");
            String[] trans_provincials = trans_provincial.split("/");
            for (int i = 0; i < trans_provincials.length; i++) {
                if (toAddress.contains(trans_provincials[i])) {
                    result = 3;
                    break;
                }
            }
            // 偏远省跨省
            if (result == 0) {
                String remote_trans_provincial = PropertyUtil.getValue("remote_trans_provincial", "train.properties");
                String[] remote_trans_provincials = remote_trans_provincial.split("/");
                for (int i = 0; i < remote_trans_provincials.length; i++) {
                    if (toAddress.contains(remote_trans_provincials[i])) {
                        result = 4;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    private String getRealTimes(String dates, String time1, String time2) {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates = sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if (date0.before(date1)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date1));
            }
            else if (date0.after(date1) && date0.before(date2)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date2));
            }
            else if (date0.after(date2)) {
                Date ds = getDate(new Date());
                String nextd = sdf1.format(ds);
                result = (nextd.substring(0, 10) + " " + sdf.format(date1));
            }
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        return result;
    }

    private Date getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

    /**
     * EMS加时
     * 
     * @param date
     * @param addDays 添加天数
     * @return
     * @time 2017年11月10日 下午1:44:35
     * @author liujun
     */
    private String emsDeliverTimeAdd(String deliverTime, String days) {
        String result = "";
        deliverTime = deliverTime + ":00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(deliverTime);
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        if (date == null) {
            WriteLog.write(logName, random + "EMS加时失败" + deliverTime);
            return deliverTime;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if ("0.5".equals(days)) {
            if (cal.get(Calendar.HOUR_OF_DAY) >= 18) {
                deliverTime = addHour(deliverTime, 18);
            }
            else {
                deliverTime = addHour(deliverTime, 6);
            }
        }
        else if ("1".equals(days)) {
            deliverTime = addHour(deliverTime, 24);
        }
        else if ("1.5".equals(days)) {
            if (cal.get(Calendar.HOUR_OF_DAY) >= 18) {
                deliverTime = addHour(deliverTime, 42);
            }
            else {
                deliverTime = addHour(deliverTime, 30);
            }
        }
        else {
            deliverTime = addHour(deliverTime, 48);
        }
        result = deliverTime;
        return result;
    }

    /**
     * 添加小时
     * 
     * @param date
     * @param addDays 添加天数
     * @return
     * @time 2017年11月10日 下午1:44:35
     * @author liujun
     */
    private String addHour(String deliverTime, int hours) {
        String result = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(deliverTime);
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        if (date == null) {
            WriteLog.write(logName, random + "加时失败" + deliverTime);
            return deliverTime;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, hours);// 24小时制
        date = cal.getTime();
        // 更新后的日期 
        result = format.format(date);
        return result;
    }
}
