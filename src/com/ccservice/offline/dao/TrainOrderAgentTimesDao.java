/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOrderAgentTimes;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOfflineExpressCodeDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月28日 下午4:48:32 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderAgentTimesDao {
    //根据ID获取订单对象
    public TrainOrderAgentTimes findTrainOrderAgentTimesByAgentId(String agentId) throws Exception {
        String sql = "SELECT * FROM TrainOrderAgentTimes WHERE agentId='" + agentId +"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOrderAgentTimes) new BeanHanlder(TrainOrderAgentTimes.class).handle(dataTable);
    }

}
