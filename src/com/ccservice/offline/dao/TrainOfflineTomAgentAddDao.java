/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.util.ArrayList;
import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;

/**
 * @className: com.ccservice.offline.dao.TrainOfflineTomAgentAddDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月4日 下午6:23:17 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineTomAgentAddDao {

    //配送到站的分单逻辑
    
    //获取 到所有可以配送到站的站点的信息

    public List<String> findTrainStationList() throws Exception {
        String sql = "SELECT trainStation FROM TrainOfflineTomAgentAdd";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        
        List<String> trainStationList = new ArrayList<String>();
        for (DataRow dataRow : dataTable.GetRow()) {
            trainStationList.add(dataRow.GetColumnString("trainStation"));
        }
        return trainStationList;
    }

    public Integer findAgentIdByTrainStation(String fromStationName) throws Exception {
        String sql = "SELECT agentId FROM TrainOfflineTomAgentAdd WHERE trainStation='" + fromStationName + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("agentId").GetValue()));
    }

    public String findDeliveryAddressByTrainStation(String fromStationName) throws Exception {
        String sql = "SELECT deliveryAddress FROM TrainOfflineTomAgentAdd WHERE trainStation='" + fromStationName + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("deliveryAddress").GetValue());
    }

}
