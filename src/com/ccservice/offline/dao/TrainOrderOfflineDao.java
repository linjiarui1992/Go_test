/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.huamin.WriteLog;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:08:02 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineDao {
    //插入线下火车票订单信息，并返回订单号
    public String addTrainOrderOffline(TrainOrderOffline trainOrderOffline) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        //超时时间是从数据库中配置的？
        String sql = "OFFLINE_addTrainOrderOffline @OrderNumber='" + trainOrderOffline.getOrderNumber()
                + "',@CreateTime='" + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@AgentId="
                + trainOrderOffline.getAgentId() + ",@CreateUId=" + trainOrderOffline.getCreateUId() + ",@CreateUser='"
                + trainOrderOffline.getCreateUser() + "',@ContactUser='" + trainOrderOffline.getContactUser()
                + "',@ContactTel='" + trainOrderOffline.getContactTel() + "',@OrderPrice="
                + trainOrderOffline.getOrderPrice() + ",@OrderStatus=" + trainOrderOffline.getOrderStatus()
                + ",@TradeNo='" + trainOrderOffline.getTradeNo()
                //+"',@AgentProfit=" + trainOrderOffline.getAgentProfit()
                //+",@SupplyPayWay=" + trainOrderOffline.getSupplyPayWay() 
                + "',@TicketCount=" + trainOrderOffline.getTicketCount()
                //+",@RefundReason=" + trainOrderOffline.getRefundReason()
                //+",@RefundReasonStr='" + trainOrderOffline.getRefundReasonStr() 
                + ",@OrderTimeout='" + TrainOrderOfflineUtil.getTimestampByStr("1900-01-01 00:00:00.000")
                //+"',@ChuPiaoAgentid=" + trainOrderOffline.getChuPiaoAgentid()
                //+",@ChuPiaoTime='" + TrainOrderOfflineUtil.getTimestampByStr(trainOrderOffline.getChuPiaoTime())
                //+",@Remark='" + trainOrderOffline.getRemark() 
                + "',@OrderNumberOnline='" + trainOrderOffline.getOrderNumberOnline()
                //+"',@paystatus=" + trainOrderOffline.getPaystatus() 
                + "',@PaperType=" + trainOrderOffline.getPaperType() + ",@PaperBackup="
                + trainOrderOffline.getPaperBackup() + ",@paperLowSeatCount=" + trainOrderOffline.getPaperLowSeatCount()
                + ",@extSeat='" + trainOrderOffline.getExtSeat() + "',@lockedStatus="
                + trainOrderOffline.getLockedStatus();
        //+",@operateid=" + trainOrderOffline.getOperateid() 
        //+",@operatetime=" + trainOrderOffline.getOperatetime() 
        //+",@operatetime=" + null
        //+",@refusereason=" + trainOrderOffline.getRefusereason()
        //+",@refusereasonstr='" + trainOrderOffline.getRefusereasonstr() 
        //+"',@expressDeliver='" + trainOrderOffline.getExpressDeliver() 
        //+"',@telephone='" + trainOrderOffline.getTelephone()
        //+"',@questionDraw=" + trainOrderOffline.getQuestionDraw() 
        //+",@questionMail=" + trainOrderOffline.getQuestionMail() 
        //+",@isDelivery=" + trainOrderOffline.getIsDelivery()
        //+",@offlineOrderType=" + trainOrderOffline.getOfflineOrderType() 
        //+",@needDeliveryTime='" + trainOrderOffline.getNeedDeliveryTime() 
        //+",@needDeliveryTime=" + null
        //+",@QuestionOrder='" + trainOrderOffline.getQuestionOrder()+"'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "线下火车票订单信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    public String addTrainOrderOfflineTC(TrainOrderOffline trainOrderOffline) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "OFFLINETC_addTrainOrderOfflineTC @OrderNumber='" + trainOrderOffline.getOrderNumber()
                + "',@CreateTime='" + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@AgentId="
                + trainOrderOffline.getAgentId() + ",@CreateUId=" + trainOrderOffline.getCreateUId() + ",@CreateUser='"
                + trainOrderOffline.getCreateUser() + "',@ContactUser='" + trainOrderOffline.getContactUser()
                + "',@ContactTel='" + trainOrderOffline.getContactTel() + "',@OrderPrice="
                + trainOrderOffline.getOrderPrice() + ",@OrderStatus=" + trainOrderOffline.getOrderStatus()
                + ",@TradeNo='" + trainOrderOffline.getTradeNo() + "',@TicketCount="
                + trainOrderOffline.getTicketCount() + ",@OrderTimeout='"
                + TrainOrderOfflineUtil.getTimestampByStr("1900-01-01 00:00:00.000") + "',@OrderNumberOnline='"
                + trainOrderOffline.getOrderNumberOnline() + "',@PaperType=" + trainOrderOffline.getPaperType()
                + ",@PaperBackup=" + trainOrderOffline.getPaperBackup() + ",@paperLowSeatCount="
                + trainOrderOffline.getPaperLowSeatCount() + ",@extSeat='" + trainOrderOffline.getExtSeat()
                + "',@lockedStatus=" + trainOrderOffline.getLockedStatus() + ",@isDelivery="
                + trainOrderOffline.getIsDelivery() + ",@isGrab=" + trainOrderOffline.getIsGrab();
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "线下火车票订单信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    public String addTrainOrderOfflineTCRapidSend(TrainOrderOffline trainOrderOffline) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "OFFLINETC_addTrainOrderOfflineTCRapidSend @OrderNumber='" + trainOrderOffline.getOrderNumber()
                + "',@CreateTime='" + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',@AgentId="
                + trainOrderOffline.getAgentId() + ",@CreateUId=" + trainOrderOffline.getCreateUId() + ",@CreateUser='"
                + trainOrderOffline.getCreateUser() + "',@ContactUser='" + trainOrderOffline.getContactUser()
                + "',@ContactTel='" + trainOrderOffline.getContactTel() + "',@OrderPrice="
                + trainOrderOffline.getOrderPrice() + ",@OrderStatus=" + trainOrderOffline.getOrderStatus()
                + ",@TradeNo='" + trainOrderOffline.getTradeNo() + "',@TicketCount="
                + trainOrderOffline.getTicketCount() + ",@OrderTimeout='"
                + TrainOrderOfflineUtil.getTimestampByStr("1900-01-01 00:00:00.000") + "',@OrderNumberOnline='"
                + trainOrderOffline.getOrderNumberOnline() + "',@PaperType=" + trainOrderOffline.getPaperType()
                + ",@PaperBackup=" + trainOrderOffline.getPaperBackup() + ",@paperLowSeatCount="
                + trainOrderOffline.getPaperLowSeatCount() + ",@extSeat='" + trainOrderOffline.getExtSeat()
                + "',@lockedStatus=" + trainOrderOffline.getLockedStatus() + ",@isDelivery="
                + trainOrderOffline.getIsDelivery() + ",@isGrab=" + trainOrderOffline.getIsGrab() + ",@isRapidSend="
                + trainOrderOffline.getIsRapidSend();

        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return "线下火车票订单信息插入失败";
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
    }

    //根据ID获取订单对象
    public TrainOrderOffline findTrainOrderOfflineById(Long Id) throws Exception {
        String sql = "OFFLINE_findTrainOrderOfflineById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOrderOffline) new BeanHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public TrainOrderOffline findTrainOrderOfflineByOrderNumberOnline(String OrderNumberOnline) throws Exception {
        String sql = "OFFLINE_findTrainOrderOfflineByOrderNumberOnline @OrderNumberOnline='" + OrderNumberOnline + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOrderOffline) new BeanHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public TrainOrderOffline findTrainOrderOfflineByOrderNumber(String OrderNumber) throws Exception {
        String sql = "SELECT * FROM TrainOrderOffline WHERE OrderNumber='" + OrderNumber + "'";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainOrderOffline) new BeanHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public List<TrainOrderOffline> findTrainOrderOfflineLockTicketByAgentId(Integer useridi) throws Exception {
        String sql = "SELECT * FROM TrainOrderOffline WITH(NOLOCK) WHERE AgentId=" + useridi
                + " AND CreateUId IN (56,80,81) AND OrderStatus=1 AND lockedStatus!=0";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainOrderOffline>) new BeanListHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public List<TrainOrderOffline> findTrainOrderOfflineLockTicketTNByAgentId(Integer useridi) throws Exception {
        String sql = "SELECT * FROM TrainOrderOffline WHERE AgentId=" + useridi
                + " AND CreateUId = 80 AND OrderStatus=1 AND lockedStatus!=0";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainOrderOffline>) new BeanListHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public List<TrainOrderOffline> findTrainOrderOfflineLockTicketTCByAgentId(Integer useridi) throws Exception {
        String sql = "SELECT * FROM TrainOrderOffline WHERE AgentId=" + useridi
                + " AND CreateUId = 81 AND OrderStatus=1 AND lockedStatus!=0";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainOrderOffline>) new BeanListHanlder(TrainOrderOffline.class).handle(dataTable);
    }

    public Boolean findIsLockCallbackById(Long Id) throws Exception {
        String sql = "OFFLINE_findIsLockCallbackById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Boolean.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("isLockCallback").GetValue()));
    }

    public Integer findLockedStatusById(Long Id) throws Exception {
        String sql = "OFFLINE_findLockedStatusById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return Integer.valueOf(String.valueOf(dataTable.GetRow().get(0).GetColumn("lockedStatus").GetValue()));
    }

    public String findCtripOrderInfoById(Long Id) throws Exception {
        String sql = "SELECT ctripOrderInfo FROM TrainOrderOffline WHERE Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("ctripOrderInfo").GetValue());
    }

    //修改，一般是修改指定值
    /*public Integer updateTrainOrderOfflineChuPiaoTimeById(TrainOrderOffline trainOrderOffline) {
        String sql = "OFFLINE_updateTrainOrderOfflineChuPiaoTimeById @ChuPiaoTime='" + trainOrderOffline.getNeedDeliveryTime() + "'";
        return DBHelper.UpdateData(sql);
    }*/

    //根据ID删除订单对象-本地测试
    public Integer delTrainOrderOfflineById(Long Id) {
        String sql = "DELETE FROM TrainOrderOffline WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    //根据ID锁票
    public Integer updateTrainOrderOfflinelockedStatusById(Long Id, Integer lockedStatus) {
        String sql = "OFFLINE_updateTrainOrderOfflinelockedStatusById @Id=" + Id + ",@lockedStatus=" + lockedStatus;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineOrderStatusById(Long Id, Integer OrderStatus) {
        String sql = "OFFLINE_updateTrainOrderOfflineOrderStatusById @Id=" + Id + ",@OrderStatus=" + OrderStatus;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer calcelTrainOrderOfflineOrder(Long Id) {
        String sql = "UPDATE TrainOrderOffline SET AgentId=2, OrderStatus=3 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateOrderTimeoutById(Long Id, String OrderTimeout) throws Exception {//2017-08-21 14:49:00
        String sql = "UPDATE TrainOrderOffline SET OrderTimeout ='"
                + TrainOrderOfflineUtil.getTimestampByStr(OrderTimeout + ".000") + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineSuccessById(Long Id, Integer ChuPiaoAgentid) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "UPDATE TrainOrderOffline SET ChuPiaoAgentid = " + ChuPiaoAgentid + ",ChuPiaoTime ='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineStatusQuestionById(Long Id, Integer OrderStatus, Integer questionDraw) {
        String sql = "UPDATE TrainOrderOffline SET OrderStatus=" + OrderStatus + ",questionDraw=" + questionDraw
                + " WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineFailById(Long Id, Integer ChuPiaoAgentid, String refundReason,
            String refundreasonstr) throws Exception {
        String nowDateStr = TrainOrderOfflineUtil.getNowDateStr();
        String sql = "UPDATE TrainOrderOffline SET operateid = " + ChuPiaoAgentid + ",operatetime ='"
                + TrainOrderOfflineUtil.getTimestampByStr(nowDateStr) + "',refusereason=" + refundReason
                + ",refusereasonstr='" + refundreasonstr + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineexpressDeliverById(Long Id, String expressDeliver) {
        String sql = "UPDATE TrainOrderOffline SET expressDeliver = '" + expressDeliver + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineisLockCallbackById(Long Id) {
        String sql = "UPDATE TrainOrderOffline set isLockCallback=1 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineisRapidSendCallbackById(Long Id) {
        String sql = "UPDATE TrainOrderOffline set isRapidSendCallback=1 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineisRapidSendSuccessById(Long Id) {
        String sql = "UPDATE TrainOrderOffline set isRapidSendSuccess=1 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineisRapidSendSuccessCallbackById(Long Id) {
        String sql = "UPDATE TrainOrderOffline set isRapidSendSuccess=0,isRapidSendCallback=0 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineLockTimeById(Long Id) throws Exception {//20170919-新增需求 - 锁单时间
        String sql = "UPDATE TrainOrderOffline set lockTime='"
                + TrainOrderOfflineUtil.getTimestampByStr(TrainOrderOfflineUtil.getNowDateStr()) + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    /**
     * 查询车次信息
     * 
     * @param orderId
     * @return
     * @throws Exception
     * @time 2017年9月25日 下午6:41:34
     * @author liujun
     */
    public TrainTicketOffline getTrainTicketOffline(String orderId) throws Exception {
        String sql = "select * from TrainTicketOffline where OrderId='" + orderId + "'";
        WriteLog.write("线上线下抢票接口对接", "------查询车次信息------" + sql);
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainTicketOffline) new BeanHanlder(TrainTicketOffline.class).handle(dataTable);
    }

    /**
     * 查询客人信息
     * 
     * @param orderId
     * @return
     * @throws Exception
     * @time 2017年9月25日 下午6:41:34
     * @author liujun
     */
    public List<TrainPassengerOffline> getTrainPassengerOffline(String orderId) throws Exception {
        String sql = "select * from TrainPassengerOffline  where OrderId='" + orderId + "'";
        WriteLog.write("线上线下抢票接口对接", "------查询客人信息------" + sql);
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainPassengerOffline>) new BeanListHanlder(TrainPassengerOffline.class).handle(dataTable);
    }

    /**
     * 查询客人邮寄信息
     * 
     * @param orderId
     * @return
     * @throws Exception
     * @time 2017年9月25日 下午6:41:34
     * @author liujun
     */
    public MailAddress getMailAddress(String orderId) throws Exception {
        String sql = "select * from mailaddress where OrderId='" + orderId + "'";
        WriteLog.write("线上线下抢票接口对接", "------查询客人邮寄信息------" + sql);
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (MailAddress) new BeanHanlder(MailAddress.class).handle(dataTable);
    }

    /**
     * 更新抢票状态
     * 
     * @param orderNumber
     * @param orderStatus
     * @return
     * @throws Exception
     * @time 2017年9月25日 下午6:41:34
     * @author liujun
     */
    public int updateTrainOrderOfflineOrderStatus(String orderNumber, int orderStatus) throws Exception {
        String sql = "update TrainOrderOffline set OrderStatus=" + orderStatus + " where OrderNumber='" + orderNumber
                + "'";
        WriteLog.write("线上线下抢票接口对接", "------更新抢票状态------" + sql);
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateTrainOrderOfflineTradeNoById(Long orderId, String TradeNo) {
        String sql = "UPDATE TrainOrderOffline SET TradeNo='" + TradeNo + "' WHERE Id=" + orderId;
        return DBHelperOffline.UpdateData(sql);
    }

    /**
     * @description: TODO - 
     * 
     * 更新并还原相关性的初始化标志位
     * OrderStatus - 1;//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理 - 
     * lockedStatus - 0;//0表示未被锁定，1表示被锁定 - 2-锁单失败 3-锁单中 - 4-锁单超时 -【途牛】 状态5-闪送单的锁单超时 - 6-客服的闪送单的二次下单锁单操作
     * 
     * isLockCallback - false
     * 
     * lockTime - 1905-03-14 00:00:00.000
     * 
     * isRapidSendSuccess - false
     * isRapidSendCallback - false
     * 
     * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
     * @createTime: 2017年11月4日 下午6:37:39
     * @param orderId
     * @param agentId 
     * @return
     * @throws Exception 
     */
    public Integer initSecondOrder(Long orderId, Integer AgentId, Integer isRapidSend) throws Exception {
        String sql = "UPDATE TrainOrderOffline SET AgentId = " + AgentId
                + ",OrderStatus=1,lockedStatus=0,isLockCallback=0," + "lockTime='"
                + TrainOrderOfflineUtil.getTimestampByStr("1905-03-14 00:00:00.000") + "'," + "isRapidSend="
                + isRapidSend + ",isRapidSendSuccess=0,isRapidSendCallback=0,isSecondOrder=1 WHERE Id=" + orderId;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateIsSecondDateCallbackByOrderId(Long orderId, Integer isSecondDateCallback) {
        String sql = "UPDATE TrainOrderOffline set isSecondDateCallback=" + isSecondDateCallback + " WHERE Id="
                + orderId;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateIsRapidSendAndCallbackExpressDeliverById(Long Id, Integer isRapidSend, String expressDeliver) {
        String sql = "UPDATE TrainOrderOffline SET isRapidSend = " + isRapidSend + ",expressDeliver = '"
                + expressDeliver + "',isRapidSendCallback=0 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateIsRapidSendExpressDeliverById(Long Id, Integer isRapidSend, String expressDeliver) {
        String sql = "UPDATE TrainOrderOffline SET isRapidSend = " + isRapidSend + ",expressDeliver = '"
                + expressDeliver + "' WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateAgentIdById(Long Id, Integer AgentId) {
        String sql = "UPDATE TrainOrderOffline SET AgentId=" + AgentId + " WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

    public Integer updateAgentIdIsRapidSendAndCallbackExpressDeliverById(Long Id, Integer AgentId, Integer isRapidSend,
            String expressDeliver) {
        String sql = "UPDATE TrainOrderOffline SET AgentId=" + AgentId + ", isRapidSend = " + isRapidSend
                + ", expressDeliver = '" + expressDeliver + "',isRapidSendCallback=0 WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }

}
