package com.ccservice.offline.dao;

import java.util.List;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainPassengerOffline;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineRecordDao
 * @description: TODO -
 * @author: 郑州-技术-陈亚峰
 * @createTime: 2017年8月16日 下午03:36:02
 * @version: v 1.0
 * @since
 *
 */
public class TrainPassengerOfflineDao {
	//插入
	public String addTrainPassengerOffline(TrainPassengerOffline trainPassengerOffline) throws Exception {
		String sql = "OFFLINE_addTrainPassengerOffline @OrderId=" + trainPassengerOffline.getOrderId()
                +",@Name='" + trainPassengerOffline.getName() 
                +"',@IdType=" + trainPassengerOffline.getIdType()
                +",@IdNumber='" + trainPassengerOffline.getIdNumber()
                +"',@PassengerId='" + trainPassengerOffline.getPassengerId()+"'";

		DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
		if (dataTable.GetRow().size() == 0) {
			return "插入失败";
		}
		return String.valueOf(dataTable.GetRow().get(0).GetColumn("Id").GetValue());
	}
	
    //根据ID获取订单对象
    public TrainPassengerOffline findTrainTicketOfflineById(Long Id) throws Exception {
        String sql = "OFFLINE_findTrainPassengerOfflineById @Id=" + Id;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);

        //SQLServerUtil.printTable(dataTable);
        
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (TrainPassengerOffline) new BeanHanlder(TrainPassengerOffline.class).handle(dataTable);
    }

    public List<TrainPassengerOffline> findTrainPassengerOfflineListByOrderId(Long OrderId) throws Exception {
        String sql = "OFFLINE_findTrainPassengerOfflineListByOrderId @OrderId=" + OrderId;
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainPassengerOffline>) new BeanListHanlder(TrainPassengerOffline.class).handle(dataTable);
    }

    //根据ID删除订单对象-本地测试
    public Integer delTrainTicketOfflineById(Long Id) throws Exception {
        String sql = "DELETE FROM TrainPassengerOffline WHERE Id=" + Id;
        return DBHelperOffline.UpdateData(sql);
    }
	
}

