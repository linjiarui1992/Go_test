package com.ccservice.offline.dao;

import com.ccservice.offline.domain.MailAddress;

public class MailAddressDaoTest {
    private MailAddressDao mailAddressDao = new MailAddressDao();

    public void testAddMailAddress() throws Exception {
        //订单信息入库
        MailAddress mailAddress = new MailAddress();

        /*mailAddress.setMailName("123");
        mailAddress.setMailTel("123");
        mailAddress.setPostCode("123");
        mailAddress.setAddress("123");
        mailAddress.setOrderId(123);*/
        mailAddress.setExpressNum("123456789012");

        //        System.out.println(mailAddressDao.addMailAddress(mailAddress));
        System.out.println(mailAddressDao.delMailAddressById(1494L));
    }

    public void testFindMailAddressById() throws Exception {
        System.out.println(mailAddressDao.findMailAddressById(1017L));
    }

    public void testFindADDRESSCITYNAMEByORDERID() throws Exception {
        System.out.println(mailAddressDao.findADDRESSCITYNAMEByORDERID(1290447L));
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        MailAddressDaoTest mailAddressDaoTest = new MailAddressDaoTest();

        //mailAddressDaoTest.testAddMailAddress();
        mailAddressDaoTest.testFindADDRESSCITYNAMEByORDERID();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
