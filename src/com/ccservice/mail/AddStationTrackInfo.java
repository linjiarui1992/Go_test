package com.ccservice.mail;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

/**
 * 送票到站物流跟踪接口
 * */
public class AddStationTrackInfo extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(request, response);
		
		
		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        
        String orderNo = request.getParameter("orderNo");
        String deliveryTime = request.getParameter("deliveryTime");
        int deliveryStatus = Integer.valueOf(request.getParameter("deliveryStatus"));
        
        
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
//        responsejson.put("canDelivery", canDelivery);
//        responsejson.put("arriveTime", arriveTime);
//        responsejson.put("expressType", expressType);
//        responsejson.put("isSuccess", isSuccess);
//        responsejson.put("msgCode", msgCode);
//        responsejson.put("msgInfo", msgInfo);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
        
	}

	
}
