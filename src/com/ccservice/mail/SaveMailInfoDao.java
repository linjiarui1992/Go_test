package com.ccservice.mail;

import java.util.List;

import com.callback.SendPostandGet;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.dao.BeanHanlder;
import com.ccservice.offline.domain.MailAddress;


/**
 * 邮寄接口
 * */
public class SaveMailInfoDao {

	/**
	 * 邮寄接口
	 * @param orderId
	 * @return jsonStr
	 * */
	public String SaveMailInfo(Long orderId) {
		String jsonStr ="";
		String sql = "SELECT o.OrderNumber,m.expressAgent, ExpressNum from TrainOrderOffline O  JOIN mailaddress M on O.Id = m.ORDERID where o.id=" + orderId;
		MailAddress mailAddress = null;
		DataTable dataTable = null;
		try {
			dataTable = DBHelperOffline.GetDataTable(sql, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (dataTable != null && dataTable.GetRow().size() > 0) {
			mailAddress = (MailAddress) new BeanHanlder(MailAddress.class).handle(dataTable);
			// 同城快递单号
			String orderNo = dataTable.GetRow().get(0).GetColumn("OrderNumber").GetValue().toString(); 
			// 快递类别
			int expressType = Integer.valueOf(dataTable.GetRow().get(0).GetColumn("expressAgent").GetValue().toString()); 
			// 快递单号
			String expressNo = dataTable.GetRow().get(0).GetColumn("ExpressNum").GetValue().toString(); 
			
		
			// 同城平台邮寄接口URL
			String tongchengURL = "/offtickets/api/HsSupplier/SaveMailInfo";
			// 调用同城接口
			String paramContent = "orderNo="+orderNo+"&expressType="+expressType+"&expressNo="+expressNo;
		    jsonStr = SendPostandGet.submitPost(tongchengURL, paramContent, "UTF-8").toString();
		}

		
		return jsonStr;
	}
}
