/**
 * 
 */
package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;

/**
 * http://www.yma0.com/list.aspx?cid=2
 * 这个是已获取的号码列表。
 * http://api.yma0.com/http.aspx?action=getRecvingInfo&pid=8106&uid=cd1989929&token=51a75506c9801c88
 * 拉黑号码
 * http://api.yma0.com/http.aspx?action=addIgnoreList&uid=cd1989929&token=51a75506c9801c88&mobiles=15019036251&pid=8106
 * @time 2015年8月20日 下午12:43:14
 * @author chendong
 */
public class YMa0MobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "8106";//12306项目id
        //        pid 9859         12306上行(发短信)
        String uid = "120865";//12306
        //        String author_uid = "cd1989929";//开发者用户名
        //        String author_pwd = "cd1989929";//开发者用户名
        //        String token = "51a75506c9801c88";
        YMa0MobileCode yma0mobilecode = YMa0MobileCode.getInstance(pid);
        String mobile = yma0mobilecode.GetMobilenum(uid, pid, "");//获取手机号 13531961860|51a75506c9801c88
        //        String mobile = "15919424884";
        System.out.println(mobile);
        String result = yma0mobilecode.send(mobile, "999");
        System.out.println(result);
        //        String getVcodeAndReleaseMobile = yma0mobilecode.getVcodeAndReleaseMobile("", "13531961860", pid, "", "");
        //        String mobiles = "";
        //        String addIgnoreList = yma0mobilecode.AddIgnoreList("", mobiles, "", "");
        //        System.out.println(getVcodeAndReleaseMobile);
    }

    /**
     * 
     * 
     * @param pid 8403(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static YMa0MobileCode getInstance(String pid) {
        //        String pid = "8403";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("YMa0Author_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("YMa0Author_pwd", "MobileCode.properties");//"cd1989929123";//开发者用户名
        String token = PropertyUtil.getValue("YMa0Author_token", "MobileCode.properties");//token
        YMa0MobileCode yMa0MobileCode = new YMa0MobileCode(pid, uid, author_uid, author_pwd, token);
        return yMa0MobileCode;
    }

    public YMa0MobileCode(String pid, String uid, String author_uid, String author_pwd, String token) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = token;
    }

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    final static String initUrl = "http://api.yma0.com/http.aspx";//?action=

    //平台登陆
    final static String LOGININURL = "http://api.yma0.com/http.aspx?action=loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://api.yma0.com/http.aspx";

    //释放已获取的手机号码
    final static String RELEASEMOBILEMOBILENUMURL = "http://api.yma0.com/http.aspx?action=ReleaseMobile";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://api.yma0.com/http.aspx?action=getVcodeAndReleaseMobile";

    //拉黑号码
    final static String ADDIGNORELISTURL = "http://api.yma0.com/http.aspx?action=addIgnoreList";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.6yzm.com/DevApi/getRecvingInfo";

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String, java.lang.String)
     */
    @Override
    public String LoginIn(String uid, String pwd) {//&uid=用户名&pwd=密码
        String paramContent = "&uid=" + uid + "&pwd=" + pwd;
        String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("YMa0MobileCode_LoginIn", paramContent + ":LoginIn:" + result);
        if (result.startsWith(uid)) {
            result = result.replace(uid + "|", "");
        }
        else {
            result = "-1";
        }
        return result;
    }

    /** 
     * 获取手机号
     * 
     * 13531961860|51a75506c9801c88
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        //        8106  项目编号 铁路购票
        //        &pid=项目ID&uid=用户名&token=&size=1
        String paramContent = "action=getMobilenum&pid=" + this.pid + "&size=1&uid=" + this.author_uid + "&token="
                + this.token;
        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("YMa0MobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        if ("系统暂时没可用号码".contains(result)) {
            System.out.println("等待3秒!");
        }
        else {
            String[] results = result.split("[|]");
            if (results.length > 1) {
                result = results[0];
            }
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    // 获取短信验证码并释放号码
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String paramContent = "&uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token + "&mobile="
                + mobile + "&author_uid=" + this.author_uid;
        //        &uid=用户&token=登录时返回的令牌&pid=项目ID&mobile=获取到的手机号码
        String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
        WriteLog.write("YMa0MobileCode_getVcodeAndReleaseMobile", paramContent + ":getVcodeAndReleaseMobile:" + result);
        if ("not_receive".equals(result.trim())) {
            result = "no_data";
        }
        else {
            String[] results = result.split("[|]");
            if (results.length > 1) {
                result = results[1];
            }
        }
        return result;

    }

    // 添加黑名单号码
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        //        &uid=用户名&token=登录时返回的令牌&mobiles=号码1,号码2,号码3&pid=项目ID
        //        http://api.yma0.com/http.aspx?action=addIgnoreList&uid=用户名&token=登录时返回的令牌&mobiles=号码1,号码2,号码3&pid=项目ID
        if (this.uid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            ReleaseMobile(mobiles);
            String url = ADDIGNORELISTURL + "&uid=" + this.author_uid + "&token=" + this.token + "&mobiles=" + mobiles
                    + "&pid=" + this.pid;
            String result = SendPostandGet.submitGet(url, "utf-8");
            try {
                int resultInt = Integer.parseInt(result);
                if (resultInt > 0) {
                }
            }
            catch (Exception e) {
            }
            WriteLog.write("YMa0MobileCode_AddIgnoreList", url + ":AddIgnoreList:" + result);
            return result;
        }

    }

    /**
     * 
     * @time 2015年9月12日 下午3:47:33
     * @author chendong
     */
    private void ReleaseMobile(String mobiles) {
        //        http://api.yma0.com/http.aspx?action=ReleaseMobile&uid=用户名&token=登录时返回的令牌
        String url = RELEASEMOBILEMOBILENUMURL + "&uid=" + this.author_uid + "&token=" + this.token + "&mobiles="
                + mobiles + "&pid=" + this.pid;
        String result = SendPostandGet.submitGet(url, "utf-8");
        try {
            int resultInt = Integer.parseInt(result);
            if (resultInt > 0) {
            }
        }
        catch (Exception e) {
        }
        WriteLog.write("YMa0MobileCode_AddIgnoreList", url + ":ReleaseMobile:" + result);
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String send(String mobile, String content) {
        //        http://api.yma0.com/http.aspx?action=sendSms
        //        &uid=用户名&token=登录时返回的令牌&pid=项目ID&mobile=获取的手机号&recv=短信接收号码&content=短信内容&author_uid=开发者用户名(可选)
        String url = "http://api.yma0.com/http.aspx?action=sendSms&uid=" + this.author_uid + "&token=" + this.token
                + "&mobile=" + mobile + "&pid=" + this.pid + "&recv=12306&content=" + content + "&author_uid="
                + this.author_uid;
        String result = SendPostandGet.submitGet(url, "utf-8");
        if (result.equalsIgnoreCase("OK")) {
            result = "true";
        }
        WriteLog.write("YMa0MobileCode_send", mobile + ":" + url + ":AddIgnoreList:" + result);
        return result;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }
}
