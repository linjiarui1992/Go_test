package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.ZhuanfaUtil;

public class YaoMaMobileCode implements IMobileCode {

    public static void main(String[] args) {
        String pid = "469";
        String uid = "";
        String author_uid = "zh1995812";
        String author_pwd = "zh1995812";
        IMobileCode yaomamobilecode = YaoMaMobileCode.getInstance(pid);

        String mobile = "15017432565";
        //        mobile = yaomamobilecode.GetMobilenum(author_uid, pid, "");
        //        System.out.println(mobile);
        //        String yzm = yaomamobilecode.getVcodeAndReleaseMobile("", mobile, pid, "", author_uid);
        //        System.out.println(yzm);
        //        do {
        //            try {
        //                Thread.sleep(3000l);
        //            }
        //            catch (Exception e) {
        //            }
        //            yzm = yaomamobilecode.getVcodeAndReleaseMobile(mobile, pid, author_uid, result);
        //            System.out.println(yzm);
        //        }
        //        while (yzm.equals(null) || yzm.equals("no_data"));
        //        System.out.println(yzm);
        String black = yaomamobilecode.AddIgnoreList(author_uid, mobile, pid, "");
        System.out.println(black);
    }

    String pid; //项目编号

    String uid; //用户编号

    String author_uid;//用户名

    String author_pwd;//用户密码

    public String token; //令牌

    /**
     * 
     * @param string
     * @return
     * @time 2015年9月24日 下午12:01:21
     * @author chendong
     */
    public static IMobileCode getInstance(String pid) {
        //        String pid = "469";//12306项目项目编号
        String uid = PropertyUtil.getValue("YaoMa_uid", "MobileCode.properties");//12306
        String author_uid = PropertyUtil.getValue("YaoMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("YaoMaAuthor_pwd", "MobileCode.properties");//"cd1989929123";//开发者用户名
        YaoMaMobileCode yaomamobilecode = new YaoMaMobileCode(pid, uid, author_uid, author_pwd);
        return yaomamobilecode;
    }

    public YaoMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = LoginIn(author_uid, author_pwd);
        System.out.println(this.token);
    }

    /**
     *  接口地址： http://api.iyaoma.com/Api/index
     **/
    final static String LOGININURL = "http://api.iyaoma.com/Api/index/loginIn"; //登录

    //	final static String getItemsURL="http://api.83r.com:20153/User/getItems";	  //登录

    final static String GETMOBILENUMURL = "http://api.iyaoma.com/Api/index/getMobilenum"; //手机号

    final static String GETVCODEANDRELEASEMOBILEURL = "http://api.iyaoma.com/Api/index/getVcodeAndReleaseMobile"; //验证码

    final static String ADDIGNORELISTURL = "http://api.iyaoma.com/Api/index/addIgnoreList"; //黑名单

    final static String CANCELSMSRECV = "http://api.iyaoma.com/Api/index/cancelSMSRecv"; //释放

    /**
     * @param uid 用户名
     * @param pwd 密码
     * @return 登录
     * @time 2015年9月16日 22:33:24
     * @author ZhiHong
     */
    @Override
    public String LoginIn(String uid, String pwd) {
        if (this.author_uid == null || this.author_pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + uid + "&pwd=" + this.author_pwd;
            //            String result = SendPostandGet.submitPost(LOGININURL, paramContent, "UTF-8").toString();
            String url = LOGININURL + "?" + paramContent;
            System.out.println(url);
            String result = "-1";
            try {
                //                result = ZhuanfaUtil.posthttpclientdata(url, null, 20000L, null);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            WriteLog.write("YaoMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            String results[] = result.split("[|]");
            if (results.length == 3) {
                result = results[2];
            }
            this.token = result;
            return result;
        }
    }

    /**
     * @param pid 项目编号
     * @param token 令牌
     * @return 获取手机号
     * @time 2015年9月16日 22:34:14
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        if (this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "pid=" + this.pid + "&uid=" + this.author_uid + "&token=" + this.token;
            String result = "";
            for (int i = 0; i < 5; i++) {
                result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();
                WriteLog.write("YaoMaMobileCode_getMobilenum", paramContent + ":getMobilenum:" + i + ":" + result);
                if (result.contains("暂时无可用号码,可尝试释放号码")) {
                    System.out.println("要码:" + result);
                    continue;
                }
                else {
                    break;
                }
            }
            String results[] = result.split("[|]");
            if (results.length == 2) {
                result = results[0];
            }
            return result;
        }
    }

    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @param token 令牌
     * @return 获取验证码短信
     * @time 2015年9月16日 22:36:16
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (this.token == null || this.author_uid == null || this.token == null || this.pid == null) {
            return "传参有误";
        }
        else {
            String paramContent = "mobile=" + mobile + "&pid=" + this.pid + "&uid=" + this.author_uid + "&author_uid="
                    + this.author_uid + "&token=" + this.token;
            String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
            WriteLog.write("YaoMaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":etVcodeAndReleaseMobile:" + result);
            if (result.contains("还没有接收到验证码,请让程序等待几秒后再次尝试 code:002")
                    || result.contains("还没有接收到验证码,请让程序等待几秒后再次尝试 code:001")) {
                result = "no_data";
            }
            return result;
        }
    }

    /**
     * @param token 令牌
     * @param mobiles 加入黑名单
     * @return 2015年9月16日 22:39:18
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.token == null || this.pid == null || this.author_uid == null) {
            return "传参有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&token=" + this.token + "&mobiles=" + mobiles + "&pid="
                    + this.pid;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();
            WriteLog.write("YaoMabileCode_addIgnoreList", paramContent + ":addIgnoreList:" + result);
            return result;
        }
    }

    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    //释放手机号
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (mobile == null) {
            return "传参有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&token=" + this.token + "&mobile=" + mobile + "&pid="
                    + this.pid;
            String result = SendPostandGet.submitPost(CANCELSMSRECV, paramContent, "utf-8").toString();
            WriteLog.write("YaoMabileCode_CancelSMSRecv", paramContent + ":addIgnoreList:" + result);
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
