/**
 * 
 */
package com.ccservice.mobileCode;

/**
 * 
 * @time 2015年7月29日 下午4:03:59
 * @author chendong
 */
public interface IMobileCode {

    public String LoginIn(String uid, String pwd);

    public String GetMobilenum(String uid, String pid, String token);

    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token);

    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid);

    /**
     * 手机号释放
     * 
     * @param uid
     * @param mobiles
     * @param pid
     * @param token
     * @return
     * @time 2015年11月13日 下午12:31:01
     * @author chendong
     */
    public String ReleaseMobile(String uid, String mobiles, String pid, String token);

    public String AddIgnoreList(String uid, String mobiles, String pid, String token);

    public String GetRecvingInfo(String uid, String pid, String token);

    public String CancelSMSRecvAll(String uid, String token);

    public String CancelSMSRecv(String uid, String mobile, String token);

    /**
     * 发送短信
     * 
     * @param mobile 手机号
     * @param content 内容
     * @return
     * @time 2015年11月11日 上午11:16:58
     * @author chendong
     */
    public String send(String mobile, String content);
}
