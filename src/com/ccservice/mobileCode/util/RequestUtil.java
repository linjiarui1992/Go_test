package com.ccservice.mobileCode.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class RequestUtil {

    /**获取当前日期，格式为yyyy-MM-dd*/
    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }

    /**计算两个yyyy-MM-dd日期间的天数*/
    public static int getSubDays(String start, String end) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date s = sdf.parse(start);
            Date e = sdf.parse(end);
            long days = (e.getTime() - s.getTime()) / 1000 / 60 / 60 / 24;
            return Integer.parseInt(String.valueOf(days));
        }
        catch (Exception e) {
            return 0;
        }
    }

    public static String get(String url, String charset, Map<String, String> header, int timeout) {
        return submit(url, "", "get", charset, header, timeout);
    }

    public static String post(String url, String param, String charset, Map<String, String> header, int timeout) {
        return submit(url, param, "post", charset, header, timeout);
    }

    public String post2(String url, String param, String charset, Map<String, String> header, int timeout) {
        return submit(url, param, "post", charset, header, timeout);
    }

    /**
     * 网页公共请求头
     * @param isAjaxRequest 网页异步请求 --> X-Requested-With:XMLHttpRequest
     * @param completeAccept 完整的Accept --> Accept:text/html,application/xhtml+xml,application/xml;q=0.9...
     * @param IfModifiedSince true时 --> If-Modified-Since：0
     * @return HeaderMap，key：请求头名称，value：请求头值
     */
    public static Map<String, String> getRequestHeader(boolean isAjaxRequest, boolean completeAccept,
            boolean IfModifiedSince) {
        Map<String, String> map = new HashMap<String, String>();
        if (completeAccept) {
            map.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        }
        else {
            map.put("Accept", "*/*");
        }
        map.put("Accept-Encoding", "gzip, deflate");
        map.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        map.put("Cache-Control", "no-cache");
        map.put("Connection", "keep-alive");
        map.put("Host", "kyfw.12306.cn");
        map.put("Pragma", "no-cache");
        map.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:34.0) Gecko/20100101 Firefox/34.0");
        if (isAjaxRequest) {
            map.put("X-Requested-With", "XMLHttpRequest");
        }
        if (IfModifiedSince) {
            map.put("If-Modified-Since", "0");
        }
        return map;
    }

    public static boolean StringIsNull(String param) {
        return param == null || "".equals(param.trim()) ? true : false;
    }

    public static String getJsonString(JSONObject obj, String key) {
        if (obj == null || StringIsNull(key) || !obj.containsKey(key)) {
            return "";
        }
        else {
            String value = obj.getString(key);
            return StringIsNull(value) ? "" : value;
        }
    }

    /**float加法*/
    public static float floatAdd(float a, float b) {
        BigDecimal x = new BigDecimal(a + "");
        BigDecimal y = new BigDecimal(b + "");
        return x.add(y).floatValue();
    }

    /**float减法*/
    public static float floatSubtract(float a, float b) {
        BigDecimal x = new BigDecimal(a + "");
        BigDecimal y = new BigDecimal(b + "");
        return x.subtract(y).floatValue();
    }

    /**float乘法 */
    public static float floatMultiply(float a, float b) {
        BigDecimal x = new BigDecimal(a + "");
        BigDecimal y = new BigDecimal(b + "");
        return x.multiply(y).floatValue();
    }

    //{"validateMessagesShowId":"_validatorMessage","url":"login/init","status":true,"httpstatus":200,"messages":["用户未登录"],"validateMessages":{}}
    public static String getErrorMsg(JSONObject obj12306) {
        StringBuffer buf = new StringBuffer();
        //错误信息
        JSONArray messages = obj12306.containsKey("messages") ? obj12306.getJSONArray("messages") : new JSONArray();
        int size = messages.size();
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                buf.append(messages.get(i).toString());
            }
            else {
                buf.append(messages.get(i).toString() + "、");
            }
        }
        String ret = buf.toString();
        if (ret.endsWith("。") || ret.endsWith(".")) {
            ret = ret.substring(0, ret.length() - 1);
        }
        return ret;
    }

    /**
     * @param url 请求链接，以http或https开头
     * @param data 请求数据，post请求有效
     * @param reqType 请求类型，get或post，不填视为post
     * @param charset 字符集，不填视为"utf-8"
     * @param header 请求头
     * @param timeout 超时时间，大于0时有效
     */
    public static String submit(String url, String data, String reqType, String charset, Map<String, String> header,
            int timeout) { //HTTPS SSL
        Long l1 = System.currentTimeMillis();
        url = url.trim();
        charset = charset == null || charset.trim().equals("") ? "utf-8" : charset.trim();
        //HTTPS
        if (url.startsWith("https")) {
            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("SSL"); //或SSL
                X509TrustManager[] xtmArray = new X509TrustManager[] { new myX509TrustManager() };
                sslContext.init(null, xtmArray, new java.security.SecureRandom());
            }
            catch (GeneralSecurityException e) {
            }
            if (sslContext != null) {
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            }
            HttpsURLConnection.setDefaultHostnameVerifier(new myHostnameVerifier());
        }
        InputStream in = null;
        URLConnection con = null;
        DataOutputStream out = null;
        BufferedReader reader = null;
        StringBuffer res = new StringBuffer();
        try {
            con = (new URL(url)).openConnection();
            con.setDoOutput(true);
            con.setUseCaches(false);
            if (timeout > 0) {
                con.setReadTimeout(timeout);
                con.setConnectTimeout(timeout);
            }
            if (header != null && header.size() > 0) {
                for (String key : header.keySet()) {
                    con.setRequestProperty(key, header.get(key));
                }
            }
            if ("get".equalsIgnoreCase(reqType)) {
                con.connect();
            }
            else {
                out = new DataOutputStream(con.getOutputStream());
                out.write(data.getBytes(charset));
            }
            in = con.getInputStream();
            //判断是否压缩
            if ("gzip".equalsIgnoreCase(con.getHeaderField("Content-Encoding"))) {
                reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, charset));
            }
            String lineTxt = null;
            while ((lineTxt = reader.readLine()) != null) {
                res.append(lineTxt);
            }
        }
        catch (Exception e) {
            res = new StringBuffer();
            //错误信息
            String msg = e.getMessage() == null ? "空指针" : e.getMessage().trim();
            //记录日志
            StringBuffer log = new StringBuffer();
            log.append(msg).append("##########");
            //错误详情
            StackTraceElement[] messages = e.getStackTrace();
            int len = messages == null ? 0 : messages.length;
            for (int i = 0; i < len; i++) {
                if (i == len - 1) {
                    log.append(messages[i].toString());
                }
                else {
                    log.append(messages[i].toString()).append("##########");
                }
            }
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
            catch (Exception e) {
            }
        }
        return res.toString();
    }

    /**
     * @param url 请求链接，以http或https开头
     * @param data 请求数据，post请求有效
     * @param reqType 请求类型，get或post，不填视为post
     * @param charset 字符集，不填视为"utf-8"
     * @param header 请求头
     * @param timeout 超时时间，大于0时有效
     */
    public static String submitProxy(String url, String data, String reqType, String charset,
            Map<String, String> header, int timeout, String proxyHost, int proxyPort) { //HTTPS SSL
        // 使用java.net.Proxy类设置代理IP
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        Long l1 = System.currentTimeMillis();
        url = url.trim();
        charset = charset == null || charset.trim().equals("") ? "utf-8" : charset.trim();
        //HTTPS
        if (url.startsWith("https")) {
            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("SSL"); //或SSL
                X509TrustManager[] xtmArray = new X509TrustManager[] { new myX509TrustManager() };
                sslContext.init(null, xtmArray, new java.security.SecureRandom());
            }
            catch (GeneralSecurityException e) {
            }
            if (sslContext != null) {
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            }
            HttpsURLConnection.setDefaultHostnameVerifier(new myHostnameVerifier());
        }
        InputStream in = null;
        URLConnection con = null;
        DataOutputStream out = null;
        BufferedReader reader = null;
        StringBuffer res = new StringBuffer();
        try {
            URL urlTemp = new URL(url);
            con = urlTemp.openConnection(proxy);
            con.setDoOutput(true);
            con.setUseCaches(false);
            if (timeout > 0) {
                con.setReadTimeout(timeout);
                con.setConnectTimeout(timeout);
            }
            if (header != null && header.size() > 0) {
                for (String key : header.keySet()) {
                    con.setRequestProperty(key, header.get(key));
                }
            }
            if ("get".equalsIgnoreCase(reqType)) {
                con.connect();
            }
            else {
                out = new DataOutputStream(con.getOutputStream());
                out.write(data.getBytes(charset));
            }
            in = con.getInputStream();
            //判断是否压缩
            if ("gzip".equalsIgnoreCase(con.getHeaderField("Content-Encoding"))) {
                reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, charset));
            }
            String lineTxt = null;
            while ((lineTxt = reader.readLine()) != null) {
                res.append(lineTxt);
            }
        }
        catch (Exception e) {
            res = new StringBuffer();
            //错误信息
            String msg = e.getMessage() == null ? "空指针" : e.getMessage().trim();
            //记录日志
            StringBuffer log = new StringBuffer();
            log.append(msg).append("##########");
            //错误详情
            StackTraceElement[] messages = e.getStackTrace();
            int len = messages == null ? 0 : messages.length;
            for (int i = 0; i < len; i++) {
                if (i == len - 1) {
                    log.append(messages[i].toString());
                }
                else {
                    log.append(messages[i].toString()).append("##########");
                }
            }
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
            catch (Exception e) {
            }
        }
        return res.toString();
    }

    private static class myX509TrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    private static class myHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}