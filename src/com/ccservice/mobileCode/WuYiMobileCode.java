package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;

public class WuYiMobileCode implements IMobileCode {

    String pid; //项目编号

    String author_uid;//用户名

    String author_pwd;//用户密码

    String token; //令牌

    String mobile; //手机号码

    //-----------------------栋哥，这个传密码要加MD5码，不会弄，如果不需要就直接删了把。
    //-----------------------栋哥，这个传密码要加MD5码，不会弄，如果不需要就直接删了把。
    //-----------------------栋哥，这个传密码要加MD5码，不会弄，如果不需要就直接删了把。
    public WuYiMobileCode(String pid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
    }

    /**
     *  项目类型返回值说明：
     *  1 表示此项目用于接收验证码
     *  2表示此项目可发送短信
     *  3表示此项目即可接收验证码，也可以发送短信
     *  4表示此项目同一个号码可接收多次验证码
     **/
    final static String loginInURL = "http://www.jikesms.com/common/ajax.htm/action=user:UserEventAction"; //登录

    final static String getItemsURL = "http://api.83r.com:20153/User/getItems"; //登录

    final static String getPhoneURL = "http://api.83r.com:20153/User/getPhone"; //手机号

    final static String getMessageURL = "http://api.83r.com:20153/User/getMessage"; //验证码

    final static String addBlackURL = "http://api.83r.com:20153/User/addBlack"; //黑名单

    public static void main(String[] args) {
        String pid = "6512";
        String author_uid = "zh1995812";
        String author_pwd = "zh1995812";

        WuYiMobileCode feimamobilecode = new WuYiMobileCode(pid, author_uid, author_pwd);
        String result = feimamobilecode.LoginIn(author_uid, author_pwd);
        System.out.println(result);
    }

    /**
     * @param uid 用户名
     * @param pwd 密码
     * @return 登录
     * @time 2015年9月16日 22:33:24
     * @author ZhiHong
     */
    public String LoginIn(String uid, String pwd) {
        if (this.author_uid == null || this.author_pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = this.loginInURL + "?uName=" + uid + "&pWord=" + this.author_pwd;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            //			String results[] = result.split("[&]");
            //			result=results[0];
            this.token = result;
            return result;
        }
    }

    /**
     * @param pid 项目编号
     * @param token 令牌
     * @return 获取手机号
     * @time 2015年9月16日 22:34:14
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        if (this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = this.getPhoneURL + "?ItemId=" + pid + "&token=" + this.token;
            String result = SendPostandGet.submitGet(paramContent, "GB2312").toString();
            result = result.substring(0, 11);
            this.mobile = result;
            return result;
        }
    }

    /**
     * @param token 令牌
     * @return 获取验证码短信
     * @time 2015年9月16日 22:36:16
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (this.token == null) {
            return "传参有误";
        }
        else {
            String paramContent = this.getMessageURL + "?token=" + this.token;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            return result;
        }
    }

    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @param token 令牌
     * @param mobiles 加入黑名单
     * @return 2015年9月16日 22:39:18
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.token == null || this.mobile == null) {
            return "传参有误";
        }
        else {
            String paramContent = this.addBlackURL + "?token=" + token + "&phoneList=" + pid + "-" + mobiles;
            System.out.println(paramContent);
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            return result;
        }
    }

    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
