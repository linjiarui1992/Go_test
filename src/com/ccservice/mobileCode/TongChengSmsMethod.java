package com.ccservice.mobileCode;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.tenpay.util.MD5Util;

/**
 * 同程短信验证码接口的方法
 * @time 2015年12月3日15:19:22
 * @author chendong
 */
public class TongChengSmsMethod {
    public static void main(String[] args) {
        String result = "";
        String channel = "hs";
        //        String password = "tXSvI25hI17z1BHQ";
        String password = "Btdvg0rRtBQBc1Q7";
        //        Channel = hs
        //                password = tXSvI25hI17z1BHQ
        TongChengSmsMethod tongChengSmsMethod = new TongChengSmsMethod(channel, password);
        //        String mobile = tongChengSmsMethod.get();
        String mobile = "17730090265";
        //        System.out.println(mobile);
        //        result = tongChengSmsMethod.send(mobile, "999");
        result = tongChengSmsMethod.query(mobile);
        //        result = tongChengSmsMethod.update(mobile, "0", "mobile is used");
        System.out.println(result);
        //                System.out.println(tongChengSmsMethod.getsign("1448850426787"));
        //        tongChengSmsMethod.send();
        String aaa = tongChengSmsMethod.get();
        System.out.println(aaa);
    }

    //    final static String URL = "http://115.29.205.240:7099/";
    final static String URL = PropertyUtil.getValue("TongChengSmsUrl", "MobileCode.properties");

    final static String SENDURL = URL + "sms/send";

    final static String GETURL = URL + "sms/get";

    final static String QUERYURL = URL + "sms/query";

    final static String UPDATEURL = URL + "sms/update";

    String channel;

    String password;

    public TongChengSmsMethod(String channel, String password) {
        super();
        this.channel = channel;
        this.password = password;
    }

    public final static SimpleDateFormat yyyyMMddHHmmssSSS = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    /**
     * 同程 申请发送短信成功后，查询回复的短信内容
     * @time 2015年11月30日 下午2:20:22
     * @author chendong
     * @param mobile 
     */
    public String query(String mobile) {
        String result = "";
        String timestamp = getreqtime();
        String sign = getsign(timestamp);
        //        channel=hs&timestamp=1448850426787&sign=ca25ec5fca74e39b56454c91a71ca9c5&destNumber=12306&count=3
        String paramContent = "channel=" + this.channel + "&timestamp=" + timestamp + "&sign=" + sign + "&destNumber="
                + mobile + "&count=1";
        result = SendPostandGet.submitPost(QUERYURL, paramContent, "utf-8").toString();
        WriteLog.write("TongChengMobileCode_getVcodeAndReleaseMobile", mobile + ":" + result + ":" + paramContent + ":"
                + QUERYURL);
        JSONObject jsonObject = JSONObject.parseObject(result);
        //        "result":[{"message":"12306用户注册或既有用户手机核验专用验证码：231762。如非本人直接访问12306，请停止操作，切勿将验证码提供给第三方。【铁路客服】","send_time":"2015-11-29 09:33:59","src_number":"12306","dest_number":"18096594290","type":"下行"},{"message":"12306用户注册或既有用户手机核验专用验证码：228390。如非本人直接访问12306，请停止操作，切勿将验证码提供给第三方。【铁路客服】","send_time":"2015-11-30 09:22:30","src_number":"12306","dest_number":"13309620071","type":"下行"}],"code":"0","msg":"查询成功"}
        JSONArray jSONArray = jsonObject.getJSONArray("result");
        if (jSONArray != null && jSONArray.size() > 0) {
            for (int i = 0; i < jSONArray.size(); i++) {
                JSONObject jsonObject1 = jSONArray.getJSONObject(i);
                jsonObject1.getString("send_time");
                String message = jsonObject1.getString("message");
                result = message;
                break;
            }
        }
        else {
            if ("0".equals(jsonObject.getString("code"))) {
                result = "no_data";
            }
            else {
                result = jsonObject.getString("msg");
            }
        }
        WriteLog.write("TongChengMobileCode_getVcodeAndReleaseMobile", mobile + ":" + result);
        return result;
    }

    /**
     * 同程从接口获取指定数量的手机号码
     * @time 2015年11月30日 下午2:20:22
     * @author chendong
     */
    public String get() {
        System.out.println("开始获取...");
        String result = "";
        String timestamp = getreqtime();
        String sign = getsign(timestamp);
        String paramContent = "channel=" + this.channel + "&timestamp=" + timestamp + "&sign=" + sign
                + "&count=1&srcNumber=12306";
        for (int i = 0; i < 50; i++) {
            result = SendPostandGet.submitPost(GETURL, paramContent, "utf-8").toString();
            if (result.contains("申请号码失败") || "".equals(result)) {
                WriteLog.write("TongChengMobileCode_GetMobilenum", i + ":" + result + ":" + GETURL + "?" + paramContent);
                try {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }
            else {
                break;
            }
        }
        WriteLog.write("TongChengMobileCode_GetMobilenum", result + ":" + GETURL + "?" + paramContent);
        JSONObject jsonObject = JSONObject.parseObject(result);
        //        {"result":["13395524673","18009624930","15395246386"],"code":"0","msg":"申请成功"}
        //        {"result":null, "code":"-1", "msg":"申请号码失败"}
        JSONArray jSONArray = jsonObject.getJSONArray("result");
        if (jSONArray != null && jSONArray.size() > 0) {
            result = jSONArray.getString(0);
        }
        else {
            result = jsonObject.getString("msg");
        }
        return result;
    }

    /**
     * 同程发送短信的方法
     * @time 2015年11月30日 下午2:20:22
     * 13309620071
     * @author chendong
     */
    public String send(String srcNumber, String message) {
        String result = "";
        String timestamp = getreqtime();
        String sign = getsign(timestamp);
        String paramContent = "channel=" + this.channel + "&timestamp=" + timestamp + "&sign=" + sign
                + "&destNumber=12306&srcNumber=" + srcNumber + "&message=" + message;
        result = SendPostandGet.submitPost(SENDURL, paramContent, "utf-8").toString();
        WriteLog.write("TongChengMobileCode_send", srcNumber + ":" + result + ":" + paramContent + ":" + SENDURL);
        JSONObject jsonObject = JSONObject.parseObject(result);
        if ("0".equals(jsonObject.getString("code")) && "发送成功".equals(jsonObject.getString("msg"))) {
            result = "true";
        }
        else {
            result = "false";
        }
        WriteLog.write("TongChengMobileCode_send", srcNumber + ":" + result);
        return result;
    }

    /**
     * 手机号核验/注册 使用成功后，反馈状态
     * @time 2015年11月30日 下午2:20:22
     * @param register 0注册失败，1注册成功 ,2验证码未收到
     * 13309620071
     * @author chendong
     */
    public String update(String telphone, String register, String remark) {
        String result = "";
        String timestamp = getreqtime();
        String sign = getsign(timestamp);
        String paramContent = "channel=" + this.channel + "&timestamp=" + timestamp + "&sign=" + sign
                + "&srcNumber=12306&telphone=" + telphone + "&register=" + register + "&remark=" + remark;
        //        channel=hs&timestamp=1448850426787&sign=ca25ec5fca74e39b56454c91a71ca9c5& srcNumber=12306&telphone=13309620071
        result = SendPostandGet.submitPost(UPDATEURL, paramContent, "utf-8").toString();
        WriteLog.write("TongChengMobileCode_AddIgnoreList", telphone + ":" + result + ":" + paramContent + ":"
                + UPDATEURL);
        JSONObject jsonObject = JSONObject.parseObject(result);
        if ("0".equals(jsonObject.getString("code"))) {
            result = "true";
        }
        else {
            result = "false";
        }
        return result;
    }

    public String getreqtime() {
        return yyyyMMddHHmmssSSS.format(new Date());
    }

    /**
     * 
     * MD5(channel+ password+timestamp)
     * @time 2014年12月12日 下午2:44:31
     * @author chendong
     */
    public String getsign(String timestamp) {
        //        System.out.println(this.password);
        String sign = this.channel + this.password + timestamp;
        //        System.out.println("sign:before:" + sign);
        sign = MD5Util.MD5Encode(sign, "UTF-8");
        //        System.out.println("sign:after:" + sign);
        return sign;
    }
}
