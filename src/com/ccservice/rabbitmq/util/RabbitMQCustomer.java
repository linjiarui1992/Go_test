package com.ccservice.rabbitmq.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.qunar.util.TimeUtil;
import com.ccservice.rabbitmq.bean.RabbitMQConnectsBean;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * rabbitMQ   消费者他爹
 * 里面的都是大爷！
 * @author 之至
 * @time 2016年12月20日 下午1:11:25
 */
public class RabbitMQCustomer extends Thread{
    //短信更新时间
    protected String smsTimeString = TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss);

    //短信发送条数
    protected int smsNumString = 0;

    //map  消费者集合     队列名称 ，消费者
    /**
     * 所有消费者的合集 key 是随机的uuid, value是消费者这个对象
     */
    public static Map<String, RabbitMQConnectsBean> Mapcustomers = new HashMap<String, RabbitMQConnectsBean>();

    //消费者名称list
    /**
     * 给某个消费者分配的唯一的uuid合集
     */
    public static List<String> ListcustomerNames = new ArrayList<String>();//

    //链接
    protected Connection connection;

    //通道
    protected Channel channel;

    //消费者爸爸
    protected QueueingConsumer consumer;

    //猜猜看
    protected RabbitMQConnectsBean rabbitMQConnectsBean = new RabbitMQConnectsBean();

    public RabbitMQCustomer rabbitMQCustomer;

    protected QueueingConsumer.Delivery delivery;

    private String CustomerName;//消费者名字  唯一标识 uuid

    private boolean willByClose;//是否即将关闭,如果即将关闭在进行完业务逻辑后关闭消费者
    
    public String StatusString = "";//消费者是否开启1开启0关闭
    public int typePingTai;//type  空铁是1;同程是2
    public String queueNameString;//队列的名字
    public boolean consumerStatus = true;//当前队列状态
    public String host;
    public String username;
    public String password;
    
    public RabbitMQCustomer() {
    }

    /**
     * 祖坟      地址分空铁和同程   千万不能错！
     * @param queueNameString
     * @param typePingTai  空铁是1;同程是2
     * @throws IOException
     */
    public RabbitMQCustomer(String queueNameString, int typePingTai) throws IOException {
//        //空铁
//        if (typePingTai == 1) {
//            connection = getConnection(PublicConnectionInfos.KTHOST, PublicConnectionInfos.USERNAME,
//                    PublicConnectionInfos.KTPASSWORD);
//        }
//        //同程
//        else if (typePingTai == 2) {
//            connection = getConnection(PublicConnectionInfos.TCHOST, PublicConnectionInfos.USERNAME,
//                    PublicConnectionInfos.TCPASSWORD);
//        }
//        int type = getIntType(queueNameString);
//        channel = connection.createChannel();
//        consumer = getQueueingConsumer(channel, queueNameString);
//        UUID uuid = UUID.randomUUID();
//        String CustomerName = uuid.toString();
//        rabbitMQConnectsBean.setConnection(connection);
//        rabbitMQConnectsBean.setChannel(channel);
//        rabbitMQConnectsBean.setKey(CustomerName);
//        rabbitMQConnectsBean.setUse(true);
//        rabbitMQConnectsBean.setConsumer(consumer);
//        rabbitMQConnectsBean.setType(type);
//        ListcustomerNames.add(CustomerName);
//        Mapcustomers.put(CustomerName, rabbitMQConnectsBean);
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午1:43:03
     * @Description 根据队列名称获取type
     * @param queueNameString   队列名称
     * @return
     */
    public int getIntType(String queueNameString) {
        int type = 0;
        if (queueNameString.equals(PublicConnectionInfos.RABBITWAITORDER)) {
            type = 1;
        }
        else if (queueNameString.equals(PublicConnectionInfos.QUEUEMQ_TRAINORDER_WAITORDER_ORDERID_PAIDUI)) {
            type = 2;
        }
        else if (queueNameString.equals(PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER)) {
            type = 3;
        }
        else if (queueNameString.equals(PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION)) {
            type = 4;
        }
        else if (queueNameString.equals(PublicConnectionInfos.CHANGEWAITORDER)) {
            type = 5;
        }
        else if (queueNameString.equals(PublicConnectionInfos.CHANGECONFIRMORDER)) {
            type = 6;
        }
        else if (queueNameString.equals(PublicConnectionInfos.CHANGEPAYEXAMINE)) {
            type = 7;
        }
        else if (queueNameString.equals(PublicConnectionInfos.RABBIT_CHANGEORDER_PAIDUI)) {
            type = 8;
        }
        else if (queueNameString.equals(PublicConnectionInfos.QUEUEMQ_TRAINORDERDEDUCTIONGQ)) {
            type = 9;
        }
        else if (queueNameString.equals(PublicConnectionInfos.QUEUEMQ_CANCELORDER)) {
            type = 10;
        }
        else if (queueNameString.equals(PublicConnectionInfos.TB_CHANGE_ORDER)) {
            type = 11;
        }
        else if (queueNameString.equals(PublicConnectionInfos.QUEUEMQ_TRAINTICKET_REFUNDTICKET)) {
            type = 12;
        }
        else if (queueNameString.equals(PublicConnectionInfos.QUEUEMQ_12306ACCOUNTSYSTEM_EXCUTEUPDATE)) {
            type = 13;
        }
        return type;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午2:28:36
     * @Description 获取一条消息对外
     * @param queueNameString 队列名称
     * @return
     * @throws IOException 
     * @throws InterruptedException 
     * @throws ConsumerCancelledException 
     * @throws ShutdownSignalException 
     */
    public String getNewMessageString(String queueNameString) throws IOException, ShutdownSignalException,
            ConsumerCancelledException, InterruptedException {
        String resultString = "";
        resultString = getMessgeString(consumer, channel);
        return resultString;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午2:21:16
     * @Description 得到一条消息
     * @param consumer  消费者
     * @param channel   通道
     * @return
     * @throws ShutdownSignalException
     * @throws ConsumerCancelledException
     * @throws InterruptedException
     */
    private String getMessgeString(QueueingConsumer consumer, Channel channel) throws ShutdownSignalException,
            ConsumerCancelledException, InterruptedException {
        String resultString = "0";
        try {
            delivery = consumer.nextDelivery();
            Map<?, ?> map = (HashMap<?, ?>) SerializationUtils.deserialize(delivery.getBody());
            resultString = map.get("message number").toString();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return resultString;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午1:43:24
     * @Description 得到一个消费者
     * @param channel
     * @param queueNameString
     * @return
     * @throws IOException 
     */
    public QueueingConsumer getQueueingConsumer(Channel channel, String queueNameString) throws IOException {
        QueueingConsumer consumer = createQueueingConsumer(channel, queueNameString);
        return consumer;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午1:42:22
     * @Description  创建一个消费者
     * @param channel
     * @param queueNameString  队列名称
     * @return
     * @throws IOException 
     */
    private QueueingConsumer createQueueingConsumer(Channel channel, String queueNameString) throws IOException {
        QueueingConsumer consumer = new QueueingConsumer(channel);
        //取消 autoAck  
        boolean autoAck = false;
        channel.basicQos(1);
        channel.basicConsume(queueNameString, autoAck, consumer);
        channel.queueDeclare(queueNameString, true, false, false, null);
        return consumer;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午1:36:36
     * @Description 对外暴露方法
     * @param host
     * @param userName
     * @param passWord
     * @return
     * @throws IOException 
     */
    public Connection getConnection(String host, String userName, String passWord) throws IOException {
        Connection connection = createConnection(host, userName, passWord);
        return connection;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月20日 下午1:29:25
     * @Description 创建一个链接
     * @param host  ip地址
     * @param userName  登录用户名
     * @param passWord  登录密码
     * @return
     * @throws IOException 
     */
    private Connection createConnection(String host, String userName, String passWord) throws IOException {
        Connection connection = null;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(userName);
        factory.setPassword(passWord);
        connection = factory.newConnection();
        return connection;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午4:59:40
     * @Description 在一时间内搞些事情
     */
    public boolean youCanDo() {
        boolean result = false;
        String nowTimeDay = TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMdd);
        String startString = PublicConnectionInfos.STARTTIME;
        String endString = PublicConnectionInfos.ENDTIME;
        long startTimeString = 0;
        long endTimeString = 0;
        try {
            startTimeString = TimeUtil.stringToLong(nowTimeDay + " " + startString, TimeUtil.yyyyMMddHHmmss);
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            endTimeString = TimeUtil.stringToLong(nowTimeDay + " " + endString, TimeUtil.yyyyMMddHHmmss);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        long nowTimeReal = TimeUtil.dateToLong(new Date());
        if (nowTimeReal > startTimeString && nowTimeReal < endTimeString) {
            result = true;
        }
        return result;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月22日 下午2:45:23
     * @Description 得到一个时间段返回boolean
     * @param startString  开始时间
     * @param endString    结束时间
     * @param num    间隔时间按分钟算
     * @return
     */
    public boolean timeFrame(String startString, String endString, long num) {
        boolean result = false;
        long startTimeString = 0;
        long endTimeString = 0;
        try {
            startTimeString = TimeUtil.stringToLong(startString, TimeUtil.yyyyMMddHHmmss);
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            endTimeString = TimeUtil.stringToLong(endString, TimeUtil.yyyyMMddHHmmss);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if ((endTimeString - startTimeString) > (num * 60 * 1000)) {
            result = true;
        }
        return result;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 上午9:57:01
     * @Description 短信提示公共方法
     * @param strTypeString   短信头
     */
    @SuppressWarnings("rawtypes")
    public void sendSMS(String strTypeString) {
        try {//發短信
            String mobilePhoneStrings = "";
            List list1 = Server
                    .getInstance()
                    .getSystemService()
                    .findMapResultBySql(" select ReMark from TrainPingTai with(nolock)  where PingTai =99 and type =1",
                            null);
            if (list1.size() > 0) {
                Map map1 = (Map) list1.get(0);
                mobilePhoneStrings = map1.containsKey("ReMark") ? map1.get("ReMark").toString() : "";
                if (!"".equals(mobilePhoneStrings)) {
                    String msg = URLEncoder.encode(strTypeString, "GBK");
                    String smsString = PublicConnectionInfos.SMSURL + "?name=" + PublicConnectionInfos.SMSNAME
                            + "&pwd=" + PublicConnectionInfos.SMSPWD + "&dst=" + mobilePhoneStrings + "&msg=" + msg
                            + "&txt=ccdx";
                    WriteLog.write("Rabbit_QueueMQ_短信提醒", "发送短信详情 -- " + smsString);
                    String resultString = SendPostandGet.submitGet(smsString, "GBK");
                    WriteLog.write("Rabbit_QueueMQ_短信提醒", "发送短信结果 -- " + resultString);
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("Rabbit_QueueMQ_短信提醒_Exception", e, strTypeString);
        }
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public boolean isWillByClose() {
        return willByClose;
    }

    public void setWillByClose(boolean willByClose) {
        this.willByClose = willByClose;
    }

    public static Map<String, RabbitMQConnectsBean> getMapcustomers() {
        return Mapcustomers;
    }

    public static void setMapcustomers(Map<String, RabbitMQConnectsBean> mapcustomers) {
        Mapcustomers = mapcustomers;
    }
    public Connection getConnectionNew(String host, String username, String password) {
        Connection connection = null;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(username);
        factory.setPassword(password);
        // 创建一个新的消息队列服务器实体的连接
        try {
            connection = factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
