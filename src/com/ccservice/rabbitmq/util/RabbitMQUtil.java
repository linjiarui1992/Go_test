package com.ccservice.rabbitmq.util;

import java.io.IOException;
import java.util.HashMap;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.rabbitmq.bean.RabbitMQConnectsBean;
import com.ccservice.rabbitmq.util.byhost.RabbitMqUtilNew;

public class RabbitMQUtil {

    private static RabbitMQConnectsBean rmcb;

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月16日 上午9:28:43
     * @Description rabbitMQ 发送消息方法
     * @param messages  消息
     * @param queueName  队列名称
     */
    public static void sendOnemessage(long messages, String queueName) {
        sendOnemessageV2(messages+"", queueName);
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月16日 上午9:28:43
     * @Description rabbitMQ 发送消息方法
     * @param messages  消息  String
     * @param queueName  队列名称
     */
    public static void sendOnemessageV2(String messages, String queueName) {
        String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
        try {
            RabbitMqUtilNew.sendOneRabbitMqMessage(queueName, messages,pingTaiType);
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMQUtil-exception", e, "run=" +queueName+":"+ messages);
        }
    }

    /**
     * 支付用于发MQ消息的方法
     * 
     * @param messages
     * @param queueName
     * @time 2016年12月22日 下午3:03:53
     * @author fiend
     */
    public static void sendOnemessagePay(String messages, String queueName) {
        try {
            RabbitMqUtilNew.sendOneRabbitMqMessage(queueName, messages,"3");
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMQUtil-exception", e, "run=" +queueName+":"+ messages);
        }
    }
    
    //下面的是老的
    /**
     * 
     * @author RRRRRR
     * @time 2016年11月16日 上午9:28:43
     * @Description rabbitMQ 发送消息方法
     * @param messages  消息
     * @param queueName  队列名称
     */
    public static void sendOnemessage_back(long messages, String queueName) {
        HashMap<String, Long> message = new HashMap<String, Long>();
        message.put("message number", messages);
        Producer producer = null;
        try {
            producer = new Producer(queueName);
            producer.sendMessage(message);
            WriteLog.write("rabbitMQ发送消息", "队列名称 ---->" + queueName + " messages  ---->" + messages);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("rabbitMQ发送消息_Exception", e, queueName + "--->" + messages);
        }
        if (producer != null) {
            RabbitMQChannelPool.getinstance().returnRabbitMQMethod(producer.rmcb);
        }
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月16日 上午9:28:43
     * @Description rabbitMQ 发送消息方法
     * @param messages  消息  String
     * @param queueName  队列名称
     */
    public static void sendOnemessageV2_back(String messages, String queueName) {
        Producer producer = null;
        try {
            producer = new Producer(queueName);
        }
        catch (IOException e) {
            e.printStackTrace();
            WriteLog.write("rabbitMQ发送消息", "producer" + producer + "异常信息" + e);
        }
        WriteLog.write("rabbitMQ发送消息", "准备发送");
        HashMap<String, String> message = new HashMap<String, String>();
        message.put("message number", messages);
        try {
            producer.sendMessage(message);
        }
        catch (IOException e) {
            e.printStackTrace();
            WriteLog.write("rabbitMQ发送消息", "producer" + producer + "异常信息" + e.fillInStackTrace().toString());
        }
        WriteLog.write("rabbitMQ发送消息", "队列名称 ---->" + queueName + " messages  ---->" + messages);
        RabbitMQChannelPool.getinstance().returnRabbitMQMethod(producer.rmcb);
    }

    /**
     * 支付用于发MQ消息的方法
     * 
     * @param messages
     * @param queueName
     * @time 2016年12月22日 下午3:03:53
     * @author fiend
     */
    public static void sendOnemessagePay_back(String messages, String queueName) {
        Producer producer = null;
        try {
            producer = new Producer(queueName, MQUtil.PAY_TYPE);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("sendOnemessagePay_Exception", e, queueName + "--->" + messages);
        }
        HashMap<String, String> message = new HashMap<String, String>();
        message.put("message number", messages);
        try {
            producer.sendMessage(message);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("sendOnemessagePay_Exception", e, queueName + "--->" + messages);
        }
        WriteLog.write("sendOnemessagePay", "队列名称---->" + queueName + ";messages---->" + messages);
        RabbitMQPayChannelPool.getinstance().returnRabbitMQMethod(producer.rmcb);
    }

    /**
     * 
     * @author RRRRRR
     * @time 2016年11月15日 下午7:32:43
     * @Description 从对象池中拿出一个空闲连接
     * @return
     */
    public static RabbitMQConnectsBean getOneRabbitMQConnectsBean() {
        RabbitMQConnectsBean rmcbb = null;
        int poolNum = RabbitMQChannelPool.getinstance().getRmqcbs().size();
        for (int i = 0; i < poolNum; i++) {
            rmcb = RabbitMQChannelPool.getinstance().getRmqcbs().get(i);
            if (rmcb.isUse()) {
                continue;
            }
            else {
                rmcb.setUse(true);
                rmcbb = rmcb;
                break;
            }
        }
        return rmcbb;
    }
    
    public static void main(String[] args) {
        try {
            Integer.parseInt("a");
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("RabbitMQUtil-exception", e, "AAAArun=aaaAAAA");
        }
    }
    
}
