package com.ccservice.rabbitmq.util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.ccservice.rabbitmq.bean.RabbitMQConnectsBean;
import com.ccservice.rabbitmq.util.RabbitMQChannelPool;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * 抽象类
 * @author Kiven
 * 我们首先写一个类，将产生产者和消费者统一为 EndPoint类型的队列。不管是生产者还是消费者， 连接队列的代码都是一样的，这样可以通用一些。
 */
public abstract class EndPoint{
	
    protected Channel channel;//通道
    protected Connection connection;
    protected String endPointName;//队列名称
    protected  RabbitMQConnectsBean rmcb;
    protected  String EXCHANGE_NAME = "";
    protected  String ROUTINGKEY = "";
	
    public EndPoint(String endpointName) throws IOException{
         this.endPointName = endpointName;
         //获取一个连接
         rmcb=getCanUseRmqB();
         if(rmcb==null){
             rmcb=getCanUseRmqB();
         }
         channel =rmcb.getChannel();
       //declaring a queue for this channel. If queue does not exist,
       //it will be created on the server.  
         channel.queueDeclare(endpointName, true, false, false, null);//old  
    }
	
    /**
     * 
     * @param endpointName
     * @param mqUrlType 获取类型，1为支付。
     * @throws IOException
     */
    public EndPoint(String endpointName, int mqUrlType) throws IOException {
        this.endPointName = endpointName;
        //获取一个连接
        rmcb = getCanUseRmqB(mqUrlType);
        if (rmcb == null) {
            rmcb = getCanUseRmqB(mqUrlType);
        }
        connection = rmcb.getConnection();
        channel = rmcb.getChannel();
        //declaring a queue for this channel. If queue does not exist,
        //it will be created on the server.  
        channel.queueDeclare(endpointName, true, false, false, null);//old  
    }
    /**
     * 关闭channel和connection。并非必须，因为隐含是自动调用的。 
     * @throws IOException
     * @throws TimeoutException 
     */
     public void close() throws IOException, TimeoutException{
         this.channel.close();
         this.connection.close();
     }
     
     /**
      * 
      * @author RRRRRR
      * @time 2016年11月15日 下午6:47:35
      * @Description 得到一个连接对象   如果没有则新建
      * @return
      */
     private RabbitMQConnectsBean getCanUseRmqB(){
         RabbitMQConnectsBean rmcbb=null;
         int poolNum=RabbitMQChannelPool.getinstance().getRmqcbs().size();
         for (int i = 0; i < poolNum; i++) {
             rmcb=RabbitMQChannelPool.getinstance().getRmqcbs().get(i);
             if (rmcb.isUse()) {
                continue;
            }else{
                rmcb.setUse(true);
                rmcbb=rmcb;
                break;
            }
         }
         if(rmcbb==null){
             rmcbb =RabbitMQChannelPool.getinstance().createNewRabbitMQConnectsBean(); 
             rmcbb.setUse(true);
         }
         return rmcbb;
     }
     /**
      * 得到一个连接对象   如果没有则新建
      * 
      * @param mqUrlType 获取类型，1为支付。
      * @return
      * @time 2016年12月22日 下午1:52:03
      * @author fiend
      */
     private RabbitMQConnectsBean getCanUseRmqB(int mqUrlType) {
         RabbitMQConnectsBean rmcbb = null;
         int poolNum = 0;
         if (MQUtil.PAY_TYPE == mqUrlType) {
             poolNum = RabbitMQPayChannelPool.getinstance().getRmqcbs().size();
         }
         else {
             poolNum = RabbitMQChannelPool.getinstance().getRmqcbs().size();
         }
         for (int i = 0; i < poolNum; i++) {
             if (MQUtil.PAY_TYPE == mqUrlType) {
                 rmcb = RabbitMQPayChannelPool.getinstance().getRmqcbs().get(i);
             }
             else {
                 rmcb = RabbitMQChannelPool.getinstance().getRmqcbs().get(i);
             }
             if (rmcb.isUse()) {
                 continue;
             }
             else {
                 rmcb.setUse(true);
                 rmcbb = rmcb;
                 break;
             }
         }
         if (rmcbb == null) {
             if (MQUtil.PAY_TYPE == mqUrlType) {
                 rmcbb = RabbitMQPayChannelPool.getinstance().createNewRabbitMQConnectsBean();
             }
             else {
                 rmcbb = RabbitMQChannelPool.getinstance().createNewRabbitMQConnectsBean();
             }
             rmcbb.setUse(true);
         }
         return rmcbb;
     }
}
