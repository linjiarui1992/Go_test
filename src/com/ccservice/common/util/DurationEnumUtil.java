package com.ccservice.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.db.TrainCreateOrderDBUtil;


public class DurationEnumUtil {
    private static HashMap<String, Boolean> map = new HashMap();

    private static long time = System.currentTimeMillis();

    private static final long Time_OUT_TIME = 5 * 60 * 1000;

    /**
     * 得到是否开启时长统计
     * 
     * @time:2017年6月13日上午9:13:54
     * @auto:baozz
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static boolean getIsOpenDuration() {
        boolean isOpenDuration = false;
        reSet();
        try {
            isOpenDuration = map.containsKey("isOpenDuration") ? map.get("isOpenDuration") : false;
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("Duration_exception", e);
        }
        return isOpenDuration;
    }

    @SuppressWarnings("rawtypes")
    private static void reSet() {
        String sql = "[dbo].[select_DurationDecide]";
        if (!map.containsKey("isOpenDuration") || isOutTime()) {
            try {
                List list = TrainCreateOrderDBUtil.findMapResultByProcedure(sql);
                if (list.size() > 0) {
                    Map dbMap = (Map) list.get(0);
                    map.put("isOpenDuration", "1".equals(dbMap.get("isOpenDuration").toString()));
                    WriteLog.write("Duration_log", "将数据放入缓存中"+map);
                }
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("Duration_exception", e);
            }
            resetTime();
        }
    }

    private static void resetTime() {
        time = System.currentTimeMillis();
    }

    private static boolean isOutTime() {
        if (System.currentTimeMillis() - time > Time_OUT_TIME) {
            return true;
        }
        else {
            return false;
        }
    }
}
