package com.ccservice.report.train;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.customeragent.Customeragent;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 火车票报表更新定时更新
 *
 */
public class TrainReportUpdate implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        List<Customeragent> agentlist = Server.getInstance().getMemberService().findAllCustomeragent("where id>=46", "order by id asc ", -1, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        String begin_time = sdf.format(cal.getTime());
        String end_time = sdf.format(cal.getTime());
        for (int i = 0; i < agentlist.size(); i++) {
            Customeragent agent = agentlist.get(i);
            try {
                new TrainReportJob().tcexprotfilereport(agent.getId() + "", begin_time, end_time);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        /**
         * 读取任务列表 待执行
         */
        String sql = "select * from T_TRAINREPORTTASKLIST where C_STATE=1 order by ID DESC";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                try {
                    Map map = (Map) list.get(i);
                    long agentid = Long.valueOf(map.get("C_AGENTID").toString());//代理商
                    String begintime = map.get("C_BEGINTIME").toString();//开始时间
                    String endtime = map.get("C_ENDTIME").toString();//结束时间
                    new TrainReportJob().tcexprotfilereport(agentid + "", begintime, endtime);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
