/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.ctripoffsts.ctripPlatform.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.ctripoffsts.back.TrainCtripOfflineBack;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 携程_破解线下票 - 携程_破解线下票出票回调请求接口 - 由CN平台发起
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainCtripOfflineOrderCallbackSuccessServletCN extends HttpServlet {
    private static final String LOGNAME = "携程_破解线下票出票回调请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();//记录操作日志，单独封装工具类

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":携程_破解线下票出票回调请求信息-orderId-->" + orderIdl + ",userid-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起携程_破解线下票的出票回调请求");

        JSONObject result = ctripOrderSuccessCN(orderIdl, useridi);

        WriteLog.write(LOGNAME, r1 + ":携程_破解线下票出票回调请求结果-result" + result);

        if (!result.getBooleanValue("success")) {
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, result.getString("msg"));
        }

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        //out.print(TrainOrderOfflineUtil.getNowDateStr()+"同程线下票闪送单的二次下单锁票请求完成");
        out.flush();
        out.close();
    }

    private JSONObject ctripOrderSuccessCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        String username = PropertyUtil.getValue("TrainCtripOfflineLoginUsername", "Train.GuestAccount.properties");//从配置文件取出
        TrainCtripOfflineBack trainCtripOfflineBack = new TrainCtripOfflineBack(username);

        Boolean isOrderSuccess = false;
        try {
            String jonString = trainOrderOfflineDao.findCtripOrderInfoById(orderIdl);//参数的拼接

            WriteLog.write(LOGNAME, r1 + ":携程_破解线下票出票回调请求-进入到方法-ctripOrderSuccessCN-username:" + username + "-orderIdl:"
                    + orderIdl + "-jonString:" + jonString);

            isOrderSuccess = trainCtripOfflineBack.BackInput(orderIdl, jonString);

            WriteLog.write(LOGNAME, r1 + ":携程_破解线下票出票回调请求-isOrderSuccess:" + isOrderSuccess);
        }
        catch (Exception e3) {
            String content = "携程_破解线下票出票回调交互请求异常";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
            ExceptionTCUtil.handleTCException(e3);
            resResult.put("success", "false");
            resResult.put("msg", content);
            return resResult;
        }

        if (isOrderSuccess) {
            String content = "携程_破解线下票出票回调请求成功";

            resResult.put("success", "true");
            resResult.put("msg", content);

            //{"msg":"携程_破解线下票出票回调请求成功","success":"true"}

            //更新出票点的相关信息
            //修改订单状态和出票时间 - ChuPiaoTime - OrderStatus
            try {
                trainOrderOfflineDao.updateTrainOrderOfflineSuccessById(orderIdl, useridi);
            }
            catch (Exception e) {
                return ExceptionTCUtil.handleTCException(e);
            }

            //记录日志记录
            TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 1);//1 - 订单出票完成

            //更新票的状态
            //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
            //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
            trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 2, 2);

            //更新响应时间
            try {
                trainOrderOfflineRecordDao
                        .updateTrainOrderOfflineRecordResponseTimeByOrderIdAndProviderAgentid(orderIdl, useridi);
            }
            catch (Exception e) {
                return ExceptionTCUtil.handleTCException(e);
            }
        }
        else {
            String content = "携程_破解线下票出票回调请求失败";
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
            resResult.put("success", "false");
            resResult.put("msg", content);
        }
        return resResult;
    }
}
