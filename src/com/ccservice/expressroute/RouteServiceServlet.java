package com.ccservice.expressroute;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DataTable;
import com.callback.SendPostandGet;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.grabticket.util.MD5Util;
import com.ccservice.huamin.WriteLog;

public class RouteServiceServlet extends HttpServlet{

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        JSONObject resultJson = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        String jsonStr = request.getParameter("jsonStr");
        WriteLog.write("同城__获取快递路由信息", "------参数信息------" + jsonStr);
        JSONObject jsonStrJson = JSONObject.parseObject(jsonStr);
        String expressAgent = jsonStrJson.getString("expressAgent");
        String expressNum = jsonStrJson.getString("expressNum");
        if (expressAgent == null || "".equals(expressAgent) || expressNum == null || "".equals(expressNum)) {
            resultJson.put("msg", "业务参数缺失");
            resultJson.put("code", "102");
            resultJson.put("success", false);
            resultJson.put("traces", jsonArray);
            WriteLog.write("同城__获取快递路由信息", "------业务参数缺失------");
        }
        String partnerid = jsonStrJson.getString("partnerid");
        String sign = jsonStrJson.getString("sign");
        String reqtime = jsonStrJson.getString("reqtime");
        String search_partnerid = PropertyUtil.getValue("search_partnerid", "expressroute.properties");
        String search_key = PropertyUtil.getValue("search_key", "expressroute.properties");
        WriteLog.write("同城__获取快递路由信息", "partnerid：" + partnerid);
        WriteLog.write("同城__获取快递路由信息", "search_partnerid：" + search_partnerid + "search_key：" + search_key);
        if (resultJson.getString("code") == null) {
            if (partnerid == null || "".equals(partnerid) || !search_partnerid.equals(partnerid)) {
                resultJson.put("msg", "账户不存在");
                resultJson.put("code", "103");
                resultJson.put("success", false);
                resultJson.put("traces", jsonArray);
                WriteLog.write("同城__获取快递路由信息", "------账户不存在------");
            }
        }
        String search_sign = "";
        if (resultJson.getString("code") == null) {
            try {
                search_sign = MD5Util.MD5(search_partnerid + reqtime + MD5Util.MD5(search_key));
            } catch (Exception e) {
                resultJson.put("msg", "服务器错误");
                resultJson.put("code", "104");
                resultJson.put("success", false);
                resultJson.put("traces", jsonArray);
                WriteLog.write("同城__获取快递路由信息", "------服务器错误------");
            }
        }
        if (resultJson.getString("code") == null) {
            WriteLog.write("同城__获取快递路由信息", "search_sign：" + search_sign);
            if (sign == null || !sign.equals(search_sign)) {
                resultJson.put("msg", "签名失败");
                resultJson.put("code", "105");
                resultJson.put("success", false);
                resultJson.put("traces", jsonArray);
                WriteLog.write("同城__获取快递路由信息", "------签名失败------");
            }
        }
        resultJson.put("expressNum", expressNum);
        
        if (resultJson.getString("code") == null) {
            // SF
            if ("0".equals(expressAgent)) {
                String urlString = PropertyUtil.getValue("expressRouteUrl", "expressroute.properties");
                String param = "expressno=" + expressNum;
                String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
                WriteLog.write("同城__获取快递路由信息", "SFurlString:" + urlString + "-->param:" + param + "-->result:" + result);
                try {
                    Document document = DocumentHelper.parseText(result);
                    Element root = document.getRootElement();
                    Element head = root.element("Head");
                    Element body = root.element("Body");
                    Element routeResponse = body.element("RouteResponse");
                    WriteLog.write("同城__获取快递路由信息", "SF路由信息查询结果:" + root.elementText("Head") + "-->路由信息:" + routeResponse);
                    if ("OK".equals(root.elementText("Head")) && routeResponse != null) {
                        resultJson.put("msg", "路由信息获取成功");
                        resultJson.put("code", "100");
                        resultJson.put("success", true);
                        List elements = routeResponse.elements("Route");
                        if (elements.size() > 0) {
                            for (int i = 0; i < elements.size(); i++) {
                                Element route = (Element) elements.get(i);
                                JSONObject accept = new JSONObject();
                                String accept_time = route.attributeValue("accept_time");
                                String remark = route.attributeValue("remark");
                                accept.put("acceptTime", accept_time);
                                accept.put("acceptAddress", "");
                                accept.put("remark", remark);
                                jsonArray.add(accept);
                            }
                            WriteLog.write("同城__获取快递路由信息", "SF路由信息:" + jsonArray);
                        }
                    } else {
                        resultJson.put("msg", "路由信息获取失败");
                        resultJson.put("code", "101");
                        resultJson.put("success", false);
                        WriteLog.write("同城__获取快递路由信息", "------路由信息获取失败------");
                    }
                } catch (DocumentException e) {
                    resultJson.put("msg", "路由信息获取失败");
                    resultJson.put("code", "101");
                    resultJson.put("success", false);
                    WriteLog.write("同城__获取快递路由信息", "------路由信息获取失败------");
                }
                resultJson.put("traces", jsonArray);
                // EMS
            } else if ("1".equals(expressAgent)){
                String urlString = PropertyUtil.getValue("EMSexpressRouteUrl", "expressroute.properties") + "?mail_num=" + expressNum;
                String result = SendPostandGet.submitGet(urlString, "UTF-8");
                WriteLog.write("同城__获取快递路由信息", "EMSurlString:" + urlString + "-->result:" + result);
                JSONObject responseJSON = JSONObject.parseObject(result);
                String traces = responseJSON.getString("traces");
                JSONArray tracesJSON = JSONArray.parseArray(traces);
                WriteLog.write("同城__获取快递路由信息", "EMS路由信息查询结果:" + tracesJSON);
                if (tracesJSON == null || tracesJSON.size() == 0) {
                    resultJson.put("msg", "路由信息获取失败");
                    resultJson.put("code", "101");
                    resultJson.put("success", false);
                    WriteLog.write("同城__获取快递路由信息", "------路由信息获取失败------");
                } else {
                    resultJson.put("msg", "路由信息获取成功");
                    resultJson.put("code", "100");
                    resultJson.put("success", true);
                    for (int i = 0; i < tracesJSON.size(); i++) {
                        JSONObject acceptJSON = JSONObject.parseObject(tracesJSON.getString(i));
                        // 表示处理时间
                        String acceptTime = acceptJSON.getString("acceptTime");
                        // 表示处理城市
                        String acceptAddress = acceptJSON.getString("acceptAddress");
                        // 表示处理动作
                        String remark = acceptJSON.getString("remark");
                        JSONObject accept = new JSONObject();
                        accept.put("acceptTime", acceptTime);
                        accept.put("acceptAddress", acceptAddress);
                        accept.put("remark", remark);
                        jsonArray.add(accept);
                    }
                }
                resultJson.put("traces", jsonArray);
            } else if ("10".equals(expressAgent)){
                String sql = "select * from uupt_route where order_code = '"+ expressNum +"' ORDER BY acceptTime DESC";
                try {
                    DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
                    WriteLog.write("同城__获取快递路由信息", "UU跑腿路由信息查询结果：" + dataTable);
                    if (dataTable != null) {
                        resultJson.put("msg", "路由信息获取成功");
                        resultJson.put("code", "100");
                        resultJson.put("success", true);
                        for (DataRow dataRow : dataTable.GetRow()) {
                            // 表示处理时间
                            String acceptTime = dataRow.GetColumnString("acceptTime");
                            // 跑男姓名(跑男接单后)
                            String driver_name = dataRow.GetColumnString("driver_name");
                            // 跑男工号(跑男接单后)
                            String driver_jobnum = dataRow.GetColumnString("driver_jobnum");
                            // 跑男电话(跑男接单后)
                            String driver_mobile = dataRow.GetColumnString("driver_mobile");
                            // 当前状态
                            String state_text = dataRow.GetColumnString("state_text");
                            // 表示处理动作
                            String remark = "跑男姓名：" + driver_name + "；跑男工号：" + driver_jobnum + "；跑男电话：" + driver_mobile + "；当前状态：" + state_text;
                            JSONObject accept = new JSONObject();
                            accept.put("acceptTime", acceptTime);
                            accept.put("acceptAddress", "");
                            accept.put("remark", remark);
                            jsonArray.add(accept);
                        }
                    } else {
                        resultJson.put("msg", "路由信息获取失败");
                        resultJson.put("code", "101");
                        resultJson.put("success", false);
                        WriteLog.write("同城__获取快递路由信息", "------UU跑腿路由信息获取失败------");
                    }
                } catch (Exception e) {
                    WriteLog.write("同城__获取快递路由信息", "UU跑腿路由信息查询失败");
                }
                resultJson.put("traces", jsonArray);
            }
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(resultJson.toJSONString().getBytes(Charset.forName("UTF-8")));
    }
}
