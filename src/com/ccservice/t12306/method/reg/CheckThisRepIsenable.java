/**
 * 
 */
package com.ccservice.t12306.method.reg;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import com.ccservice.mobileCode.util.RequestUtil;
/**
 * 
 * @time 2015年8月27日 下午9:12:19
 * @author chendong
 */
public class CheckThisRepIsenable {
    public static void main(String[] args) {
        System.out.println(CheckThisRepIsenable.checkThisRepIsenable("0", "112.84.130.3", "80"));
    }

    /**
     * 检测这台rep是否可用
     * @time 2015年8月27日 下午2:08:49
     * @author chendong
     * @param proxyPort 
     * @param proxyHost 
     * @param useProxy 
     */
    public static String checkThisRepIsenable(String useProxy, String proxyHost, String proxyPort) {
        String CheckResult = "-1";
        try {
            String html = "-1";
            if ("1".equals(useProxy)) {
                Map<String, String> header = RequestUtil.getRequestHeader(true, false, false);
                html = RequestUtil.submitProxy("https://kyfw.12306.cn/otn/login/init", "", "post", "utf-8", header, 0,
                        proxyHost, Integer.parseInt(proxyPort));
                System.out.println(proxyHost + "==>" + html);
            }
            else {
                html = HttpsUtil.post12306("https://kyfw.12306.cn/otn/login/init", "", "UTF-8",
                        new HashMap<String, String>());
            }
            CheckResult = html;
        }
        catch (KeyManagementException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return CheckResult;
    }
}
