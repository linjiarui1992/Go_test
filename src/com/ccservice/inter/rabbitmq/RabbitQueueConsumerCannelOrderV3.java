package com.ccservice.inter.rabbitmq; 

import java.io.IOException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.train.thread.ThirdPartOrderUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 取消订单
 * 
 * @author 之至
 * @time 2016年12月29日 下午5:12:16
 */
public class RabbitQueueConsumerCannelOrderV3 extends RabbitMQCustomer{
    
    
    private String queueNameString;

    private int type;
    //消息内容
    private String orderNoticeResult = "";

    public RabbitQueueConsumerCannelOrderV3(String queueNameString, int type) throws IOException {
        super(queueNameString, type);
        this.queueNameString = queueNameString;
        this.type = type;
    }
    @Override
    public void run() {
        while (true) {
            try {
                orderNoticeResult=getNewMessageString(queueNameString);
                //确认消息已经收到.必须
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                if (!"0".equals(orderNoticeResult)) {
                    onMessage(orderNoticeResult);
                }
            }
            catch (ShutdownSignalException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("Rabbit取消订单MQ_ShutdownSignalException", e, ""
                        + orderNoticeResult);
            }
            catch (ConsumerCancelledException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("Rabbit取消订单MQ_ConsumerCancelledException", e, ""
                        + orderNoticeResult);
            }
            catch (IOException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("Rabbit取消订单MQ_IOException", e, ""
                        + orderNoticeResult);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("Rabbit取消订单MQ_InterruptedException", e, ""
                        + orderNoticeResult);
            }
            catch (Exception e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException("Rabbit取消订单MQ_Exception", e, ""
                        + orderNoticeResult);
            }
        }
    }
    
    public void onMessage(String message) {
        long l1 = System.currentTimeMillis();
        TrainCreateOrderSupplyMethod TrainCreateOrderSupplyMethod = new TrainCreateOrderSupplyMethod();
        WriteLog.write("Rabbit取消订单MQ", "进入消费者取消~~");
        Customeruser use = new Customeruser();
        boolean cancelIsTrue = false;
        String orderNoticeResult = message;
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "MQ传送值：" + orderNoticeResult);
        JSONObject jsonObject = new JSONObject();
        jsonObject = JSONObject.parseObject(orderNoticeResult);
        String extnumber = jsonObject.containsKey("extnumber") ? jsonObject.getString("extnumber") : "";
        String trainorderid = jsonObject.containsKey("trainorderid") ? jsonObject.getString("trainorderid") : "0";
        //            String freeAccount = jsonObject.containsKey("freeAccount") ? jsonObject.getString("freeAccount") : "false";
        long orderid = Long.parseLong(trainorderid);
        //如果为第三方订单，发第三方取消占座mq
        if(ThirdPartOrderUtil.handleThirdPartOrder(orderid)){
        	return;
        }
        Trainorder order = Server.getInstance().getTrainService().findTrainorder(orderid);
        String result = "";
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "MQ订单号：" + trainorderid + ",查询订单号：" + order.getId());
        use = TrainCreateOrderSupplyMethod.getCustomerUserBy12306Account(order, false);//根据订单获取账号
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "查询订单号：" + order.getSupplyaccount() + "账号获取：" + use.getLoginname());
        int i = 0;
        do {
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单参数：票号:" + extnumber + ",订单号:" + orderid);
            result = new TrainCreateOrderSupplyMethod().cancel12306Order(use, extnumber, orderid, false);
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单结果:" + result);
            if (result.contains("取消订单成功") || "无未支付订单".equals(result)) {
                cancelIsTrue = true;
            }
            i++;
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + trainorderid + "取消失败，消费者取消循环次数：" + i + "--->取消,返回:" + result);
        }
        while (!cancelIsTrue && i < 5);
        //释放账号
        TrainCreateOrderSupplyMethod.freeCustomeruser(use, cancelIsTrue ? AccountSystem.FreeNoCare
                : AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                AccountSystem.NullDepartTime);
    }
}
