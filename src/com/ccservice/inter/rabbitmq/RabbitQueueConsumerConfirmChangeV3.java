package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrderPaiDui;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
//import com.ccservice.qunar.util.TimeUtil;
import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 确认改签 真
 * 
 * @author 之至
 * @time 2016年12月28日 上午10:38:18
 */
public class RabbitQueueConsumerConfirmChangeV3  extends RabbitMQDefaultCustomer implements IConsumer {//extends RabbitMQCustomer {
 
    //队列名称
    private String queueNameString;

    private long changeId;

    //平台类型
    private int type;

    //消息内容
    private String orderNoticeResult = "";

    public RabbitQueueConsumerConfirmChangeV3(String queueNameString, int type) throws IOException {
//        super(queueNameString, type);
//        this.queueNameString = queueNameString;
//        this.type = type;
    }

    /**
     * 确认改签
     * @param name
     * @param pingtaiType
     * @param host
     * @param username
     * @param password
     * @throws IOException
     */
    public RabbitQueueConsumerConfirmChangeV3(String name, String pingtaiType, String host, String username, String password)
            throws IOException {
        super(name, pingtaiType, host, username, password);
    }

    public long tempTime;
    public String logName = this.getClass().getSimpleName();
    @Override
    public void run() {
        UUID uuid = UUID.randomUUID(); 
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        this.logName = this.getClass().getSimpleName();
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) { 
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            if(!super.connection.isOpen()){//如果连接已经关闭了就关闭
                willClose = true;
                WriteLog.write(this.logName + "-Exception", "连接关了");
            }
            else {
                this.tempTime = System.currentTimeMillis();
                if (!isCanOrdering(new Date())) {//是否可以下单 
                    try {
                        sleep(1000L);//如果不能下单休息1秒
                    }
                    catch (Exception e) {
                    }
                    continue;
                }
                String msg = "";
                String orderNoticeResult = "";
                try {
                    msg = getMsgNew();//【第四步】:获取消息
                    orderNoticeResult = msg;//消息内容
                    System.out.println(TimeUtil.gettodaydate(5) + ":接收到消息:" + orderNoticeResult);
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + orderNoticeResult + "】");
                    willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + willClose + "】");
                    AckMsg();//【第五步】:确认消息，已经收到
                }
                catch (ShutdownSignalException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (ConsumerCancelledException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                
            }
        }
    }
    
    
    public void runback() {
        try {
            try {
                orderNoticeResult = getNewMessageString(queueNameString);
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            }
            catch (Exception e) {
                channel.basicReject(delivery.getEnvelope().getDeliveryTag(), false);
                e.printStackTrace();
            }
            
        }
        catch (ShutdownSignalException e1) {
            e1.printStackTrace();
            ExceptionUtil.writelogByException(queueNameString + "Exception", e1, "ShutdownSignalException处理体异常");
        }
        catch (ConsumerCancelledException e1) {
            e1.printStackTrace();
            ExceptionUtil.writelogByException(queueNameString + "Exception", e1, "ConsumerCancelledException处理体异常");
        }
        catch (Exception e1) {
            e1.printStackTrace();
            ExceptionUtil.writelogByException(queueNameString + "Exception", e1, "Exception处理体异常");
        }finally{
        }
    }

    private void RepOperate() {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            return;
        }
        //状态判断
        int tcstatus = trainOrderChange.getTcstatus();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        float changePrice = trainOrderChange.getTcprice();
        //非等待下单
        if (tcstatus != Trainorderchange.CHANGEWAITPAY || isQuestionChange != 0 || changePrice <= 0) {
            WriteLog.write("重复进入改签确认队列", random + ":改签ID:" + changeId);
            return;
        }
        //更新改签
        int C_TCSTATUS = Trainorderchange.CHANGEPAYING;
        int C_STATUS12306 = Trainorderchange.ORDEREDPAYING;
        String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = " + Trainorderchange.CHANGEWAITPAY;
        //更新结果
        int updateResult = Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        //更新失败
        if (updateResult != 1) {
            WriteLog.write("RabbitMQ改签占座队列异常", random + ":改签ID:" + changeId + ":更新改签:" + updateResult);
            return;
        }
        //异步确认
        Server.getInstance().getTrain12306Service().AsyncChangeConfirm(trainOrderChange);
    }
    
    
    
    private void QueueConsumerConfirmChangeMethod(String msgInfo) {
        System.out.println(this.tempTime + "Rabbit确认改签  接收 ：————————>" + msgInfo);
        this.changeId = Long.valueOf(msgInfo);
        //确认改签操作
        if (changeId > 0) {
            RepOperate();
        }}
    
    @Override
    public boolean processTheMessage(String msgInfo) {
        boolean willClose = false;//是否即将关闭
        try {
            QueueConsumerConfirmChangeMethod(msgInfo);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }
}
