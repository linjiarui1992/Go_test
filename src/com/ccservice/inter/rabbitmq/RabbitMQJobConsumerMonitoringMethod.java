package com.ccservice.inter.rabbitmq;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.rabbitmq.monitoring.MonitoringRabbitMQCustomer;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
 
/**
 * 根据不同的消费者  有不同的实现
 * 
 * @author 之至
 * @time 2016年12月21日 下午1:02:04
 */
public class RabbitMQJobConsumerMonitoringMethod extends RabbitMQCustomer { 
    private String strTypeString = "";//消费者类型

    //短信发送内容
    String smsContentString = ""; 
 
    //上一次维护消费者时间
    private String mainTainConsumerLastTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());//TimeUtil.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");

    //是否可以维护消费者数量
    private boolean mainTainConsumerNum = true;

    /**
     * 当前tomcat中实际的下单消费者数量 
     */
    private int RabbitWaitOrder_nums = 0;//下单消费者

    private int QueueMQ_trainorder_waitorder_orderid_PaiDui_nums = 0;

    private int Rabbit_QueueMQ_QueryOrder_nums = 0;

    private int Rabbit_QueueMQ_Deduction_nums = 0;

    private int changeWaitOrder_nums = 0;

    private int changeConfirmOrder_nums = 0;

    private int changePayExamine_nums = 0;

    private int Rabbit_ChangeOrder_PaiDui_nums = 0;

    private int QueueMQ_TrainorderDeductionGq_nums = 0;

    private int QueueMQ_CancelOrder_nums = 0;

    private int TB_Change_Order_nums = 0;

    private int QueueMQ_TrainTicket_RefundTicket_nums = 0;

    private int QueueMQ_12306AccountSystem_ExcuteUpdate_nums = 0;

    public void run() {
        int minute = 1;
        try {
            System.out.println("消费者监控程序:先睡【"+minute+"】分钟");
            Thread.sleep(1000 * 60 * minute);
            while (true) {
                if (isCanOrdering()) {//是否可以下单 06:00:00-07:01:30不能下单
                    //初始化消费者数量
                    Init();
                    //当前消费者数量
                    getConsumerNum();
                    //根据数量进行操作
                    decideConsumerNums();
                }
                //一分钟检测一次
                Thread.sleep(1000 * 60 * 1);
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQJobConsumerMonitoringMethod_Exception", e, "run");
        }

    }

    /**
     * 
     * @author 之至
     * @time 2016年12月22日 下午1:08:18
     * @Description 每次循环初始化数据
     */
    private void Init() {
        RabbitWaitOrder_nums = 0;

        QueueMQ_trainorder_waitorder_orderid_PaiDui_nums = 0;

        Rabbit_QueueMQ_QueryOrder_nums = 0;

        Rabbit_QueueMQ_Deduction_nums = 0;

        changeWaitOrder_nums = 0;

        changeConfirmOrder_nums = 0;

        changePayExamine_nums = 0;

        Rabbit_ChangeOrder_PaiDui_nums = 0;

        QueueMQ_TrainorderDeductionGq_nums = 0;

        QueueMQ_CancelOrder_nums = 0;

        TB_Change_Order_nums = 0;

        QueueMQ_TrainTicket_RefundTicket_nums = 0;

        QueueMQ_12306AccountSystem_ExcuteUpdate_nums = 0;
        //每隔二十四小时维护一次消费者
        getMainTainConsumerNumType();
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月22日 下午7:13:06
     * @Description 每隔二十四小时，维护一次状态
     */
    private void getMainTainConsumerNumType() {
        long lastTime = 0;
        try {
            lastTime = TimeUtil.stringToLong(mainTainConsumerLastTime, TimeUtil.yyyyMMddHHmmss);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        long nowTime = TimeUtil.dateToLong(new Date());
        long hour = ((nowTime - lastTime) / (1000 * 60 * 60));
        if (hour > 12) {
            mainTainConsumerNum = true;
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午2:12:19
     * @Description 判断当前tomcat下的消费者数量  多退少补
     * 
     */
    @SuppressWarnings("rawtypes")
    private void decideConsumerNums() {
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁消费者编号
        String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");//平台类型1空铁 2同程
        try {
            if (RabbitWaitOrder_nums > 0) {//下单消费者大于0进行操作
               new MonitoringRabbitMQCustomer().waitOrderMethod(kt110,pingTaiType,PublicConnectionInfos.RABBITWAITORDER);
            }
            if (QueueMQ_trainorder_waitorder_orderid_PaiDui_nums > 0) {
            }
            if (Rabbit_QueueMQ_QueryOrder_nums > 0) {
            }
            if (Rabbit_QueueMQ_Deduction_nums > 0) {
            }
            if (changeWaitOrder_nums > 0) {
            }
            if (changeConfirmOrder_nums > 0) {
            }
            if (changePayExamine_nums > 0) {
            }
            if (Rabbit_ChangeOrder_PaiDui_nums > 0) {
            }
            if (QueueMQ_TrainorderDeductionGq_nums > 0) {
            }
            if (QueueMQ_CancelOrder_nums > 0) {
            }
            if (TB_Change_Order_nums > 0) {
            }
            if (QueueMQ_TrainTicket_RefundTicket_nums > 0) {
            }
            if (QueueMQ_12306AccountSystem_ExcuteUpdate_nums > 0) {
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("RabbitMQJobConsumerMonitoringMethod_Exception", e, "");
        }
    }
    /**
     * 消费者监控的方法
     * 
     * @param kt110 消费者编号
     * @param pingTaiType 1=空铁;2=同程
     * @time 2017年1月9日 下午11:22:26
     * @author chen
     * @param ConsumerType 要监控的 消费者队列的名字。消费者类型,下单消费者, 
     */
    private void MonitoringConsumer_back(String kt110, String pingTaiType, String ConsumerType) {
        String Status = "";//当前消费者状态0关1开,
        int DB_ConsumerNum = 0;//数据库配置的默认数量//默认数量
        int DB_ConsumerNumReal = 0;//当前消费者数量当前真实数量
        List list = null;//getDbListConsumerData(kt110,pingTaiType);//获取库中所存数据，依次：状态,默认数量真实数量
        if (list.size() > 0) {
            Map map3 = (Map) list.get(0);
            Status = map3.containsKey("Status") ? map3.get("Status").toString() : "1";
            String ConsumerNumString = map3.containsKey("ConsumerNum") ? map3.get("ConsumerNum").toString() : "0";//数据库里配置的消费者的个数
            String ConsumerNumRealString = map3.containsKey("ConsumerNumReal") ? map3.get("ConsumerNumReal").toString()
                    : "0";
            DB_ConsumerNum = Integer.valueOf(ConsumerNumString);//数据库里配置的消费者的个数
            DB_ConsumerNumReal = Integer.valueOf(ConsumerNumRealString);//数据库里当前真实的消费者个数
        }//数据查询完成
        int ThreadConsumerNumReal = getConsumerNum(ConsumerType);//获取到真实的消费者的数量
        System.out.println("当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + ":当前线程消费者数量-->"
                + ThreadConsumerNumReal + ":消费者默认数量:" + DB_ConsumerNum);
        strTypeString = PublicConnectionInfos.getstrTypeNameStringByConsumerType(ConsumerType);//获取消费者的类型名字
        if (DB_ConsumerNum == 0 && DB_ConsumerNumReal == 0) {//如果都为0，那就修改为默认的数量
            DB_ConsumerNum = PublicConnectionInfos.RabbitWaitOrder_V;//默认消费者数
        }
        else if (RabbitWaitOrder_nums != DB_ConsumerNumReal) {//如果当前真实的和数据库真实的消费者数不一致,就维护数据库中消费者数量    1m
            String sqlString = "update TrainPingTai set ConsumerNumReal = " + RabbitWaitOrder_nums + " where PingTai = "
                    + pingTaiType + " and type= " + kt110;
            Server.getInstance().getSystemService().findMapResultBySql(sqlString, null);
        }
        //当前消费者的状态为1 并且 当前消费者数量小于默认值增加
        if ("1".equals(Status) && ThreadConsumerNumReal < DB_ConsumerNum) {
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums",
                    "当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->系统配置消费者数量-->"
                            + DB_ConsumerNum + "--->当前消费者数量-->" + ListcustomerNames.size());
            int poor = (DB_ConsumerNum - RabbitWaitOrder_nums);//还差多少个消费者
            for (int i = 0; i < poor; i++) {
                try {
                    int type = 1;//平台   1空鐵   2同程
                    type = Integer.parseInt(pingTaiType);//
                    new RabbitQueueConsumerCreateOrderV3(PublicConnectionInfos.RABBITWAITORDER, type).start();//增加一个对应的消费者
                }
                catch (Exception e) {
                    ExceptionUtil
                            .writelogByException("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums_Exception", e);
                }
            }
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums",
                    "当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")"
                            + "--->添加完毕--->系统配置消费者数量-->" + DB_ConsumerNum + "--->当前消费者数量-->" + ListcustomerNames.size());
            System.out.println(
                    "当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "当前消费者数量 -->   " + ListcustomerNames.size());
        }
        //如果消费者数量大于默认数量且在消费者的数量维护时间内,且每日有且只维护一次
        else if (mainTainConsumerNum && ThreadConsumerNumReal > DB_ConsumerNum) {//大于了
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums",
                    "当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->系统配置消费者数量-->"
                            + DB_ConsumerNum + "--->当前消费者数量-->" + RabbitWaitOrder_nums);
            int numDifferent = (RabbitWaitOrder_nums - DB_ConsumerNum);//实际的减去默认的，就是比设置的多的那一部分消费者
            while (true) {
                for (int i = 0; i < numDifferent; i++) {//开始减消费者
                    if (Mapcustomers.get(ListcustomerNames.get(i)).getKey().equals(ListcustomerNames.get(i))) {
                        Mapcustomers.get(ListcustomerNames.get(i)).setUse(false);
                    }
                    if (getRabbitWaitOrderTrue_nums() == DB_ConsumerNum) {
                        break;
                    }
                }
                mainTainConsumerNum = false;
                mainTainConsumerLastTime = TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss);
                break;
            }
            WriteLog.write("RabbitMQJobConsumerMonitoringMethod_decideConsumerNums",
                    "当前队列名称--->" + PublicConnectionInfos.RABBITWAITORDER + "(" + kt110 + ")" + "--->修改完毕");
        }
        toSendSms(pingTaiType,kt110);//操作发送短信的方法
    }
    

    /**
     * 操作发送短信的方法
     * @param kt110 消费者编号
     * @param pingTaiType 1=空铁;2=同程
     * @time 2017年2月10日 下午5:13:57
     * @author chen
     */
    private void toSendSms(String pingTaiType, String kt110) {
      //短信提示每3分钟发送一次
        if (timeFrame(smsTimeString, TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss), 3)) {
            //不懂为什么要有大于10这个概念
            if (Mapcustomers.size() < 60/* && ListcustomerNames.size() > 10*/) {
                if (smsNumString == 0) {
                    String pingtaiName = "1".equals(pingTaiType) ? "空铁" : "同程";
                    smsContentString = pingtaiName + ":第" + kt110 + "号" + strTypeString + "，消费者剩余" + Mapcustomers.size()
                            + "实际应为  " + PublicConnectionInfos.RabbitWaitOrder_V;
                    WriteLog.write("Rabbit_QueueMQ_短信提醒", TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss)
                            + "发送短信详情 -- " + smsContentString);
                    sendSMS(smsContentString);
                    smsNumString = 1;
                    smsTimeString = TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss);
                }
            }
        }
        else {
            //超过3分钟短信数
            WriteLog.write("Rabbit_QueueMQ_短信提醒",
                    TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + "发送短信详情 -- " + smsContentString);
            smsNumString = 0;
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月22日 下午4:46:11
     * @Description 获取正在使用中的消费者的数量
     * @return
     */
    private int getRabbitWaitOrderTrue_nums() {
        int num = 0;
        for (int i = 0; i < ListcustomerNames.size(); i++) {
            if (Mapcustomers.get(ListcustomerNames.get(i)).getKey().equals(ListcustomerNames.get(i))) {
                if (Mapcustomers.get(ListcustomerNames.get(i)).isUse()) {
                    num++;
                }
            }
        }
        return num;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午1:44:31
     * @Description 获取当前各个消费者数量
     */
    private void getConsumerNum() {
        //当前消费者数量
        int num = ListcustomerNames.size();
        for (int i = 0; i < num; i++) {
            String consumerNameString = ListcustomerNames.get(i);
            if (Mapcustomers.get(consumerNameString).getKey().equals(consumerNameString)) {
                if (Mapcustomers.get(ListcustomerNames.get(i)).isUse()) {
                    int type = Mapcustomers.get(consumerNameString).getType();
                    getIntTypeNum(type);
                }
            }
        }
        System.out.println("下单消费者数量:" + RabbitWaitOrder_nums);
    }
    
    /**
     * 根据消费者的名字获取到消费者的数量
     * @author 之至
     * @time 2016年12月21日 下午1:44:31
     * @Description 获取当前各个消费者数量
     */
    private int getConsumerNum(String ConsumerType) {
        int customerCount =0 ;
        int num = ListcustomerNames.size();
        for (int i = 0; i < num; i++) {
            String consumerNameString = ListcustomerNames.get(i);
            if (Mapcustomers.get(consumerNameString).getKey().equals(ConsumerType)) {
                if (Mapcustomers.get(ListcustomerNames.get(i)).isUse()) {
                    int type = Mapcustomers.get(consumerNameString).getType();
                    getIntTypeNum(type);
                }
            }
        }
        return customerCount;
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午1:46:48
     * @Description  匹配计数
     * @param type  类型
     * @return
     */
    private void getIntTypeNum(int type) {
        if (type == 1) {
            RabbitWaitOrder_nums++;
        }
        else if (type == 2) {
            QueueMQ_trainorder_waitorder_orderid_PaiDui_nums++;
        }
        else if (type == 3) {
            Rabbit_QueueMQ_QueryOrder_nums++;
        }
        else if (type == 4) {
            Rabbit_QueueMQ_Deduction_nums++;
        }
        else if (type == 5) {
            changeWaitOrder_nums++;
        }
        else if (type == 6) {
            changeConfirmOrder_nums++;
        }
        else if (type == 7) {
            changePayExamine_nums++;
        }
        else if (type == 8) {
            Rabbit_ChangeOrder_PaiDui_nums++;
        }
        else if (type == 9) {
            QueueMQ_TrainorderDeductionGq_nums++;
        }
        else if (type == 10) {
            QueueMQ_CancelOrder_nums++;
        }
        else if (type == 11) {
            TB_Change_Order_nums++;
        }
        else if (type == 12) {
            QueueMQ_TrainTicket_RefundTicket_nums++;
        }
        else if (type == 13) {
            QueueMQ_12306AccountSystem_ExcuteUpdate_nums++;
        }
    }

    /**
     * 是否可以下单 06:00:00-07:01:30不能下单
     * @param date
     * @return
     * @time 2015年4月13日 下午9:58:34
     */
    public static boolean isCanOrdering() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try { 
            Date dateBefor = df.parse("00:00:00");
            Date dateAfter = df.parse("06:01:30");
            Date time = df.parse(df.format(new Date()));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                WriteLog.write("Rabbit_开始自动下单_error", "当前时间不能下单");
                return false;
            }
        }
        catch (ParseException e) {
            WriteLog.write("Rabbit_开始自动下单_Exception", "获取时间出错");
        }
        return true;
    }
}
