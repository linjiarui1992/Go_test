package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrder;
import com.ccservice.inter.server.Server;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.rabbitmq.util.RabbitMQUtil;
/**
 * 下单消费者
 * 
 * @author 之至
 * @time 2016年12月16日 下午4:58:02
 */
public class RabbitQueueConsumerCreateOrder extends QueueConsumer {
    public RabbitQueueConsumerCreateOrder(String endPointName) throws IOException {
        super(endPointName);
    }

    private long orderid;

    public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body) throws IOException {
        String orderNoticeResult="";
        try {
            
            Map<?, ?> map = (HashMap<?, ?>) SerializationUtils.deserialize(body);
            orderNoticeResult = map.get("message number").toString();
            WriteLog.write("Rabbit_开始自动下单", System.currentTimeMillis() + ":orderNoticeResult:" + orderNoticeResult);
            if (isCanOrdering(new Date())) {
                try {
                    this.orderid = Long.parseLong(orderNoticeResult);
                }
                catch (NumberFormatException e) {
                    //同程3151
                    try {
                        //JSON格式
                        JSONObject json = JSONObject.parseObject(orderNoticeResult);
                        //JSON数据正确
                        if (json != null && json.containsKey("orderId")) {
                            //订单ID
                            orderid = json.getLongValue("orderId");
                            //ID正确
                            if (orderid > 0) {
                                //JSON转换
                                Customeruser user = json.getObject("customeruser", Customeruser.class);
                                //开始下单
                                new TrainCreateOrder(orderid).createOrderStart(user, true);
                            }
                            //中断返回
                            return;
                        }
                    }
                    catch (Exception E) {
                        this.orderid = 0;
                    }
                }
                //空铁下蛋蛋
                int t = new Random().nextInt(10000);
                if (this.orderid > 0) {
                    new TrainCreateOrder(this.orderid).createOrderStart(new Customeruser(), false);
                    String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");
                    String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
                    List list = Server.getInstance().getSystemService().findMapResultBySql("select Status from [TrainPingTai] with(nolock)  where PingTai ="+pingTaiType+"and type ="+kt110, null);
                    Map map1=(Map)list.get(0);
                    String StatusString= map1.containsKey("Status")?map1.get("Status").toString():"";
                    System.out.println("当前时间"+new Date()+ "订单号 "+orderid+" 当前状态"+StatusString);
                    if (!"".equals(StatusString)&&"0".equals(StatusString)) {
                        rmcb.getChannel().close();
                        rmcb.getConnection().close();
                        System.out.println("关闭消费者-----------");
                        return;
                    }
                }
                WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", t + ":" + orderNoticeResult);
            }
            else {
                try {
                    Thread.sleep(7000L);
                }

                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String QUEUE_NAME = PropertyUtil.getValue("Rabbit_QueueMQ_trainorder_waitorder_orderid",
                        "rabbitMQ.properties");
                try {
                    RabbitMQUtil.sendOnemessage(Long.parseLong(orderNoticeResult), QUEUE_NAME);
                    WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单");
                }
                catch (Exception e) {
                    WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult + "--->重新下单异常");
                    try {
                        RabbitMQUtil.sendOnemessage(Long.parseLong(orderNoticeResult), QUEUE_NAME);
                        WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult
                                + "--->重新下单");
                    }
                    catch (Exception e1) {
                        WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult
                                + "--->重新下单异常");
                    }
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", 
                    e.toString()+"订单号   ：   "+ orderNoticeResult);
            String QUEUE_NAME = PropertyUtil.getValue("Rabbit_QueueMQ_trainorder_waitorder_orderid",
                    "rabbitMQ.properties");
            try {
                RabbitMQUtil.sendOnemessage(Long.parseLong(orderNoticeResult), QUEUE_NAME);
                WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult
                        + "--->重新下单");
            }
            catch (Exception e1) {
                WriteLog.write("Rabbit_12306_TrainCreateOrderMessageListener_MQ", orderNoticeResult
                        + "--->重新下单异常");
            }
        }
    }
    
    /**
     * 是否可以下单 06:00:00-07:01:30不能下单
     * @param date
     * @return
     * @time 2015年4月13日 下午9:58:34
     * @author fiend
     */
    private boolean isCanOrdering(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("05:00:00");
            Date dateAfter = df.parse("06:01:30");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                WriteLog.write("Rabbit_开始自动下单_error", "当前时间不能下单");
                return false;
            }
        }
        catch (ParseException e) {
            WriteLog.write("Rabbit_开始自动下单_Exception", "获取时间出错");
        }
        return true;
    }

}
