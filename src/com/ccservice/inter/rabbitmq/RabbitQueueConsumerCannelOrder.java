package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.TextMessage;

import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.train.thread.ThirdPartOrderUtil;
import com.ccservice.inter.job.train.thread.TrainCreateOrderSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
//import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.AMQP.BasicProperties;
//import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 取消订单实现rabbit
 * 
 * @author 之至
 * @time 2016年11月19日 下午5:39:07
 */
public class RabbitQueueConsumerCannelOrder extends RabbitMQDefaultCustomer implements IConsumer {//  extends QueueConsumer implements Runnable, Consumer {

    public RabbitQueueConsumerCannelOrder(String endPointName) throws IOException {
        //        super(endPointName);
    }

    /**
     * 取消订单
     * @param name
     * @param pingtaiType
     * @param host
     * @param username
     * @param password
     * @throws IOException
     */
    public RabbitQueueConsumerCannelOrder(String name, String pingtaiType, String host, String username, String password)
            throws IOException {
        super(name, pingtaiType, host, username, password);
    }

    public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body) throws IOException {
        Map<?, ?> map = (HashMap<?, ?>) SerializationUtils.deserialize(body);
        System.out.println("取消订单  接收 ：————————>" + map.get("message number").toString());
        onMessage(map.get("message number").toString());

    }

    public long tempTime;

    public String logName = this.getClass().getSimpleName();

    @Override
    public void run() {
        UUID uuid = UUID.randomUUID();
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        this.logName = this.getClass().getSimpleName();
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            this.tempTime = System.currentTimeMillis();
            if (!isCanOrdering(new Date())) {//是否可以下单 
                try {
                    sleep(1000L);//如果不能下单休息1秒
                }
                catch (Exception e) {
                }
                continue;
            }
            String msg = "";
            String orderNoticeResult = "";
            try {
                msg = getMsgNew();//【第四步】:获取消息
                orderNoticeResult = msg;//消息内容
                System.out.println(TimeUtil.gettodaydate(5) + ":接收到消息:" + orderNoticeResult);
                WriteLog.write(this.logName, this.tempTime + ":" + ":【" + orderNoticeResult + "】");
                willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                WriteLog.write(this.logName, this.tempTime + ":" + ":【" + willClose + "】");
                AckMsg();//【第五步】:确认消息，已经收到
            }
            catch (ShutdownSignalException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                willClose = true;
            }
            catch (ConsumerCancelledException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                willClose = true;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                willClose = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                willClose = true;
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void onMessage(String message) {
        long l1 = System.currentTimeMillis();
        TrainCreateOrderSupplyMethod TrainCreateOrderSupplyMethod = new TrainCreateOrderSupplyMethod();
        WriteLog.write("Rabbit取消订单MQ", "进入消费者取消~~");
        Customeruser use = new Customeruser();
        boolean cancelIsTrue = false;
        String orderNoticeResult = message;
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "MQ传送值：" + orderNoticeResult);
        JSONObject jsonObject = new JSONObject();
        jsonObject = JSONObject.parseObject(orderNoticeResult);
        String extnumber = jsonObject.containsKey("extnumber") ? jsonObject.getString("extnumber") : "";
        String trainorderid = jsonObject.containsKey("trainorderid") ? jsonObject.getString("trainorderid") : "0";
        //        String twoFree = jsonObject.containsKey("TwoFree") ? jsonObject.getString("TwoFree") : "";//判断释放账号次数
        String freeAccount = jsonObject.containsKey("freeAccount") ? jsonObject.getString("freeAccount") : "1";
        long orderid = Long.parseLong(trainorderid);
        //如果为第三方订单，发第三方取消占座mq
        //        if (ThirdPartOrderUtil.handleThirdPartOrder(orderid)) {
        //            return;
        //        }
        Trainorder order = Server.getInstance().getTrainService().findTrainorder(orderid);
        String result = "";
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "MQ订单号：" + trainorderid + ",查询订单号：" + order.getId());
        use = TrainCreateOrderSupplyMethod.getCustomerUserBy12306Account(order, false);//根据订单获取账号
        WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "查询订单号：" + order.getSupplyaccount() + "账号获取：" + use.getLoginname());
        int i = 0;
        do {
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单参数：票号:" + extnumber + ",订单号:" + orderid);
            result = new TrainCreateOrderSupplyMethod().cancel12306Order(use, extnumber, orderid, false);
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + "取消订单结果:" + result);
            if (result.contains("取消订单成功") || "无未支付订单".equals(result)) {
                cancelIsTrue = true;
            }
            i++;
            WriteLog.write("Rabbit取消订单MQ", l1 + ":" + trainorderid + "取消失败，消费者取消循环次数：" + i + "--->取消,返回:" + result);
        }
        while (!cancelIsTrue && i < 5);
        if ("2".equals(freeAccount)) {
            //释放账号 2次
            TrainCreateOrderSupplyMethod.freeCustomeruser(use, cancelIsTrue ? AccountSystem.FreeNoCare
                    : AccountSystem.FreeCurrent, AccountSystem.TwoFree, AccountSystem.OneCancel,
                    AccountSystem.NullDepartTime);
        }
        else {
            //释放账号 默认 1次
            TrainCreateOrderSupplyMethod.freeCustomeruser(use, cancelIsTrue ? AccountSystem.FreeNoCare
                    : AccountSystem.FreeCurrent, AccountSystem.OneFree, AccountSystem.OneCancel,
                    AccountSystem.NullDepartTime);
        }
    }

    @Override
    public boolean processTheMessage(String msgInfo) {
        boolean willClose = false;//是否即将关闭
        try {
            onMessage(msgInfo);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }
}
