package com.ccservice.inter.rabbitmq;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.JobQueryMyOrder;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 
 * @author zlx
 * @time 2016年11月18日 上午9:20:01
 * @Description TODO
 *美团云MQ 审核队列
 */
public class RabbitQueueMQ_QueryOrder extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        super.init();
        System.out.println("Rabbit审核队列MQ--------开启");
        queryNtice();
    }

    int threadNum = 0;

    private void queryNtice() {
        //        String queueName = PropertyUtil.getValue("Rabbit_QueueMQ_QueryOrder", "rabbitMQ.properties");//审核消费者队列名字
        //        int consumerType = PublicConnectionInfos.getConsumerType(queueName);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        //        String pingtaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");////平台类型
        //        RabbitmqBean rabbitmqBean = new MonitoringSupply().getRabbitmqBean();//获取当前使用的哪个mq服务的对象
        //        if (rabbitmqBean != null) {
        //            MonitoringSupply monitoringSupply = new MonitoringSupply();
        //            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
        //            for (int i = 0; i < threadNum; i++) {
        //                //                RabbitQueueConsumerQueryOrderV3 consumerQueryOrder = new RabbitQueueConsumerQueryOrderV3(queryName,type);
        //                //                consumerQueryOrder.start();
        //                monitoringSupply.newRabbitQueueConsumerQueryOrderByHost(queueName, pingtaiType, rabbitmqBean);//开启审核
        //            }
        //            //监控开始
        //            String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的消费者编号
        //            new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启消费者监控
        //            //监控结束
        //        }

        System.out.println("审核消费者队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER);//消费者类型0代表测试 1-13代表自己的业务 3代表审核消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("审核消费者队列【" + PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER + "】【consumerType:"
                + consumerType + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {

                    new RabbitQueueConsumer(PublicConnectionInfos.RABBIT_QUEUEMQ_QUERYORDER, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitQueueMQ_QueryOrder_log") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            JSONObject jsonobject = JSONObject.parseObject(notice);
                            long orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
                            long intotime = jsonobject.containsKey("intotime") ? jsonobject.getLongValue("intotime")
                                    : 0;
                            long successtime = jsonobject.containsKey("successtime") ? jsonobject
                                    .getLongValue("successtime") : 0;
                            int queryno = jsonobject.containsKey("queryno") ? jsonobject.getIntValue("queryno") : 0;
                            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
                            WriteLog.write("Rabbit_QueueMQ_QueryOrder", "orderid:" + orderid + "type:" + type);
                            if (MQMethod.QUERY == type) {
                                //系统配置的审核间隔时间
                                new JobQueryMyOrder().gotoQuery(orderid, successtime, queryno);
                            }
                            return notice + "---> 处理审核订单";
                        }
                    }.start();
                }
            }

        }.start();
        //监控结束

    }

    /**
     * 之前的审核做备份
     * 
     * @time 2017年3月2日 下午7:03:18
     * @author chendong
     */
    private void queryNtice_back20170302() {
        String queryName = PropertyUtil.getValue("Rabbit_QueueMQ_QueryOrder", "rabbitMQ.properties");
        //平台类型
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                RabbitQueueConsumerQueryOrderV3 consumerQueryOrder = new RabbitQueueConsumerQueryOrderV3(queryName,
                        type);
                consumerQueryOrder.start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
