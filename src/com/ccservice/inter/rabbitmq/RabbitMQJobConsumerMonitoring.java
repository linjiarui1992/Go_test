package com.ccservice.inter.rabbitmq;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 
 * rabbitMQ   消费者实时监控
 * @author 之至
 * @time 2016年12月21日 下午12:02:53
 */
public class RabbitMQJobConsumerMonitoring extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void init() throws ServletException {
        super.init();
        orderNtice();
    }
	
	public void orderNtice(){
	    String start="";
	    try {
	        start=getInitParameter("start");
        }
        catch (Exception e) {
            e.printStackTrace();
            start="0";
        }
	    if (!"".equals(start)&&"1".equals(start)) {
	        System.out.println("RabbitMQ消费者监控-----开启");
	        new RabbitMQJobConsumerMonitoringMethod().start();//老的监控程序
	        
	        
        }else {
            System.out.println("RabbitMQ消费者监控-----关闭");
        }
	    
	}
}
