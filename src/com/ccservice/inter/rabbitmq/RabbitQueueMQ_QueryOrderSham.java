package com.ccservice.inter.rabbitmq;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.ccservice.Util.file.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.thread.MyThreadQuerySham;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 假支付消费者
 * （原审核消费者逻辑）
 * 
 * @parameter
 * @time 2018年3月22日 上午10:00:47
 * @author YangFan
 */
public class RabbitQueueMQ_QueryOrderSham extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        super.init();
        System.out.println("Rabbit假支付队列MQ--------开启");
        queryNtice();
    }

    int threadNum = 0;

    private void queryNtice() {
        System.out.println("假支付消费者消费者队列RabbitMQ-----！！！初始化！！！=" + this.threadNum + "个消费者 ");
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.SHAM_ORDERPAY);//消费者类型0代表测试 1-13代表自己的业务 3代表审核消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.SHAM_ORDERPAY + ":pingtaiType:" + pingtaiType + ":consumerType:" + consumerType);
        System.out.println("假支付消费者队列【" + PublicConnectionInfos.SHAM_ORDERPAY + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        //new MonitoringRabbitMQCustomer(kt110, pingtaiType, consumerType).start();//开启下单消费者监控
        new ConsumerMaintenance(PublicConnectionInfos.SHAM_ORDERPAY,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {
                    new RabbitQueueConsumer(PublicConnectionInfos.SHAM_ORDERPAY, pingtaiString, rabbitmqBean.getHost(),
                            rabbitmqBean.getUsername(), rabbitmqBean.getPassword(), "sham_OrderPay_log") {
                        @Override
                        public String execMethod(String notice) {
                            long orderid = Long.parseLong(notice);//订单id
                            new MyThreadQuerySham(orderid, System.currentTimeMillis()).start();
                            WriteLog.write("sham_OrderPay", "orderid:" + orderid);
                            return notice + "---> 处理审核订单";
                        }
                    }.start();
                }
            }
        }.start();
        //监控结束
    }

    /**
     * 之前的审核做备份
     * 
     * @time 2017年3月2日 下午7:03:18
     * @author chendong
     */
    private void queryNtice_back20170302() {
        String queryName = PropertyUtil.getValue("Rabbit_QueueMQ_QueryOrder", "rabbitMQ.properties");
        //平台类型
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                RabbitQueueConsumerQueryOrderV3 consumerQueryOrder = new RabbitQueueConsumerQueryOrderV3(queryName,
                        type);
                consumerQueryOrder.start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
