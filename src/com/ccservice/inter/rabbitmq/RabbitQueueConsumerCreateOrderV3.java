package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.TrainCreateOrder;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.rabbitmq.util.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 下单消费者
 * 
 * @author 之至
 * @time 2016年12月24日 下午2:25:59
 */
public class RabbitQueueConsumerCreateOrderV3 extends RabbitMQDefaultCustomer implements IConsumer {//extends RabbitMQCustomer {
    //    private int type;
    private long orderid;
    
    
    /**
     * 构造方法
     * @param queueNameString
     * @param type
     * @param host
     * @param username
     * @param password
     * @throws IOException
     */
    public RabbitQueueConsumerCreateOrderV3(String queueNameString, String type,String host,String username,String password) throws IOException {
        super(queueNameString, type, host, username, password);
    }
    
    /**
     * 构造方法
     * @param queueNameString
     * @param type
     * @param host
     * @param username
     * @param password
     * @throws IOException
     */
    public RabbitQueueConsumerCreateOrderV3(String queueNameString, int type) throws IOException {
        this.queueNameString = queueNameString;
        this.pingtaiType = type+"";
        if (type == 1) {//空铁
            host = PublicConnectionInfos.KTHOST;
            username = PublicConnectionInfos.USERNAME;
            password = PublicConnectionInfos.KTPASSWORD;
        }
        else if (type == 2) {//同程
            host = PublicConnectionInfos.TCHOST;
            username = PublicConnectionInfos.USERNAME;
            password = PublicConnectionInfos.TCPASSWORD;
        }
    }
    public long tempTime;
    public String logName = this.getClass().getSimpleName();
    @Override
    public void run() {
        UUID uuid = UUID.randomUUID(); 
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        this.logName = this.getClass().getSimpleName();
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            if(!super.connection.isOpen()){//如果连接已经关闭了就关闭
                willClose = true;
                WriteLog.write(this.logName + "-Exception", "连接关了");
            }
            else {
                this.tempTime = System.currentTimeMillis();
                if (!isCanOrdering(new Date())) {//是否可以下单 
                    try {
                        sleep(1000L);//如果不能下单休息1秒
                    }
                    catch (Exception e) {
                    }
                    continue;
                }
                String msg = "";
                String orderNoticeResult = "";
                try {
                    msg = getMsgNew();//【第四步】:获取消息
                    orderNoticeResult = msg;//消息内容
                    System.out.println(
                            TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":接收到消息:" + orderNoticeResult);
                    WriteLog.write(this.logName,
                            this.tempTime + ":" + TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":【"
                                    + orderNoticeResult + "】");
                    willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                    WriteLog.write(this.logName, this.tempTime + ":"
                            + TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":【" + willClose + "】");
                    AckMsg();//【第五步】:确认消息，已经收到
                }
                catch (ShutdownSignalException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (ConsumerCancelledException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    public void runOld() {
        System.out.println("下单占座队列RabbitMQ-----开启   数量--->" + RabbitMQCustomerMonitoringUtil.Mapcustomers.size());
        while (true) {
            String orderNoticeResult = "";//消息内容
            try {
                if (isCanOrdering(new Date())) {//是否可以下单 
                    orderNoticeResult = getNewMessageString(queueNameString); //通过队列的名字获取消息
                    boolean willClose = processTheMessage(orderNoticeResult);//去下单的方法挪到了一个方法里了
                    //确认消息已经收到.必须
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                        break;
                    }
                }
                else {
                    try {
                        sleep(1000L);
                    }
                    catch (Exception e) {
                    }
                }
            }
            catch (ShutdownSignalException e) {
                ExceptionUtil.writelogByException("RabbitQueueConsumerCreateOrderV3-Exception", e,
                        "" + orderNoticeResult);
            }
            catch (ConsumerCancelledException e) {
                ExceptionUtil.writelogByException("RabbitQueueConsumerCreateOrderV3-Exception", e,
                        "" + orderNoticeResult);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                try {
                    channel.basicReject(delivery.getEnvelope().getDeliveryTag(), false);
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            finally {
                if (!consumerStatus || (null != StatusString && !"".equals(StatusString) && "0".equals(StatusString))) {
                    System.out.println("下单消费者队列  接收 ：————————> 最终关闭 ,剩余消费者 --- >" + RabbitMQCustomerMonitoringUtil.Mapcustomers.size());
                }
            }
        }
    }

    /**
     * 
     * 
     * @return 是否即将关闭 true即将关闭,false继续执行
     * @time 2017年1月11日 上午10:25:39
     * @author chen
     * @param orderNoticeResult 
     */
    public boolean processTheMessage(String orderNoticeResult) {
        boolean willClose = false;//是否即将关闭
        try {
            createOrder(orderNoticeResult);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException(this.logName + "-Exception", e, "processTheMessage=" + orderNoticeResult);
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }

    /**
     * 真正执行的下单的逻辑
     * 
     * @param orderNoticeResult
     * @time 2017年3月2日 下午7:54:35
     * @author chendong
     */
    private void createOrder(String orderNoticeResult) { 
        WriteLog.write(this.logName, this.tempTime + ":" + TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss)
                + ":【" + orderNoticeResult + "】");
        orderid = Long.valueOf(orderNoticeResult);
        System.out.println(TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss) + ":准备处理订单:" + orderid);
        WriteLog.write(this.logName, this.tempTime + ":orderid:" + orderid);
        if (this.orderid > 0) {
            new TrainCreateOrder(this.orderid).createOrderStart(new Customeruser(), false);//执行下单的逻辑
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年12月24日 下午2:49:17
     * @Description 控制消費者
     */
    @SuppressWarnings("rawtypes")
    private void controlCustomer() {
        try {
            String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//消费者编号
            System.out.println("kt110->"+kt110);
            String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");//平台类型 1空铁2同程
            System.out.println("pingTaiType->"+pingTaiType);
            //是否执行关闭消费者操作     1.判断当前对象状态   2.从数据库获取
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();
            
//            if (rabbitMQConnectsBean.isUse()) {
//            }
//            else {
//                consumerStatus = rabbitMQConnectsBean.isUse();
//                if (RabbitMQCustomerMonitoringUtil.Mapcustomers.get(rabbitMQConnectsBean.getKey()).getKey().equals(rabbitMQConnectsBean.getKey())) {
//                    for (int i = 0; i < RabbitMQCustomerMonitoringUtil.ListcustomerNames.size(); i++) {
//                        if (rabbitMQConnectsBean.getKey().equals(RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i))) {
//                            RabbitMQCustomerMonitoringUtil.ListcustomerNames.remove(i);
//                        }
//                    }
//                    RabbitMQCustomerMonitoringUtil.Mapcustomers.remove(rabbitMQConnectsBean.getKey());
//                }
                //                channel.close();
                //                connection.close();
                //                System.out.println(TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss)
                //                        + " 下单消费者队列  当前消费者:————————>已关闭,消费者剩余 -- >" + RabbitMQCustomerMonitoring.ListcustomerNames.size());
//            }
//            List list = Server.getInstance().getSystemService()
//                    .findMapResultBySql("select Status from [TrainPingTai] with(nolock)  where PingTai =" + pingTaiType
//                            + "and type =" + kt110, null);
//            if (list.size() > 0) {
//                Map map3 = (Map) list.get(0);
//                StatusString = map3.containsKey("Status") ? map3.get("Status").toString() : "";
//                if (!"".equals(StatusString) && "0".equals(StatusString)) {
//                    if (RabbitMQCustomerMonitoringUtil.Mapcustomers.get(rabbitMQConnectsBean.getKey()).getKey()
//                            .equals(rabbitMQConnectsBean.getKey())) {
//                        RabbitMQCustomerMonitoringUtil.Mapcustomers.remove(rabbitMQConnectsBean.getKey());
//                        for (int i = 0; i < RabbitMQCustomerMonitoringUtil.ListcustomerNames.size(); i++) {
//                            if (rabbitMQConnectsBean.getKey().equals(RabbitMQCustomerMonitoringUtil.ListcustomerNames.get(i))) {
//                                RabbitMQCustomerMonitoringUtil.ListcustomerNames.remove(i);
//                            }
//                        }
//                    }
//                    //                    channel.close();
//                    //                    connection.close();
//                    if (RabbitMQCustomerMonitoringUtil.Mapcustomers.size() == 0 || RabbitMQCustomerMonitoringUtil.ListcustomerNames.size() == 0) {
//                        String pingtaiName = "1".equals(pingTaiType) ? "空铁" : "同程";
//                        //                        sendSMS(pingtaiName + ":" + strTypeString);
//                    }
//                    consumerStatus = false;
//                }
//            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private  void main1() {
        // TODO Auto-generated method stub
        System.out.println(this.getClass().getSimpleName());
    }

    public static void main(String[] args) {
        try {
            new RabbitQueueConsumerCreateOrderV3("", 1).main1();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
//            new RabbitQueueConsumerCreateOrderV3("QUEUE_TEST", 1).start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    

}
