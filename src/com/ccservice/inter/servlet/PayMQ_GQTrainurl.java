package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.train.mqlistener.GQTrainGetURLMessageListener;

/**
 * 
 * @author wzc
 * 改签获取支付链接
 *
 */
public class PayMQ_GQTrainurl extends HttpServlet {
    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int geturlcount = 2;

    @Override
    public void init() throws ServletException {
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        this.geturlcount = Integer.valueOf(this.getInitParameter("geturlcount"));
        if ("1".equals(isstart)) {
            System.out.println("改签获取支付链接队列:开启");
            payordergqNotice();
        }
    }

    /**
     * wzc
     * 改签支付消费者
     */
    public void payordergqNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < geturlcount; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new GQTrainGetURLMessageListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
