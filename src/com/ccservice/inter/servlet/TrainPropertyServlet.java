package com.ccservice.inter.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.job.TrainPropertyMessageJob;
import com.ccservice.qunar.util.ExceptionUtil;


public class TrainPropertyServlet extends HttpServlet{
    @Override
    public void init() throws ServletException {
        try {
        JobDetail jobDetail = new JobDetail("TrainPropertyMessageJob", "TrainPropertyMessageJobGroup",
                TrainPropertyMessageJob.class);
            String expression = "";
            try {
                expression = PropertyUtil.getValue("trainExpression", "train.property.properties");
            }
            catch (Exception e) {
                expression = "0/5 * 6-23 * * ?";
            }
            CronTrigger trigger = new CronTrigger("TrainPropertyMessageJob", "TrainPropertyMessageJobGroup", expression);
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            scheduler.start();
            WriteLog.write("TrainPropertyServlet_init", "job启动,表达式为"+expression);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("TrainPropertyServlet_init_Excetion", e, "");
        }
    }
}
