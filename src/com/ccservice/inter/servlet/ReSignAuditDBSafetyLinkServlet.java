package com.ccservice.inter.servlet;

import java.util.List;

import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;
import com.ccservice.Util.file.WriteLog;
import com.ggjs.oauth.client.servlet.AbstractOAuthClientServlet;

/**
 *  取消消费者  DB安全包服务
 * @author 蔡培锋
 * @version 1.0.0
 * @time 2018年1月31日 上午11:02:59
 */
public class ReSignAuditDBSafetyLinkServlet extends AbstractOAuthClientServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void afterInit() { 
    	//1.db链接加载
        //随机数用于记录日志
        int flagInt = (int) (Math.random() * 1000);
        try {   
            WriteLog.write("ReSignAuditDBSafetyLinkServlet首次链接记录", "flagInt=["+flagInt+"]>>>>>取消订单消费者项目--->开始连接第一次链接DB");//开启发下单模式 接收下单消费者发送的需求
            long l1 = System.currentTimeMillis();
            DataTable getDataTable = DBHelper.GetDataTable("select 1", DBSourceEnum.TONGCHENG_DB);
            List<DataRow> getRow = getDataTable.GetRow();
            DataRow dataRow = getRow.get(0);
            List<DataColumn> getColumn = dataRow.GetColumn();
            DataColumn dataColumn = getColumn.get(0);
            Integer getValue = (Integer) dataColumn.GetValue();
            WriteLog.write("ReSignAuditDBSafetyLinkServlet首次链接记录",  "flagInt=["+flagInt+"]>>>>>sendsql=[select 1]返回=[" + getValue
                    + "]自加载ReSignAuditDBSafetyLinkServlet-->首次链接结束，耗时：" + (System.currentTimeMillis() - l1) + "ms");
        }
        catch (Exception e) {
            e.printStackTrace();
            ExceptionUtil.writelogByException("ReSignAuditDBSafetyLinkServlet首次链接记录", e,
                    "flagInt=["+flagInt+"]>>>>>取消订单消费者项目-------------ReSignAuditDBSafetyLinkServlet DB首次链接异常---------");
        }
        
       
    }
}
