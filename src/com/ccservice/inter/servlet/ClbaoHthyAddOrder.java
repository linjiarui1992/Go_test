package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

public class ClbaoHthyAddOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ClbaoHthyAddOrder() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        //String Address = new String(request.getParameter("Address").getBytes("iso-8859-1"), "utf-8");
        //当前时间，格式为YYYY-MM-DD HH:MM:SS
        String timeStamp=request.getParameter("timeStamp");
        //合作方ID，例如gaotieguanjia
        String partnerName=request.getParameter("partnerName");
        //messageIdentity = MD5加密 ( partnerName + timeStamp+ KEY )
        String messageIdentity=request.getParameter("messageIdentity");
        //订单编号
        String orderNumber=request.getParameter("orderNumber");
        //出发站名
        String fromSname=request.getParameter("fromSname");
        //到达站名
        String toSname=request.getParameter("toSname");
        //出发日期yyyy-MM-dd
        String departDate=request.getParameter("departDate");
        //出发时间 HH:mm
        String departTime=request.getParameter("departTime");
        //车次号
        String trainNumber=request.getParameter("trainNumber");
        //席别类型，中文名(如"硬卧")
        String seatName=request.getParameter("seatName");
        //席别详情（如"上铺"、"必须靠窗"等）
        String seatDetail=request.getParameter("seatDetail");
        //是否接受其他席位，0不接受，1接受
        String acceptOtherSeat=request.getParameter("acceptOtherSeat");
        //优先席别票最少抢票张数
        String seatLeat=request.getParameter("seatLeast");
        //证件信息  姓名,证件,证件号,票类型,价格 如果有多条以'|'分开。张三,1,210106196003074997,1,8|王五,1,422201198608300028,1,8
        String passport=request.getParameter("passport");
        //收件人姓名
        String userName=request.getParameter("userName");
        //收件人电话
        String userTel=request.getParameter("userTel");
        //配送地址
        String address=request.getParameter("address");

        //车票价格
        String ticketPrice=request.getParameter("ticketPrice");
        //1、送票到站，2、快递
//        String stationget=request.getParameter("stationget");
        WriteLog.write("易订行请求下单接口", "orderNumber="+orderNumber+";fromSname="+fromSname+";toSname="+toSname+";departDate="+departDate+";departTime="+departTime+";trainNumber="+trainNumber+";seatName="+seatName+";seatDetail="+seatDetail+";acceptOtherSeat="+acceptOtherSeat+";seatLeat="+seatLeat+";passport="+passport+";userName="+userName+";userTel="+userTel+";address="+address+";ticketPrice="+ticketPrice);
        String sql = "SELECT keys from TrainOfflineAgentKey where partnerName='"+partnerName+"'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String key="";
        String code="";
        String msg="";
        //防止重复下单
        String sqllist="select * from TrainOrderOffline where OrderNumberOnline = '"+orderNumber+"'";
        List listunique = Server.getInstance().getSystemService().findMapResultBySql(sqllist, null);
        
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            key = map.get("keys").toString();
            String md5s = MD5Util.MD5Encode(partnerName + timeStamp + key,
                    "UTF-8").toUpperCase();
            if (messageIdentity.equals(md5s) && listunique.size()==0) {// 验证通过
                
                trainorderofflineadd(orderNumber, fromSname, toSname,
                        departDate, departTime, trainNumber, seatName,
                        seatDetail, acceptOtherSeat, seatLeat, passport,
                        userName, userTel, address, ticketPrice);
                code = "0";
                msg = "新增订单成功";
            }else if(messageIdentity.equals(md5s) && listunique.size()>0){
                code = "0";
                msg = "新增订单成功";
            }else{
                code = "1";
                msg = "账号核验失败";
            }
        } else{
            code="1";
            msg="账号核验失败！";
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        WriteLog.write("易订行请求下单接口", "orderNumber="+orderNumber+";responsejson="+responsejson.toString());
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    private DBoperationUtil dt = new DBoperationUtil();
    
    /**
     * 添加火车票线下订单
     */
    public void trainorderofflineadd(String orderNumber,String fromSname,String toSname,String departDate,String departTime,String trainNumber,String seatName,String seatDetail,String acceptOtherSeat,String seatLeat,String passport,String userName,String userTel,String address,String ticketPrice) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        String addresstemp = address;
        
        // 通过订单邮寄地址匹配出票点
        String[] ticketcount=passport.split("[|]");
        String agentidtemp ="414";
        agentidtemp = distribution2(addresstemp);
        WriteLog.write("易订行线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setCreateUId(65l);//ok
        trainOrderOffline.setCreateUser("易订行");//ok
        trainOrderOffline.setContactUser(userName);//ok
        trainOrderOffline.setContactTel(userTel);//ok
        trainOrderOffline.setOrderPrice(Float.parseFloat(ticketPrice));//ok
        trainOrderOffline.setAgentProfit(0f);//ok
        trainOrderOffline.setOrdernumberonline(orderNumber);//ok
        trainOrderOffline.setTicketCount(ticketcount.length);//ok
        int papertype=1;
        if("靠窗".equals(seatDetail)){
            papertype=1;
        }else if("连座".equals(seatDetail)){
            papertype=2;
        }else if("上铺".equals(seatDetail)){
            papertype=3;
        }else if("中铺".equals(seatDetail)){
            papertype=4;
        }else if("下铺".equals(seatDetail)){
            papertype=5;
        }else if("同包厢".equals(seatDetail)){
            papertype=6;
        }else if("中上铺".equals(seatDetail)){
            papertype=7;
        }else if("一起".equals(seatDetail)){
            papertype=8;
        }
        trainOrderOffline.setPaperType(papertype);
        trainOrderOffline.setPaperBackup(Integer.parseInt(acceptOtherSeat));
        trainOrderOffline.setPaperLowSeatCount(Integer.parseInt(seatLeat));
        trainOrderOffline.setExtSeat("无");
        SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        trainOrderOffline.setOrderTimeOut(Timestamp.valueOf(sdf1.format(new Date())));
        Timestamp startTime = new Timestamp(new Date().getTime());
        trainOrderOffline.setOrderTimeOut(startTime);
        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        //通过出票点和邮寄地址获取预计到达时间results
//        String delieveStr=getDelieveStr(agentidtemp,address);//
        String delieveStr="暂无快递信息!";
        try {
            delieveStr=getDelieveStr(agentidtemp,address);//
        }
        catch (Exception e) {
        }
        
        String updatesql="UPDATE TrainOrderOffline SET expressDeliver ='"+delieveStr+"' WHERE ID="+orderid;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", updatesql);
        Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress addressbean = new MailAddress();
        addressbean.setMailName(userName);
        addressbean.setMailTel(userTel);
        addressbean.setPrintState(0);
//        String insureaddress = addresstemp;
//        String[] splitadd = insureaddress.split(",");
        addressbean.setPostcode("100000");
        addressbean.setAddress(address);
        addressbean.setOrderid(Integer.parseInt(orderid));
//        addressbean.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        addressbean.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(addressbean);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (int i=0;i<ticketcount.length;i++) {
            // 火车票乘客线下TrainPassengerOffline
            String[] passengers=ticketcount[i].split(",");
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(passengers[0]);
            int idtype=1;
            if("1".equals(passengers[1])){
                idtype=1;
            }else if("C".equals(passengers[1])){
                idtype=4;
            }else if("G".equals(passengers[1])){
                idtype=5;
            }else if("B".equals(passengers[1])){
                idtype=3;
            }
            trainPassengerOffline.setidtype(idtype);
            trainPassengerOffline.setidnumber(passengers[2]);
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
//            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(departDate+" "+departTime+":00"));
                trainTicketOffline.setDeparture(fromSname);
                trainTicketOffline.setArrival(toSname);
                trainTicketOffline.setTrainno(trainNumber);
                trainTicketOffline.setTicketType(Integer.parseInt(passengers[3]));
                trainTicketOffline.setSeatType(seatName);
                trainTicketOffline.setPrice(Float.parseFloat(passengers[4]));
                trainTicketOffline.setCostTime("0");
                trainTicketOffline.setStartTime(departTime);
                trainTicketOffline.setArrivalTime("0");
//                trainTicketOffline.setSubOrderId(ticket.getTicketno());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
//            }
        }
    }
    /**
     * 根据数据库设置匹配出票点
     * @param address1
     * @return
     */
    public static String distribution2(String address1) {
        boolean flag=false;
        //默认出票点
        String sql1="SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid="+"65";
        List list1=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        String agentId="378";
        if(list1.size()>0){
            Map map=(Map)list1.get(0);
            agentId=map.get("agentId").toString();
        }
        //程序自动分配出票点
        String sql2="SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid="+"65";
        List list2=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        for(int i=0;i<list2.size();i++){
            Map mapp=(Map)list2.get(i);
            String provinces=mapp.get("provinces").toString();
            String agentid=mapp.get("agentId").toString();
            String[] add=provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                if (address1.startsWith(add[j])) {
                    agentId=agentid;
                    flag=true;
                }
                if(flag){
                    break;
                }
            }
            if(flag){
                break;
            }
        }
        WriteLog.write("易订行新版分配订单", "agentId="+agentId+";address1="+address1);
        return agentId;
    }

    public static String getDelieveStr(String agengId,String address){
        String results = "";
        String fromcode = "010";
        String tocode = getExpressCodes(address);
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId=" + agengId;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            fromcode = map.get("fromcode").toString();
            time1 = map.get("time1").toString();
            time2 = map.get("time2").toString();
        }
        String realTime = getRealTimes(dates, time1, time2);
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;
        WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId=" + agengId + "------->" + "address=" + address
                + "---------->" + urlString + "?" + param);
        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
        if (result.contains("OK") && result.contains("deliver_time")) {
            try {
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                Element head = root.element("Head");
                Element body = root.element("Body");
                if ("OK".equals(root.elementText("Head"))) {
                    if (result.contains("deliver_time")) {
                        Element deliverTmResponse = body.element("DeliverTmResponse");
                        Element deliverTm = deliverTmResponse.element("DeliverTm");
                        String business_type_desc = deliverTm.attributeValue("business_type_desc");
                        String deliver_time = deliverTm.attributeValue("deliver_time");
                        String business_type = deliverTm.attributeValue("business_type");
                        results = "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                                + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                    }
                    else {
                        results = "获取快递时间失败！请上官网核验快递送达时间。";
                    }
                }
            }
            catch (DocumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else {
            results = "暂无快递信息!";
        }  
        return results;
    }
    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public static String getExpressCodes(String address){
        String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        String cityCode="010";
        if(list.size()>0){
            Map map=(Map)list.get(0);
            cityCode=map.get("CityCode").toString();
        }
        return cityCode;
    }
    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public static String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }
    public static Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

}
