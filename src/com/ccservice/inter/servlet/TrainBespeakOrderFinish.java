package com.ccservice.inter.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

/**
 * 同程抢票线下出票成功 线下平台出票成功或者失败回调
 * 
 * @author wangdiao 2017-01-04
 */
public class TrainBespeakOrderFinish extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public TrainBespeakOrderFinish() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("content-type", "text/html;charset=UTF-8");
		PrintWriter out = null;
		JSONObject result = new JSONObject();
		try {
			out = response.getWriter();
			String orderId = request.getParameter("orderId");
			String isSuccess = request.getParameter("isSuccess");
			String failCode = request.getParameter("failCode");
			WriteLog.write("同程线下抢票出票回调接收", "订单号：" + orderId + "成功标识：" + isSuccess + "成功或失败的code" + failCode);
			JSONArray passengers = new JSONArray();
			String sqlsel = "exec sp_TrainTicketOffline_TrainPassengerOffline_TrainOrderOffline_byId  @orderId=" + orderId;
			List<?> listPassengers = Server.getInstance().getSystemService().findMapResultByProcedure(sqlsel);
			Map<?, ?> mapTotalPrice = (Map<?, ?>) listPassengers.get(0);
			String totalPrice = mapTotalPrice.get("OrderPrice") == null ? "" : mapTotalPrice.get("OrderPrice").toString();// 获取订单价格
			String orderNum = mapTotalPrice.get("OrderNumberOnline") == null ? "" : mapTotalPrice.get("OrderNumberOnline").toString(); // 获取订单号
			if ("success".equals(isSuccess) && !"".equals(isSuccess)) {// 出票成功
				passengers = failureOrSuccessCallback(listPassengers);
				result.put("totalPrice", totalPrice);
				result.put("isSuccess", true);
				result.put("orderNum", orderNum);
				result.put("msg", "出票成功");
				result.put("code", failCode);
				result.put("tickets", passengers);
				WriteLog.write("同程线下抢票下单出票成功回调", "拼接好的参数：" + result.toString());
			} else {
				passengers = failureOrSuccessCallback(listPassengers);
				result.put("totalPrice", totalPrice);
				result.put("isSuccess", false);
				result.put("orderNum", orderNum);
				result.put("msg", "出票失败");
				result.put("code", failCode);
				result.put("tickets", passengers);
				WriteLog.write("同程线下抢票下单出票失败回调", "拼接好的参数：" + result.toString());
			}
		}
		catch (Exception e) {
			result.put("isSuccess", false);
			result.put("orderNum", "ERROR");
			result.put("msg", "系统错误异常");
			result.put("tickets", "ERROR");
			WriteLog.write("同程线下抢票下单出票回调异常", "异常捕获：" + e + "---->系统错误异常：" + result.toJSONString());
		}
		finally {
			WriteLog.write("同程线下抢票下单出票回调", "请求参数：" + result.toString());
			// String urlString =
			// "http://121.41.35.117:19064/trainorder_bespeak/"+"TrainTCBespeakOfflineCreateOrder";//同程线下抢票出票回调
			String tongchengCallBackUrl = PropertyUtil.getValue("tongchengCallBackUrl", "train.properties");
			String urlString = tongchengCallBackUrl + "TrainTCBespeakOfflineCreateOrder";// 出票回调地址
			String jsonStr = URLEncoder.encode(result.toString(), "utf-8");
			String paramContent = "jsonStr=" + jsonStr;
			String resultss = SendPostandGet.submitPost(urlString, paramContent, "UTF-8").toString();
			JSONObject jsonObject = JSONObject.parseObject(resultss);
			WriteLog.write("同程线下抢票下单出票回调", "返回结果：" + resultss);
			if (out != null) {
				out.print(jsonObject);
				out.flush();
				out.close();
			}
		}

	}

	/**
	 * 获取乘客的信息
	 * 
	 * @param list
	 * @return 乘客信息的array数组
	 */
	private JSONArray failureOrSuccessCallback(List<?> list) {
		// 出票失败或者成功出票都需要回调的信息
		JSONArray passenger = new JSONArray();
		for (int i = 0; i < list.size(); i++) {
			JSONObject json = new JSONObject();
			Map map = (Map) list.get(i);
			String name = map.get("Name") == null ? "" : map.get("Name").toString();
			String trainNo = map.get("TrainNo") == null ? "" : map.get("TrainNo").toString();
			String seatType = map.get("realSeat") == null ? "" : map.get("realSeat").toString();
			String price = map.get("sealPrice") == null ? "" : map.get("sealPrice").toString();
			String coach = map.get("Coach") == null ? "" : map.get("Coach").toString();
			String seatNo = map.get("SeatNo") == null ? "" : map.get("SeatNo").toString();
			String ticketNo = map.get("ticketNo") == null ? "" : map.get("ticketNo").toString();
			String ticketId = map.get("Id") == null ? "" : map.get("Id").toString();
			json.put("passengerName", name); // 乘客姓名
			json.put("trainNo", trainNo); // 车次
			json.put("seatTypeName", seatType); // 坐席
			json.put("price", price); // 价格
			json.put("coach", coach); // 车厢
			json.put("seatNo", seatNo); // 座位
			json.put("ticketNo", ticketNo); // 票号
			json.put("ticketId", ticketId); // 票ID
			passenger.add(json);
		}
		return passenger;
	}
}
