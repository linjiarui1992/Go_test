package com.ccservice.inter.servlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.mail.Address;

import com.alibaba.fastjson.JSONObject;
import com.callback.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;
import com.hp.hpl.sparta.xpath.ThisNodeTest;

public class JDexpressData {

    public String getJDExpressDelivery(String address) {
        String agentId = distribution2(address);
        String cityName = getCityByAgentId(agentId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String url = "http://120.26.100.206:12345/JD/jdisacept";//正式jdisacept  jdgetkey
        int max = 10000;
        int min = 1000;
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        String orderid = "T" + sdf.format(new Date()) + "" + s;
        String sendCode = getProvinceCode(cityName.split("_")[0], cityName.split("_")[1]);
        String senderprovinceid = "1";
        String sendercityid = "72";
        if (sendCode != null && !"".equals(sendCode)) {
            senderprovinceid = sendCode.split("_")[0];
            sendercityid = sendCode.split("_")[1];

        }
        String param = "address=" + address + "&orderid=" + orderid + "&senderprovinceid=" + senderprovinceid
                + "&sendercityid=" + sendercityid;
        WriteLog.write("抢票转线下获取jd实效", "address:" + address + ";orderid:" + orderid + ";senderprovinceid:"
                + senderprovinceid + ";sendercityid:" + sendercityid);
        String resultjson = SendPostandGet.submitPost(url, param, "UTF-8").toString();
        WriteLog.write("抢票转线下获取jd实效", "address:" + address + ";orderid:" + orderid + ";senderprovinceid:"
                + senderprovinceid + ";sendercityid:" + sendercityid + ";resultjson:" + resultjson);
        JSONObject js = new JSONObject();
        js.put("resultjson", JSONObject.parse(resultjson));
        js.put("orderid", orderid);
        return js.toString();
    }

    /**
     * 根据数据库设置匹配出票点
     * @param address1
     * @return
     */
    public String distribution2(String address1) {
        boolean flag = false;
        //默认出票点
        String sql1 = "SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=68";
        List list1 = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        String agentId = "378";
        if (list1.size() > 0) {
            Map map = (Map) list1.get(0);
            agentId = map.get("agentId").toString();
        }
        //程序自动分配出票点
        String sql2 = "SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid=68";
        List list2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        for (int i = 0; i < list2.size(); i++) {
            Map mapp = (Map) list2.get(i);
            String provinces = mapp.get("provinces").toString();
            String agentid = mapp.get("agentId").toString();
            String[] add = provinces.split(",");
            for (int j = 0; j < add.length; j++) {
                if (address1.startsWith(add[j])) {
                    agentId = agentid;
                    flag = true;
                }
                if (flag) {
                    break;
                }
            }
            if (flag) {
                break;
            }
        }
        WriteLog.write("抢票分线下快递路由分配订单", "agentId=" + agentId + ";address1=" + address1);
        return agentId;
    }

    public String getCityByAgentId(String agentId) {
        String provinceName = "北京";
        String cityName = "朝阳区";
        String sql = "SELECT ProvinceName,CityName,RegionName FROM T_CUSTOMERAGENT WHERE ID = '" + agentId + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            provinceName = map.get("ProvinceName").toString();
            cityName = map.get("CityName").toString();
            String regionName = map.get("RegionName").toString();
            if (provinceName.equals(cityName.replace("市", ""))) {
                cityName = regionName;
            }
        }
        WriteLog.write("抢票分线下快递路由分配订单", "agentId=" + agentId + ";city=" + provinceName + "_" + cityName);
        return provinceName + "_" + cityName;
    }

    /**
     * jd获取省市code
     * @param province
     * @param city
     * @param random
     * @return
     */

    public String getProvinceCode(String province, String city) {
        String provinces = province.replace("省", "").replace("市", "");
        String privinceCode = "";
        String cityCode = "";
        String parnetid = "";
        String sendCode = "";
        String psql = "SELECT areaid FROM JDprovince WITH (NOLOCK) WHERE areaname = '" + provinces + "'";
        List plist = Server.getInstance().getSystemService().findMapResultBySql(psql, null);
        if (plist.size() > 0) {
            Map map = (Map) plist.get(0);
            privinceCode = map.get("areaid").toString();
        }

        String csql = "SELECT areaid,parnetid FROM JDprovince WITH (NOLOCK) WHERE areaname = '" + city + "'";
        List clist = Server.getInstance().getSystemService().findMapResultBySql(csql, null);
        if (clist.size() > 0) {
            Map map = (Map) clist.get(0);
            cityCode = map.get("areaid").toString();
            parnetid = map.get("parnetid").toString();
        }
        if (!"".equals(privinceCode) && privinceCode.equals(parnetid)) {
            sendCode = privinceCode + "_" + cityCode;
        }
        WriteLog.write("线下快递时效接口", Math.random() * 1000 + "--(JD)--province:" + province + "--->city:" + city
                + "--->sendCode:" + sendCode);
        return sendCode;
    }

    public Date getDateAfter(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        return now.getTime();
    }

    public static void main(String[] args) {
        System.out.println(new JDexpressData().getJDExpressDelivery("北京市海淀区"));
    }
}
