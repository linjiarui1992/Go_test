package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.train.mqlistener.TrainGetURLMessageListener;

/**
 * http://192.168.0.5:8161/admin/
 * 获取支付链接mq
 */
@SuppressWarnings("serial")
public class PayMQ_TrainGetURL extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int geturlcount = 2;

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        this.geturlcount = Integer.valueOf(this.getInitParameter("geturlcount"));
        if ("1".equals(isstart)) {
            System.out.println("更新支付链接队列:开启");
            payorderNotice();
        }
    }

    public void payorderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < geturlcount; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainGetURLMessageListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
