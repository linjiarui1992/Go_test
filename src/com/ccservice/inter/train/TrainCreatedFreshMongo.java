package com.ccservice.inter.train;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.qunar.util.ExceptionUtil;
import com.mongodb.DBObject;

public class TrainCreatedFreshMongo {
    private final String[] freshSeatTypes = { "3", "4", "6", "F", "A" };

    public void freshMongo(JSONObject jsonObject) {
        String url = "-1";
        String quenename = "";
        if (jsonObject != null) {
            url = PropertyUtil.getValue("TrainCreatedFreshMongo_url", "train.properties");
            quenename = PropertyUtil.getValue("TrainCreatedFreshMongo_quenename", "train.properties");
        }
        try {
            WriteLog.write("TrainCreatedFreshMongo2Mq", jsonObject.toString());
            ActiveMQUtil.sendMessage(url, quenename, jsonObject.toString());
        }
        catch (JMSException e) {
            ExceptionUtil.writelogByException("Error_TrainCreatedFreshMongo2Mq", e);
        }
    }

    /**
     * 刷新Mongo 
     * 
     * @param jsonObject
     * @time 2015年9月14日 下午3:26:21
     * @author fiend
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void freshMongoByMq(JSONObject jsonObject) {
        int randomi = (int) (Math.random() * 1000000);
        try {
            WriteLog.write("下单后的价格刷新mongo_json", randomi + ":json:" + jsonObject);
            JSONObject ticketFor12306 = jsonObject.getJSONArray("tickets").getJSONObject(0);
            //订单坐席
            String seatType = ticketFor12306.getString("seat_type_code");
            boolean isCanFresh = false;
            for (String freshSeatType : this.freshSeatTypes) {
                if (freshSeatType.equals(seatType)) {
                    isCanFresh = true;
                    break;
                }
            }
            if (!isCanFresh) {
                return;
            }
            //出发站3字码
            String dep = ticketFor12306.getJSONObject("stationTrainDTO").getString("from_station_telecode");
            //到达站3字码
            String arr = ticketFor12306.getJSONObject("stationTrainDTO").getString("to_station_telecode");
            //车次
            String traincode = ticketFor12306.getJSONObject("stationTrainDTO").getString("station_train_code");
            //发车日期
            String traindate = ticketFor12306.getString("train_date").split(" ")[0];
            String key = dep + arr + traindate;
            MongoLogic mongoLogic = new MongoLogic();
            Map<String, Object> keyMap = mongoLogic.keyMap(key, traincode);
            if (keyMap == null || keyMap.size() == 0) {
                return;
            }
            List<DBObject> list = mongoLogic.SelectPrice(keyMap);
            if (list.size() != 1) {
                return;
            }
            boolean isNeedFreshPrice = false;
            Map<String, Object> dbmongo = list.get(0).toMap();
            JSONArray ticketArray = jsonObject.getJSONArray("tickets");
            //上铺价格的KEY
            String priceKeyMongoS = "";
            //上铺价格
            Double priceMongoS = 0d;
            Map<String, Object> changeMap = new HashMap<String, Object>();
            if (dbmongo.get("Start") != null && dep.equals(dbmongo.get("Start").toString())
                    && dbmongo.get("Arrive") != null && arr.equals(dbmongo.get("Arrive").toString())) {
                for (int i = 0; i < ticketArray.size(); i++) {
                    //12306数据
                    JSONObject ticketJsonObject = ticketArray.getJSONObject(i);
                    String ticket_type_name = ticketJsonObject.getString("ticket_type_name");
                    if ("成人票".equals(ticket_type_name)) {
                        String ticketSeatType = ticketJsonObject.getString("seat_type_code");
                        String ticketSeatName = ticketJsonObject.getString("seat_name");
                        double ticketSeatPrice = Double.valueOf(ticketJsonObject.getString("ticket_price")) / 100;
                        //mongo数据
                        String priceKeyMongo = priceKeyMongo(ticketSeatType, ticketSeatName);
                        double priceMongo = Double.valueOf(dbmongo.get(priceKeyMongo).toString());
                        if (ticketSeatPrice > 0 && priceMongo != ticketSeatPrice) {
                            WriteLog.write("下单后的价格刷新mongo_price", randomi + ":key:" + key + ":traincode:" + traincode
                                    + ":" + priceKeyMongo + "--->" + priceMongo + ":change:" + ticketSeatPrice);
                            dbmongo.put(priceKeyMongo, ticketSeatPrice);
                            changeMap.put(priceKeyMongo, ticketSeatPrice);
                            if (priceKeyMongoS == null || "".equals(priceKeyMongoS)) {
                                priceKeyMongoS = priceKeyMongoS(ticketSeatType);
                                priceMongoS = Double.valueOf(dbmongo.get(priceKeyMongoS).toString());
                            }
                            isNeedFreshPrice = true;
                        }
                    }
                }
            }
            if (isNeedFreshPrice) {
                //更新当天数据
                mongoLogic.UpdatePrice(keyMap, dbmongo);
                if (priceMongoS == 0) {
                    return;
                }
                if (changeMap == null || changeMap.size() == 0) {
                    return;
                }
                for (int i = 0; i < 61; i++) {
                    //获取mongo其它日期的key
                    String nextdaykey = dep + arr + getNextDayString(i);
                    if (key.equals(nextdaykey)) {
                        continue;
                    }
                    //获取mongo其它日期的查询条件map
                    Map<String, Object> nextdaykeyMap = mongoLogic.keyMap(nextdaykey, traincode);
                    if (nextdaykeyMap == null || nextdaykeyMap.size() == 0) {
                        continue;
                    }
                    //获取mongo中其它日期的数据
                    List<DBObject> nextdayList = mongoLogic.SelectPrice(nextdaykeyMap);
                    if (nextdayList.size() != 1) {
                        continue;
                    }
                    Map<String, Object> nextdaymongo = nextdayList.get(0).toMap();
                    //获取mongo中其它日期的上铺价格
                    double nextdaypriceMongoS = Double.valueOf(nextdaymongo.get(priceKeyMongoS).toString());
                    if (nextdaypriceMongoS != priceMongoS) {
                        continue;
                    }
                    WriteLog.write("下单后的价格刷新mongo_all", randomi + ":key:" + key + ":nextdaykey:" + nextdaykey
                            + ":traincode:" + traincode + "--->" + nextdaypriceMongoS + ":equals:" + priceMongoS);
                    //遍历改变Map修改数据
                    Iterator iter = changeMap.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry) iter.next();
                        nextdaymongo.put(entry.getKey().toString(), entry.getValue());
                    }
                    //更新其它天数据
                    mongoLogic.UpdatePrice(nextdaykeyMap, nextdaymongo);
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("Error_下单后的价格刷新mongo", randomi + "");
            ExceptionUtil.writelogByException("Error_下单后的价格刷新mongo", e);
        }
    }

    /**
     * 通过坐席类型和
     * 
     * @param seattype
     * @param seatname
     * @return
     * @time 2015年9月14日 下午4:25:23
     * @author fiend
     */
    private String priceKeyMongo(String seattype, String seatname) {
        String pricekey = "";
        if ("3".equals(seattype)) {//硬卧
            if (seatname.contains("上")) {
                pricekey = "YWSPrice";
            }
            if (seatname.contains("中")) {
                pricekey = "YWZPrice";
            }
            if (seatname.contains("下")) {
                pricekey = "YWXPrice";
            }
        }
        else if ("4".equals(seattype) || "F".equals(seattype)) {//软卧、动卧
            if (seatname.contains("上")) {
                pricekey = "RWSPrice";
            }
            if (seatname.contains("下")) {
                pricekey = "RWXPrice";
            }
        }
        else if ("6".equals(seattype) || "A".equals(seattype)) {//高级软卧、高级动卧
            if (seatname.contains("上")) {
                pricekey = "GWSPrice";
            }
            if (seatname.contains("下")) {
                pricekey = "GWXPrice";
            }
        }
        return pricekey;
    }

    /**
     * 通过坐席类型和
     * 
     * @param seattype
     * @param seatname
     * @return
     * @time 2015年9月14日 下午4:25:23
     * @author fiend
     */
    private String priceKeyMongoS(String seattype) {
        String pricekey = "";
        if ("3".equals(seattype)) {//硬卧
            pricekey = "YWSPrice";
        }
        else if ("4".equals(seattype) || "F".equals(seattype)) {//软卧、动卧
            pricekey = "RWSPrice";
        }
        else if ("6".equals(seattype) || "A".equals(seattype)) {//高级软卧、高级动卧
            pricekey = "GWSPrice";
        }
        return pricekey;
    }

    public static void main(String[] args) {
        String str = "{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{\"orderDBList\":[{\"sequence_no\":\"E814837300\",\"order_date\":\"2015-09-15 10:14:35\",\"ticket_totalnum\":1,\"ticket_price_all\":14650.0,\"cancel_flag\":\"Y\",\"resign_flag\":\"4\",\"return_flag\":\"N\",\"print_eticket_flag\":\"N\",\"pay_flag\":\"Y\",\"pay_resign_flag\":\"N\",\"confirm_flag\":\"N\",\"tickets\":[{\"stationTrainDTO\":{\"trainDTO\":{},\"station_train_code\":\"2126\",\"from_station_telecode\":\"BAB\",\"from_station_name\":\"北安\",\"start_time\":\"1970-01-01 14:37:00\",\"to_station_telecode\":\"HBB\",\"to_station_name\":\"哈尔滨\",\"arrive_time\":\"1970-01-01 19:46:00\",\"distance\":\"333\"},\"passengerDTO\":{\"passenger_name\":\"邓小勇\",\"passenger_id_type_code\":\"1\",\"passenger_id_type_name\":\"二代身份证\",\"passenger_id_no\":\"230119197405270472\",\"total_times\":\"98\"},\"ticket_no\":\"E8148373001080051\",\"sequence_no\":\"E814837300\",\"batch_no\":\"1\",\"train_date\":\"2015-09-15 00:00:00\",\"coach_no\":\"08\",\"coach_name\":\"08\",\"seat_no\":\"0051\",\"seat_name\":\"05号下铺\",\"seat_flag\":\"0\",\"seat_type_code\":\"4\",\"seat_type_name\":\"软卧\",\"ticket_type_code\":\"1\",\"ticket_type_name\":\"成人票\",\"reserve_time\":\"2015-09-15 10:14:35\",\"limit_time\":\"2015-09-15 10:14:35\",\"lose_time\":\"2015-09-15 10:44:56\",\"pay_limit_time\":\"2015-09-15 10:44:56\",\"ticket_price\":14650.0,\"print_eticket_flag\":\"N\",\"resign_flag\":\"4\",\"return_flag\":\"N\",\"confirm_flag\":\"N\",\"pay_mode_code\":\"Y\",\"ticket_status_code\":\"i\",\"ticket_status_name\":\"待支付\",\"cancel_flag\":\"Y\",\"amount_char\":0,\"trade_mode\":\"\",\"start_train_date_page\":\"2015-09-15 14:37\",\"str_ticket_price_page\":\"146.5\",\"come_go_traveller_ticket_page\":\"N\",\"return_deliver_flag\":\"N\",\"deliver_fee_char\":\"\",\"is_need_alert_flag\":false,\"is_deliver\":\"N\",\"dynamicProp\":\"\"}],\"reserve_flag_query\":\"p\",\"if_show_resigning_info\":\"N\",\"recordCount\":\"1\",\"isNeedSendMailAndMsg\":\"N\",\"array_passser_name_page\":[\"邓小勇\"],\"from_station_name_page\":[\"北安\"],\"to_station_name_page\":[\"哈尔滨\"],\"start_train_date_page\":\"2015-09-15 14:37\",\"start_time_page\":\"14:37\",\"arrive_time_page\":\"19:46\",\"train_code_page\":\"2126\",\"ticket_total_price_page\":\"145.5\",\"come_go_traveller_order_page\":\"N\",\"canOffLinePay\":\"N\",\"if_deliver\":\"N\"}],\"to_page\":\"db\"},\"messages\":[],\"validateMessages\":{}}";
        JSONObject jsono = JSONObject.fromObject(str);
        if (jsono.has("data")) {
            JSONObject jsonodata = jsono.getJSONObject("data");
            if (jsonodata.has("orderDBList")) {
                JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                    JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                    new TrainCreatedFreshMongo().freshMongo(jsonoorderDBList);
                }
            }
        }

    }

    /**
     * 获取n天后的DateString   负数代表n天前 0代表今天
     * yyyy-MM-dd
     * @param nexts
     * @return
     * @time 2015年9月15日 上午10:20:19
     * @author fiend
     */
    private String getNextDayString(int nexts) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(getNextDay(nexts));
    }

    /**
     * 获取n天后的Date   负数代表n天前 0代表今天
     * 
     * @param nexts
     * @return
     * @time 2015年9月15日 上午10:18:15
     * @author fiend
     */
    private Date getNextDay(int nexts) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, nexts);
        date = calendar.getTime();
        return date;
    }
}
