package com.ccservice.inter.train;

public class TrainWithholeResult {
    private String Remark;

    private String OrderNo;

    private String statuscode;

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    @Override
    public String toString() {
        return "TrainWithholeResult [Remark=" + Remark + ", OrderNo=" + OrderNo + ", statuscode=" + statuscode + "]";
    }

}
