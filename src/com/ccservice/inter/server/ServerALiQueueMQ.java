/**
 * 
 */
package com.ccservice.inter.server;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Session;

import com.aliyun.openservices.ons.api.Consumer;
  

/**
 * 放消费者监听的全局 工具类
 * @time 2016年8月11日 下午16:46:39
 * @author zhangqifei
 */
public class ServerALiQueueMQ {

    public static List<Session> TrainCreateOrderSessionList = new ArrayList<Session>();

    public static List<Consumer> MessageListenerChendongTestList = new ArrayList<Consumer>();

    public static List<Consumer> TrainCreateOrderConsumerList = new ArrayList<Consumer>();

    /**
     * 核验手机号的消费者的List
     */
    public static List<Consumer> MqCheckTrainAccountMobileNoList = new ArrayList<Consumer>();

    /**
     * 下单消费者的List
     */
    public static List<Consumer> MqTrainCreateOrderList = new ArrayList<Consumer>();
    /**
     * 下单消费者排队
     */
    public static List<Consumer> MqTrainCreateOrderListpaidui = new ArrayList<Consumer>();

}
