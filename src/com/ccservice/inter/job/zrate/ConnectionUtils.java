package com.ccservice.inter.job.zrate;

import java.io.*;
import java.sql.*;
import java.util.*;

public class ConnectionUtils {
    private static String Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    private static String url = "jdbc:sqlserver://localhost:1433;DatabaseName=B2B_DB";

    private static String dbUser = "sa";

    private static String dbPassword = "5n0wbIrd";

    /**
     * 读入filename指定的文件，并解析，把键值对中的数据取出 给全局变量url, dbUser, dbPassword赋值
     * 
     * @param filename :
     *            传入的文件名
     */
    public static void getParam(String filename) {
        Properties propes = new Properties();

        File file = new File(filename);
        try {
            FileInputStream fis = new FileInputStream(file);
            // 加载输入流指定的文件，数据放入键值对对象。
            propes.load(fis);
            // 获取文件中url(key)对应的value，给全局变量赋值
            url = propes.getProperty("url");
            dbUser = propes.getProperty("dbUser");
            dbPassword = propes.getProperty("dbPassword");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 利用getParam方法获得的参数，构造连接并返回
     * 
     * @return 连接对象
     */
    public static Connection getConnection() {
        // getParam("D://db_sqlserver.properties");
        Connection conn = null;
        try {
            Class.forName(Driver);
            conn = DriverManager.getConnection(url, dbUser, dbPassword);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 关闭连接
     * 
     * @param conn
     */
    public static void close(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭语句对象
     * 
     * @param stmt
     */
    public static void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭结果集
     * 
     * @param rs
     */
    public static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
