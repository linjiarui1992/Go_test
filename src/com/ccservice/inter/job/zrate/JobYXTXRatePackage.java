package com.ccservice.inter.job.zrate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.dom4j.DocumentException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.YeeXingBook;
import com.ccservice.inter.server.Server;
import com.ccservice.test.MD5Util;
import com.yeexing.webservice.service.IBEService;

public class JobYXTXRatePackage implements Job {
    YeeXingBook yeeXing = new YeeXingBook();

    IBEService serv = new IBEService();

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("�������»�û�������ʼ");
        String updsql1 = "UPDATE T_B2BSEQUENCE SET C_VALUE='1' WHERE C_NAME='YIXINGISSTAR'";
        Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
        if (yeeXing.getStaus().equals("1")) {
            // ɾ�����еľ���������
            System.out.println("ɾ���������¾����ݿ�ʼ");
            Server.getInstance().getAirService().excuteZrateBySql("delete from T_ZRATE where C_AGENTID=4 and ID > 10");
            System.out.println("ɾ���������¾����ݽ���");
            String sign = "";
            String zipUrl = "";
            try {
                String userName = yeeXing.getUsername();
                sign = MD5Util.MD5(URLEncoder.encode(userName + yeeXing.getKey(), "UTF-8").toUpperCase());
                System.out.println(userName + ":" + sign);
                String xml = serv.getIBEServiceHttpPort().queryAllAirp(userName, sign);
                System.out.println(xml);
                org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(xml);
                org.dom4j.Element root = doc.getRootElement();
                String is_success = root.elementText("is_success");
                if (is_success.equals("T")) {
                    zipUrl = root.elementText("fileUrl");
                }
                else {
                    System.out.println("��ȡURLʧ��");
                    return;
                }
                String s = "";
                if (zipUrl.length() > 0) {
                    s = getZipDate(zipUrl);
                }

                org.dom4j.Document doc1 = org.dom4j.DocumentHelper.parseText(s);
                org.dom4j.Element root1 = doc1.getRootElement();
                List list = root1.element("discounts").elements("discount");
                System.out.println(list.size());
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    Zrate zrate = new Zrate();
                    zrate.setAgentid(4L);
                    zrate.setCreateuser("job");
                    zrate.setCreatetime(new Timestamp(new Date().getTime()));
                    zrate.setModifytime(new Timestamp(new Date().getTime()));
                    zrate.setModifyuser("job");
                    zrate.setIsenable(1);
                    zrate.setUsertype("1");

                    org.dom4j.Element elmt = (org.dom4j.Element) it.next();
                    String departureport = elmt.attributeValue("orgCity");
                    zrate.setDepartureport(departureport);
                    String arrivalport = elmt.attributeValue("dstCity");
                    zrate.setArrivalport(arrivalport);
                    String aircompanycode = elmt.attributeValue("airComp");
                    zrate.setAircompanycode(aircompanycode);
                    String flightnumber = elmt.attributeValue("flight");
                    if (!flightnumber.equals("-")) {
                        zrate.setFlightnumber(flightnumber);
                    }
                    String weeknum = elmt.attributeValue("flightNPermit");
                    if (!weeknum.equals("-")) {
                        zrate.setWeeknum(weeknum);
                    }
                    String cabincode = elmt.attributeValue("cabin");
                    zrate.setCabincode(cabincode);
                    Float ratevalue = Float.parseFloat(elmt.attributeValue("disc"));
                    zrate.setRatevalue(ratevalue);
                    String outid = elmt.attributeValue("plcid");
                    zrate.setOutid(outid);
                    String extReward = elmt.attributeValue("extReward");
                    zrate.setAddratevalue(Float.parseFloat(extReward));
                    Timestamp begindate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                            elmt.attributeValue("startDate")).getTime());
                    zrate.setBegindate(begindate);
                    Timestamp enddate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                            elmt.attributeValue("endDate")).getTime());
                    zrate.setEnddate(enddate);
                    String tickettype = elmt.attributeValue("tickType");// 1=BSP,2=B2B
                    if (tickettype.equals("1")) {
                        zrate.setTickettype(2);
                    }
                    else {
                        zrate.setTickettype(1);
                    }
                    String changePnr = elmt.attributeValue("changePnr");
                    zrate.setIschange(Long.parseLong(changePnr));
                    Long general = Long.parseLong(elmt.attributeValue("isSphigh"));
                    zrate.setGeneral(general);
                    zrate.setZtype(general + "");
                    String remark = elmt.attributeValue("memo");
                    zrate.setRemark(remark);
                    String plcType = elmt.attributeValue("plcType");// ��ǰ�����1λ�ǵ��̵�2λ�����س̵�3λ�����س̵Ϳ�����Ϊ1��������Ϊ0
                    for (int i = 0; i < plcType.length(); i++) {
                        if (plcType.charAt(i) == '1') {
                            int t = i + 1;
                            zrate.setVoyagetype(t + "");
                        }
                    }
                    // String dayLimit = elmt.attributeValue("dayLimit");
                    String userType = elmt.attributeValue("userType");
                    zrate.setUsertype(userType);
                    String workTime = elmt.attributeValue("workTime");
                    zrate.setWorktime(workTime.split("[-]")[0]);
                    zrate.setAfterworktime(workTime.split("[-]")[1]);
                    Integer isenable = Integer.parseInt(elmt.attributeValue("plcEnable"));
                    zrate.setIsenable(isenable);
                    // String paytype = elmt.attributeValue("paytype");
                    // String modifyTime = elmt.attributeValue("modifyTime");
                    if (zrate.getIsenable().equals(1)) {
                        System.out.println("��������insert:" + zrate.getOutid());
                        Server.getInstance().getAirService().createZrate(zrate);
                    }
                }
            }
            catch (DocumentException e) {
                e.printStackTrace();
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            System.out.println("��������ȫȡ����");
            updsql1 = "UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='YIXINGISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            updsql1 = "UPDATE T_B2BSEQUENCE SET C_VALUE='12' WHERE C_NAME='YIXINGTIME'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);

        }
        else {
            System.out.println("�������½ӿڱ�����");
        }

    }

    public static void main(String[] args) {
        IBEService serv = new IBEService();
        String xml = serv.getIBEServiceHttpPort().queryAllAirp("zyfx", "65a65eb5c567d7c98be04cc6f6f68fae");
        System.out.println(xml);
    }

    // ����ȫȡ
    private String QueryAllAirp() {
        String userName = yeeXing.getUsername();
        // IBEService serv = new IBEService();
        // String sign = "";
        // // String url = "";
        String xml = "";
        // try {
        // sign = MD5Util.MD5(URLEncoder.encode(userName + yeeXing.getKey(),
        // "UTF-8").toUpperCase());
        // xml = serv.getIBEServiceHttpPort().queryAllAirp1(userName,
        // sign,1);
        // String xml = "<?xml version=\"1.0\"
        // encoding=\"GB2312\"?><result><fileUrl>http://www2.yeexing.com</fileUrl><is_success>T</is_success></result>";
        // org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(xml);
        // org.dom4j.Element root = doc.getRootElement();
        // String is_success = root.elementText("is_success");
        // if (is_success.equals("T")) {
        // url = root.elementText("fileUrl");
        // }
        // } catch (UnsupportedEncodingException e) {
        // e.printStackTrace();
        // }
        return xml;

    }

    /**
     * ����url����zip�����ҷ���zip��xml������ cd 2012-5-4 14:18:58
     * 
     * @param zipUrl
     * @return
     */
    public static String getZipDate(String zipUrl) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder result = new StringBuilder();
        URL url = null;
        FileOutputStream outfile = null;
        InputStream infile = null;
        URLConnection con = null;
        //		
        try {
            File tempFile = new File("D:\\YXTXBaseZrate");
            if (!tempFile.exists()) {
                tempFile.mkdir();
            }
            url = new URL(zipUrl);
            outfile = new FileOutputStream("D:\\YXTXBaseZrate\\" + sdf.format(new Date()) + ".zip", true);
            con = url.openConnection();
            infile = con.getInputStream();
            int f = 0;
            while ((f = infile.read()) != -1) {
                outfile.write(f);
            }
            infile.close();
            outfile.close();

            String filePath = "D:\\YXTXBaseZrate\\" + sdf.format(new Date()) + ".zip";
            ZipFile file = new ZipFile(filePath);
            ZipEntry en = file.getEntry(zipUrl.substring(zipUrl.indexOf("e=") + 2, zipUrl.length()) + ".xml");
            if ((en) != null) {
                String name = en.getName();
                if ((en = file.getEntry(name)) != null) {
                    InputStream input = file.getInputStream(en);
                    InputStreamReader insputStr = new InputStreamReader(input, "GB2312");
                    long size = en.getSize();
                    if (size > 0) {
                        BufferedReader br = new BufferedReader(insputStr);
                        String line = "";
                        while ((line = br.readLine()) != null) {
                            result.append(line);
                        }
                        br.close();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='YIXINGISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            return null;
        }
        return result.toString();
    }

    /**
     * ����url����zip�����ҷ���zip��xml������ cd 2012-5-4 14:18:58
     * 
     * @param zipUrl
     * @return
     */
    public static String getZipDateBy(String zipUrl) {
        StringBuilder result = new StringBuilder();
        try {
            String filePath = "D:\\YXTXBaseZrate\\2012-07-17.zip";
            ZipFile file = new ZipFile(filePath);
            ZipEntry en = file.getEntry("1342516050781.xml");
            if ((en) != null) {
                String name = en.getName();
                if ((en = file.getEntry(name)) != null) {
                    InputStream input = file.getInputStream(en);
                    InputStreamReader insputStr = new InputStreamReader(input, "GB2312");
                    long size = en.getSize();
                    if (size > 0) {
                        BufferedReader br = new BufferedReader(insputStr);
                        String line = "";
                        while ((line = br.readLine()) != null) {
                            result.append(line);
                        }
                        br.close();
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            String updsql1 = " UPDATE T_B2BSEQUENCE SET C_VALUE='0' WHERE C_NAME='YIXINGISSTAR'";
            Server.getInstance().getSystemService().findMapResultBySql(updsql1, null);
            return null;
        }
        return result.toString();
    }

}
