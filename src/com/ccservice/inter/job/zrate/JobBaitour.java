package com.ccservice.inter.job.zrate;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.server.Server;

public class JobBaitour implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            System.out.println(new Date() + " start job" + context.getScheduler().getContext().get("baitour"));
        }
        catch (SchedulerException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        /**
         * ���ز��� ���߱䶯
         */
        try {

            String url = "http://123.196.120.102:99/CoManage/version1/AirWsCoManage.asmx";
            com.baitour.www.version1.AirWsCoManageStub awstub2 = new com.baitour.www.version1.AirWsCoManageStub(url);
            com.baitour.www.version1.AirWsCoManageStub.GetAlterDomesticCabinZRateByParam p = new com.baitour.www.version1.AirWsCoManageStub.GetAlterDomesticCabinZRateByParam();

            p.setCoAgentOffice("B2B_035847");
            p.setCoPwd("237161");
            p.setCoUserName("thlm");
            p.setRQStartDateTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "T00:00:00.000");
            p.setTicketType("2");
            p.setTripType("1");

            com.baitour.www.version1.AirWsCoManageStub.GetAlterDomesticCabinZRateByParamResponse res = awstub2
                    .getAlterDomesticCabinZRateByParam(p);

            OMElement e = res.getGetAlterDomesticCabinZRateByParamResult().getExtraElement();

            Iterator<OMElement> it = e.getChildElements();
            System.out.println(e.getLocalName() + " e " + e.getText());

            while (it.hasNext()) {
                OMElement o = it.next();
                Zrate rate = new Zrate();
                try {
                    String srate = o.getText();
                    String[] ss = srate.split("\\|");
                    String id = o.getAttributeValue(new QName("Id"));
                    String state = o.getAttributeValue(new QName("State"));

                    rate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    rate.setCreateuser("job");
                    rate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    rate.setModifyuser("job");

                    rate.setOutid(id);
                    rate.setAgentid(Long.parseLong((String) (context.getScheduler().getContext().get("baitour"))));

                    if (ss[0] != null) {
                        rate.setDepartureport(ss[0]);
                    } //��ʼ����
                    if (ss[1] != null) {
                        rate.setArrivalport(ss[1]);
                    } //�������
                    if (ss[2] != null) {
                        rate.setAircompanycode(ss[2]);
                    }//���չ�˾
                    if (ss[3] != null) {
                        rate.setTraveltype(Integer.parseInt(ss[3]));
                    }//�г�����

                    if (ss[4] != null && ss[4].length() > 0) { //���ú���

                        rate.setType(1);
                        rate.setFlightnumber(ss[4]);
                    }
                    if (ss[5] != null && ss[5].length() > 0) { //�����ú���

                        rate.setType(2);
                        rate.setFlightnumber(ss[5]);
                    }
                    if (ss[7] != null) { // 
                        if (ss[7].equals("1")) { //bsp
                            rate.setTickettype(1);
                        }
                        else if (ss[7].equals("2")) { // b2b
                            rate.setTickettype(0);
                        }
                        else {
                            rate.setTickettype(3); //all
                        }

                    }
                    if (ss[8] != null) {
                        rate.setCabincode(ss[8]);
                    } //���ò�λ
                    if (ss[9] != null) {
                        rate.setRatevalue(Float.parseFloat(ss[9]) * 100);
                    } //���߷���

                    String[] ds = ss[10].split(",");

                    if (ds[0] != null) {
                        rate.setIssuedstartdate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(ds[0]).getTime()));
                    } //��Ʊ��ʼʱ��
                    if (ds[1] != null) {
                        rate.setIssuedendate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(ds[1]).getTime()));
                    } //��Ʊ����ʱ��

                    if (ss[11].indexOf(";") > 0) {
                        ss[11] = ss[11].substring(0, ss[11].indexOf(";"));
                    }
                    ds = ss[11].split(",");
                    if (ds[0] != null) {
                        rate.setBegindate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(ds[0]).getTime()));
                    } //������Ч�ڿ�ʼʱ��
                    if (ds[1] != null) {
                        rate.setEnddate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(ds[1]).getTime()));
                    } //������Ч�ڽ���ʱ��

                    rate.setRemark(ss[16]);
                    //				

                    List<Zrate> list = Server.getInstance().getAirService()
                            .findAllZrate(" where " + Zrate.COL_outid + "='" + rate.getOutid() + "'", "", -1, 0);
                    if (list == null || list.isEmpty()) {
                        Server.getInstance().getAirService().createZrate(rate);
                    }
                    else if (list.size() == 1) {
                        rate.setId(list.get(0).getId());
                        Server.getInstance().getAirService().updateZrateIgnoreNull(rate);
                    }
                    else {
                        Server.getInstance()
                                .getAirService()
                                .excuteZrateBySql(
                                        "delete from " + Zrate.TABLE + " where " + Zrate.COL_outid + "='"
                                                + rate.getOutid() + "'");
                        Server.getInstance().getAirService().createZrate(rate);
                    }

                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }

            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}

/**
*
1	��������	PEK	һ������
2	�������	SHA,PVG,CKG,BAV	һ������
3	���չ�˾	CA	ֻ����һ��
4	�г�����	1��2��3	1-����  2-���� 3-���̼�����
5	���ú���	1234,3456	һ������
6	�����ú���	5623,3456	һ������
7	��������	1,2,3	Ϊ����û�а�������
8	Ʊ֤����	1��2��3	1-BSP-ET 2-B2B-ET 3 ȫ��
9	���ò�λ	FCYBC	
10	��Ӷ����	0.052	ƽ̨��Ӧ�̵ķ���
11	��Ʊʱ��	2008-08-01,2008-08-31	ƽ̨��Ӧ���ܳ�Ʊ������
12	��Ч����	2008-10-01,2008-10-31
	��2008-10-01,2008-10-31;
	2008-11-01,2008-11-30	����м�����Էֺŷָ���������ʾ�������ж����Ч���ޡ�
13	��Ӧ�̺��Ŵ���	bjs807	��Ӧ���ں��еĴ���
14	��������	2008-11-18T20:20:32.827	������ƽ̨��¼������
15	�޸�����	2008-11-18T20:20:32.827	������ƽ̨���޸�����
16	��ӦӦƽ̨����	B2B_001514	
17	��ע		
*/
