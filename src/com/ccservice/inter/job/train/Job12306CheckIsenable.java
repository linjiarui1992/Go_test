package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadVerification;
import com.ccservice.inter.server.Server;

/**
 * 定时检测所有符合条件的12306账号是否可用
 * 
 * @time 2015年5月12日 下午2:00:46
 * @author chendong
 */
public class Job12306CheckIsenable extends TrainSupplyMethod implements Job {

    public static void main(String[] args) {
        new Job12306CheckIsenable().execute();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        Long l1 = System.currentTimeMillis();
        execute();
        System.out.println("Job12306CheckIsenable:耗时:" + (System.currentTimeMillis() - l1));
    }

    private void execute() {
        int threadcount = 1;
        List customerusers = Server.getInstance().getSystemService()
                .findMapResultByProcedure("sp_CustomerUser_Job12306CheckIsenable");
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(threadcount);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        String sql11 = null;
        if (customerusers.size() > 0) {
            Customeruser customeruser = new Customeruser();
            Map map = (Map) customerusers.get(0);
            String loginname = map.get("C_LOGINNAME").toString();
            String password = map.get("C_LOGPASSWORD").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            //            String repUrl = getrepurl(2).getUrl();
            String repUrl = "";
            //            repUrl = "http://localhost:9016/Reptile/traininit";
            repUrl = getrepurl().getUrl();
            String cardnunber = rep12Method(loginname, password, repUrl);//登录获取cookie;
            WriteLog.write("Job12306CheckIsenable", "id:" + id + ":loginname:" + loginname + ":cardnunber:"
                    + cardnunber + ":repUrl:" + repUrl);
            if (!"".equals(cardnunber) && !cardnunber.contains("失败")) {
                customeruser.setId(id);
                customeruser.setLoginname(loginname);
                customeruser.setCardnunber(cardnunber);
                String url = getrepurl().getUrl();
                t1 = new MyThreadVerification(customeruser, url, 1);
                pool.execute(t1);
                pool.shutdown();
            }
            else if (cardnunber.contains("登录名不存在")) {
                sql11 = "update T_CUSTOMERUSER set C_ISENABLE='16' where id=" + id;
            }
            else if ("失败".equals(cardnunber)) {
                sql11 = "update T_CUSTOMERUSER set C_ISENABLE='0' where id=" + id;
            }
            if (sql11 != null) {
                int count = Server.getInstance().getSystemService().excuteEaccountBySql(sql11);
                WriteLog.write("Job12306CheckIsenable", "id:" + id + ":count:" + count + ":sql11:" + sql11);
            }
        }
        else {
            System.out.println("Job12306CheckIsenable_没人了");
        }

    }

    public RepServerBean getrepurl() {
        //        String serviceurl = PropertyUtil.getValue("reg_serviceurl_getrepurl", "train.properties");
        //        String serviceurl = "http://121.40.174.4:39101/cn_service/service/";
        String serviceurl = "http://localhost:9001/cn_service/service/";
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //        List list = Server.getInstance().getSystemService().findMapResultByProcedure("sp_RepServer_GetOneRep");
        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;

    }

    /**
     * type 1=登录（打码）2=保持在线（不打码）
     * 此方法是根据 C_USENUMBER 使用次数排序低的排到前面
     * @param type
     * @return
     * @time 2015年3月20日 下午4:45:58
     * @author chendong
     */
    public RepServerBean getrepurl(int type) {
        List list = new ArrayList();
        RepServerBean rep = new RepServerBean();
        String url1 = "http://192.168.0.5:9001/cn_service/service/";
        try {
            ISystemService iSystemService = (ISystemService) new HessianProxyFactory().create(ISystemService.class,
                    url1 + ISystemService.class.getSimpleName());
            list = iSystemService.findMapResultByProcedure("sp_RepServer_GetOneRep");
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;
    }
}
