package com.ccservice.inter.job.train;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * 获取支付链接工具类
 * @time 2014年12月19日 下午8:01:55
 * @author wzc
 */
public class Job12306GetPayUrl extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        payjob();
    }

    public static void main(String[] args) {
        new Job12306GetPayUrl().payjob();
    }

    /**
     * 自动获取支付链接Job
     * 
     * @time 2014年12月20日 下午2:21:09
     * @author wzc
     */
    public void payjob() {
        String getpayurltype = getSysconfigString("getpayurltype");//支付类型
        String seachsql = " select ID,C_SUPPLYPRICE,C_SUPPLYACCOUNT,C_EXTNUMBER,C_ORDERNUMBER,C_ISJOINTTRIP from T_TRAINORDER where  C_EXTNUMBER is not null "
                + "and C_SUPPLYACCOUNT is not null  and  C_STATE12306=4 and C_ISQUESTIONORDER!=2 and C_ORDERSTATUS in (1,2) ";
        if (getpayurltype != null) {
            boolean flag = true;
            if ("1".equals(getpayurltype)) {//获取银联支付链接
                seachsql += " AND (C_AUTOUNIONPAYURL IS NULL OR C_AUTOUNIONPAYURL='') ";
            }
            else if ("2".equals(getpayurltype)) {//获取支付宝支付链接
                seachsql += " AND (C_CHANGESUPPLYTRADENO IS NULL OR C_CHANGESUPPLYTRADENO='') ";
            }
            else {
                flag = false;
                WriteLog.write("12306银联自动支付链接", "没有获取到获取支付链接方式……");
            }
            seachsql += "  order by C_ORDERSTATUS desc ";
            WriteLog.write("12306银联自动支付链接", "sql:" + seachsql);
            if (flag) {
                String paygeturlcount = getSysconfigString("paygeturlcount");
                WriteLog.write("12306银联自动支付链接", ":当前线程数：" + paygeturlcount + ":");
                List<Trainorder> orderlist = getTrainorder(seachsql);
                ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(paygeturlcount).intValue());
                for (int i = 0; i < orderlist.size(); i++) {
                    try {
                        //                        Trainorder order = orderlist.get(i);
                        //                        Customeruser user = get12306AccountByLoginname(order);
                        //                        String url = randomIp();
                        //                        Thread thr = new MyThreadGetPayURL(order, user, getpayurltype, url);
                        //                        pool.execute(thr);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                pool.shutdown();
            }
        }
        else {
            WriteLog.write("12306银联自动支付链接", "没有获取到获取支付链接方式……");
        }
    }

    /**
     *  随机调取下单接口，获取12306-cookie所在IP
     * @return cookie所在IP
     * @time 2014年12月16日 下午12:07:53
     * @author fiend
     */
    public String randomgetpayIp(int i) {
        String canorderips = getSysconfigString("alipaygeturlstr");
        if (canorderips.equals("-1") || canorderips.trim().equals("")) {
            return getSysconfigString("alipaygeturl");
        }
        int num = (i % canorderips.split(",").length) + 1;
        return getSysconfigString("alipaygeturl" + num);
    }

    /**
     * 根据订单查询绑定12306用户
     * 
     * @param order
     * @return
     * @time 2014年12月20日 下午2:06:40
     * @author wzc
     * 联程 wsad00005001/shuwsad00005001/JTYH
     * 单程 wsad00005001/shu
     * 
     * 需返回已登录的12306账号
     * 
     */
    public Customeruser get12306AccountByLoginname(Trainorder order) {
        String supplyacount = order.getSupplyaccount();
        if (supplyacount != null) {
            String[] ary = supplyacount.split("/");
            if (ary.length > 0) {
                String wheresql = " where C_LOGINNAME='" + ary[0] + "' ";
                List<Customeruser> users = Server.getInstance().getMemberService()
                        .findAllCustomeruser(wheresql, "", -1, 0);
                if (users.size() > 0) {
                    return users.get(0);
                }
                WriteLog.write("12306银联自动支付链接", order.getOrdernumber() + ",未绑定12306账号");
            }
            else {
                WriteLog.write("12306银联自动支付链接", order.getOrdernumber() + ",未绑定12306账号");
            }
        }
        else {
            WriteLog.write("12306银联自动支付链接", order.getOrdernumber() + ",未绑定12306账号");
        }
        return null;
    }

    /**
     * 
     * 
     * @param exesql 须执行sql
     * @return
     * @time 2014年12月19日 下午9:52:48
     * @author wzc
     */
    public List<Trainorder> getTrainorder(String exesql) {
        List<Trainorder> orderlist = new ArrayList<Trainorder>();
        List list = Server.getInstance().getSystemService().findMapResultBySql(exesql, null);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                try {
                    Map map = (Map) list.get(i);
                    Trainorder order = new Trainorder();
                    long id = Long.valueOf(map.get("ID").toString());//订单id
                    String supplyaccount = map.get("C_SUPPLYACCOUNT") == null ? null : map.get("C_SUPPLYACCOUNT")
                            .toString();//支付账号
                    String supplyprice = map.get("C_SUPPLYPRICE") == null ? null : map.get("C_SUPPLYPRICE").toString();//支付供应价格
                    order.setId(id);
                    String extnumber = map.get("C_EXTNUMBER") == null ? null : map.get("C_EXTNUMBER").toString();//供应订单号
                    String isjointrip = map.get("C_ISJOINTTRIP") == null ? "0" : map.get("C_ISJOINTTRIP").toString();//是否为联程
                    order.setOrdernumber(map.get("C_ORDERNUMBER").toString());
                    if (supplyaccount != null) {
                        order.setSupplyaccount(supplyaccount);
                    }
                    if (supplyprice != null) {
                        order.setSupplyprice(Float.valueOf(supplyprice));
                    }
                    if (extnumber != null) {
                        order.setExtnumber(extnumber);
                    }
                    order.setIsjointtrip(Integer.parseInt(isjointrip));
                    orderlist.add(order);
                }
                catch (Exception e) {
                    WriteLog.write("12306银联自动支付链接", "异常:getTrainorder" + e.getMessage());
                }
            }
        }
        return orderlist;
    }

}
