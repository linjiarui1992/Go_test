package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Random;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.util.PageInfo;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadAutoPayUnion;
import com.ccservice.inter.server.Server;

/**
 * 银联自动支付 
 * @time 2014年12月16日 上午11:46:04
 * @author fiend
 */
public class JobAutoPayUnion extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String isqunaracquisitionrefund = getSysconfigString("unionpayurlstate");
        WriteLog.write("12306银联自动支付", "开关：" + isqunaracquisitionrefund);
        if ("0".equals(isqunaracquisitionrefund)) {
            try {
                changeSystemCofigbyname("unionpayurlstate", "1");//标记为在获取
                getTrainorderToPay();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            changeSystemCofigbyname("unionpayurlstate", "0");//标记为获取结束
        }
    }

    /**
     * 交行自动支付
     * 
     * @time 2014年12月26日 上午11:37:50
     * @author wzc
     */
    public void getTrainorderToPay() {
        String wheresql = " where O.C_ORDERSTATUS=2 and O.C_STATE12306=4 and (C_ISQUESTIONORDER=0 "
                + "or C_ISQUESTIONORDER is null) and O.C_AUTOUNIONPAYURL is not null and O.C_AUTOUNIONPAYURL!='' ";
        String[] strs = getSysconfigString("unionpayurlnum").split(",");
        PageInfo pageinfo = new PageInfo();
        pageinfo.setPagerow(200);
        List<Trainorder> list = Server.getInstance().getTrainService()
                .findAllTrainorderBySql(wheresql, " order by O.id asc ", pageinfo);
        if (list.size() > 0) {
            list.remove(0);
        }
        int accountindex = 0;
        for (int i = 0; i < list.size(); i++) {
            int t = new Random().nextInt(100000);
            try {
                Trainorder trainorder = list.get(i);
                trainorder = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
                WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber());
                if (trainorder.getPassengers().get(0).getTraintickets().size() == 1) {
                    try {
                        WriteLog.write("12306银联自动支付线程", t + ":开启线程" + trainorder.getOrdernumber());
                        String i_url = strs[accountindex];
                        String unionpayurl = getSysconfigString("unionpayurl" + i_url);
                        String tcTrainCallBack = getSysconfigString("tcTrainCallBack");
                        MyThreadAutoPayUnion unthread = new MyThreadAutoPayUnion(trainorder, i_url, t, unionpayurl,
                                tcTrainCallBack);
                        unthread.run();
                        long time = Long.valueOf(getSysconfigString("unionstaytime"));
                        Thread.sleep(time / strs.length);
                        WriteLog.write("12306银联自动支付线程", t + ":结束线程：" + trainorder.getOrdernumber());
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            catch (Exception e) {
                WriteLog.write("12306银联自动支付", t + ":error：" + e.getMessage());
            }
            accountindex++;
            if (accountindex == strs.length) {
                accountindex = 0;
            }
        }
    }
}
