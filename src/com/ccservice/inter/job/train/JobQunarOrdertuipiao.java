package com.ccservice.inter.job.train;

import java.net.URL;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.rule.QunarTrainRule;
import com.tenpay.util.MD5Util;

/**
 * 
 * 获取去哪退票数据
 * @time 2014年12月18日 下午6:04:15
 * @author yinshubin
 */
public class JobQunarOrdertuipiao extends TrainSupplyMethod implements Job {
    private final String refund_url = "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=hangt&type=APPLY_REFUND&HMAC=8C208FCE86461911FE129D4BB261828E";

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub
        URL refundurl;
        String isqunaracquisitionrefund = getSysconfigStringbydb("isqunaracquisitionrefund");
        if ("0".equals(isqunaracquisitionrefund)) {
            try {
                changeSystemCofigbyname("isqunaracquisitionrefund", "1");//标记为在获取
                refundurl = new URL(this.refund_url);
                WriteLog.write("qunartrainorder_test", "refundurl:" + refundurl);

                doGET_APPLY_REFUND(QunarTrainRule.getJson(refundurl));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            changeSystemCofigbyname("isqunaracquisitionrefund", "0");//标记为获取结束
        }
    }

    /**
     * 说明：获取申请退票订单内部信息，传入数据库
     * @param jsonObject
     * @time 2014年8月30日 上午11:07:19
     * @author yinshubin
     */
    public void doGET_APPLY_REFUND(JSONObject jsonObject) {
        if (jsonObject.containsKey("data")) {
            WriteLog.write("qunartrainorder_test", "jsonObject:" + jsonObject);
            JSONArray data = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonObject.getIntValue("total"); i++) {//遍历获取的去哪儿网订单
                JSONObject info = data.getJSONObject(i);
                WriteLog.write("qunartrainorder_test", "info:" + info);
                String QunarOrdernumber = info.getString("orderNo");//加个去哪儿网订单号去查
                JSONArray passengers = info.getJSONArray("passengers");
                Trainform trainform = new Trainform();
                int agentid = Integer.valueOf(getSysconfigString("qunar_agentid"));
                trainform.setAgentid(agentid);
                trainform.setQunarordernumber(QunarOrdernumber);
                trainform.setOrderstatus(Trainorder.ISSUED);
                List<Trainorder> trainorderList = Server.getInstance().getTrainService()
                        .findAllTrainorder(trainform, null);
                WriteLog.write("qunartrainorder_test", "trainorderList:" + trainorderList.size());
                int m = 0;
                for (Trainorder t : trainorderList) {//遍历本地的去哪儿网已出票并且去哪儿网订单号相等的订单
                    t = Server.getInstance().getTrainService().findTrainorder(t.getId());
                    //申请退票唯一标识（同一个订单的同一批次请求的标识一致）
                    String identificationString = QunarOrdernumber + "_" + (int) (Math.random() * 10000);
                    if (QunarOrdernumber != null && QunarOrdernumber.equals(t.getQunarOrdernumber())) {//判断去哪儿订单号是否相同
                        WriteLog.write("qunartrainorder_test", "QunarOrdernumber:" + t.getQunarOrdernumber());
                        for (int j = 0; j < passengers.size(); j++) {//遍历获取的去哪儿网订单所包含的乘客
                            JSONObject infop = passengers.getJSONObject(j);
                            if (infop.containsKey("ticketType")) {//web版本有ticketType
                                WriteLog.write("qunartrainorder_test", infop + "");
                                for (Trainpassenger p : t.getPassengers()) {//遍历本地的去哪儿网订单所包含的乘客
                                    if (infop.getString("certNo") != null
                                            && (infop.getString("certNo").equals(p.getIdnumber()))) {//判断乘客证件号是否一致
                                        WriteLog.write("qunartrainorder_test", p.getName());
                                        for (Trainticket tk : p.getTraintickets()) {//遍历本地的去哪儿网订单所包含的乘客手里的票
                                            if (infop.getString("ticketType") != null
                                                    && infop.getString("ticketType").equals(
                                                            String.valueOf(tk.getTickettype()))
                                                    && (tk.getStatus() == Trainticket.ISSUED || tk.getStatus() == Trainticket.NONREFUNDABLE)) {//通过ticketType判断乘客是否为同一个乘客,当前票状态是不是已出票或者无法退票
                                                WriteLog.write("qunartrainorder_test", tk.getId() + "");
                                                Trainticket ticket = new Trainticket();
                                                ticket.setId(tk.getId());
                                                ticket.setIsapplyticket(1);
                                                ticket.setStatus(Trainticket.APPLYTREFUND);
                                                ticket.setRefundRequestTime(ElongHotelInterfaceUtil.getCurrentTime());
                                                Server.getInstance().getTrainService().updateTrainticket(ticket);
                                                trainRefundAppoint(t.getId(), tk.getId(), identificationString);
                                                m++;
                                            }
                                        }
                                    }
                                }
                            }
                            else {//去哪儿网的旧版本没有ticketType
                                int k = 0;
                                for (Trainpassenger p : t.getPassengers()) {//遍历本地的去哪儿网订单所包含的乘客
                                    if (infop.getString("certNo") != null
                                            && (infop.getString("certNo").equals(p.getIdnumber()))) {//判断乘客证件号是否一致
                                        for (Trainticket tk : p.getTraintickets()) {//遍历本地的去哪儿网订单所包含的乘客手里的票
                                            if ((tk.getStatus() == Trainticket.ISSUED || tk.getStatus() == Trainticket.NONREFUNDABLE)) {//当前票状态是不是已出票或者无法退票
                                                Trainticket ticket = new Trainticket();
                                                ticket.setId(tk.getId());
                                                ticket.setIsapplyticket(1);
                                                ticket.setStatus(Trainticket.APPLYTREFUND);
                                                Server.getInstance().getTrainService().updateTrainticket(ticket);
                                                trainRefundAppoint(t.getId(), tk.getId(), identificationString);
                                                k++;
                                                m++;
                                            }
                                        }
                                    }
                                    if (k > 0) {
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
                if (m == 0) {
                    WriteLog.write("qunartrainorder_test", "未出票订单:" + m);
                    trainform.setOrderstatus(Trainorder.WAITISSUE);
                    List<Trainorder> trainorderListElse = Server.getInstance().getTrainService()
                            .findAllTrainorder(trainform, null);
                    for (Trainorder tElse : trainorderListElse) {//遍历本地的去哪儿网未出票订单
                        if (QunarOrdernumber != null && QunarOrdernumber.equals(tElse.getQunarOrdernumber())) {//判断去哪儿订单号是否相同
                            WriteLog.write("qunartrainorder_test", "未出票订单:" + QunarOrdernumber);
                            Trainorder trainorder = new Trainorder();
                            trainorder.setId(tElse.getId());
                            trainorder.setOrderstatus(Trainorder.CANCLED);
                            Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            break;
                        }
                    }
                }
            }
        }
        else {
            WriteLog.write("qunartrainorder_test", "jsonObject:" + jsonObject);
        }
    }

    /**
     * qunar申请退票标识表
     * @param orderid
     * @param ticketid
     * @param identificationString
     * @author fiend
     */
    private void trainRefundAppoint(long orderid, long ticketid, String identificationString) {
        String sql = "insert into TrainRefundAppoint(OrderId,TicketId,Identification,Status,Remark) values(" + orderid
                + "," + ticketid + ",'" + identificationString + "',0,'')";
        try {
            WriteLog.write("QUNAR_TRAINREFUNDAPPOINT", sql);
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
        catch (Exception e) {
            WriteLog.write("QUNAR_TRAINREFUNDAPPOINT_ERROR", sql);
            ExceptionUtil.writelogByException("QUNAR_TRAINREFUNDAPPOINT_ERROR", e);
        }
    }
}
