package com.ccservice.inter.job.train;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.inter.train.tongcheng.FinshPullOrderUtil;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.Method.KongTieRabbtMQMethod;

public class JobQueryFinshPullOrder implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println("定时审核同程订单开启(" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + ")");
        try {
            initsSQLCallBackStatus();
            initsSQLDepTimeFinshPullOrder();
            initDepTimeFinshPullOrder();
            initsSQLType_FinshPullOrder();
            initsSQLtype_FinshPullOrder();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    //CallBackStatus=2--这种回调出票接口加上failofpull12306字段（InterfaceStatus）
    public void initsSQLCallBackStatus() {
        String sSQL = "select_CallBackStatus_FinshPullOrder";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sSQL);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                long FKOrderId = Long.valueOf(map.get("FKOrderId").toString());
                int InterfaceStatus = Integer.valueOf(map.get("InterfaceStatus").toString());
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(FKOrderId);
                String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
                SendTongChengFalse(trainorder, tongchengcallbackurl, InterfaceStatus);
            }
        }

    }

    //发审核
    public void initsSQLDepTimeFinshPullOrder() {
        String sSQL = "select_DepTime1_FinshPullOrder";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sSQL);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                long FKOrderId = Long.valueOf(map.get("FKOrderId").toString());
                JSONObject jsoseng = new JSONObject();
                jsoseng.put("type", MQMethod.QUERY);
                jsoseng.put("orderid", FKOrderId);
                jsoseng.put("successtime", System.currentTimeMillis());
                jsoseng.put("intotime", System.currentTimeMillis());
                jsoseng.put("queryno", 0);
                new KongTieRabbtMQMethod().activeMQquery(jsoseng.toString());
            }
        }

    }

    //发审核
    public void initDepTimeFinshPullOrder() {
        String sSQL = "select_DepTime2_FinshPullOrder";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sSQL);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                long FKOrderId = Long.valueOf(map.get("FKOrderId").toString());
                JSONObject jsoseng = new JSONObject();
                jsoseng.put("type", MQMethod.QUERY);
                jsoseng.put("orderid", FKOrderId);
                jsoseng.put("successtime", System.currentTimeMillis());
                jsoseng.put("intotime", System.currentTimeMillis());
                jsoseng.put("queryno", 0);
                new KongTieRabbtMQMethod().activeMQquery(jsoseng.toString());
            }
        }

    }

    //这种回调出票接口加上failofpull12306字段（Status==2||Status==3?Status:4）
    public void initsSQLType_FinshPullOrder() {
        String sSQL = "select_Type1_FinshPullOrder";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sSQL);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                long FKOrderId = Long.valueOf(map.get("FKOrderId").toString());
                int Status = Integer.valueOf(map.get("Status").toString());
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(FKOrderId);
                String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
                SendTongChengFalse(trainorder, tongchengcallbackurl, Status == 2 || Status == 3 ? Status : 4);
            }
        }

    }

    public void initsSQLtype_FinshPullOrder() {
        //这种回调出票接口加上failofpull12306字段（Status==2||Status==3?Status:4）
        String sSQL = "select_Type2_FinshPullOrder";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sSQL);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                long FKOrderId = Long.valueOf(map.get("FKOrderId").toString());
                int Status = Integer.valueOf(map.get("Status").toString());
                Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(FKOrderId);
                String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
                SendTongChengFalse(trainorder, tongchengcallbackurl, Status == 2 || Status == 3 ? Status : 4);
            }
        }

    }

    /**
     * 回调同程失败failofpull12306 1
     * @param trainorder
     * @param tongchengcallbackurl
     * @param failofpull12306
     * @param Status
     * @return
     */
    public String SendTongChengFalse(Trainorder trainorder, String tongchengcallbackurl, int failofpull12306) {
        String url = tongchengcallbackurl;
        JSONObject jso = new JSONObject();
        String result = "false";
        jso.put("pkid", trainorder.getId());
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getTradeno());
        jso.put("method", "train_pay_callback");
        if (failofpull12306 != 3 && failofpull12306 != 4) {
            jso.put("isSuccess", "N");
        }
        boolean isTongCheng = FinshPullOrderUtil.isTongChengOrder(trainorder);
        if (trainorder.getOrderstatus() == 19 && isTongCheng) {//19是Trainorder.FINSHPULLORDER
            jso.put("failofpull12306", failofpull12306);
            try {
                WriteLog.write("审核回调同程出票JobQueryFinshPullOrder", trainorder.getId() + ":tongcheng_tongzhi_chupiao:"
                        + url + ":" + jso.toString());
                long t1 = System.currentTimeMillis();
                result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
                long t2 = System.currentTimeMillis();
                WriteLog.write("审核回调同程出票JobQueryFinshPullOrder", trainorder.getId() + ":返回:" + result + ":用时："
                        + (t2 - t1));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 根据sysconfig的name获得value
     * 内存
     * @param name
     * @return
     */
    public String getSysconfigString(String name) {
        String result = "-1";
        try {
            if (Server.getInstance().getDateHashMap().get(name) == null) {
                List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                        .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
                if (sysoconfigs.size() > 0) {
                    result = sysoconfigs.get(0).getValue();
                    Server.getInstance().getDateHashMap().put(name, result);
                }
            }
            else {
                result = Server.getInstance().getDateHashMap().get(name);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
