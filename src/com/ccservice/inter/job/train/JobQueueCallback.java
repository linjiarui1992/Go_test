package com.ccservice.inter.job.train;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadQueueCallBack;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

public class JobQueueCallback implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String tcTrainCallBack = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
        queueCreate(tcTrainCallBack);
        queueChange(tcTrainCallBack);
    }

    /**
     * 处理排队订单
     * 
     * @time 2016年4月19日 下午2:55:03
     * @author fiend
     */
    private void queueCreate(String tcTrainCallBack) {

        queue(1, tcTrainCallBack);
    }

    /**
     * 处理排队改签
     * 
     * @time 2016年4月19日 下午2:55:03
     * @author fiend
     */
    private void queueChange(String tcTrainCallBack) {
        queue(2, tcTrainCallBack);
    }

    /**
     * 处理排队
     * 
     * @time 2016年4月19日 下午2:55:03
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    private void queue(int sqlType, String tcTrainCallBack) {
        String logName = "定时回调排队状态";
        if (sqlType == 1) {
            logName += "_占座";
        }
        else {
            logName += "_改签";
        }
        try {
            String sql = " [sp_TrainOrderQueue_SelectAll] @Type=" + sqlType;
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            WriteLog.write(logName, "需要回调的个数--->" + list.size() + "<---");
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                String reqToken = map.containsKey("reqToken") && map.get("reqToken") != null ? map.get("reqToken")
                        .toString() : "";
                String AgentId = map.containsKey("AgentId") ? map.get("AgentId").toString() : "";
                String interfaceOrderNumber = map.containsKey("interfaceOrderNumber") ? map.get("interfaceOrderNumber")
                        .toString() : "";
                String localOrderNumber = map.containsKey("localOrderNumber") ? map.get("localOrderNumber").toString()
                        : "";
                String QueueCollectTime = map.containsKey("QueueCollectTime") ? map.get("QueueCollectTime").toString()
                        : "";
                String QueueStartTime = map.containsKey("QueueStartTime") ? map.get("QueueStartTime").toString() : "";
                String WaitCount = map.containsKey("WaitCount") ? map.get("WaitCount").toString() : "";
                String WaitTime = map.containsKey("WaitTime") ? map.get("WaitTime").toString() : "";
                String Type = map.containsKey("Type") ? map.get("Type").toString() : "";
                WriteLog.write(logName, interfaceOrderNumber + "--->oldQueueCollectTime--->" + QueueCollectTime
                        + "--->oldWaitTime--->" + WaitTime);
                if (!ElongHotelInterfaceUtil.StringIsNull(reqToken) && !ElongHotelInterfaceUtil.StringIsNull(AgentId)
                        && !ElongHotelInterfaceUtil.StringIsNull(interfaceOrderNumber)
                        && !ElongHotelInterfaceUtil.StringIsNull(localOrderNumber)
                        && !ElongHotelInterfaceUtil.StringIsNull(QueueCollectTime)
                        && !ElongHotelInterfaceUtil.StringIsNull(QueueStartTime)
                        && !ElongHotelInterfaceUtil.StringIsNull(WaitCount)
                        && !ElongHotelInterfaceUtil.StringIsNull(WaitTime)) {
                    long QueueCollectLongTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(QueueCollectTime)
                            .getTime();
                    long nowLongTime = System.currentTimeMillis();
                    //如果当前时间nowLongTime-1分钟 在查看时间QueueCollectLongTime之后
                    //并且排队的时间WaitTime不为-1
                    //并且查看时间QueueCollectLongTime+排队的时间WaitTime大于当前时间nowLongTime+1分钟
                    //我们需要把查看时间QueueCollectLongTime改成当前时间nowLongTime
                    //排队的时间WaitTime减去查看时间QueueCollectLongTime所修改的时间（当前时间-QueueCollectLongTime）
                    if ((nowLongTime - 1000 * 60 > QueueCollectLongTime) && !"-1".equals(WaitTime)
                            && (QueueCollectLongTime + Integer.valueOf(WaitTime) * 1000 > nowLongTime + 1000 * 60)) {
                        QueueCollectLongTime = System.currentTimeMillis();
                        WaitTime = String.valueOf(Integer.valueOf(WaitTime)
                                - ((nowLongTime - QueueCollectLongTime) / 1000));
                    }
                    QueueCollectTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(QueueCollectLongTime));
                    QueueStartTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss.SSS").parse(QueueStartTime));
                    WriteLog.write(logName, interfaceOrderNumber + "--->newQueueCollectTime--->" + QueueCollectTime
                            + "--->newWaitTime--->" + WaitTime);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("agentid", AgentId);
                    jsonObject.put("interfaceOrderNumber", interfaceOrderNumber);
                    jsonObject.put("localOrderNumber", localOrderNumber);
                    jsonObject.put("reqtoken", reqToken);
                    jsonObject.put("queue_type", Type);
                    jsonObject.put("queue_start_time", QueueStartTime);
                    jsonObject.put("queue_collect_time", QueueCollectTime);
                    jsonObject.put("queue_wait_time", WaitTime);
                    jsonObject.put("queue_wait_nums", WaitCount);
                    jsonObject.put("method", "query_queue");
                    new MyThreadQueueCallBack(interfaceOrderNumber, tcTrainCallBack, jsonObject.toString()).start();
                }
                if (i % 100 == 0) {
                    try {
                        Thread.sleep(1000L);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException(logName + "_ERROR", e);
        }
    }
}
