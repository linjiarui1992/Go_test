package com.ccservice.inter.job.train.thread;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.db.util.MongoLogic;
import com.ccervice.util.db.DBHelperAccount;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;
import com.ccservice.qunar.util.ExceptionUtil;
import com.mongodb.DBObject;

public class CheckAccountAndSendMeiTuanPassengerThread extends Thread {

    public Customeruser user;

    public CheckAccountAndSendMeiTuanPassengerThread(Customeruser user) {
        this.user = user;
    }

    @Override
    public void run() {
        if (user == null) {
            WriteLog.write("CheckAccountAndSendMeiTuanPassengerThread", "user is null");
        }
        else {

            List<Trainpassenger> trainPassengers = new ArrayList<Trainpassenger>();
            try {
                //根据帐号名在mogoDB中查出该帐号下所有的常旅
                List<DBObject> personlist = new MongoLogic().FindMongoByCustomerUser(user.getLoginname());
                if (personlist.size() > 0) {
                    for (int i = 0; i < personlist.size(); i++) {
                        JSONObject json = JSONObject.parseObject(personlist.get(i).toString());
                        String RealName = json.getString("RealName");
                        String IDNumber = json.getString("IDNumber").toUpperCase();
                        if (IDNumber.contains("X")) {
                            IDNumber = IDNumber.replace("X", "10");
                        }
                        //根据常旅的身份证号查出该乘客的所有帐号
                        List list = new MongoLogic().findIDAll(Long.valueOf(IDNumber));
                        boolean isUsable = false;
                        Trainpassenger trainPassenger = new Trainpassenger();
                        for (int j = 0; j < list.size(); j++) {
                            JSONObject jsonList = JSONObject.parseObject(list.get(j).toString());
                            //拿到该帐号后去帐号库里查出帐号状态
                            String SupplyAccount = jsonList.get("SupplyAccount").toString();
                            String sql = "SELECT ID,C_ISENABLE FROM T_CUSTOMERUSER WITH(NOLOCK) WHERE C_LOGINNAME='"
                                    + SupplyAccount + "'";
                            DataTable result = DBHelperAccount.GetDataTable(sql);
                            List<DataRow> dataRows = result.GetRow();

                            if (dataRows.size() > 0) {
                                DataRow dataRow = dataRows.get(0);
                                int isEnable = Integer.parseInt(dataRow.GetColumnString("C_ISENABLE"));
                                if (isEnable == 1 || isEnable == 3 || isEnable == 4 || isEnable == 8 || isEnable == 10) {
                                    isUsable = true;
                                    break;
                                }
                            }
                        }
                        if (!isUsable) {
                            trainPassenger.setName(RealName);
                            trainPassenger.setIdnumber(IDNumber);
                            trainPassengers.add(trainPassenger);
                        }
                    }
                }
                WriteLog.write("CheckAccountAndSendMeiTuanPassengerThread", user.getLoginname() + "--->"
                        + trainPassengers.size());
                if (trainPassengers.size() > 0) {
                    CallBackPassengerUtil.callBackTongcheng(user, trainPassengers, 2);
                }
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("CheckAccountAndSendMeiTuanPassengerThread_err", e);
            }

        }
    }
}
