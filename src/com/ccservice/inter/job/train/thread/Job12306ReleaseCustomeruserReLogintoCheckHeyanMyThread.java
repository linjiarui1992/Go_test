package com.ccservice.inter.job.train.thread;

import java.sql.Timestamp;
import java.util.Random;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.server.Server;

/**
 * 定时访问12306，保持12306账号在线，并将已通过人数同步到数据库
 * 
 * @time 2014年12月27日 上午9:52:42
 * @author chendong
 */
public class Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread extends Thread {

    String url;

    String loginname;

    String password;

    String createtime;

    Long id;

    ISystemService isystemservice;

    String msg;

    int type;//0 老核验 1核验手机号

    String useProxy;

    public Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread(String loginname, String password, String createtime,
            Long id, String url, ISystemService isystemservice, int type) {
        super();
        this.loginname = loginname;
        this.password = password;
        this.createtime = createtime;
        this.id = id;
        this.url = url;
        this.isystemservice = isystemservice;
        this.type = type;//0 老核验 1核验手机号
    }

    @Override
    public void run() {
        //        是否使用代理
        useProxy = PropertyUtil
                .getValue("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_checkMobileNoProxyUseProxy",
                        "train.properties");
        torunMethod();
    }

    /**
     * 
     * @time 2015年7月12日 上午9:07:43
     * @author chendong
     */
    private void torunMethod() {
        String resultString = "-1";
        int r1 = new Random().nextInt(1000000);
        Long l1 = System.currentTimeMillis();
        String cookieString = JobTrainUtil.getCookie(this.url, loginname, password);
        String foldername = createtime.substring(0, 10);
        if (type == 1) {//1核验手机号
            Server.dangqianheyancount++;
            try {
                checkMobile(cookieString, foldername);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            Server.dangqianheyancount--;
            System.out.println(loginname + ":当前线程结束,总核验线程数:" + Server.dangqianheyancount);
        }
        else if (type == 0 || type == 2) { //0 老核验
            String ticketnumsql = "";
            String writeString = "";
            String sql_type = "";
            //您的用户信息被他人冒用，请重新在网上注册新的账户，为了确保您的购票安全，您还需尽快到就近的办理客运售票业务的铁路车站完成身份核验，谢谢您对12306网站的支持。
            if (cookieString.indexOf("登录名不存在") >= 0 || cookieString.indexOf("请核实您注册用户信息是否真实") >= 0
                    || cookieString.indexOf("用户信息被他人冒用") >= 0) {
                //C_ISENABLE=2:账号已废
                ticketnumsql = "update T_CUSTOMERUSER set C_MODIFYTIME='" + new Timestamp(System.currentTimeMillis())
                        + "',C_ISENABLE=2 where ID=" + this.id;
                //找回的账号都设置为16的状态
                int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
                WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_bucunzai", foldername + ":"
                        + loginname + ":" + cookieString);
                WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_bucunzai", foldername + ":"
                        + count1 + ":" + ticketnumsql);
                return;
            }
            else if (cookieString.indexOf("该用户已被暂停使用") >= 0) {
                ticketnumsql = "update T_CUSTOMERUSER set C_MODIFYTIME='" + new Timestamp(System.currentTimeMillis())
                        + "',C_ISENABLE=22 where ID=" + this.id;
                //找回的账号都设置为16的状态
                int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
                WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_zhanting", foldername + ":"
                        + loginname + ":" + cookieString + ":" + ticketnumsql);
                return;
            }
            else if ("-1".equals(cookieString) || cookieString.contains("失败")) {
                System.out.println("cookieString:" + cookieString);
                WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_login_fail", loginname + ":"
                        + this.url + ":" + cookieString);
                return;
            }
            JSONObject json = JobTrainUtil.getCountString(this.url, cookieString);
            int count = json.getInteger("passengercount");//{"queryMyOrderNoComplete":"","passengercount":1,"msg":"已通过"}
            this.msg = json.getString("msg");

            WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_passengercount", foldername + ":"
                    + loginname + ":count:" + count);
            writeString = r1 + ":现有:" + count + ":人:" + this.loginname;
            System.out.println(loginname + "====================================================" + this.msg);
            if ("用户未登录".equals(this.msg)) {
                torunMethod();
            }
            else if ("未通过".equals(this.msg)) {
                sql_type = ",C_ISENABLE=-1";
                ticketnumsql = "update T_CUSTOMERUSER set C_MODIFYTIME='" + new Timestamp(System.currentTimeMillis())
                        + "'" + sql_type + ",C_LOGINNUM=" + count + ",C_LEVEL=NULL where ID=" + this.id;
                int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
                WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_weitongguo", loginname
                        + ":ticketnumsql:" + ticketnumsql);
                return;
            }
            else if (count >= 0) {// && "已通过".equals(msg)
                count = count == 0 ? 1 : count;
                sql_type = ",C_ISENABLE=1";
                ticketnumsql = "update T_CUSTOMERUSER set C_MODIFYTIME='" + new Timestamp(System.currentTimeMillis())
                        + "'" + sql_type + ",C_LOGINNUM=" + count + ",C_LEVEL=16 where ID=" + this.id;
                //找回的账号都设置为16的状态
                int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
                WriteLog.write(foldername, "Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread", loginname + ":"
                        + cookieString);
                WriteLog.write(foldername, "Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread", loginname + ":"
                        + writeString + ":现有人数:" + ":ticketnumsql:" + ticketnumsql + ":count1:" + count1
                        + ":resultString:" + resultString + "========" + this.url);
            }
            else {
                //                sql_type = ",C_ISENABLE=1";
                //                ticketnumsql = "update T_CUSTOMERUSER set C_STATE=0" + sql_type + " where ID=" + this.id;
                if ("待核验".equals(msg)) {
                    ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=17,C_STATE=0 where ID=" + this.id;
                    int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
                }
                writeString = r1 + ":" + this.loginname + ":已掉线|msg:" + msg + ":" + writeString;
                WriteLog.write(foldername, "Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_err", loginname
                        + ":" + cookieString);
                WriteLog.write(foldername, "Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_err", loginname
                        + ":" + writeString + ":现有人数:" + ":ticketnumsql:" + ticketnumsql + ":" + resultString
                        + "========" + this.url);
            }
        }

    }

    /**
     * 
     * @time 2015年9月9日 上午11:25:52
     * @author chendong
     * @param foldername 
     * @param cookieString 
     */
    private void checkMobile(String cookieString, String foldername) {
        Long l1 = System.currentTimeMillis();
        WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMobileNo", loginname + ":开始:"
                + TimeUtil.gettodaydate(5));
        JSONObject json = JobTrainUtil.getCountString(this.url, cookieString);//获取常旅客数量
        msg = json.getString("msg");
        for (int i = 0; i < 3; i++) {
            System.out.println(loginname + ":cookieString->:" + cookieString);
            if ("用户未登录".equals(msg)) {
                cookieString = JobTrainUtil.getCookie(this.url, loginname, password);
                json = JobTrainUtil.getCountString(this.url, cookieString);//获取常旅客数量
                msg = json.getString("msg");
            }
            else if (cookieString.contains("失败")) {
                System.out.println(loginname + ":登录失败" + i + "次");
            }
            else {
                break;
            }
        }
        int count = json.getInteger("passengercount");//{"queryMyOrderNoComplete":"","passengercount":1,"msg":"已通过"}
        WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMobileNo", loginname + ":count:"
                + count + ":msg:" + msg);
        if (count >= 0 && "已通过".equals(msg)) {
            int chongshiCount = 3;
            try {
                chongshiCount = Integer.parseInt(PropertyUtil.getValue(
                        "Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_chongshiCount", "train.properties"));
            }
            catch (Exception e) {
            }
            for (int i = 0; i < chongshiCount; i++) {
                String heyanResultString = chuliheyanshoujihao(cookieString, 0, "-1", l1);
                System.out.println(loginname + ":处理核验手机号->" + i + "次最终结果:" + heyanResultString);
                //没有核验成功记录下原因
                if ("true".equals(heyanResultString)) {
                    break;
                }
                else if (heyanResultString.contains("系统忙，请稍后再试") ||heyanResultString.contains("您获取验证码短信次数过多") || heyanResultString.contains("网络繁忙")) {
                    JobTrainUtil.reConnectAdsl();//断开adsl重新连接
                }
                else {
                    WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_failReason", loginname
                            + ":heyanResultString:" + heyanResultString);
                }
            }
        }
        else {//C_ISENABLE=20等待核验手机号的账号
            String ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=20,C_STATE=0 where ID=" + this.id;
            int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
            WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMobileNo_err", loginname
                    + ":count:" + count + ":msg:" + msg);
        }
    }

    /**
     * 处理核验手机号
     * @time 2015年7月29日 下午8:34:05
     * @author chendong
     * @param l1 
     * @param resultString 
     * @param string 
     * @param i 
     */
    private String chuliheyanshoujihao(String cookieString, int heyancishu, String proxyHost_proxyPort, Long l1) {
        String returnresultString = "-1";
        String url_t = this.url;
        String mobileCodeType = "2";//1:优码,2:淘码,3:y码
        mobileCodeType = PropertyUtil.getValue("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_mobileCodeType",
                "train.properties");
        String proxyPar = "";
        if ("1".equals(useProxy)) {
            proxyPar = "&" + getProxyPar(heyancishu, proxyHost_proxyPort);
        }
        String par = "datatypeflag=201&cookie=" + cookieString + "&logpassword=" + password + "&mobileCodeType="
                + mobileCodeType + proxyPar;
        WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMobileNo", loginname + ":url_t:"
                + url_t + "?" + par);
        int timeout = 240000;
        String resultString = "-1";
        try {
            resultString = SendPostandGet.submitPostTimeOut(url_t, par, "UTF-8", timeout).toString();
        }
        catch (Exception e) {
        }
        System.out.println(loginname + "::>" + resultString);
        WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMobileNo", loginname
                + ":resultString:" + resultString + ":>耗时>" + (System.currentTimeMillis() - l1));
        if (!"-1".equals(resultString)) {
            String[] results = resultString.split("[|]");
            String resultBoolean = results[0];
            if ("true".equals(resultBoolean)) {
                updateDb(results);
                returnresultString = resultBoolean;
            }
            else {
                returnresultString = resultString;
            }
        }
        return returnresultString;
    }

    /**
     * 
     * @time 2015年9月11日 下午7:54:31
     * @author chendong
     * @param results 
     */
    private void updateDb(String[] results) {
        String ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=21,C_CARDTYPE='1'";
        if (results.length > 1) {
            String mobile = results[1];
            ticketnumsql += ",C_MOBILE='" + mobile + "' ";
        }
        ticketnumsql += " where ID=" + this.id;
        int count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
        System.out.println(count1 + ":" + ticketnumsql);
        WriteLog.write("Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread_heyanMObileno", "loginname:" + loginname
                + ":" + isystemservice + ":count1:" + count1 + ":ticketnumsql:" + ticketnumsql);
    }

    /**
     * 获取代理参数
     * @return
     * @time 2015年8月3日 下午2:24:48
     * @author chendong
     * @param heyancishu 
     * @param proxyHost_proxyPort 
     */
    public String getProxyPar(int heyancishu, String proxyHost_proxyPort) {
        String proxyPar = "-1";
        //        List list = localhostSystemservice
        //                .findMapResultBySql(
        //                        "SELECT ID, C_NAME,C_VALUE, C_DESCRIPTION, C_CONFIGTYPE FROM T_SYSCONFIG where c_name = 'checkMobileNoProxyUseProxy'",
        //                        null);
        //        if (list.size() > 0) {
        //            Map map = (Map) list.get(0);
        //            String checkMobileNoProxy = objectisnull(map.get("C_VALUE"));
        //            useProxy = checkMobileNoProxy;//是否使用代理
        //        }
        if (heyancishu == 0 && "1".equals(useProxy)) {// && (resultString.indexOf("由于您获取验证码短信次数过多") >= 0)
            String dataString = SendPostandGet.submitGet(
                    "http://ip.zdaye.com/?api=20150803200040011100620034949384&port=80&dengji=%B8%DF%C4%E4&"
                            + "gb=2&https=%D6%A7%B3%D6&yys=%D2%C6%B6%AF&ct=3&daochu=1", "GBK");
            WriteLog.write("zdaye", dataString);
            if (dataString.indexOf("请求过快") < 0) {
                String sql2 = "update T_SYSCONFIG set C_VALUE+='" + dataString
                        + "|' where c_name = 'checkMobileNoProxy'";
                System.out.println("sql2:" + sql2);
            }
        }
        if ("1".equals(useProxy)) {
        }
        return proxyPar;
    }

    /**
     * 
     * @param proxyHost_proxyPort
     * @param dataString
     * @time 2015年8月3日 下午7:52:13
     * @author chendong
     */
    private void daproxyHost_proxyPort(String proxyHost_proxyPort) {
        if (!"-1".equals(proxyHost_proxyPort) && proxyHost_proxyPort.indexOf("请求过快") < 0) {
            String sql = "update T_SYSCONFIG set C_VALUE=replace(cast(C_VALUE as varchar(8000)) ,'"
                    + proxyHost_proxyPort + "|','') where C_NAME='checkMobileNoProxy'";
            System.out.println("daproxyHost_proxyPort:" + sql);
        }
    }

    /**
    * 如果为空返回 "0"
    * 
    * @param object
    * @return
    * @time 2015年4月15日 下午1:57:54
    * @author chendong
    */
    public String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    public static void main(String[] s) {
        String proxyPar = "useProxy=1&proxyHost=223.68.162.138&proxyPort=80";
        System.out.println(proxyPar.substring(proxyPar.indexOf("Host=") + 5, proxyPar.indexOf("&proxyPort=")));
        System.out.println(proxyPar.substring(proxyPar.indexOf("Port=") + 5, proxyPar.length()));

    }
}
