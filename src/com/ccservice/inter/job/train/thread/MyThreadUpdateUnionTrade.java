package com.ccservice.inter.job.train.thread;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobGenerateOrder;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainActiveMQ;

/**
 * 
 * @author wzc
 * 银联更新交易号
 *
 */
public class MyThreadUpdateUnionTrade extends Thread {
    TrainSupplyMethod trainSupplyMethod;

    String tongcheng_agentid;

    String qunar_agentid;

    String dangqianagentid;

    String tongchengcallbackurl;

    int paycounttime;

    String payremandtel;

    long orderid;

    public MyThreadUpdateUnionTrade(TrainSupplyMethod trainSupplyMethod, String tongcheng_agentid,
            String qunar_agentid, String dangqianagentid, String tongchengcallbackurl, int paycounttime,
            String payremandtel, long orderid) {
        this.trainSupplyMethod = trainSupplyMethod;
        this.tongcheng_agentid = tongcheng_agentid;
        this.qunar_agentid = qunar_agentid;
        this.dangqianagentid = dangqianagentid;
        this.tongchengcallbackurl = tongchengcallbackurl;
        this.paycounttime = paycounttime;
        this.payremandtel = payremandtel;
        this.orderid = orderid;
    }

    @Override
    public void run() {
        int t = new Random().nextInt(10000);
        try {
            String id = orderid + "";
            long Oid = Long.valueOf(id);
            Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Oid);
            int paycount = 0;
            try {
                paycount = (int) trainorder.getTcprocedure();
            }
            catch (Exception e1) {
            }
            String supplyaccount = trainorder.getSupplyaccount();
            String ordernum = trainorder.getOrdernumber();
            String url = "";
            String kaindex = "";
            kaindex = supplyaccount.charAt(supplyaccount.length() - 1) + "";
            String unaccount = "unionpayurl" + kaindex + "";
            url = getSystemConfigbyName(unaccount);
            String par = "{'ordernum':'" + ordernum + "','cmd':'seachliushuihao'}";
            WriteLog.write("12306银联更新交易号", t + ":" + par);
            String infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            //{"OrderID":"T141222074049241796","liushuihao":"2514122249201463","exits":"true","state":"RunOK","msg":"银行成功支付148.00,保存了返回参数"}
            //{"OrderID":"T210sdf64665151651854","liushuihao":"","exits":"true","state":"RunError","msg":"csPay页面:订单超时，请返回商户重新发起订单！"}
            //支付状态  NoRun待执行      Runing执行中     RunOK执行完成     RunError执行错误    Abandon放弃执行T141213145609231286
            WriteLog.write("12306银联更新交易号", t + ":" + infodata + ":" + supplyaccount);
            if (infodata != null && infodata.contains("liushuihao")) {
                JSONObject obj = JSONObject.fromObject(infodata);
                String liushuihao = obj.getString("liushuihao");
                String state = "";
                if (obj.containsKey("state")) {
                    state = obj.getString("state");
                }
                String msg = "";
                if (obj.containsKey("msg")) {
                    msg = obj.getString("msg");
                }
                //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "RunOK".equals(state)) {
                    String sqlpaytime = " update T_TRAINREFINFO set C_PAYSUCCESSTIME='" + TimeUtil.gettodaydate(4)
                            + "' where C_ORDERID=" + trainorder.getId();
                    List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpaytime, null);
                    WriteLog.write("12306银联更新交易号", t + ":" + sqlpaytime + ":" + liushuihao);
                    trainorder.setSupplytradeno(liushuihao);
                    trainorder.setState12306(Trainorder.ORDEREDPAYED);//更新12306订单为  已支付订单
                    trainorder.setPaysupplystatus(0);
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    trainRC(Long.valueOf(id), msg);
                    new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                            System.currentTimeMillis(), 0).sendqueryMQ();
                }
                else if ("RunError".equals(state)) {//运行错误
                    if (msg.contains("账户余额不足")) {
                        String content = kaindex + "卡账户余额没钱了该支付账户已禁用,请尽快充值。";
                        WriteLog.write("12306tz资金账户余额", t + ":" + content);
                        String tetctelphonenum = payremandtel;
                        trainSupplyMethod.sendSmspublic(content, tetctelphonenum, 46);
                        updatepaycount(kaindex);
                    }
                    else {
                        trainRC(Long.valueOf(id), msg);//保存运行错误信息
                        trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                        trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    }
                }
                else {
                    try {
                        String sqlpay = "select C_ORDERID,C_SUPPLYPAYTIME from T_TRAINREFINFO where C_ORDERID="
                                + trainorder.getId();
                        List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpay, null);
                        if (list != null && list.size() > 0) {
                            Map m = (Map) list.get(0);
                            String supplypaytime = m.get("C_SUPPLYPAYTIME").toString();
                            if (getTimeRelayFlag(supplypaytime)) {
                                paycount++;
                                trainRC(Long.valueOf(id), "1分钟未支付,支付需审核,支付次数：" + paycount);//保存运行错误信息
                                trainorder.setTcprocedure(paycount);
                                if (paycount >= paycounttime) {//如果支付次数大于设置值
                                    trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                                    trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                                }
                                else {
                                    trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
                                }
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            }
                            else {//支付中状态
                                trainorder.setState12306(Trainorder.ORDEREDPAYING);
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                            }
                        }
                        else {
                            trainRC(Long.valueOf(id), "未找到支付记录,支付失败");//保存运行错误信息
                            trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                            trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                            Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    WriteLog.write("12306银联更新交易号", t + "：更新状态失败：" + ordernum);
                }
            }
            else {
                trainRC(Long.valueOf(id), "未查到支付记录,支付需审核");//保存运行错误信息
                trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getSystemConfigbyName(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig(" where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "";
    }

    /**
     * 
     * 
     * @param trainorder
     * @param msg 保存日志信息
     * @time 2014年12月26日 下午1:12:27
     * @author wzc
     */
    public void trainRC(long orerid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(orerid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 易定行出票 
     * @param trainorder
     * @time 2014年12月26日 上午9:27:51
     * @author fiend
     */
    public void issueYEE(Trainorder trainorder) {
        new JobGenerateOrder().yeeIssue(trainorder.getId(), ordercustomeruser(trainorder));
    }

    /**
     * 通过订单查找下单的12306账号 
     * @param trainorder
     * @return
     * @time 2014年12月25日 下午9:33:36
     * @author fiend
     */
    public long ordercustomeruser(Trainorder trainorder) {
        String sql = "SELECT ID FROM T_CUSTOMERUSER WHERE C_LOGINNAME='" + trainorder.getSupplyaccount().split("/")[0]
                + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map map = (Map) list.get(0);
        return Long.valueOf(map.get("ID").toString());
    }

    /**
     * 回调支付成功 
     * @param i
     * @param orderid
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackTongChengPayed(long orderid, String tongchengcallbackurl) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        String url = tongchengcallbackurl;
        JSONObject jso = new JSONObject();
        String result = "false";
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getTradeno());
        jso.put("method", "train_pay_callback");
        try {
            WriteLog.write("12306银联更新交易号", ":前:" + url + ":" + jso.toString());
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            WriteLog.write("12306银联更新交易号", ":前:" + result + ":" + result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 判断时间为两分钟
     * @return
     */
    public boolean getTimeRelayFlag(String t) {
        boolean flag = false;
        try {
            Date d1 = new Date();
            Date d2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(t);
            long diff = d1.getTime() - d2.getTime();
            long misecord = diff / 1000;
            if (misecord > 60) {
                flag = true;
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigString_(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

    /**
     * 王战朝
     * @param index
     */
    public void updatepaycount(String index) {
        String payaccount = getSysconfigString_("unionpayurlnum");
        String resultstr = payaccount + ",";
        if (payaccount.split(",").length <= 1) {
            String content = "支付账户不足了，尽快处理";
            trainSupplyMethod.sendSmspublic(content, "15811073432", 46);
        }
        else {
            if (resultstr.indexOf(index + ",") >= 0) {
                resultstr = resultstr.replace(index + ",", "");
                resultstr = resultstr.substring(0, resultstr.lastIndexOf(","));
                trainSupplyMethod.changeSystemCofigbyname("unionpayurlnum", resultstr);
            }
        }
    }
}
