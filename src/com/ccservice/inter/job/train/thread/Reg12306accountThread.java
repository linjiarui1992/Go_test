package com.ccservice.inter.job.train.thread;

import java.sql.Timestamp;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.Jmail;
import com.ccservice.inter.server.Server;

public class Reg12306accountThread extends Thread {
    Customeruser customeruser;

    String email_ip;

    public Reg12306accountThread(Customeruser customeruser, String email_ip) {
        super();
        this.customeruser = customeruser;
        this.email_ip = email_ip;
    }

    @Override
    public void run() {
        super.run();
        String loginame = customeruser.getLoginname();
        String pwd = "asd123456";
        String name = customeruser.getMembername();//客服:宋莉
        name = name.split(":").length > 1 ? name.split(":")[1] : name;
        String email = customeruser.getMemberemail();
        String mailname = email.split("@")[0];
        String jihuourl = jihuoyouxiang(email_ip, email, loginame);
        int success = sendjihuourl(jihuourl);
        if (success > 0) {
            customeruser.setIsenable(1);//激活成功后把这个customeruser改成可用
            customeruser.setMemberfax(jihuourl);//激活成功后把这个激活链接放到Memberfax里customeruser改成可用
            customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
            Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
            String delusersult = Jmail.deluser(mailname, email_ip).trim();//成功了就把那个邮箱删除了
            WriteLog.write("job_自动注册12306_mail", "激活成功次数:" + success + ":deletemail:" + this.email_ip + ":" + email
                    + ":loginame:" + loginame + ":" + delusersult);
        }
        else {
            customeruser.setIsenable(2);//激活失败
            customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
            Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
            WriteLog.write("job_自动注册12306_mail", "激活失败:" + success + ":deletemail:" + this.email_ip + ":" + email
                    + ":loginame:" + loginame);

        }
        //            String email_host = email.substring(email.indexOf("@"));
        System.out.println();
    }

    /**
     * 获取激活邮箱里的12306的激活链接 sendurl
     * @param email 邮箱地址
     * @param loginame 12306用户名
     * @return
     * @time 2014年12月14日 下午5:59:40
     * @author chendong
     */
    private String jihuoyouxiang(String mail_ip, String email, String loginame) {
        String mailname = email.split("@")[0];
        String str = Jmail.getMail(mailname, "123456", mail_ip, 20000L);
        //            String str = JMail.getMail(email.replace("@51dzp.com", ""), "123456");
        WriteLog.write("job_自动注册12306_mail", loginame + "(:)" + email + ";获取邮箱激活地址:" + str);
        return str;
    }

    /**
     * 发送激活的方法
     * 返回激活次数
     * @param str
     * @return
     * @time 2014年12月28日 下午1:26:30
     * @author chendong
     */
    private int sendjihuourl(String str) {
        int sendsuccess = 0;
        String resulthtml = "-1";
        if (str.contains("activeAccount")) {
            try {
                resulthtml = HttpsUtil.Get(str);
                Thread.sleep(10000);
                if (resulthtml.indexOf("账户已成功激活") > 0) {
                    sendsuccess++;
                }
                resulthtml = HttpsUtil.Get(str);
                if (resulthtml.indexOf("账户已成功激活") > 0) {
                    sendsuccess++;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sendsuccess;
    }
}
