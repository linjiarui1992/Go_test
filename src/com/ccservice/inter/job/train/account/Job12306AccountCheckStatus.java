/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;

/**
 * 
 * @time 2015年9月30日 下午1:50:14
 * @author chendong
 */
public class Job12306AccountCheckStatus extends TrainSupplyMethod implements Job {
    public static void main(String[] args) {
        Job12306AccountCheckStatus job12306accountcheckstatus = new Job12306AccountCheckStatus();
        boolean isexist = job12306accountcheckstatus.checkLoginName("zlas9rnlr1");
        System.out.println(isexist);
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String Job12306AccountCheckStatus_Url = PropertyUtil.getValue("CheckStatusIsstartUrl",
                "train.checkStatus.properties");
        String Job12306AccountCheckStatus = SendPostandGet.submitGet(Job12306AccountCheckStatus_Url);
        if ("1".equals(Job12306AccountCheckStatus)) {//是否开启
            List customerusers = getCustomeruserCheckMobile();
            if (customerusers.size() == 0) {
                System.out.println("没有待核验手机号的账号了,等会");
            }
            else {
                for (int i = 0; i < customerusers.size(); i++) {
                    Customeruser customeruser = new Customeruser();
                    Map map = (Map) customerusers.get(customerusers.size() - 1);
                    String loginname = map.get("C_LOGINNAME").toString();
                    String password = map.get("C_LOGPASSWORD").toString();
                    Long id = Long.parseLong(map.get("ID").toString());
                    customeruser.setId(id);
                    customeruser.setLoginname(loginname);
                    customeruser.setLogpassword(password);
                    String repUrl = "";
                    boolean isExist = checkLoginName(loginname);
                    if (isExist) {//12306存在的话去核验
                        String cookieString = JobTrainUtil.getCookie(repUrl, loginname, password);
                        //{"orderType":"","queryLink":"leftTicket/queryT","seatTypeOf12306":"P#tz_num#特等座@M#zy_num#一等座@O#ze_num#二等座@F#rw_num#动卧@E#qt_num#特等软座@A#qt_num#高级动卧@9#swz_num#商务座@8#ze_num#二等软座@7#zy_num#一等软座@6#gr_num#高级软卧@4#rw_num#软卧@3#yw_num#硬卧@2#rz_num#软座@1#yz_num#硬座@0#wz_num#无座","from_station_name":"杭州东","to_station_name":"合肥南","cookie":"JSESSIONID=0A02F00AC4F39F7A96DA34A84333E48527CE022CF7; BIGipServerotn=183501322.50210.0000; current_captcha_type=Z","passengers":"{"oldPassengerStr":"赵文刚,1,341124197212121013,1_","prices":"178.0","zwcodes":"1","passengerTicketStr":"1,0,1,赵文刚,1,341124197212121013,,N"}","to_station":"ENH","train_date":"2015-10-01","from_station":"HGH","loginPwd":"asd123456","seatChange":true,"orderId":7351191,"train_code":"G9476/G9477","loginName":"zhangzukjm"}
                    }
                    else {//12306不存在 修改状态
                          //#TODO
                          //12注册完邮箱账号拿完乘客等待去12306注册信息
                          //                        把这个账号的isenable改为12
                    }
                }
            }
        }
        else {
            System.out.println("Job12306AccountCheckStatus_html没开");
        }
    }

    /**
     * 获取到n个准备核验的账号
     * @return
     * @time 2015年9月30日 下午3:10:00
     * @author chendong
     */
    private List getCustomeruserCheckMobile() {
        String Job12306AccountCheckStatus_serviceurl = PropertyUtil.getValue("serviceurl",
                "train.checkStatus.properties");
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    Job12306AccountCheckStatus_serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //这个存储过程还没写,isenable =-11的customeruser
        //        SELECT TOP 10 *
        //        FROM [B2B_DB_TC].[dbo].[T_CUSTOMERUSER] with(nolock) where c_type=4 
        //        and c_isenable=-11 order by id desc
        String procedureSqlString = "sp_CustomerUser_Job12306AccountCheckStatus";
        List customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
        return customerusers;
    }

    /**
     * 账号是否存在
     * 
     * @param loginname
     * @return true 存在   false不存在
     * @time 2015年9月30日 下午2:50:23
     * @author chendong 
     */
    private boolean checkLoginName(String loginname) {
        String resultString = HttpsUtil.Get("https://kyfw.12306.cn/otn/regist/checkUserName?user_name=" + loginname);
        //        {"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":true,"messages":[],"validateMessages":{}}
        boolean isexist = false;
        JSONObject jsonObject = JSONObject.parseObject(resultString);
        try {
            isexist = jsonObject.getBoolean("data");
        }
        catch (Exception e) {
        }

        return isexist;
    }

}
