/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.DesUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.EncryptionUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;
import com.ccservice.inter.server.ServerTrainJobUtil;
import com.ccservice.inter.server.ServerUtil;

/**
 * http://localhost:9010/ticket_inter/job/Job12306AccountCheckOnlineAccount.jsp?type=0
 * 检测线上的未核验手机号的账号是否都能正常下单
 * @time 2015年10月19日 下午12:03:14
 * @time 2017年4月5日17:28:48
 * @author chendong
 */
public class Job12306AccountCheckOnlineAccount implements Job {
    //第一步:sp_Customeruser_Job12306AccountCheckOnlineAccount_getOneAccount 
    //通过这个存储过程获取到待验证的是否能下单的账号

    public static int JobCount = 0;

    public static int maxJob = 15;

    //    private String repUrl = "http://121.40.162.18:9016/Reptile/traininit";

    /* (non-Javadoc)
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println(TimeUtil.gettodaydate(4));
        execute_main();
    }

    /**
     * 
     * @time 2015年10月19日 下午12:58:59
     * @author chendong
     */
    private void execute_main() {
        Job12306AccountCheckOnlineAccount.JobCount++;
        if (Job12306AccountCheckOnlineAccount.JobCount > Job12306AccountCheckOnlineAccount.maxJob) {
            System.out.println(
                    "Job12306AccountCheckOnlineAccount_同时进行的job太多(" + Job12306AccountCheckOnlineAccount.JobCount + ":"
                            + Job12306AccountCheckOnlineAccount.maxJob + "),等一会:" + TimeUtil.gettodaydate(4));
        }
        else {
            String serviceurl = PropertyUtil.getValue("serviceurl", "train.account.properties");
            ISystemService isystemservice = ServerUtil.getISystemService(serviceurl);

            List customerusers_map = execute_1(isystemservice);
            if (customerusers_map.size() == 0) {
                System.out.println(TimeUtil.gettodaydate(4) + ":没有账号了->检测线上的未核验手机号的账号是否都能正常下单");
                System.out.println(TimeUtil.gettodaydate(4) + "Job12306AccountCheckOnlineAccount");
            }
            else {
                String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
                //                String repUrl = Server.getInstance().getTrain12306Service().commonRepServer(true).getUrl();
                execute_2(customerusers_map, isystemservice, repUrl);
            }
        }
        Job12306AccountCheckOnlineAccount.JobCount--;
    }

    /**
     * 
     * @time 2015年10月19日 下午12:59:29
     * @author chendong
     * @param customerusers_map 
     * @param isystemservice2 
     */
    private void execute_2(List customerusers_map, ISystemService isystemservice, String repUrl) {
        for (int i = 0; i < customerusers_map.size(); i++) {
            Map map = (Map) customerusers_map.get(i);
            String loginname = map.get("C_LOGINNAME").toString();
            //解密
            try {
                loginname = DesUtil.decrypt(loginname, "A1B2C3D4E5F60708");
            }
            catch (Exception e) {
            }
            String password = map.get("C_LOGPASSWORD").toString();
          //解密
            try {
                password = DesUtil.decrypt(password, "A1B2C3D4E5F60708");
            }
            catch (Exception e) {
            }
            String mobile = map.get("C_MOBILE") == null ? "" : map.get("C_MOBILE").toString();
            String customeruserId = map.get("ID").toString();
            String reg_mobile = PropertyUtil.getValue("reg_mobile", "train.properties");
            if (mobile != null && mobile.trim().length() == 11 && reg_mobile.contains(mobile)) {
                String sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=25 where id=" + customeruserId;
                int count = isystemservice.excuteAdvertisementBySql(sql);
                WriteLog.write("Job12306AccountCheckOnlineAccount_checkPhone_sql",
                        loginname + ":count:" + count + ":" + sql);
                System.out.println(mobile + ":" + loginname + ":count:" + count + ":" + sql);
                continue;
            }
            System.out.println(loginname + ":"+password+":开始登陆" + repUrl);
            String cookieString = getCookie(loginname, password, repUrl);
            //            System.out.println(loginname + ":登陆结果:" + cookieString + ":" + repUrl);
            int count = 0;
            String sql = "";
            if (cookieString.contains("您的用户信息被他人冒用，请重新在网上注册新的账户")) {
                //43 [登录提示] 您的用户信息被他人冒用，请重新在网上注册新的账户，为了确保您的购票安全，您还需尽快到就近的办理客运售票业务的铁路车站完成身份核验，谢谢您对12306网站的支持。
                sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=43 where id=" + customeruserId;
            }
            else if (cookieString.contains("未登录") || cookieString.contains("失败") || "-1".equals(cookieString)) {
                System.out.println(loginname + "登录失败");
            }
            else {
                //                {"shoujihaoma":"13169820344","shoujihaoma_heyanstatus":"已通过","heyanzhuangtai":"已通过"}
                JSONObject jsonObject = JobTrainUtil.initQueryUserInfo(cookieString);
                System.out.println(loginname + ":jsonObject:" + jsonObject);
                String shoujihaoma = jsonObject.getString("shoujihaoma");
                if (jsonObject.toJSONString().contains("未登录") || jsonObject.toJSONString().contains("失败")
                        || "-1".equals(jsonObject.toJSONString())) {
                    System.out.println(loginname + "登录失败");
                }
                //身份已通过
                else if ("已通过".equals(jsonObject.getString("heyanzhuangtai"))) {
                    if ("已通过".equals(jsonObject.getString("shoujihaoma_heyanstatus"))) {
                        //1 【手机已通过】身份已通过  
                        sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=1,C_CARDTYPE=100,C_MOBILE='" + shoujihaoma
                                + "'  where id=" + customeruserId;
                    }
                    else {
                        //38 【手机未通过】身份已通过  
                        sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=38,[C_MODIFYTIME]=getdate() where id=" + customeruserId;
                    }
                }
                //身份未通过
                else {
                    if ("已通过".equals(jsonObject.getString("shoujihaoma_heyanstatus"))) {
                        //37 【手机已通过】身份未通过  
                        System.out.println(loginname + ":手机号核验通过,身份未通过,直接修改状态");
                        sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=37 where id=" + customeruserId;
                    }
                    else {
                        //40 【手机未通过】身份未通过 
                        System.out.println(loginname + ":【手机未通过】身份未通过 ");
                        sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=40 where id=" + customeruserId;
                        //                        checkPhone(cookieString, loginname, password, customeruserId, isystemservice, repUrl);
                    }
                }
            }
            if (sql.length() > 0) {
                count = isystemservice.excuteAdvertisementBySql(sql);
                System.out.println(loginname + ":count:" + count + ":" + sql);
            }
            else {
                System.out.println(loginname + ":sql_err");
            }
        }
    }

    /**
     * 
     * @param loginname
     * @param password
     * @return
     * @time 2015年10月19日 下午3:19:24
     * @author chendong
     */
    private String getCookie(String loginname, String password, String repUrl) {
        String cookie = "-1";
        for (int i = 0; i < 5; i++) {
            cookie = JobTrainUtil.getCookie(repUrl, loginname, password);
            System.out.println(loginname + "(" + password + ")" + ":登陆(" + repUrl + "):" + i + "次:" + cookie);
            if (cookie.contains("您的用户信息被他人冒用")) {
                break;
            }
            else if (cookie.contains("失败") || cookie.contains("-1")) {
                continue;
            }
            else {
                break;
            }
        }
        return cookie;
    }

    public void checkPhone(String cookieString, String loginName, String loginPwd, String customeruserId,
            ISystemService isystemservice, String repUrl) {
        String jsonStr = getJsonStr(cookieString, loginName, loginPwd);
        try {
            jsonStr = URLEncoder.encode(jsonStr, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //请求参数
        String param = "cookie=" + cookieString + "&datatypeflag=301&jsonStr=" + jsonStr;

        //请求REP
        String retdata = "-1";
        for (int i = 0; i < 3; i++) {
            retdata = SendPostandGet.submitPost(repUrl, param, "UTF-8").toString();
            if ("-1".equals(retdata)) {
                System.out.println(loginName + ":" + i + "次核验结果:" + retdata);
                continue;
            }
            else {
                break;
            }
        }
        System.out.println(loginName + ":核验结果:" + retdata);
        if ("-1".equals(retdata)) {
            System.out.println(loginName + "->over");
            WriteLog.write("Job12306AccountCheckOnlineAccount_checkPhone", loginName + ":" + param + ":" + repUrl);
            WriteLog.write("Job12306AccountCheckOnlineAccount_checkPhone", loginName + ":retdata:" + retdata);
            return;
        }
        try {
            JSONObject repobj = JSONObject.parseObject(retdata);
            if (repobj.getBooleanValue("success")) {
                String sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=1,C_CARDTYPE=-100 where id=" + customeruserId;
                int count = isystemservice.excuteAdvertisementBySql(sql);
                WriteLog.write("Job12306AccountCheckOnlineAccount_checkPhone_sql",
                        loginName + ":count:" + count + ":" + sql);
                return;
            }
            else {
                String msg = repobj.getString("msg");
                /* 
                * 31身份信息未通过核验
                * 32注册的用户与其他用户重复
                * 33手机核验
                * 41备用1
                * 42备用2
                */
                if (msg.indexOf("您在12306网站注册时填写信息有误") > -1) {//31
                    ChangeStatusPhone2Ennused(isystemservice, customeruserId, 31, loginName);
                }
                else if (msg.indexOf("您注册的信息与其他用户重复") > -1) {//32
                    ChangeStatusPhone2Ennused(isystemservice, customeruserId, 32, loginName);
                }
                else if (msg.indexOf("手机核验") > -1) {//33
                    ChangeStatusPhone2Ennused(isystemservice, customeruserId, 33, loginName);
                }
                else if (msg.indexOf("您注册时的手机号码被多个用户使用") > -1) {//34
                    ChangeStatusPhone2Ennused(isystemservice, customeruserId, 34, loginName);
                }
                else {
                    System.out.println("其他原因先不处理-->" + msg);
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //解析数据
    }

    /**
     * 
     * @param customeruserId
     * @param i
     * @time 2015年10月19日 下午4:54:54
     * @author chendong
     */
    private void ChangeStatusPhone2Ennused(ISystemService isystemservice, String customeruserId, int ISENABLE,
            String loginName) {
        String sql = "update T_CUSTOMERUSER set c_localcity=CONVERT(varchar(100), GETDATE(), 21),C_ISENABLE=" + ISENABLE + " where id=" + customeruserId;
        int count = isystemservice.excuteAdvertisementBySql(sql);
        System.out.println(loginName + ":count:" + count + ":" + sql);
        WriteLog.write("Job12306AccountCheckOnlineAccount_checkPhone_sql", loginName + ":count:" + count + ":" + sql);
    }

    /**
     * 
     * @time 2015年10月19日 下午1:04:44
     * @author chendong
     * @param customeruser 
     */
    private String getJsonStr(String cookieString, String loginName, String loginPwd) {
        JSONObject reqobj = new JSONObject();
        reqobj.put("loginName", loginName);
        reqobj.put("loginPwd", loginPwd);
        reqobj.put("orderId", 1008611);//用于日志记录
        reqobj.put("cookie", cookieString);//cookie
        reqobj.put("train_date", TimeUtil.gettodaydatebyfrontandback(1, 30));//乘车日期,往后推30天
        String from_station = PropertyUtil.getValue("from_station", "train.account.properties");
        reqobj.put("from_station", from_station);//出发站编码
        String to_station = PropertyUtil.getValue("to_station", "train.account.properties");
        reqobj.put("to_station", to_station);//到达站编码
        String from_station_name = PropertyUtil.getValue("from_station_name", "train.account.properties");
        reqobj.put("from_station_name", from_station_name);//出发站名
        String to_station_name = PropertyUtil.getValue("to_station_name", "train.account.properties");
        reqobj.put("to_station_name", to_station_name);//到达站名
        String train_code = PropertyUtil.getValue("train_code", "train.account.properties");
        reqobj.put("train_code", train_code);//车次
        reqobj.put("orderType", 0);//1：去哪儿订单
        reqobj.put("queryLink", PropertyUtil.getValue("queryTicketLink"));//查询车票链接，防止12306变数据，从cn_interface传入
        reqobj.put("seatTypeOf12306",
                "P#tz_num#特等座@M#zy_num#一等座@O#ze_num#二等座@F#rw_num#动卧@E#qt_num#特等软座@A#qt_num#高级动卧@9#swz_num#商务座@8#ze_num#二等软座@7#zy_num#一等软座@6#gr_num#高级软卧@4#rw_num#软卧@3#yw_num#硬卧@2#rz_num#软座@1#yz_num#硬座@0#wz_num#无座");//12306座席
        reqobj.put("passengers",
                "{\"oldPassengerStr\":\"杨鹏鸿,1,452226200007215714,1_\",\"prices\":\"189.5\",\"zwcodes\":\"1\",\"passengerTicketStr\":\"1,0,1,杨鹏鸿,1,452226200007215714,,N\"}");
        String jsonStr = reqobj.toJSONString();
        return jsonStr;
    }

    /**
     * 获取一个需要核验的账号
     * @time 2015年10月19日 下午12:55:54
     * @author chendong
     */
    private List execute_1(ISystemService isystemservice) {
        System.out.println(System.currentTimeMillis());
        String procedureSqlString = "sp_Customeruser_Job12306AccountCheckOnlineAccount_getOneAccount";
        procedureSqlString = PropertyUtil.getValue("getOneAccountTocheck_procedure", "train.account.properties");
        List customerusers_map = isystemservice.findMapResultByProcedure(procedureSqlString);
        return customerusers_map;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        String name = Job12306AccountCheckOnlineAccount.class.getSimpleName() + System.currentTimeMillis();
        String group = Job12306AccountCheckOnlineAccount.class.getSimpleName() + "_Group";
        JobDetail jobDetail = new JobDetail(name, group, Job12306AccountCheckOnlineAccount.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger(name, group, expr);// 触发器名，触发器组名
        if (ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount == null
                || ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.isShutdown()) {
            Scheduler scheduler_Job12306AccountCheckOnlineAccount = StdSchedulerFactory.getDefaultScheduler();
            ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount = scheduler_Job12306AccountCheckOnlineAccount;
            ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.scheduleJob(jobDetail, trigger);
            ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.start();
        }
    }

    /**
     * 
     * @time 2015年10月19日 下午7:44:25
     * @author chendong
     */
    public static void pauseJob() {
        try {
            if (ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount != null) {
                ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.shutdown();
            }
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        try {
            Job12306AccountCheckOnlineAccount.startScheduler("0/1 0/1 7-23 * * ?");
            //            ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.pauseAll();
            //            ServerTrainJobUtil.scheduler_Job12306AccountCheckOnlineAccount.resumeAll();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
//        Job12306AccountCheckOnlineAccount job12306AccountCheckOnlineAccount = new Job12306AccountCheckOnlineAccount();
//        job12306AccountCheckOnlineAccount.execute_main();
    }
    
}
