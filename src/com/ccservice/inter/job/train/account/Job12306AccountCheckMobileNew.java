/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.account.thread.Thread12306AccountCheckMobileNewThread;
import com.ccservice.inter.server.Server;

/**
 * CustomeruserCheckMobile 
 * 获取到这个表里面的 获取一个待核验的账号去核验
 * 带cookie
 * @time 2015年9月30日 下午1:50:14
 * @author chendong
 */
public class Job12306AccountCheckMobileNew implements Job {
    public static void main(String[] args) throws Exception {
        //        Job12306AccountCheckMobileNew.startScheduler("0/1 0/1 7-23 * * ?");
        //        Job12306AccountCheckMobileNew job12306accountcheckmobilenew = new Job12306AccountCheckMobileNew();
        //        job12306accountcheckmobilenew.logintocheckMobile();
        System.out.println(Server.getInstance().getSystemService());
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        logintocheckMobile();
    }

    /**
     * 核验账号 
     * 核验手机号的定时任务
     * @time 2015年8月19日 下午1:26:34
     * @author chendong
     */
    private void logintocheckMobile() {
        //获取html是否开启
        String mobileCheckIsstart = getmobileCheckIsstart();
        if (!"1".equals(mobileCheckIsstart)) {
            System.out.println("Job12306AccountCheckMobileNew_html_没开=>" + TimeUtil.gettodaydate(4)
                    + ":mobileCheckIsstart:" + mobileCheckIsstart);
            return;
        }
        System.out.println("Job12306AccountCheckMobileNew_html_开=>" + TimeUtil.gettodaydate(4));
        reLogintocheckHeyan();
    }

    /**
     * 
     * @return
     * @time 2015年9月30日 下午3:23:22
     * @author chendong
     */
    private String getmobileCheckIsstart() {
        //是否开启的html判断
        String mobileCheckIsstartUrl = PropertyUtil.getValue("mobileCheckIsstartUrl", "train.checkMobile.properties");
        String mobileCheckIsstart = "0";
        try {
            mobileCheckIsstart = SendPostandGet.submitGet(mobileCheckIsstartUrl);
        }
        catch (Exception e) {
            mobileCheckIsstart = "0";
        }
        return mobileCheckIsstart;

    }

    /**
     * 获取到待核验的账号去核验
     * @time 2015年6月25日 上午11:19:01
     * @author chendong
     * @param type  0 老核验 1核验手机号
     */
    private void reLogintocheckHeyan() {
        try {
            String Maxdangqianheyancount = PropertyUtil.getValue("Maxdangqianheyancount",
                    "train.checkMobile.properties");
            int i_Maxdangqianheyancount = Integer.parseInt(Maxdangqianheyancount);
            if (Account12306Server.dangqiancheckMobileCount >= i_Maxdangqianheyancount) {
                System.out.println("当前(" + Server.dangqianheyancount + ")允许同时最大核验手机号数(" + Maxdangqianheyancount
                        + ")达到最大了,等会再跑。。。");
                return;
            }
        }
        catch (Exception e) {
            System.out.println("报错了");
            return;
        }
        ISystemService isystemservice = getISystemService();
        String LogintocheckHeyan_serviceurl = PropertyUtil.getValue("serviceurl", "train.checkMobile.properties");
        String procedureSqlString = " sp_CustomeruserCheckMobile_getOneCustomeruserCheckMobile ";
        List customerusers = isystemservice.findMapResultByProcedure(procedureSqlString);
        String repUrl = PropertyUtil.getValue("repUrl", "train.checkMobile.properties");//"http://localhost:9016/Reptile/traininit";
        String mobileCodeType = PropertyUtil.getValue("mobileCodeType", "train.checkMobile.properties");//"http://localhost:9016/Reptile/traininit";
        if (customerusers.size() == 0) {
            System.out.println(TimeUtil.gettodaydate(4) + ":没有待核验手机的账号了");
        }
        else {
            for (int i = 0; i < customerusers.size(); i++) {
                Map map = (Map) customerusers.get(i);
                String loginname = map.get("loginname").toString();
                String password = map.get("loginPass").toString();
                Long id = Long.parseLong(map.get("UserId").toString());
                String cookieString = map.get("cookieString").toString();
                new Thread12306AccountCheckMobileNewThread(id, loginname, password, repUrl,
                        LogintocheckHeyan_serviceurl, cookieString, mobileCodeType).start();
            }
        }
    }

    /**
     * 
     * @time 2015年9月30日 下午3:28:40
     * @author chendong
     * @return 
     */
    private ISystemService getISystemService() {
        String serviceurl = "";
        String LogintocheckHeyan_serviceurl = PropertyUtil.getValue("serviceurl", "train.checkMobile.properties");
        serviceurl = LogintocheckHeyan_serviceurl;
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return isystemservice;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306AccountCheckMobileNew", "Job12306AccountCheckMobileNewGroup",
                Job12306AccountCheckMobileNew.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306AccountCheckMobileNew", "Job12306AccountCheckMobileNewGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
