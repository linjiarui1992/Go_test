package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.caucho.hessian.client.HessianProxyFactory;
//import com.ccservice.b2b2c.atom.service12306.TimeUtil;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.Jmail;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.Util.TelnetBase;
import com.ccservice.inter.job.train.account.Account12306Server;
import com.ccservice.inter.job.train.reg.Job12306Registration_RegThread;
import com.ccservice.inter.job.train.reg.Job12306Registration_RepDaempnThread;
import com.ccservice.inter.job.train.thread.Reg12306accountThread;
import com.ccservice.inter.server.Server;
import com.ccservice.t12306.method.reg.CheckThisRepIsenable;

import net.sourceforge.pinyin4j.PinyinHelper;

/**
 * 1、检查 18ma.html 文件 tc000153953@mlzg99.com 自动注册12306账号 开始自动注册12306账号
 * 账号，密码，E-MAIL
 * 
 * @time 2014年12月14日 下午3:57:21
 * @author chendong
 * @author yinshubin
 */

public class Job12306Registration extends TrainSupplyMethod implements Job {
    public static void main(String[] args) {
        List<Customeruser> list = Server
                .getInstance()
                .getMemberService()
                .findAllCustomeruser(" where C_TYPE=4 ",
                        "ORDER BY ID DESC", 10, 0);
        System.out.println(list.size());
        try {
            //            Job12306Registration.startScheduler("0/1 0/1 * * * ?");
            //            mainReg();// main方法注册
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        // Job12306Registration job12306registration = new
        // Job12306Registration();
        // job12306registration.main_execute();
        // String useProxy =
        // PropertyUtil.getValue("Job12306Registration_useProxy",
        // "train.reg.properties");
        // job12306registration.toGoReg(0,useProxy);

    }

    private static void mainReg() {
        Job12306Registration job12306registration = new Job12306Registration();
        job12306registration.main_execute_new();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // execute_old();
        System.out.println("定时注册--------->" + TimeUtil.gettodaydate(5));
        main_execute_new();
    }

    /**
     * 老逻辑的注册
     * 
     * @time 2015年11月17日 下午1:39:44
     * @author chendong
     */
    private void execute_old() {
        // 1:while 方式 2:定时任务方式
        String damaRuleUrl = PropertyUtil.getValue("Job12306Registration_main_type", "train.reg.properties");
        if ("1".equals(damaRuleUrl)) {// 1:while 方式 这个方式先不用了
            main_execute();
        }
        else if ("2".equals(damaRuleUrl)) {
        }
        else {
            System.out.println("不注册啦啦啦");
        }
        // String isreg12306 = getSysconfigStringbydb("isreg12306");
        // if ("1".equals(isreg12306)) {
        // main_execute();
        // }
        // else {
        // System.out.println("暂时停止注册(DB)");
        // }

    }

    /**
     * 
     * @time 2015年9月7日 上午10:20:18
     * @author chendong
     */
    private void main_execute_new() {
        int ri = new Random().nextInt(1000000);
        initData();
        WriteLog.write("12306Reg", "Job12306Registration_Thread_debug", ri + ":start while " + TimeUtil.gettodaydate(5));
        String config = getConfig1();// 开@2.5秒一次打码 0@15110
        if ("1".equals(config)) {
            System.out.println("注册开了(dama18):" + TimeUtil.gettodaydate(5));
            // 当前正在注册的线程数 大于等于 最大同时注册的线程数
            if (Account12306Server.regdangqianCount >= Account12306Server.regTongshiCountMax) {
                System.out.println("注册线程超过最大数,等会再开-thread-" + Account12306Server.regdangqianCount);// regdangqianCount
            }
            else {
                toGoReg(ri);// 去注册
            }
        }
        else {
            System.out.println("注册没开(dama18):" + TimeUtil.gettodaydate(5));
        }
    }

    private String getConfig1() {
        // damal18连接
        String damaRuleUrl = PropertyUtil.getValue("Job12306Registration_damaRuleUrl", "train.reg.properties");
        String config = "0";
        try {
            config = SendPostandGet.submitGet(damaRuleUrl);
        }
        catch (Exception e) {
        }
        return config;
    }

    private void initData() {
        Integer itongshiCountMax = 10;
        try {
            String tongshiCountMax = PropertyUtil.getValue("tongshiCountMax", "train.reg.properties");
            itongshiCountMax = Integer.parseInt(tongshiCountMax);
        }
        catch (Exception e) {
        }
        // regTongshiCountMax 最大同时注册的线程数
        Account12306Server.regTongshiCountMax = itongshiCountMax;
    }

    /**
     * 
     * @time 2015年9月7日 上午8:57:30
     * @author chendong
     * @param ri
     * @param useProxy
     * @param array1
     *            睡眠多长时间
     */
    public void toGoReg(int ri) {
        // 是否使用adsl
        String job12306Registration_useadsl = PropertyUtil.getValue("Job12306Registration_useadsl",
                "train.reg.properties");
        String proxyHost = "-1";
        String proxyPort = "-1";
        // #注册的rep
        // String Job12306Registration_repIpString =
        // PropertyUtil.getValue("Job12306Registration_repIpString",
        // "train.reg.properties");
        String systemString = "=>:开始扔注册的线程:" + TimeUtil.gettodaydate(4);
        System.out.println(systemString + "-ThreadCount-" + Account12306Server.regdangqianCount);// 当前正在注册的线程数
        System.out.println("-->>>>" + "rep开啦:" + "Job12306Registration_RegThread");
        WriteLog.write("12306Reg", "Job12306Registration_Thread_debug", ri + ":SleepTime:over");
        if ("1".equals(job12306Registration_useadsl)) {// 如果使用adsl
            if (Server.adslIaonLine) {// 在线
                System.out.println("已经拨号,正常走....");
            }
            else {
                WriteLog.write("12306Reg", "Job12306Registration_Thread_debug",
                        ri + ":正在拨号,请稍等。。。:" + TimeUtil.gettodaydate(4));
                System.out.println("正在拨号,请稍等。。。");
                return;
            }
        }
        // 是否使用代理
        String useProxy = PropertyUtil.getValue("Job12306Registration_useProxy", "train.reg.properties");
        // String resultString = JobTrainUtil.rep0Methods("", useProxy,
        // proxyHost, proxyPort);
        String resultString = CheckThisRepIsenable.checkThisRepIsenable(useProxy, proxyHost, proxyPort);
        if (resultString.contains("铁路客户服务中心")) {
            // 真正开始的方法!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            new Job12306Registration_RegThread("", useProxy, proxyHost, proxyPort).start();// 线程注册
        }
        else if (resultString.contains("网络繁忙") || resultString.contains("您的操作频率过快") || "-1".equals(resultString)) {// 如果被封了这个不使用代理的rep就等2小时在用
            if (resultString.contains("您的操作频率过快")) {
                System.out.println("检测情况:0:您的操作频率过快->停10秒");
                try {
                    Thread.sleep(10000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else {
                System.out.println("检测情况:0:" + resultString);
            }
            JobTrainUtil.reConnectAdsl();// 断开adsl重新连接
            WriteLog.write("12306Reg", "Job12306Registration_Thread_debug", ri + ":resultString0:" + resultString);
        }
        else {
            System.out.println("检测情况:1:" + resultString + ":" + "Job12306Registration_RegThread");
            WriteLog.write("12306Reg", "Job12306Registration_Thread_debug", ri + ":resultString1:" + resultString);
        }
        WriteLog.write("12306Reg", "Job12306Registration_Thread_debug", ri + ":all over:");
    }

    /**
     * 
     * @param damaRuleUrl
     * @return
     * @time 2015年9月6日 下午10:14:08
     * @author chendong
     */
    public String getConfig(String damaRuleUrl) {
        String config = "1@1000";// 开@2.5秒一次打码
        //读配置的
        try {
            config = SendPostandGet.submitGet(damaRuleUrl);
            // config = "4[0],5[0],1[10],7[0]@1@60000";//打码规则和间隔时间
            String[] array = config.split("@");
            config = array[1] + "@" + array[2];
        }
        catch (Exception e) {
            config = "";
        }
        if (config == null || !config.contains("@")) {
            config = "1@1000";
        }
        else {
            String[] array = config.split("@");
            long configTime = Long.parseLong(array[1]);
            if (configTime < 100) {
                configTime = 1000;
            }
            config = array[0] + "@" + configTime;
        }
        return config;
    }

    /**
     * 原来 wh修改的执行main方法的注册方法 这个方法定时任务只需要执行一次就行了
     * 
     * @time 2015年8月19日 下午2:12:19
     * @author chendong
     * @param proxyPort
     * @param proxyHost
     * @param useProxy
     */
    private void main_execute() {
        // =========================
        // #注册的rep
        // String Job12306Registration_repIpString =
        // PropertyUtil.getValue("Job12306Registration_repIpString",
        // "train.reg.properties");
        String Job12306Registration_proxyHost_proxyPort = PropertyUtil.getValue(
                "Job12306Registration_proxyHost_proxyPort", "train.reg.properties");
        Job12306Registration_RegThread[] Job12306Registration_repIpStrings = new Job12306Registration_RegThread[0];
        String[] Job12306Registration_proxyHost_proxyPorts = Job12306Registration_proxyHost_proxyPort.split("[|]");

        System.out.println("自动注册=" + Job12306Registration_repIpStrings.length + "台=开启啦===========》"
                + TimeUtil.gettodaydate(5));
        for (int i = 0; i < Job12306Registration_repIpStrings.length; i++) {
            try {
                String forSleepTime = PropertyUtil
                        .getValue("Job12306Registration_forSleepTime", "train.reg.properties");
                Long LongforSleepTime = Long.parseLong(forSleepTime);
                System.out.println("停止" + LongforSleepTime + "s,再扔");
                Thread.sleep(LongforSleepTime);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Job12306Registration_RegThread rep_url =
            // Job12306Registration_repIpStrings[i];
            String proxyHost_proxyPort = "";
            String proxyHost = "";
            String proxyPort = "";
            // 是否使用代理
            String useProxy = PropertyUtil.getValue("Job12306Registration_useProxy", "train.reg.properties");
            if ("1".equals(useProxy)) {
                DaempnRep();// 代理守护线程
                proxyHost_proxyPort = Job12306Registration_proxyHost_proxyPorts[i];
                String[] proxyHost_proxyPorts = proxyHost_proxyPort.split(":");
                proxyHost = proxyHost_proxyPorts[0];
                proxyPort = proxyHost_proxyPorts[1];
            }
            // new Job12306Registration_Thread(useProxy, proxyHost,
            // proxyPort).start();
            // 下面的是老的
            // urls.add("http://121.43.150.115:8080/Reptile/traininit");
            // reg_urls.add(rep_url);//第一次执行init所有rep
        }
    }

    /**
     * 代理守护线程
     * 
     * @time 2015年8月28日 下午6:41:45
     * @author chendong
     */
    private void DaempnRep() {
        // #//代理端口和ip
        String job12306Registration_proxyHost_proxyPort = PropertyUtil.getValue(
                "Job12306Registration_proxyHost_proxyPort", "train.properties");
        String Job12306Registration_repIpString_checkProxy = PropertyUtil.getValue(
                "Job12306Registration_repIpString_checkProxy", "train.properties");
        int proxyCount = job12306Registration_proxyHost_proxyPort.split("|").length;
        Long sleep = proxyCount * 30L;
        new Job12306Registration_RepDaempnThread(job12306Registration_proxyHost_proxyPort,
                Job12306Registration_repIpString_checkProxy, sleep).start();
    }

    /**
     * 王宏修改之前的execute 方法
     * 
     * @time 2015年8月19日 下午2:11:24
     * @author chendong
     */
    @SuppressWarnings("unused")
    private void execute_wh_before() {
        int r1 = new Random().nextInt(10000000);
        // String email_ip = PropertyUtil.getValue("reg_email_ip",
        // "train.properties");//邮箱服务器的ip
        // String email_host = PropertyUtil.getValue("reg_email_host",
        // "train.properties");
        // String url = "http://localhost:9016/Reptile/traininit";
        // url = getrepurl().getUrl();
        // execute(r1, email_ip, email_host, url);//old
        // execute_new(r1, email_ip, email_host, url);//new
    }

    // private void execute(int r1, String email_ip, String email_host, String
    // url) {
    // Long l1 = System.currentTimeMillis();
    // String logpassword = "asd123456";
    // // String logpassword = getRandomNum(9, 1);//密码随机的后来又不随机了
    //
    // String lastlogname = getSysconfigStringbydb("logname_num");//wsad00000001
    // 最后一个被自动注册的12306账号
    // String lastemail = getSysconfigStringbydb("email");//a00000001@51dzp.com
    // 最后一个用于注册12306账号的邮箱地址
    // // String email_ip =
    // getSysconfigString("mail_ip_12306");//自动注册12306账号的邮件服务的IP.
    // WriteLog.write("12306Reg","Job12306Registration", r1 + ":开始啦:email_ip:" + email_ip);
    // getPassengers(lastlogname, logpassword, lastemail, email_ip, r1, url,
    // null, 0, email_host,isystemservice);
    // System.out.println(":结束了:耗时:" + (System.currentTimeMillis() - l1));
    // WriteLog.write("12306Reg","Job12306Registration", r1 + ":结束了:耗时:" +
    // (System.currentTimeMillis() - l1));
    // }

    @SuppressWarnings("unused")
    private RepServerBean getrepurl1() {
        // #用户获取注册账号时候获取这个数据库里的rep地址而做的临时service配置
        String serviceurl = PropertyUtil.getValue("reg_serviceurl_getrepurl", "train.properties");
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // List list =
        // Server.getInstance().getSystemService().findMapResultByProcedure("sp_RepServer_GetOneRep");
        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            // REP名称
            String name = objectisnull(map.get("C_NAME"));
            // REP地址
            String url = objectisnull(map.get("C_URL"));
            // 最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            // 在下订单数
            // int useNumber =
            // Integer.parseInt(map.get("C_USENUMBER").toString());
            // 最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            // SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;

    }

    @SuppressWarnings("unused")
    private String getpinyin(String name, String id_no) {
        String PinYin = parsePinYinName(name);
        if (PinYin.length() < 6) {
        }
        else {
            PinYin = PinYin.substring(0, 6);
        }
        // if (PinYin.length() < 7) {
        PinYin = PinYin + JobTrainUtil.getRandomNum(4, 1);
        // }
        // if (PinYin.length() >= 7) {
        // PinYin = PinYin.substring(0, 6) + id_no.substring(10, 14);
        // }
        // if (PinYin.length() < 16) {
        // PinYin = PinYin + getRandomNum(4, 1);
        // }
        return PinYin;
    }

    /**
     * 12306账号存入数据库 匹配好姓名和证件号和邮箱地址
     * 
     * @param logname
     * @param logpassword
     * @return
     * @time 2014年10月20日 下午2:36:10
     * @author yinshubin
     * @param mailname
     * @param email_ip
     */
    /**
     * 传入12306用户名、密码，邮箱地址，邮箱ip 获取到一个customeruser对象
     * 
     * @param logname
     * @param logpassword
     * @param email
     * @param name
     * @param idnumber
     * @param email_ip
     * @param mailname
     * @param r1
     * @return
     * @time 2015年1月29日 下午1:20:07
     * @author chendong
     */
    /*
     * public Customeruser addCustomeruser(String logname, String logpassword,
     * String email, String email_ip, String mailname, int r1, Customeruser
     * customeruser, String email_host) { if (customeruser == null) {
     * customeruser = new Customeruser(); customeruser.setCreatetime(new
     * Timestamp(System.currentTimeMillis())); } customeruser.setModifytime(new
     * Timestamp(System.currentTimeMillis())); Trainpassenger trainpassenger =
     * getPassenger(); if (trainpassenger.getId() == 0) {
     * System.out.println(System.currentTimeMillis() + ":没有账号可以注册了"); } else {
     * String idnumber = trainpassenger.getIdnumber(); String name =
     * trainpassenger.getName(); String logname_pinyin = getpinyin(name,
     * idnumber); logname = logname_pinyin;//重新生成的新的12306的用户名 mailname =
     * logname;//把邮箱名设置成和用户名一样 long trainpassengerid = trainpassenger.getId();
     * // String addresult = Jmail.adduser(email_ip, mailname, "123456").trim();
     * String addresult = "-1"; if (customeruser.getId() > 0) { addresult =
     * "added"; email = customeruser.getMemberfax(); } else { addresult =
     * addemailuser(email_ip, mailname, "123456", r1).trim(); email = mailname +
     * email_host; customeruser.setMobile(getmobile()); }
     * WriteLog.write("12306Reg","Job12306Registration", r1 + ":添加JAMESMAIL返回结果:" +
     * addresult + ":" + email_ip + ":" + email); if (addresult.indexOf("added")
     * >= 0) { customeruser.setModifyuser("HTHY");
     * customeruser.setCreateuser("HTHY"); customeruser.setLoginname(logname);
     * customeruser.setMembername(name); customeruser.setMembermobile(idnumber);
     * customeruser.setMemberfax(email);
     * customeruser.setLogpassword(logpassword); customeruser.setType(4); long
     * agentid = Long.valueOf(getSysconfigString("qunar_agentid"));
     * customeruser.setAgentid(agentid); customeruser.setIsenable(0);
     * customeruser.setEnname("1"); customeruser.setState(0);//当前COOKIE可用
     * customeruser.setTicketnum((int) trainpassenger.getId()); try { if
     * (customeruser.getId() == 0) { customeruser =
     * Server.getInstance().getMemberService().createCustomeruser(customeruser);
     * WriteLog.write("12306Reg","Job12306Registration_createCustomeruser", r1 +
     * ":createCustomeruser:" + customeruser.getId()); } else {
     * Server.getInstance().getMemberService().updateCustomeruser(customeruser);
     * } } catch (SQLException e) { e.printStackTrace(); } } else {//邮箱增加失败
     * WriteLog.write("12306Reg","Job12306Registration", r1 + ":添加JAMESMAIL返回结果:增加失败:" +
     * email + ":" + email_ip + ":" + addresult); } } return customeruser; }
     */

    /**
     * 将数据库乘客改成已被注册
     * 
     * @param id
     * @time 2014年10月23日 上午9:38:23
     * @author yinshubin
     */
    /*
     * public void changePassenger1(long id) { String passengerSql =
     * "update T_TRAINPASSENGER set C_ADUITSTATUS=3 where ID=" + id; try {
     * Server.getInstance().getSystemService().findMapResultBySql(passengerSql,
     * null); } catch (Exception e) { e.printStackTrace(); } }
     */

    /**
     * 将数据库乘客改成未注册
     * 
     * @param id
     * @time 2014年10月23日 上午9:38:23
     * @author yinshubin
     */
    public void changePassenger_noreg(long id) {
        String passengerSql = "update T_TRAINPASSENGER set C_ADUITSTATUS=0 where ID=" + id;
        try {
            Server.getInstance().getSystemService().findMapResultBySql(passengerSql, null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 把loginname的数加1
     * 
     * @param logname
     * @return
     * @time 2014年12月14日 下午5:45:33
     * @author chendong
     */
    @SuppressWarnings("unused")
    private String getloginnameADDONE(String logname, String startString) {
        logname = startString + String.valueOf((Integer.valueOf(logname.replace(startString, "1")) + 1)).substring(1);
        return logname;
    }

    /**
     * 把email的数加1
     * 
     * @param logname
     * @return
     * @time 2014年12月14日 下午5:45:33
     * @author chendong
     */
    @SuppressWarnings("unused")
    private String getemailADDONE1(String email_host, String startString, String email) {
        try {
            email = startString
                    + String.valueOf((Integer.valueOf(email.replace(startString, "1").replace(email_host, "")) + 1))
                            .substring(1) + email_host;
        }
        catch (Exception e) {
        }
        return email;
    }

    /**
     * 重新注册12306账号 把注册失败的拿过来重新注册
     * 
     * @param cus
     * @time 2014年12月15日 下午5:04:12
     * @author chendong
     */
    @SuppressWarnings({ "unchecked", "unused" })
    private void agentreg() {
        String email_ip = "223.4.155.3";
        List<Customeruser> list = Server
                .getInstance()
                .getMemberService()
                .findAllCustomeruser(" where C_TYPE=4 AND C_ISENABLE=0 AND C_MEMBEREMAIL IS NOT NULL ",
                        "ORDER BY ID DESC", 1000, 0);
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(10);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        for (int i = 0; i < list.size(); i++) {
            Customeruser customeruser = list.get(i);
            t1 = new Reg12306accountThread(customeruser, email_ip);
            pool.execute(t1);
        }
        pool.shutdown();
    }

    public static void shoudongreg() {
        String[] ss = "".split(",");
        for (int i = 0; i < ss.length; i++) {
            // HttpsUtil.post("https://kyfw.12306.cn/otn//regist/activeAccount?userName=tc000000015&checkcode=Q9gQ",
            // "",
            // "utf-8");
            String str = Jmail.getMail(ss[i], "123456", "121.199.25.199", 0L);
            if (str.contains("activeAccount")) {
                WriteLog.write("12306Reg", "Job12306Registration_shougong", ":" + ss[i]);
                try {
                    HttpsUtil.Get(str);
                    Thread.sleep(10000);
                    HttpsUtil.Get(str);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // Customeruser customeruser =
            // Server.getInstance().getMemberService().findCustomeruser(87L);
            // customeruser.setIsenable(1);
            // Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
            // HttpsUtil.Get("https://kyfw.12306.cn/otn//regist/activeAccount?userName=tc000000017&checkcode=kbmu");
        }
    }

    /**
     * 获取一个等待注册的账号
     * 
     * @param trainorderid
     * @return
     * @time 2015年1月22日 下午1:05:36
     * @author chendong
     */
    @SuppressWarnings("unused")
    private Map getCustomeruserbydb() {
        Map map = new HashMap();
        String sql = "select top 1 ID,C_LOGINNAME,C_MEMBERNAME,C_MEMBERFAX,C_ISENABLE,C_MEMBERMOBILE "
                + "from T_CUSTOMERUSER where C_TYPE=4 and C_ISENABLE=10";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            map = (Map) list.get(0);
        }
        return map;
    }

    /**
     * 根据查到的map信息获取value
     * 
     * @param key
     * @time 2015年1月22日 下午1:08:54
     * @author chendong
     */
    @SuppressWarnings("unused")
    private String gettrainorderinfodatabyMapkey(Map map, String key) {
        String value = "-1";
        if (map.get(key) != null) {
            try {
                value = map.get(key).toString();
            }
            catch (Exception e) {
            }
        }
        return value;
    }

    /**
     * 如果名字没有生僻字返回名字如果有则，转化为拼音后返回
     * 
     * @param name
     * @return
     */
    public static String parsePinYinName(String name) {
        String result = "";
        try {
            for (int j = 0; j < name.length(); j++) {
                String pinyin = PinyinHelper.toHanyuPinyinStringArray(name.charAt(j))[0];
                pinyin = pinyin.substring(0, pinyin.length() - 1);
                result += pinyin;
            }
        }
        catch (Exception e) {
            result = JobTrainUtil.getRandomNum(6, 1);
        }
        return result;
    }

    /**
     * 增加邮箱用户
     * 
     * @param email_ip
     *            邮箱ip
     * @param mail
     *            邮箱用户名
     * @param mailpassword
     *            密码
     * @return
     * @time 2015年3月5日 下午2:42:57
     * @author chendong
     */
    @SuppressWarnings("unused")
    private String addemailuser(String email_ip, String mail, String mailpassword, int r1) {
        String resultstring = "-1";
        String[] cmds = new String[1];
        cmds[0] = "adduser " + mail + " " + mailpassword;
        WriteLog.write("12306Reg", "Job12306Registration_addemailuser", r1 + ":cmds:" + cmds);
        String[] resultstrings = sendcmd_telnet(email_ip, 4555, cmds);
        if (resultstrings.length > 0) {
            resultstring = resultstrings[0];
        }
        WriteLog.write("12306Reg", "Job12306Registration_addemailuser", r1 + ":resultstring:" + resultstring);
        return resultstring;
    }

    private static String[] sendcmd_telnet(String ip, int port, String[] cmds) {
        String[] resultstrings = new String[cmds.length];
        TelnetBase TelnetBase = new TelnetBase(ip, port);
        String user = "root";// whzf011843adm
        String password = "root";// 5n0wbIrdsMe3
        try {
            TelnetBase.connect();
            TelnetBase.sendUserName(user);
            TelnetBase.sendUserPwd(password);
            for (int i = 0; i < cmds.length; i++) {
                String resultstring = TelnetBase.sendCmd(cmds[i]).trim();
                resultstrings[i] = resultstring;
            }
        }
        catch (Exception e) {
            System.out.println(ip);
            e.printStackTrace();
        }
        return resultstrings;
    }

    // public static void main(String[] args) {
    // System.out.println(getTel());
    // }
    // 获取adsl帐号
    public static String zhangHao() {
        String adsl = PropertyUtil.getValue("adsl", "train.reg.properties");
        return adsl;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306Registration", "Job12306RegistrationGroup",
                Job12306Registration.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306Registration", "Job12306RegistrationGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
