package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccervice.util.DesUtil;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.IMemberService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.Util.TelnetBase;

/**
 * 在邮件系统里加入账号
 * 并把这个账号添加到customerusr
 * 
 * @time 2015年5月6日 下午10:12:25
 * @author chendong
 */
public class Job12306Registration_addMailUser extends TrainSupplyMethod implements Job {

    List<Trainpassenger> trainpassengers = new ArrayList<Trainpassenger>();

    public static void main(String[] args) {
        try {
                        Job12306Registration_addMailUser.startScheduler("0/1 0/1 * * * ?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        //        int r1 = new Random().nextInt(100000);
        Job12306Registration_addMailUser reg = new Job12306Registration_addMailUser();
        //        for (int i = 0; i < 1; i++) {
        //        reg.executeMethod();
//        reg.execute();
        //        }
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //        executeMethod();
        execute();
        execute();
        execute();
        execute();
    }

    /**
     * 
     * @time 2015年11月19日 下午5:21:26
     * @author chendong
     */
    private void executeMethod() {
        String Job12306Registration_addMailUser_execute_start_url = PropertyUtil.getValue(
                "Job12306Registration_addMailUser_execute_start_url", "train.properties");//邮箱服务器的ip
        String htmlIsStart = SendPostandGet.submitGet(Job12306Registration_addMailUser_execute_start_url);
        int i_executeCount = geti_executeCount();
        if ("1".equals(htmlIsStart)) {
            for (int i = 0; i < i_executeCount; i++) {
                execute();
            }
        }
        else {
            System.out.println("Job12306Registration_addMailUser:html暂停");
        }
    }

    /**
     * 
     * @return
     * @time 2015年11月19日 下午5:19:23
     * @author chendong
     */
    private int geti_executeCount() {
        int i_executeCount = 1;
        try {
            String executeCount = PropertyUtil.getValue("isstartJob12306Registration_addMailUser_executeCount",
                    "train.properties");
            i_executeCount = Integer.parseInt(executeCount);
        }
        catch (Exception e) {
        }
        return i_executeCount;
    }

    private void execute() {
        Long l1 = System.currentTimeMillis();
        int r1 = new Random().nextInt(100000);
        String logpassword = "asd123456";
        String email_ip = PropertyUtil.getValue("reg_email_ip", "train.properties");//邮箱服务器的ip
        String lastlogname = "";//wsad00000001 最后一个被自动注册的12306账号
        String lastemail = "";//a00000001@51dzp.com 最后一个用于注册12306账号的邮箱地址
        WriteLog.write("Job12306Registration_addMailUser", r1 + ":开始啦:email_ip:" + email_ip);
        String email_host = PropertyUtil.getValue("reg_email_host", "train.properties");
        Customeruser customeruser = new Customeruser();
        try {
            String mailname = lastemail.split("@")[0];//邮箱的用户名
            String JobIndexAgain_LogintocheckHeyan_serviceurl = PropertyUtil.getValue(
                    "JobIndexAgain_LogintocheckHeyan_serviceurl", "train.properties");
            String user_serviceurl = PropertyUtil.getValue("JobIndexAgain_CustomerUser_serviceurl", "train.properties");
            String serviceurl = JobIndexAgain_LogintocheckHeyan_serviceurl;
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService isystemservice = null;
            IMemberService imemberservice = null;
            IMemberService imemberUserservice = null;
            try {
                isystemservice = (ISystemService) factory.create(ISystemService.class, serviceurl
                        + ISystemService.class.getSimpleName());
                imemberservice = (IMemberService) factory.create(IMemberService.class, serviceurl
                        + IMemberService.class.getSimpleName());
                imemberUserservice = (IMemberService) factory.create(IMemberService.class, user_serviceurl
                        + IMemberService.class.getSimpleName());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            customeruser = addCustomeruser(lastlogname, logpassword, lastemail, email_ip, mailname, r1, email_host,
                    isystemservice, imemberUserservice);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(customeruser.getId() + "---->" + customeruser.getLoginname() + "---->" + "结束了:耗时:" + (System.currentTimeMillis() - l1));
        WriteLog.write("Job12306Registration_addMailUser",
                customeruser.getLoginname() + "---->" + "结束了:耗时:" + (System.currentTimeMillis() - l1));
    }

    public RepServerBean getrepurl() {
        String serviceurl = PropertyUtil.getValue("reg_serviceurl_getrepurl", "train.properties");
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //        List list = Server.getInstance().getSystemService().findMapResultByProcedure("sp_RepServer_GetOneRep");
        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;

    }

    private String getpinyin(String name, String id_no) {
        String PinYin = JobTrainUtil.parsePinYinName(name);
        if (PinYin.length() >= 7) {
            PinYin = PinYin.substring(0, 7);
        }
        PinYin = PinYin + JobTrainUtil.getRandomNum(9, 1);
        PinYin = PinYin.replaceAll(":", "");
        return PinYin;
    }

    /**
     * 12306账号存入数据库
     * 匹配好姓名和证件号和邮箱地址
     * @param logname
     * @param logpassword
     * @return
     * @time 2014年10月20日 下午2:36:10
     * @author yinshubin
     * @param mailname 
     * @param email_ip 
     */
    /**
     * 传入12306用户名、密码，邮箱地址，邮箱ip 获取到一个customeruser对象
     * @param logname
     * @param logpassword
     * @param email
     * @param name
     * @param idnumber
     * @param email_ip
     * @param mailname
     * @param r1
     * @return
     * @time 2015年1月29日 下午1:20:07
     * @author chendong
     * @param isystemservice 
     * @param imemberservice 
     */
    public Customeruser addCustomeruser(String logname, String logpassword, String email, String email_ip,
            String mailname, int r1, String email_host, ISystemService isystemservice, IMemberService imemberservice) {
        Customeruser customeruser = new Customeruser();
        customeruser.setCreatetime(new Timestamp(System.currentTimeMillis()));
        customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
        customeruser.setModifyuser("Job12306Registration_addMailUser");
        customeruser.setCreateuser("Job12306Registration_addMailUser");
        Trainpassenger trainpassenger = getPassenger(isystemservice);
        if (trainpassenger.getId() == 0) {
            System.out.println(System.currentTimeMillis() + ":没有证件号可以注册了");
        }
        else {
            String idnumber = trainpassenger.getIdnumber();
            String name = trainpassenger.getName();
            String logname_pinyin = getpinyin(name, idnumber);
            System.out.println("-------------------------------" + name + "=" + idnumber + "=" + logname_pinyin);
            logname = logname_pinyin;//重新生成的新的12306的用户名
            if (logname.startsWith("0") || logname.startsWith("1") || logname.startsWith("2")
                    || logname.startsWith("3") || logname.startsWith("4") || logname.startsWith("5")
                    || logname.startsWith("6") || logname.startsWith("7") || logname.startsWith("8")
                    || logname.startsWith("9")) {
                WriteLog.write("Job12306Registration_addMailUser", r1 + ":数字开头的:name:" + name + ":idnumber:" + idnumber
                        + ":logname_pinyin:" + logname_pinyin);
                logname = "a" + logname.substring(0, logname.length() - 2);

            }
            mailname = logname;//把邮箱名设置成和用户名一样
            String addresult = "-1";
            //            addresult = addemailuser(email_ip, mailname, "123456", r1).trim();
            addresult = "added";
            WriteLog.write("Job12306Registration_addMailUser", r1 + ":添加JAMESMAIL返回结果:" + addresult + ":" + email_ip
                    + ":" + email);
            if (addresult.indexOf("added") >= 0) {
                email = mailname + email_host;
                customeruser.setMobile(getmobile());

                String key = "A1B2C3D4E5F60708";
                try {
                    logname = DesUtil.encrypt(logname, key);
                }
                catch (Exception e1) {
                    e1.printStackTrace();
                }
                try {
                    logpassword = DesUtil.encrypt(logpassword, key);
                }
                catch (Exception e1) {
                    e1.printStackTrace();
                }

                customeruser.setLoginname(logname);
                customeruser.setMembername(name);
                customeruser.setMembermobile(idnumber);
                customeruser.setMemberfax(email);
                customeruser.setLogpassword(logpassword);
                customeruser.setType(4);
                customeruser.setAgentid(47L);
                customeruser.setIsenable(12);//12注册完邮箱账号拿完乘客等待去12306注册信息
                customeruser.setEnname("1");
                customeruser.setState(0);//当前COOKIE可用
                customeruser.setTicketnum((int) trainpassenger.getId());
                customeruser.setId(0);
                try {
                    customeruser = imemberservice.createCustomeruser(customeruser);
                    WriteLog.write("Job12306Registration_createCustomeruser", r1 + ":createCustomeruser:"
                            + customeruser.getId());
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {//邮箱增加失败
                WriteLog.write("Job12306Registration_addMailUser", r1 + ":添加JAMESMAIL返回结果:增加失败:" + email + ":"
                        + email_ip + ":" + addresult);
            }
        }
        return customeruser;
    }

    /**
     * 从存储过程中获取可以注册的人
     * 
     * 
     * @return
     * @time 2015年4月24日 下午9:24:46
     * @author chendong
     * @param isystemservice 
     */
    private Trainpassenger getPassenger(ISystemService isystemservice) {
        Trainpassenger trainpassenger = new Trainpassenger();
        if (trainpassengers.size() == 0) {
            List list = isystemservice.findMapResultByProcedure("sp_TrainPassenger_GetOnePassenger");
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Trainpassenger trainpass = new Trainpassenger();
                    Map map = (Map) list.get(i);
                    long id = Long.parseLong(objectisnull(map.get("id")));
                    String name = objectisnull(map.get("name"));
                    String idnumber = objectisnull(map.get("idnumber"));
                    trainpass.setId(id);
                    trainpass.setName(name);
                    trainpass.setIdnumber(idnumber);
                    trainpassengers.add(trainpass);
                }
            }
        }
        trainpassenger = trainpassengers.remove(0);
        return trainpassenger;
    }

    /**
     * 增加邮箱用户
     * 
     * @param email_ip 邮箱ip
     * @param mail 邮箱用户名
     * @param mailpassword 密码
     * @return
     * @time 2015年3月5日 下午2:42:57
     * @author chendong
     */
    private String addemailuser(String email_ip, String mail, String mailpassword, int r1) {
        String resultstring = "-1";
        String[] cmds = new String[1];
        cmds[0] = "adduser " + mail + " " + mailpassword;
        WriteLog.write("Job12306Registration_addMailUser", r1 + ":cmds:" + Arrays.toString(cmds));
        try {
            String[] resultstrings;
            resultstrings = sendcmd_telnet(email_ip, 4555, cmds);
            if (resultstrings.length > 0) {
                resultstring = resultstrings[0];
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("Job12306Registration_addMailUser", r1 + ":resultstring:" + resultstring);
        return resultstring;
    }

    private static String[] sendcmd_telnet(String ip, int port, String[] cmds) {
        String[] resultstrings = new String[cmds.length];
        TelnetBase TelnetBase = new TelnetBase(ip, port);
        String user = "root";//whzf011843adm
        String password = "root";//5n0wbIrdsMe3
        try {
            TelnetBase.connect();
            TelnetBase.sendUserName(user);
            TelnetBase.sendUserPwd(password);
            for (int i = 0; i < cmds.length; i++) {
                String resultstring = TelnetBase.sendCmd(cmds[i]).trim();
                resultstrings[i] = resultstring;
            }
        }
        catch (Exception e) {
            System.out.println(ip);
            e.printStackTrace();
        }
        return resultstrings;
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("Job12306Registration_addMailUser",
                "Job12306Registration_addMailUserGroup", Job12306Registration_addMailUser.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306Registration_addMailUser",
                "Job12306Registration_addMailUserGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
