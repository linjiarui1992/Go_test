package com.ccservice.inter.job.train;

import java.util.Map;

import org.quartz.Job;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import org.quartz.JobExecutionContext;
import com.alibaba.fastjson.JSONObject;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import java.util.concurrent.ExecutorService;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DBSourceEnum;
import com.ccservice.Util.db.DataColumn;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 改签退
 * @author WH
 */

public class ChangeReturnTicketJob extends TrainSupplyMethod implements Job {


	public DBSourceEnum sourceEnum = null;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if(sourceEnum == null){
			String source = PropertyUtil.getValue("DBSource", "DBSaftey.properties");
			if(!"".equals(source)){
				sourceEnum = DBSourceEnum.getDBSourceEnumByNo(Integer.parseInt(source));
				if (isopen()) {
		            try {
		                //暂停
		                stop();
		                //退票
		                getRefundTickets();
		            }
		            catch (Exception e) {
		                ExceptionUtil.writelogByException("ChangeReturnTicketJob_Exception", e);
		            }
		            finally {
		                //开启
		                start();
		            }
		        }
			}else{
				System.out.println("source is null");
				WriteLog.write("InitDBSourceEnum_ChangeReturnTicket_Exception", "source is null");
			}
		}
    }

    @SuppressWarnings("rawtypes")
    private String getStringMapValue(Map map, String key) {
        return map.get(key) == null ? "" : map.get(key).toString();
    }

    private void stop() {
        Server.getInstance().getDateHashMap().put("AutoChangeRefundOpen", "0");
    }

    private void start() {
        Server.getInstance().getDateHashMap().put("AutoChangeRefundOpen", "1");
    }

    //判断开关
    private boolean isopen() {
        //标识
        String flag = Server.getInstance().getDateHashMap().get("AutoChangeRefundOpen");
        //返回
        return ElongHotelInterfaceUtil.StringIsNull(flag) || "1".equals(flag);
    }

    @SuppressWarnings({ "rawtypes" })
    private void getRefundTickets() {
        //天数
        int refundTicketDay = Integer.parseInt(getSysconfigString("RefundTicketDay"));
        if (refundTicketDay < 0) {
            refundTicketDay = -refundTicketDay;
        }
        //退票申请时间
        if (refundTicketDay > 0) {
            refundTicketDay = -refundTicketDay;
        }
        String refundRequestTime = "";
        String currentDate = ElongHotelInterfaceUtil.getCurrentDate();
        try {
            if (refundTicketDay == 0) {
                refundRequestTime = currentDate;
            }
            else {
                refundRequestTime = ElongHotelInterfaceUtil.getAddDate(currentDate, refundTicketDay);
            }
        }
        catch (Exception e) {
            refundRequestTime = currentDate;
        }
        if (ElongHotelInterfaceUtil.StringIsNull(refundRequestTime)) {
            refundRequestTime = currentDate;
        }
        refundRequestTime = refundRequestTime + " 00:00:00";
        //退票个数
        int refund_num = Integer.parseInt(getSysconfigString("RefundTicketNumber"));
        //退票查询
        //        String sql = "select top " + refund_num + " C_ORDERID oid, ID tid from T_TRAINTICKET"
        //                + " where C_REFUNDREQUESTTIME >= '" + refundRequestTime + "' and C_STATUS = "
        //                + Trainticket.APPLYTREFUND + " and C_ISAPPLYTICKET = 1 order by C_DEPARTTIME";
        String sql = "select top " + refund_num + " p.C_ORDERID oid, t.ID tid, t.C_CHANGETYPE changeType,"
                + " t.C_DEPARTTIME oldTime, t.C_TTCDEPARTTIME newTime, t.C_REFUNDREQUESTTIME requestTime"
                + " from T_TRAINTICKET t with(nolock)"
                + " inner join T_TRAINPASSENGER p with(nolock) on p.ID = t.C_TRAINPID where t.C_STATUS = 5"
                + " and t.C_REFUNDREQUESTTIME >= '" + refundRequestTime + "' and t.C_ISAPPLYTICKET in (1, 3)"
                + " order by case when t.C_CHANGETYPE = 1 then t.C_TTCDEPARTTIME else t.C_DEPARTTIME end";
        //退票结果
        List list = new ArrayList();
        //查询数据
        try {
            if(sourceEnum != null){
            	DataTable tabale = DBHelper.GetDataTable(sql, sourceEnum);
            	List<DataRow> rows = tabale.GetRow();
            	for(DataRow rwo:rows){
            		Map<String,Object> map = new HashMap<String,Object>();
            		for(DataColumn column:rwo.GetColumn()){
            			map.put(column.GetKey(), column.GetValue());
            		}
            		list.add(map);
            	}
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("ChangeReturnTicketJob_Exception", e);
        }
        //当前退票数量
        int listSize = list == null ? 0 : list.size();
        //PRINT
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "=====开始改签退=====" + listSize);
        //更新车票为退票中
        String ids = "";
        //订单ID、车票ID
        List<JSONObject> idList = new ArrayList<JSONObject>();
        //循环
        for (int i = 0; i < listSize; i++) {
            //取值
            Map map = (Map) list.get(i);
            long oid = Long.parseLong(map.get("oid").toString());
            long tid = Long.parseLong(map.get("tid").toString());
            //发车时间
            String departTime;
            //1：线上改签
            if ("1".equals(getStringMapValue(map, "changeType"))) {
                departTime = getStringMapValue(map, "newTime");
            }
            else {
                departTime = getStringMapValue(map, "oldTime");
            }
            //单个
            JSONObject object = new JSONObject();
            object.put("oid", oid);
            object.put("tid", tid);
            object.put("departTime", departTime);
            object.put("requestTime", getStringMapValue(map, "requestTime"));
            //添加
            idList.add(object);
            //退票ID
            ids += tid + ",";
        }
        if (!ids.endsWith(",")) {
            return;
        }
        //ID
        ids = ids.substring(0, ids.length() - 1);
        //SQL
        sql = "update T_TRAINTICKET set C_REFUNDTYPE = -1, C_ISQUESTIONTICKET = 0, C_STATUS = "
                + Trainticket.REFUNDROCESSING + " where ID in (" + ids + ") and C_STATUS = " + Trainticket.APPLYTREFUND;
        //更新状态为退票中
        boolean UpdateTrue = false;
        //更新车票
        try {
            UpdateTrue = DBHelper.executeSql(sql,sourceEnum);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("ChangeReturnTicketJob_Exception", e);
        }
        //更新状态成功，开启退票
        if (UpdateTrue) {
            start();
        }
        //线程池
        ExecutorService pool = Executors.newFixedThreadPool(listSize);
        //循环查询
        for (JSONObject object : idList) {
            long tid = object.getLongValue("tid");
            try {
                if (!UpdateTrue) {
                    throw new Exception("Update Error.");
                }
                //参数
                long oid = object.getLongValue("oid");
                String departTime = object.getString("departTime");
                String requestTime = object.getString("requestTime");
                //PRINT
                System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "=====开始改签退=====" + oid + "=====" + tid);
                //改签退
                pool.execute(new ThreadChangeReturnTicket(oid, tid, departTime, requestTime));
            }
            catch (Exception e) {
                try {
                    ReductionTicketStatus(tid);
                }
                catch (Exception E) {
                    ExceptionUtil.writelogByException("ChangeReturnTicketJob_Exception", E);
                }
            }
        }
        //关闭线程池
        pool.shutdown();
    }

    //还原为申请退票
    private void ReductionTicketStatus(long ticketId) {
        if (ticketId > 0) {
            //SQL
            String sql = "update T_TRAINTICKET set C_REFUNDTYPE = 0, C_STATUS = " + Trainticket.APPLYTREFUND
                    + " where ID  = " + ticketId + " and C_STATUS = " + Trainticket.REFUNDROCESSING;
            //更新状态为退票中
            DBHelper.executeSql(sql,sourceEnum);
        }
    }

}

class ThreadChangeReturnTicket extends Thread {

    private long oid;

    private long tid;

    private String departTime;

    private String requestTime;

    public ThreadChangeReturnTicket(long oid, long tid, String departTime, String requestTime) {
        this.oid = oid;
        this.tid = tid;
        this.departTime = departTime;
        this.requestTime = requestTime;
    }

    public void run() {
        //参数
        JSONObject object = new JSONObject();
        //设置
        object.put("orderId", oid);
        object.put("ticketId", tid);
        object.put("ticket_no", "");//仅记日志用，暂设为空
        //捕捉
        try {
            //格式化
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //发车时间
            object.put("departTime", sdf.parse(departTime).getTime());
        }
        catch (Exception e) {
            //当前时间
            object.put("departTime", System.currentTimeMillis());
        }
        //捕捉
        try {
            //格式化
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //发车时间
            object.put("requestTime", sdf.parse(requestTime).getTime());
        }
        catch (Exception e) {
            //当前时间
            object.put("requestTime", System.currentTimeMillis());
        }
        try {
            Server.getInstance().getTrain12306Service().ChangeReturnTicket(object);
        }
        catch (Exception e) {
            try {
                ReductionTicketStatus(tid);
            }
            catch (Exception E) {
                ExceptionUtil.writelogByException("ChangeReturnTicketJob_Exception", E);
            }
        }
    }

    //还原为申请退票
    private void ReductionTicketStatus(long ticketId) {
        if (ticketId > 0) {
        	String source = PropertyUtil.getValue("DBSource", "DBSaftey.properties");
			if(!"".equals(source)){
				//SQL
	            String sql = "update T_TRAINTICKET set C_REFUNDTYPE = 0, C_STATUS = " + Trainticket.APPLYTREFUND
	                    + " where ID  = " + ticketId + " and C_STATUS = " + Trainticket.REFUNDROCESSING;
	            //更新状态为退票中
	            DBHelper.executeSql(sql,DBSourceEnum.getDBSourceEnumByNo(Integer.parseInt(source)));
			}
        }
    }
}