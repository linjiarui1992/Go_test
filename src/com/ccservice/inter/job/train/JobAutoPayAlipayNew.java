package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.util.PageInfo;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadAutoPayAlipaynew;
import com.ccservice.inter.server.Server;

public class JobAutoPayAlipayNew extends TrainSupplyMethod implements Job {
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String alipayautopaystate = getSysconfigStringbydb("alipayautopaystate");
        WriteLog.write("12306支付宝自动支付", "alipay开关：" + alipayautopaystate);
        if ("0".equals(alipayautopaystate)) {
            try {
                changeSystemCofigbyname("alipayautopaystate", "1");
                exThread();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            changeSystemCofigbyname("alipayautopaystate", "0");
        }
    }

    public static void main(String[] args) {
        //String i_url = "alipayurl1".replaceAll("alipayurl", "");
        //System.out.println(i_url);
        //new JobAutoPayAlipayNew().exThread();
        String i_url = "tc000009798/alipayurl2".split("/")[1].replaceAll("alipayurl", "");
        System.out.println(i_url);
    }

    public void exThread() {
        String wheresql = " where O.C_ORDERSTATUS=2 and O.C_STATE12306=4 and (O.C_ISQUESTIONORDER=0 or O.C_ISQUESTIONORDER is null)  and O.C_CHANGESUPPLYTRADENO is not null and O.C_CHANGESUPPLYTRADENO!='' ";
        String[] strs = getSysconfigStringbydb("alipayurlnum").split(",");
        PageInfo pageinfo = new PageInfo();
        pageinfo.setPagerow(100);
        List list = Server.getInstance().getTrainService()
                .findAllTrainorderBySql(wheresql, " order by O.id asc ", pageinfo);
        if (list.size() > 0) {
            list.remove(0);
        }
        int accountindex = 0;
        String paythreadcount = getSysconfigString("Alipaytradenumtreadcount");
        WriteLog.write("12306支付宝自动支付", ":当前线程数：" + paythreadcount + ":");
        ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(paythreadcount).intValue());
        for (int i = 0; i < list.size(); i++) {
            int t = new Random().nextInt(10000);
            try {
                Trainorder trainorder = (Trainorder) list.get(i);
                trainorder = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
                WriteLog.write("12306支付宝自动支付", t + ":订单号：" + trainorder.getOrdernumber());
                if (((Trainpassenger) trainorder.getPassengers().get(0)).getTraintickets().size() == 1) {
                    try {
                        String i_url = strs[accountindex];
                        String alipayurl = "-1";
                        if (trainorder.getSupplyaccount().contains("alipayurl")) {
                            alipayurl = getSysconfigString(trainorder.getSupplyaccount().split("/")[1]);
                            i_url = trainorder.getSupplyaccount().split("/")[1].replaceAll("alipayurl", "");
                            WriteLog.write("12306支付宝自动支付", t + ":二次支付：" + alipayurl + ",支付索引：" + i_url + ",支付账户："
                                    + trainorder.getSupplyaccount());
                        }
                        else {
                            alipayurl = getSysconfigString("alipayurl" + i_url);
                        }
                        Thread thr = new MyThreadAutoPayAlipaynew(trainorder, i_url, t, alipayurl);
                        pool.execute(thr);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    accountindex++;
                    if (accountindex >= strs.length) {
                        accountindex = 0;
                    }
                }
            }
            catch (Exception e) {
                WriteLog.write("12306支付宝自动支付", t + ":error：" + e.getMessage());
            }
        }
        pool.shutdown();
    }
}