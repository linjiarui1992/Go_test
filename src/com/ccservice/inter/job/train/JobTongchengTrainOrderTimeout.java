package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

//create table T_TRAINORDERTIMEOUT
//(  
//  C_ORDERID  decimal(18, 0),  订单号
//  C_STATE int 当前处理状态 0 尚未处理 1 下单成功等待支付 2 下单失败已经处理 3 下单成功支付成功 
//  C_WPS nvarchar(50) 卧铺上|中|下   PS:1|0|2
//)  
public class JobTongchengTrainOrderTimeout extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub
        String ids = getTrainorderIDs();
        //        if (!"".equals(ids.trim())) {
        //            ids = createTrainorderTimeout(ids);
        if (!"".equals(ids.trim())) {
            changeToWaitorder(ids);
        }
        //        }
    }

    /**
     * 获取满足条件的IDs 
     * 
     * @return 如果没有满足的 返回""；
     * @time 2015年1月10日 下午5:25:20
     * @author fiend
     */
    @SuppressWarnings("rawtypes")
    public String getTrainorderIDs() {
        String sql = "SELECT ID FROM T_TRAINORDER WHERE C_ORDERSTATUS=2 AND C_ISQUESTIONORDER=2 AND ( C_ISPLACEING is null or C_ISPLACEING !=1 ) "
                + "AND (C_CONTROLNAME IS NULL OR C_CONTROLNAME ='') AND C_EXTORDERCREATETIME<'"
                + getCreateTime(45)
                + "'";
        WriteLog.write("JobTongchengTrainOrderTimeout", "符合重新下单的sql:" + sql);
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String ids = "";
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String id = map.get("ID").toString();
            if (addBudanIds(id)) {
                ids += id + ",";
            }
        }
        if (!"".equals(ids.trim())) {
            ids = ids.substring(0, ids.length() - 1);
        }
        WriteLog.write("JobTongchengTrainOrderTimeout", "符合重新下单的订单IDs:" + ids);
        return ids;
    }

    /**
     * 修改订单，将订单转为可以下单
     * @time 2015年1月10日 下午5:26:24
     * @author fiend
     */
    public void changeToWaitorder(String ids) {
        try {
            String sql = "UPDATE T_TRAINTICKET SET C_SEATNO=NULL,C_SEATTYPE = REPLACE(C_SEATTYPE,'硬卧代硬座','硬座') WHERE C_TRAINPID IN (SELECT ID FROM T_TRAINPASSENGER WHERE C_ORDERID IN ("
                    + ids + "))";
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            sql = "UPDATE T_TRAINORDER SET C_STATE12306=1,C_ISQUESTIONORDER=0,C_AUTOUNIONPAYURL=null,C_CHANGESUPPLYTRADENO=NULL,C_EXTNUMBER=null WHERE ID IN ("
                    + ids + ")";
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            WriteLog.write("JobTongchengTrainOrderTimeout", "修改订单，将订单转为可以下单:订单IDs:" + ids);
            ids = createTrainorderTimeout(ids);
            if (!"".equals(ids.trim())) {
                String[] idss = ids.split(",");
                for (int i = 0; i < idss.length; i++) {
                    gotoordering(Long.valueOf(idss[i]));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            WriteLog.write("JobTongchengTrainOrderTimeout", "修改订单，将订单转为可以下单:订单IDs:" + ids);
        }

    }

    /**
     * 创建超时记录(先删除再创建) 
     * @param ids
     * @time 2015年1月10日 下午5:57:44
     * @author fiend
     */
    public String createTrainorderTimeout(String ids) {
        String[] idss = ids.split(",");
        ids = "";
        for (int i = 0; i < idss.length; i++) {
            try {
                String sql = "SELECT COUNT(ID) FROM T_TRAINTICKET WHERE C_SEATNO LIKE '%上铺%' and C_TRAINPID IN (SELECT ID FROM T_TRAINPASSENGER WHERE C_ORDERID="
                        + Long.valueOf(idss[i]) + ")";
                int counts = Server.getInstance().getSystemService().countAdvertisementBySql(sql);
                sql = "SELECT COUNT(ID) FROM T_TRAINTICKET WHERE C_SEATNO LIKE '%中铺%' and C_TRAINPID IN (SELECT ID FROM T_TRAINPASSENGER WHERE C_ORDERID="
                        + Long.valueOf(idss[i]) + ")";
                int countz = Server.getInstance().getSystemService().countAdvertisementBySql(sql);
                sql = "SELECT COUNT(ID) FROM T_TRAINTICKET WHERE C_SEATNO LIKE '%下铺%' and C_TRAINPID IN (SELECT ID FROM T_TRAINPASSENGER WHERE C_ORDERID="
                        + Long.valueOf(idss[i]) + ")";
                int countx = Server.getInstance().getSystemService().countAdvertisementBySql(sql);
                sql = "DELETE FROM T_TRAINORDERTIMEOUT WHERE C_ORDERID IN (" + Long.valueOf(idss[i]) + ")";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                sql = "INSERT INTO T_TRAINORDERTIMEOUT(C_ORDERID,C_STATE,C_WPS) VALUES(" + Long.valueOf(idss[i])
                        + ",0,'" + counts + "|" + countz + "|" + countx + "')";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                WriteLog.write("JobTongchengTrainOrderTimeout", "创建重新下单表记录:订单ID:" + idss[i]);
                ids += idss[i] + ",";
            }
            catch (NumberFormatException e) {
                WriteLog.write("JobTongchengTrainOrderTimeout_失败", "创建重新下单表记录:订单ID:" + idss[i]);
            }
        }
        if (!"".equals(ids.trim())) {
            ids = ids.substring(0, ids.length() - 1);
        }
        return ids;
    }

    public static void main(String[] args) {
        JobTongchengTrainOrderTimeout jttot = new JobTongchengTrainOrderTimeout();
        String ids = jttot.getTrainorderIDs();
        //        if (!"".equals(ids.trim())) {
        //            ids = createTrainorderTimeout(ids);
        if (!"".equals(ids.trim())) {
            jttot.changeToWaitorder(ids);
        }
        List list = Server.getInstance().getBudanids();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    /**
     * 调用下单方法,生成线程(给俺去下单) 
     * @return
     * @time 2015年1月21日 上午10:05:25
     * @author fiend
     */
    private void gotoordering(long id) {
        Long l1 = System.currentTimeMillis();
        try {
            //            String url = getSysconfigString("activeMQ_url");
            //            String QUEUE_NAME = getSysconfigString("QueueMQ_trainorder_waitorder_orderid");
            String url = PropertyUtil.getValue("activeMQ_url", "train.properties");
            String QUEUE_NAME = PropertyUtil.getValue("QueueMQ_trainorder_waitorder_orderid", "train.properties");
            try {
                ActiveMQUtil.sendMessage(url, QUEUE_NAME, id + "");
            }
            catch (JMSException e) {
                e.printStackTrace();
            }
            WriteLog.write("JobTongchengTrainOrderTimeout",
                    "补单实时下单成功:ID:" + id + ":time:" + (System.currentTimeMillis() - l1));
        }
        catch (Exception e) {
            WriteLog.write("JobTongchengTrainOrderTimeout", "补单实时下单失败:ID:" + id);
        }
    }

    /**
     * 查看缓存里是否有这个ID，如果有返回false 如果没有增加到缓存中，返回true 
     * @param id
     * @return
     * @time 2015年3月13日 下午3:52:40
     * @author fiend
     */
    private boolean addBudanIds(String id) {
        try {
            List<String> list = Server.getInstance().getBudanids();
            for (int i = 0; i < list.size(); i++) {
                if (id.equals(list.get(i))) {
                    WriteLog.write("id_增加缓存中订单ID", "增加失败已存在--->" + id);
                    return false;
                }
            }
            list.add(id);
            Server.getInstance().setBudanids(list);
            WriteLog.write("id_增加缓存中订单ID", "增加成功--->" + id);
            return true;
        }
        catch (Exception e) {
            WriteLog.write("id_增加缓存中订单ID", "增加失败--->" + id);
            return false;
        }
    }
}
