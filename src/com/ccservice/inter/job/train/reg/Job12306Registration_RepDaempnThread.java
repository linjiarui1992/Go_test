package com.ccservice.inter.job.train.reg;

import com.ccservice.inter.job.train.thread.Rep0Thread;
import com.ccservice.inter.server.Server;

/**
 * 检测rep和代理是否可用
 * 
 * @time 2015年9月6日 上午10:52:36
 * @author chendong
 */
public class Job12306Registration_RepDaempnThread extends Thread {
    String job12306Registration_proxyHost_proxyPort;

    String job12306Registration_repIpString;

    Long sleep;

    public Job12306Registration_RepDaempnThread(String job12306Registration_proxyHost_proxyPort,
            String job12306Registration_repIpString, Long sleep) {
        super();
        this.job12306Registration_proxyHost_proxyPort = job12306Registration_proxyHost_proxyPort;
        this.job12306Registration_repIpString = job12306Registration_repIpString;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        repDaempn_while();
    }

    /**
     * rep 代理守护线程
     * @time 2015年8月28日 下午5:16:03
     * @author chendong
     */
    private void repDaempn_while() {

        while (true) {
            repDaempn(job12306Registration_proxyHost_proxyPort, job12306Registration_repIpString);
            System.out.println("==============>守护rep>停止" + this.sleep + ":+后开始");
            try {
                Thread.sleep(this.sleep);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * rep 代理守护线程
     * @return
     * @time 2015年8月28日 下午3:29:13
     * @author chendong
     * @param job12306Registration_repIpString 
     * @param job12306Registration_proxyHost_proxyPort 
     * @param rep_url2 
     */
    public void repDaempn(String job12306Registration_proxyHost_proxyPort, String job12306Registration_repIpString) {
        //        String rep_url = "http://121.40.48.27:8080/Reptile/traininit";
        String[] Job12306Registration_proxyHost_proxyPorts = job12306Registration_proxyHost_proxyPort.split("[|]");
        String proxyHost_proxyPort = "-1";
        String[] Job12306Registration_repIpStrings = job12306Registration_repIpString.split("[|]");
        for (int j = 0; j < Job12306Registration_proxyHost_proxyPorts.length; j++) {
            try {
                Thread.sleep(1000L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            String rep_url = Job12306Registration_repIpStrings[0];
            //如果代理的数量大于rep的数量,就取rep的余数
            if (j > 0 && j >= Job12306Registration_repIpStrings.length) {
                int repIndex = j % Job12306Registration_repIpStrings.length;
                rep_url = Job12306Registration_repIpStrings[repIndex];
            }
            else {
                rep_url = Job12306Registration_repIpStrings[j];
            }
            proxyHost_proxyPort = Job12306Registration_proxyHost_proxyPorts[j];
            String[] proxyHost_proxyPorts = proxyHost_proxyPort.split(":");
            String proxyHost = proxyHost_proxyPorts[0];
            String proxyPort = proxyHost_proxyPorts[1];
            String useProxy = "1";
            //            String checkResult = rep0Method(rep_url, useProxy, proxyHost, proxyPort);
            String proxyHost_proxyPortKey = proxyHost + ":" + proxyPort;
            //如果代理池里没有的话才去验证这个代理ip是否可用
            if (Server.ProxyIpList.get(proxyHost_proxyPortKey) == null) {
                new Rep0Thread(rep_url, useProxy, proxyHost, proxyPort).start();
            }
        }
        //        return proxyHost_proxyPort;
    }
}
