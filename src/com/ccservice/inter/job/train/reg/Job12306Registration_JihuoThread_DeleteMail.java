package com.ccservice.inter.job.train.reg;

import org.apache.commons.httpclient.methods.DeleteMethod;

import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.Jmail;
import com.ccservice.inter.server.Server;

/**tc000153953@mlzg99.com
 * 自动注册12306账号
 * 开始自动注册12306账号
 * 账号，密码，E-MAIL
 * @time 2014年12月14日 下午3:57:21
 * @author chendong
 * @author yinshubin
 */

public class Job12306Registration_JihuoThread_DeleteMail extends Thread {
    String mailname;

    String email_ip;

    public Job12306Registration_JihuoThread_DeleteMail(String mailname, String email_ip) {
        this.mailname = mailname;
        this.email_ip = email_ip;
    }

    /**
     * 
     */
    public Job12306Registration_JihuoThread_DeleteMail() {
    }

    @Override
    public void run() {
        deleteMethod(mailname, email_ip);
    }

    /**
     * 
     * @time 2015年9月7日 下午2:14:22
     * @author chendong
     * @param email_ip2 
     * @param mailname2 
     */
    public void deleteMethod(String mailname, String email_ip) {
        String delusersult = "-1";//成功了就把那个邮箱删除了
        try {
            delusersult = Jmail.deluser(mailname, email_ip).trim();
        }
        catch (Exception e) {
        }
        WriteLog.write("Job12306Registration_JihuoThread_DeleteMail", ":激活成功删除邮箱:deletemail:" + ":" + mailname + ":"
                + email_ip + ":邮箱删除结果" + delusersult + ":12306注册成功");

    }
}
