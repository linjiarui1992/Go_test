package com.ccservice.inter.job.train;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class JobRefundChange extends TrainSupplyMethod implements Job {

    private final static String REFUNDOFFAILURE = "退票失败";

    private final static String REFUNDSUCCESS = "退票成功";

    private final static String CHANGEOFFAILURE = "改签失败";

    private final static String CHANGESUCCESS = "改签成功";

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub

    }

    /**
     * 获取等待退票订单 
     * @time 2014年11月13日 上午10:16:01
     * @author yinshubin
     */
    public void orderWaitRefund() {
        String sql = "SELECT O.ID FROM T_TRAINORDER O LEFT JOIN  T_TRAINPASSENGER P ON P.C_ORDERID=O.ID LEFT JOIN T_TRAINTICKET T ON T.C_TRAINPID=P.ID WHERE T.C_STATUS=2";
        List<Trainorder> list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        while (list.size() > 0) {
            orderResult(list.get(0).getId());
            list.remove(0);
        }
    }

    /**
     * 获取订单信息 
     * @time 2014年11月13日 上午10:11:44
     * @author yinshubin
     */
    public void orderResult(long trainorderid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        if (trainorder.getPassengers().get(0).getTraintickets().size() == 1) {
            String logname = "";
            String extnumber = trainorder.getExtnumber();
            if (extnumber.contains(",")) {
                extnumber = extnumber.split(",")[0];
            }
            if (trainorder.getSupplyaccount() != null && trainorder.getSupplyaccount().contains("/")) {
                logname = trainorder.getSupplyaccount().split("/")[0];
            }
            if (!logname.equals("")) {
                long quer_id = Long.valueOf(getSysconfigString("qunar_agentid"));
                String sql = "select * from T_CUSTOMERUSER  where C_AGENTID = " + quer_id + " AND C_LOGINNAME = \'"
                        + logname + "\' ";
                List<Customeruser> customeruserlist = Server.getInstance().getMemberService()
                        .findAllCustomeruserBySql(sql, -1, 0);
                if (0 != customeruserlist.size()) {
                    Customeruser customeruser = customeruserlist.get(0);
                    //                    String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
                    String url = customeruser.getMemberemail();
                    String par = "datatypeflag=23&cookie=" + customeruser.getCardnunber() + "&name="
                            + trainorder.getPassengers().get(0).getName() + "&trainorderid=" + trainorderid
                            + "&logname=" + logname + "&logpassword=" + customeruser.getLogpassword() + "&extnumber="
                            + extnumber;
                    WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取已完成订单 ，调取TrainInit：" + url
                            + "(:)" + par);
                    String infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                    WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取已完成订单，调取TrainInit：返回"
                            + infodata);
                    if (infodata.contains("OrderDTODataList")) {
                        isSameOrder(infodata, trainorderid);
                    }
                    else {
                        //                        writeTrainRC(trainorderid, JobRefundChange.REFUNDOFFAILURE, logname + "中无此订单，请在操作记录中寻找12306账号");
                    }
                }
                else {
                    //                    writeTrainRC(trainorderid, JobRefundChange.REFUNDOFFAILURE, "12306账号查询错误，请在操作记录中寻找12306账号");
                }
            }
            else {
                //                writeTrainRC(trainorderid, JobRefundChange.REFUNDOFFAILURE, "12306账号未填写，请在操作记录中寻找12306账号");
            }
        }
        else {
            //            writeTrainRC(trainorderid, JobRefundChange.REFUNDOFFAILURE, "联程票");
        }
    }

    /**
     * 是否可以退改签
     * @return
     * @time 2014年11月13日 下午4:17:45
     * @author yinshubin
     */
    public void isCanRefundChange(String resultStr, long trainorderid) {
        String orderstr = isSameOrder(resultStr, trainorderid);
        if (!orderstr.equals("false")) {
            refundChangeByTime();
        }
        else {
            //TODO 不能退改签
        }
    }

    /**
     * 按时间区分类别
     * @time 2014年11月13日 下午5:54:56
     * @author yinshubin
     */
    public void refundChangeByTime() {

    }

    /**
     * 拿到相同订单的信息 
     * @param resultStr
     * @param trainorderid
     * @return
     * @time 2014年11月13日 下午5:37:44
     * @author yinshubin
     */
    public String isSameOrder(String resultStr, long trainorderid) {
        boolean issame = false;
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        JSONObject jsono = JSONObject.fromObject(resultStr);
        JSONObject jodata = jsono.getJSONObject("data");
        JSONArray jadataList = jodata.getJSONArray("OrderDTODataList");
        while (jadataList.size() > 0) {
            try {
                String station_train_code = jadataList.getJSONObject(0).getString("train_code_page");
                String train_date = jadataList.getJSONObject(0).getString("start_train_date_page").substring(0, 10);
                Trainticket tk1 = trainorder.getPassengers().get(0).getTraintickets().get(0);
                if (tk1.getDeparttime().substring(0, 10).equals(train_date)
                        && mactchcode(tk1.getTrainno(), station_train_code)) {
                    String pnamelist = jadataList.getJSONObject(0).getString("array_passser_name_page");
                    String[] pnames = pnamelist.replace("[", "").replace("\"", "").replace("]", "").split(",");
                    for (int i = 0; i < trainorder.getPassengers().size(); i++) {
                        for (int j = 0; j < pnames.length; j++) {
                            if (trainorder.getPassengers().get(i).getName().equals(pnames[j])) {
                                issame = true;
                                break;
                            }
                            else {
                                issame = false;
                            }
                        }
                        if (!issame) {
                            break;
                        }
                    }
                    if (issame) {
                        return jadataList.getJSONObject(0).toString();
                    }
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jadataList.remove(0);
        }
        return "false";
    }

    /**
     * 改签 
     * @time 2014年11月13日 下午4:14:41
     * @author yinshubin
     */
    public void changeOrder() {

    }

    /**
     * 退改签 
     * @time 2014年11月13日 下午4:15:54
     * @author yinshubin
     */
    public void refundChangeOrder(long trainorderid, String cookieString, String url) {
        String infodata = "";
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String ticketdate = "5";
        //        trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparttime();
        String start_station_name = trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparture();
        String end_station_name = trainorder.getPassengers().get(0).getTraintickets().get(0).getArrival();
        String costprice = trainorder.getPassengers().get(0).getTraintickets().get(0).getPrice() + "";
        //        String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
        String querystr = getSysconfigString("querystr");
        String par = "datatypeflag=24&cookie=" + cookieString + "&extnumber=" + trainorder.getExtnumber()
                + "&start_station_name=" + start_station_name + "&end_station_name=" + end_station_name + "&costprice="
                + costprice + "&ticketdate=" + ticketdate + "&passengers=" + passengers(trainorderid)
                + "&trainorderid=" + trainorderid + "&querystr=" + querystr;
        infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
        System.out.println(infodata);
    }

    /**
     * 拼写passengers 
     * @param trainorderid
     * @return
     * @time 2014年11月18日 下午3:32:21
     * @author yinshubin
     */
    public String passengers(long trainorderid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String passengers = "";
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            String pname = trainpassenger.getName();
            String pidnumber = trainpassenger.getIdnumber();
            int pidtype = trainpassenger.getIdtype();
            Trainticket trainticket = trainpassenger.getTraintickets().get(0);
            String idtype = "1";
            if (1 == pidtype) {
                idtype = "1";
            }
            if (3 == pidtype) {
                idtype = "B";
            }
            if (4 == pidtype) {
                idtype = "C";
            }
            if (5 == pidtype) {
                idtype = "G";
            }
            String tickettype = trainticket.getTickettype() + "";
            if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                tickettype = "1";
            }
            else {
                if ("0".equals(tickettype)) {
                    tickettype = "2";
                }
            }
            String passenger = pname + "|" + idtype + "|" + pidnumber + "|" + tickettype + "|waitchange|"
                    + Float.valueOf(trainticket.getPayprice()).toString().replace(".", "");
            passengers += passenger + ",";
        }
        return passengers;
    }

    /**
     * 退票 
     * @time 2014年11月13日 下午4:15:01
     * @author yinshubin
     */
    public void refundOrder() {

    }

    /**
     * 退改签书写操作记录 
     * @param trainorderid
     * @param type 退票/改签
     * @param note 成功/失败，及其原因
     * @time 2014年11月13日 下午4:03:00
     * @author yinshubin
     */
    public void writeTrainRC(long trainorderid, String type, String note) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(note);
        rc.setCreateuser(type);
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 说明：比对车次信息
     * @param train_code
     * @param station_train_code
     * @return
     * @time 2014年11月13日 下午5:33:00
     * @author yinshubin
     */
    public boolean mactchcode(String train_code, String station_train_code) {
        if (train_code.contains("/")) {
            String[] strs = train_code.split("/");
            for (String str : strs) {
                if (station_train_code.equals(str)) {
                    return true;
                }
            }
        }
        else {
            return train_code.equals(station_train_code);
        }
        return false;
    }

    /**
     * 将订单改为需要人工操作 
     * @param trainorderid
     * @time 2014年11月13日 下午4:34:11
     * @author yinshubin
     */
    /*
    public void changeOrderToHum(long trainorderid) {
     Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
     trainorder.setOrderstatus(Trainorder.);
    }*/
    public static void main(String[] args) {
        JobRefundChange jrc = new JobRefundChange();
        String cookieString = "JSESSIONID=0A01D95C60065637AF1E5AE2D675FEF0DB593FAAE3; BIGipServerotn=1557725450.24610.0000";
        long trainorderid = 1017;
        String url = "";
        jrc.refundChangeOrder(trainorderid, cookieString, url);
    }
}
