package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadUpdateUnionTrade;
import com.ccservice.inter.server.Server;

/**
 * 
 * 定时更新支付中状态订单
 * @time 2014年12月26日 下午1:06:32
 * @author wzc
 */
public class JobUnionTradenum extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        WriteLog.write("12306银联更新交易号", "运行中：" + System.currentTimeMillis());
        Jobupdatetrade();
    }

    /**
     * 更新银联支付交易号
     * 
     * @time 2014年12月13日 下午12:40:36
     * @author wzc
     */
    public String Jobupdatetrade() {
        String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
        String sql = " select TOP 20 ID,C_SUPPLYACCOUNT,C_ORDERNUMBER,C_QUNARORDERNUMBER from T_TRAINORDER where C_ORDERSTATUS=2 and C_STATE12306=5 and C_ISQUESTIONORDER!=2 and C_SUPPLYACCOUNT like '%UNION%' AND  (C_SUPPLYTRADENO is null or C_SUPPLYTRADENO NOT LIKE '2%')  order by C_CREATETIME asc  ";
        List listresult = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        int paycounttime = Integer.valueOf(getSysconfigString("paycount"));//支付次数
        if (paycounttime < 0) {
            paycounttime = 1;
        }
        String dangqianagentid = getSysconfigString("dangqianjiekouagentid");//当前agentid
        if (listresult != null && listresult.size() > 0) {
            String paythreadcount = getSysconfigString("paythreadcount");
            ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(paythreadcount).intValue());
            TrainSupplyMethod trainSupplyMethod = new TrainSupplyMethod();
            String tongcheng_agentid = getSysconfigString("tongcheng_agentid");
            String qunar_agentid = getSysconfigString("qunar_agentid");
            String payremandtel = getSysconfigString("payremandtel");
            try {
                String idstr = "";
                for (int i = 0; i < listresult.size(); i++) {
                    Map map = (Map) listresult.get(i);
                    String id = map.get("ID").toString();
                    if (i == listresult.size() - 1) {
                        idstr += id;
                    }
                    else {
                        idstr += id + ",";
                    }
                }
                String updatestatesql = "update T_TRAINORDER set C_STATE12306=8 where ID in (" + idstr + ")";
                Server.getInstance().getSystemService().findMapResultBySql(updatestatesql, null);
                WriteLog.write("12306银联更新交易号", "支付审核中：" + updatestatesql);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            for (int i = 0; i < listresult.size(); i++) {
                try {
                    Map map = (Map) listresult.get(i);
                    long orderid = Long.valueOf(map.get("ID").toString());
                    Thread thr = new MyThreadUpdateUnionTrade(trainSupplyMethod, tongcheng_agentid, qunar_agentid,
                            dangqianagentid, tongchengcallbackurl, paycounttime, payremandtel, orderid);
                    pool.execute(thr);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pool.shutdown();
        }
        return "";
    }

    /**
     * 加工同程接口返回值，改成客服容易理解
     * @param callresult
     * @return
     * @time 2014年12月29日 下午6:57:52
     * @author fiend
     */
    public String changeCallResult(String callresult) {
        if (callresult.equals("")) {
            return "出票接口异常";
        }
        if (callresult.contains("orderid 有误")) {
            return "非接口订单，请客服核对";
        }
        return callresult;
    }

    public static void main(String[] args) {
        String payaccount = "1,2,3";
        String resultstr = payaccount + ",";
        if (resultstr.indexOf("3,") >= 0) {
            resultstr = resultstr.replace("2,", "");
            resultstr = resultstr.substring(0, resultstr.lastIndexOf(","));
            System.out.println(resultstr);
        }
        //new JobUnionTradenum().Jobupdatetrade();
        //String par = "{'ordernum':'T15011322452510354','cmd':'seachliushuihao'}";
        //String infodata = SendPostandGet.submitPost("http://211.103.207.134:8082/ccs_paymodule/unionpay", par, "UTF-8")
        //        .toString();payremandtel
        //System.out.println(infodata);
    }
}
