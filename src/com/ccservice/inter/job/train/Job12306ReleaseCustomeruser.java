package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.train.thread.Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread;
import com.ccservice.inter.server.Server;

/**
 * 定时释放发车后的账号
 * 
 * @time 2015年3月13日 下午3:51:45
 * @author chendong
 */
public class Job12306ReleaseCustomeruser extends TrainSupplyMethod implements Job {

    public static void main(String[] args) {
        Job12306ReleaseCustomeruser job12306Verification = new Job12306ReleaseCustomeruser();
        job12306Verification.reLogintocheckHeyan();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            reLogintocheckHeyan();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reLogintocheckHeyan() {
        String serviceurl = "http://121.40.62.200:40000/cn_service/service/";
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //        isystemservice = Server.getInstance().getSystemService();
        List customerusers = isystemservice
                .findMapResultByProcedure("sp_CustomerUser_Job12306ReleaseCustomeruser_reLogintocheckHeyan");
        for (int i = 0; i < customerusers.size(); i++) {
            Map map = (Map) customerusers.get(i);
            String loginname = map.get("C_LOGINNAME").toString();
            String password = map.get("C_LOGPASSWORD").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            RepServerBean rep = getrepurl();
            String url = rep.getUrl();
            //            url = "http://121.40.237.115:8080/Reptile/traininit";
            //            new Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread(loginname, password, "2015-06-29", id, url,
            //                    isystemservice).start();
        }
    }

    public RepServerBean getrepurl() {
        //        String serviceurl = PropertyUtil.getValue("reg_serviceurl_getrepurl", "train.properties");
        //        String serviceurl = "http://121.40.174.4:39101/cn_service/service/";
        String serviceurl = "http://localhost:9001/cn_service/service/";
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //        List list = Server.getInstance().getSystemService().findMapResultByProcedure("sp_RepServer_GetOneRep");
        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;

    }
}
