package com.ccservice.inter.job.train.rule;

import java.sql.Timestamp;
import java.util.Calendar;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.qunar.util.ExceptionUtil;

public class Train3151Rule {
    /**
     * 是否满足时间规则
     * 22：30分之前可以
     * @return
     * @time 2017年3月31日 上午11:21:10
     * @author fiend
     */
    private static boolean satisfyTimeRule() {
        boolean satisfy = false;
        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < 22
                || (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 22 && Calendar.getInstance().get(
                        Calendar.MINUTE) < 30)) {
            satisfy = true;
        }
        return satisfy;
    }

    public static void main(String[] args) {
        System.out.println(satisfyTimeRule());
    }

    /**
     * 是否满足接口类型规则
     * 
     * @param interfacetype
     * @return
     * @time 2017年3月31日 上午11:26:24
     * @author fiend
     */
    private static boolean satisfyInterfaceTypeRule(int interfacetype) {
        return TrainInterfaceMethod.TAOBAO == interfacetype;
    }

    /**
     * 是否满足3151规则
     * 
     * @param interfacetype
     * @return
     * @time 2017年3月31日 上午11:27:37
     * @author fiend
     */
    public static boolean satisfyRule(Trainorder trainorder) {
        return satisfyInterfaceTypeRule(trainorder.getInterfacetype()) && satisfyTimeRule()
                && satisfyTimeOutRule(trainorder);
    }

    /**
     * 3151时间是否充足
     * 
     * @param trainorder
     * @return
     * @time 2017年3月31日 上午11:41:28
     * @author fiend
     */
    private static boolean satisfyTimeOutRule(Trainorder trainorder) {
        //获取订单超时时间
        Timestamp ordertimeout = trainorder.getOrdertimeout();
        long timeOutTime = System.currentTimeMillis();
        try {
            //如果DB没有，就拿订单创建时间+30分钟
            if (ordertimeout == null) {
                timeOutTime = trainorder.getCreatetime().getTime() + 30 * 60 * 1000;
            }
            else {
                timeOutTime = ordertimeout.getTime();
            }
        }
        catch (Exception e) {
            //如果除了异常，就不走3151
            ExceptionUtil.writelogByException("Train3151Rule_satisfyTimeOutRule_Exception", e, trainorder.getId() + "");
            return false;
        }
        //返回【超时时间-当前时间>17分钟】
        return timeOutTime - System.currentTimeMillis() > 17 * 60 * 1000;
    }
}
