package com.ccservice.inter.job.tcreport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 同程新对账模式更新
 * @author zcn
 * @version 创建时间：2015年11月2日 下午4:32:00
 */
public class JobTongChengReport implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        uploadjob();
    }

    /**
     * 上传job
     */
    private void uploadjob() {
        // 控制开关的url
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -2);
        String todayday = sdf.format(cal.getTime());//今天
        while (true) {
            try {
                execute1(todayday);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        long t1 = System.currentTimeMillis();
        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        long t2 = System.currentTimeMillis();
        System.out.println(t2 - t1);
    }

    private void execute1(String rebatetime) {
        TongChengReport tongChengReport = new TongChengReport();
        try {
            tongChengReport.uploadData(rebatetime);//上传数据
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("JobTongChengReport", "JobTongChengReportTestGroup",
                JobTongChengReport.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobTongChengReportTest", "JobTongChengReportTestGroup", expr);// 触发器名，触发器组名       
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

}
