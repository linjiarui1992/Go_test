package com.ccservice.b2b2c.bus;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

import net.sf.json.JSONObject;

public class BusStationWrapper {
    public static void main(String[] args) throws Exception {

        parseSpotData();
    }

    public static void parseSpotData() throws Exception {
        ISystemService service = Server.getInstance().getSystemService();
        String sql = "select * from BusCityAsso order by Id asc";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 76000; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String fromcityname = map.get("FromCityName").toString();
            String tocityname = map.get("ToCityName").toString();
            Date dt = new Date();
            SimpleDateFormat matter1 = new SimpleDateFormat("yyyy-MM-dd");
            String date = (matter1.format(dt));
            String url = "http://bus.ctrip.com/busList.html?from=" + URLEncoder.encode(fromcityname, "UTF-8") + "&to="
                    + URLEncoder.encode(tocityname, "UTF-8") + "&date=" + date;
            String buf = Ctriputil.Geturl(url);
            //        System.out.println(buf);
            if (buf.contains("eval('(' + '") && buf.contains("' + ')');")) {
                buf = buf.substring(buf.indexOf("eval('(' + '"), buf.lastIndexOf("' + ')');"));
                //        System.out.println(buf);
                buf = buf.replace("eval('(' + '", "");
                WriteLog.write("携程车站抓取", buf);
                JSONObject result = JSONObject.fromObject(buf);
                //System.out.println(buf);
                Set data = result.keySet();
                Iterator it = data.iterator();
                while (it.hasNext()) {
                    String key = (String) it.next();
                    JSONObject station = result.getJSONObject(key);
                    String stationid = station.getString("station_id");
                    String stationname = station.getString("station_name");
                    String stationfullname = station.getString("station_full_name");
                    String stationpinyin = station.getString("station_pinyin");
                    String cityname = station.getString("city_name");
                    String address = station.getString("address");
                    String phonenumber = station.getString("phone_number");
                    String fetchhelp = station.getString("fetch_ticket_help");
                    String returnhelp = station.getString("return_change_help");
                    String coordinatex = station.getString("coordinate_x");
                    String coordinatey = station.getString("coordinate_y");
                    String presaledays = station.getString("presale_days");
                    String startbookdays = station.getString("start_booking_days");
                    String fixpresaledate = station.getString("fixed_presale_date");
                    String passengertype = station.getString("support_passenger_types");
                    String simplify = station.getString("simplify");
                    List spotstate = new ArrayList();
                    spotstate = service.findMapResultByProcedure("sp_Insert_Bus_Spot '" + stationid + "','"
                            + stationname + "','" + stationfullname + "','" + stationpinyin + "','" + cityname + "','"
                            + address + "','" + phonenumber + "','" + fetchhelp + "','" + returnhelp + "','"
                            + coordinatex + "','" + coordinatey + "','" + presaledays + "','" + startbookdays + "','"
                            + fixpresaledate + "','" + passengertype + "','" + simplify + "'");
                }
            }
        }
    }
}
