/**
 * 
 */
package com.ccservice.b2b2c.updaterankjob;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.offline.util.DistributionUtil;

/**
 * 白天未分配订单JOB
 * @time 2017年11月28日 下午19:14:29
 * @author zpy
 */
public class JobDayTimeOrderDistribution implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        execute();
    }

    public static void main(String[] args) {
        try {
            //            startScheduler("0 * 7-21 * * ?");
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 
     * @time 2017年11月28日 下午2:16:52
     * @author zpy
     */
    private void execute() {
        long l1 = System.currentTimeMillis();
        WriteLog.write("JobDayTimeOrderDistribution__expr", "l1" + l1 + ",白天未分配定单JOB单次Start");
        try {
            //取出所有白天未分配定单
            String sql = "SELECT DISTINCT m.ADDRESS,t.DepartTime FROM TrainOrderOffline o WITH (NOLOCK) join TrainTicketOffline t with(nolock) on t.OrderId=o.id join mailaddress m with(nolock) on m.orderid = o.id WHERE o.AgentId = 2 AND o.OrderStatus = 1 AND o.lockedStatus = 0 AND o.CreateTime BETWEEN DATEADD(s, - 60, getdate()) AND getdate()";
            DataTable list = DBHelperOffline.GetDataTable(sql, null);
            //判断夜间单表中是否有数据？
            if (list.GetRow().size() > 0) {
                WriteLog.write("JobDayTimeOrderDistribution__expr",
                        "l1" + l1 + ",白天未分配定单JOB当次数量=" + list.GetRow().size());
                DistributionUtil distributionUtil = new DistributionUtil();
                for (int i = 0; i < list.GetRow().size(); i++) {
                    //----拿到夜间单所需的邮寄信息和发车时间
                    //                Map map = (Map) list2.get(i);
                    DataRow map = list.GetRow().get(i);
                    String orderId = map.GetColumnString("id");//订单id
                    String departTime = map.GetColumnString("DepartTime");//发车时间
                    String address = map.GetColumnString("ADDRESS");//邮寄地址

                    //----调用分单逻辑方法获得重新分配的代售点id
                    String agentId = distributionUtil.distribution(l1, address, departTime, 1);//白天分单逻辑type=1
                    if (!agentId.equals(2)) {
                        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String now = sdfTime.format(new Date());

                        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderId
                                + ",@ProviderAgentid=" + agentId + ",@DistributionTime='" + now
                                + "',@ResponseTime='',@DealResult=4,@RefundReason=0,@RefundReasonStr='白天未分配定单自动分配'";
                        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表重新分配存储过程", procedureRecord);
                        DBHelperOffline.getResultByProc(procedureRecord, null);

                        //----修改仓库种的代售点id
                        String updateAgent = "update TrainOrderOffline set agentid=" + agentId + " where id=" + orderId
                                + " and orderstatus=1 ";
                        WriteLog.write("JobDayTimeOrderDistribution__expr",
                                "l1" + l1 + ",updateGradeSql=" + updateAgent);
                        DBHelperOffline.UpdateData(updateAgent);
                    }
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("JobDayTimeOrderDistribution__expr", "l1" + l1 + ",Exception!!" + e.toString());
            e.printStackTrace();
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("JobDayTimeOrderDistribution", "JobDayTimeOrderDistributionGroup",
                JobDayTimeOrderDistribution.class);// 任务名，任务组名，任务执行类
        WriteLog.write("JobDayTimeOrderDistribution__expr", "expr=" + expr);
        CronTrigger trigger = new CronTrigger("JobDayTimeOrderDistribution", "JobDayTimeOrderDistributionGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
