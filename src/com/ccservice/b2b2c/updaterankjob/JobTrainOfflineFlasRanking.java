package com.ccservice.b2b2c.updaterankjob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;
public class JobTrainOfflineFlasRanking implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//        Date as = new Date(new Date().getTime()-24*60*60*1000);
        Date as = new Date();
        String dates=sdf.format(as);
        String sql1="select max(avgAllTime) as maxtime,MAX(finishAvg) as maxavg from TrainOrderOfflineStatistics where staticDate='"+dates+"'";
        List list1 = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        Map map1=(Map)list1.get(0);
        double maxtime=Double.parseDouble(map1.get("maxtime").toString());
        double maxavg=Double.parseDouble(map1.get("maxavg").toString());
        String sql2="select * from TrainOrderOfflineStatistics where staticDate='"+dates+"'";
        List list2 = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
        Map maprank=new TreeMap();
        for(int i=0;i<list2.size();i++){
            Map map2=(Map)list2.get(i);
            double timeeff=(Double.parseDouble(map2.get("avgAllTime").toString())/maxtime);
            double avgeff=1-(Double.parseDouble(map2.get("finishAvg").toString())/maxavg);
            maprank.put(timeeff*0.7+avgeff*0.3, map2.get("agentId").toString());
        }
        Set<Double> keys = maprank.keySet();
        Iterator<Double> it = keys.iterator();
        int k=0;
        while(it.hasNext()) {
            k++;
            double key = it.next();
            WriteLog.write("---归一算法"+dates, "k="+k+"-->"+key + "-->" + maprank.get(key));
//            System.out.println(key + "-->" + maprank.get(key));
            String updaterank="update TrainOrderOfflineStatistics set ranking=" +k+" where agentid="+maprank.get(key)+" and STATicdate='"+dates+"'";
            WriteLog.write("updaterank_sql_"+dates, "updaterank="+updaterank);
            Server.getInstance().getSystemService().excuteAdvertisementBySql(updaterank);
        }
    
        
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        System.out.println("开始UpdateRanking_StartJobTrainOfflineFlasRanking");
        JobDetail jobDetail = new JobDetail("JobTrainOfflineFlasRanking", "JobTrainOfflineFlasRankingGroup",
                JobTrainOfflineFlasRanking.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobTrainOfflineFlasRanking","JobTrainOfflineFlasRankingGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
}
