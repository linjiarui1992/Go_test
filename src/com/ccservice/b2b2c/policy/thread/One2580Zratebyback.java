package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Method12580;
import com.ccservice.b2b2c.policy.SupplyMethod;

/**
 * 12580从数据库取政策
 * @author 栋 2013-10-29 9:57:39
 *
 */
public class One2580Zratebyback extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public One2580Zratebyback() {
    }

    public One2580Zratebyback(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> One2580Zrates = new ArrayList<Zrate>();
        try {
            //			int tempint = new Random().nextInt(1000);
            String url = getSysconfigString("12580zrateurl");
            One2580Zrates = Method12580.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin, url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return One2580Zrates;
    }
}
