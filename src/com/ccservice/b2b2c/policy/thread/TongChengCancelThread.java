package com.ccservice.b2b2c.policy.thread;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service12306.AccountSystem;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.cancel.bean.TomasCancelResultBean;
import com.ccservice.cancel.inter.IAccountLogin;
import com.ccservice.cancel.inter.IFreshCookie;
import com.ccservice.cancel.inter.IHttpSend;
import com.ccservice.cancel.logic.TomasCancelLogic;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.Account12306Util;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
import com.ccservice.train.account.CookieLogic;

public class TongChengCancelThread extends TrainSupplyMethod implements Runnable {

    //    private static final Logger logger = Logger.getLogger(TongChengCancelThread.class);

    Trainorder order;

    Customeruser customeruser;

    private boolean freeAccount;

    //    String TongChengCallBackURl = "tctraincallback.tc.hangtian123.net";

    public TongChengCancelThread(Trainorder trainorder, Customeruser customeruser, boolean freeAccount) {
        this.order = trainorder;
        this.freeAccount = freeAccount;
        //外层代码会修改Customeruser的值，防止受影响，用JSON转换一下，不可调整！！！
        String temp = JSONObject.toJSONString(customeruser);
        this.customeruser = JSONObject.parseObject(temp, Customeruser.class);
    }

    @Override
    public void run() {
        JSONObject retobj = new JSONObject();
        boolean cancelTrue = false;//是否在12306成功取消
        boolean accountNoLogin = false;//账号未登录
        //日志
        WriteLog.write(
                "t同程火车票接口_4.7取消火车票订单",
                +order.getId() + ">>>>>" + order.getExtnumber() + ">>>>>"
                        + (customeruser == null ? "" : customeruser.getLoginname()) + ">>>>>>>ordertype:"
                        + order.getOrdertype());
        //账户存在、电子单号存在
        //此逻辑不可修改，if内部已释放账号，外部调用了此类，注意账号释放问题！！！
        if (GoCancelOrder(order, customeruser)) {
            //请求12306
            String url = "";
            String result = "";
            try {
                RepServerBean rep = RepServerUtil.getRepServer(customeruser, false);
                url = rep.getUrl();
                try {
                    final Customeruser userf = customeruser;
                    TomasCancelResultBean tomasCancelResultBean = new TomasCancelLogic(new IHttpSend() {
                        public String sendPost(String url, String param) {
                            return SendPostandGet.submitPost(url, param, "UTF-8").toString();
                        }
                    }, new IFreshCookie() {
                        public void freshCookie(String result) {
                            CookieLogic.getInstance().refresh(result, userf);
                        }
                    }, new IAccountLogin() {
                        public boolean enLogin(String result) {
                            return Account12306Util.accountNoLogin(result, userf);
                        }
                    }).cancel(url, userf.getCardnunber(), order.getExtnumber(), order.getId() + "",
                            new TrainSupplyMethod().JoinCommonAccountInfo(customeruser, rep), false, "");
                    //rep取消结果
                    result = tomasCancelResultBean.getResult();
                    //rep是否取消成功
                    cancelTrue = tomasCancelResultBean.isCancelFlag();
                    //rep返回结果是否表面用户掉线
                    accountNoLogin = !tomasCancelResultBean.isLoginFlag();
                }
                catch (Exception e) {
                    ExceptionUtil.writelogByException("tc同程取消订单_ERROR", e);
                }
                finally {
                    WriteLog.write("同程取消订单result", "result:" + result);
                }
                if (cancelTrue) {
                    String sql = "Update T_Trainorder set C_ORDERSTATUS=8 where ID=" + order.getId();
                    try {
                        int i1 = Server.getInstance().getSystemService().excuteGiftBySql(sql);
                        WriteLog.write("同程订单取消成功并修改数据库", "sql执行结果--->" + i1);
                    }
                    catch (Exception e) {
                        WriteLog.write("tc同程取消订单_ERROR", order.getId() + "");
                        ExceptionUtil.writelogByException("tc同程取消订单_ERROR", e);
                    }
                }
            }
            catch (Exception e) {
                result += ">>>>>Exception>>>>>" + e.getMessage();
            }
            finally {
                WriteLog.write("t同程火车票接口_4.7取消火车票订单", order.getId() + ">>>>>REP服务器地址>>>>>" + url + ">>>>>REP返回>>>>>"
                        + result);
                //要释放
                if (freeAccount) {
                    //释放账号
                    if (cancelTrue) {
                        freeCustomeruser(customeruser, AccountSystem.FreeNoCare, AccountSystem.OneFree,
                                AccountSystem.OneCancel, AccountSystem.NullDepartTime);
                    }
                    else {
                        freeCustomeruser(customeruser, accountNoLogin ? AccountSystem.FreeNoLogin
                                : AccountSystem.FreeNoCare, AccountSystem.OneFree, AccountSystem.ZeroCancel,
                                AccountSystem.NullDepartTime);
                    }
                }
            }
        }
        try {
            String sql1 = "update T_TRAINORDER set C_SUPPLYPRICE=0,C_SUPPLYPAYWAY=100,C_SUPPLYTRADENO=''"
                    + ",C_EXTNUMBER='',C_CHANGESUPPLYTRADENO='',C_ORDERSTATUS=" + Trainorder.CANCLED + ",C_STATE12306="
                    + Trainorder.ORDERFALSE + " WHERE ID=" + order.getId();
            try {
                int i1 = Server.getInstance().getSystemService().excuteGiftBySql(sql1);
                WriteLog.write("tc同程取消订单", "订单号:" + order.getId() + ":修改数据库:" + i1 + ":" + sql1);
            }
            catch (Exception e) {
                WriteLog.write("tc同程取消订单_ERROR", order.getId() + "");
                ExceptionUtil.writelogByException("tc同程取消订单_ERROR", e);
            }
            //日志
            Trainorderrc rz = new Trainorderrc();
            rz.setYwtype(1);
            rz.setCreateuser("系统接口");
            rz.setOrderid(order.getId());
            rz.setStatus(Trainorder.CANCLED);
            rz.setContent("接口取消订单,12306取消<span style='color:red;'>成功</span>,交易关闭。");
            Server.getInstance().getTrainService().createTrainorderrc(rz);
            //文件日志
            WriteLog.write("tc同程取消订单", "订单号:" + order.getId() + ":取消订单,消除订单支付信息.");
        }
        catch (Exception e) {
            System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + " tc接口取消订单,订单ID:" + order.getId() + ","
                    + e.getMessage());
        }
        retobj.put("jiekouorderno", order.getQunarOrdernumber());
        retobj.put("method", "cancelCallback");
        String TongChengCallBackURl = PropertyUtil.getValue("tcTrainCallBack", "train.properties");
        String result = SendPostandGet.submitPost(TongChengCallBackURl, retobj.toJSONString(), "UTF-8").toString();
        WriteLog.write("同程取消订单Thread", "TongChengCancelThread" + result);
    }

    /**
     * 根据查到的map信息获取value
     * 
     * @param key
     * @time 2015年1月22日 下午1:08:54
     * @author chendong
     */
    public String gettrainorderinfodatabyMapkey(Map map, String key) {
        String value = "";
        if (map.get(key) != null) {
            try {
                value = map.get(key).toString();
            }
            catch (Exception e) {
            }
        }
        return value;
    }

    /**
     * 根据订单id获取 一些信息
     * 
     * @param trainorderid
     * @return
     * @time 2015年1月22日 下午1:05:36
     * @author chendong
     */
    public Map getTrainorderstatus(Long trainorderid) {
        Map map = new HashMap();
        String sql = "SELECT C_ORDERSTATUS,C_CONTACTUSER,C_QUNARORDERNUMBER,C_ORDERNUMBER,"
                + "C_TOTALPRICE,C_STATE12306,C_EXTNUMBER,C_SUPPLYACCOUNT,ordertype from T_TRAINORDER with(nolock) where ID="
                + trainorderid;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list.size() > 0) {
            map = (Map) list.get(0);
        }
        return map;
    }

}
