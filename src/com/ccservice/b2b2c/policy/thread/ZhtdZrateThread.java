package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ZongHengTianDiMethod;

/**
 * 
 * @author 贾建磊
 * 
 */
public class ZhtdZrateThread implements Callable<List<Zrate>> {

	private Orderinfo order;

	public ZhtdZrateThread() {
	}

	public ZhtdZrateThread(Orderinfo order) {
		this.order = order;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> zhtdZrates = new ArrayList<Zrate>();
		try {
			zhtdZrates = ZongHengTianDiMethod.getZrateByPnr(order.getPnr());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return zhtdZrates;
	}
}
