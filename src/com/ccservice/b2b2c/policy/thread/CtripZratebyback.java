package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.CtripMethod;

/**
 * 携程从数据库取政策
 * @author 栋 2013-10-29 9:57:39
 *
 */
public class CtripZratebyback implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    String url;

    public CtripZratebyback() {
    }

    public CtripZratebyback(String scity, String ecity, String sdate, String flightnumber, String cabin, String url) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
        this.url = url;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> One2580Zrates = new ArrayList<Zrate>();
        try {
            //			String url = SupplyMethod.getSysconfigString("ctripzrateurl");
            One2580Zrates = CtripMethod.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin, this.url);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return One2580Zrates;
    }
}
