package com.ccservice.b2b2c.policy.thread;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.atom.component.QunarBestBook;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.QunarBestDataV2;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.b2b2c.policy.ben.TianQubook;
import com.ccservice.inter.job.WriteLog;

/**
 * 
 * 
 * @time 2014年10月23日 上午10:44:55
 * @author wzc
 * 去哪查询政策回调
 */
public class QunarBestDataV2ZrateCallback implements Callable<List<Zrate>> {
    static QunarBestBook qunar = new QunarBestBook();

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public QunarBestDataV2ZrateCallback(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    public QunarBestDataV2ZrateCallback(Orderinfo order, List<Segmentinfo> listSinfo, List<Passenger> listPassenger) {
        Segmentinfo sinfo = listSinfo.get(0);
        this.scity = sinfo.getStartairport();
        this.ecity = sinfo.getEndairport();
        if (sinfo.getDepartureDate() == null) {
            this.sdate = new SimpleDateFormat("yyyy-MM-dd").format(sinfo.getDeparttime());
        }
        else {
            this.sdate = sinfo.getDepartureDate();
        }
        this.flightnumber = sinfo.getFlightnumber();
        this.cabin = sinfo.getCabincode();
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> qunarzrates = new ArrayList<Zrate>();
        try {
            Long t = System.currentTimeMillis();
            WriteLog.write("FindZrateByquanrv2_time", this.getClass().getSimpleName(), t + "");
            qunarzrates = QunarBestDataV2.getQunarFlightinfozrate(scity, ecity, sdate, flightnumber, cabin);
            t = System.currentTimeMillis() - t;
            WriteLog.write("FindZrateByquanrv2_time", this.getClass().getSimpleName(), t + "");
        }
        catch (Exception e) {
        }
        return qunarzrates;
    }
}
