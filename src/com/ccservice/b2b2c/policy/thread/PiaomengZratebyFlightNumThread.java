package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.Piaomeng;

/**
 * 
 * @author 栋2013-6-28 19:29:14
 *
 */
public class PiaomengZratebyFlightNumThread implements Callable<List<Zrate>> {

	String scity;
	String ecity;
	String sdate;
	String flightnumber;
	String cabin;

	public PiaomengZratebyFlightNumThread() {
	}

	public PiaomengZratebyFlightNumThread(String scity, String ecity,
			String sdate, String flightnumber, String cabin) {
		this.scity = scity;
		this.ecity = ecity;
		this.sdate = sdate;
		this.flightnumber = flightnumber;
		this.cabin = cabin;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> piaomengzrates = new ArrayList<Zrate>();
		try {
			piaomengzrates = Piaomeng.getZrateByFlightNumber(scity, ecity,
					sdate, flightnumber, cabin);
		} catch (Exception e) {
		}
		return piaomengzrates;
	}
}
