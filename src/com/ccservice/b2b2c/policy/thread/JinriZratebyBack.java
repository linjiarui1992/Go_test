package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SupplyMethod;
import com.ccservice.inter.job.WriteLog;

public class JinriZratebyBack extends SupplyMethod implements Callable<List<Zrate>> {

    String scity;

    String ecity;

    String sdate;

    String flightnumber;

    String cabin;

    public JinriZratebyBack() {
    }

    public JinriZratebyBack(String scity, String ecity, String sdate, String flightnumber, String cabin) {
        this.scity = scity;
        this.ecity = ecity;
        this.sdate = sdate;
        this.flightnumber = flightnumber;
        this.cabin = cabin;
    }

    @Override
    public List<Zrate> call() throws Exception {
        List<Zrate> jinriZrates = new ArrayList<Zrate>();
        String url = getSysconfigString("jinrizrateurl");
        Long t = System.currentTimeMillis();
        int tempRandom = new Random().nextInt(1000);
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), tempRandom + ":" + t + "");
        if (flightnumber.toUpperCase().indexOf("CZ") >= 0 && "Z".equals(cabin.toUpperCase())) {
        }
        else {
            //            jinriZrates = JinriMethodBack.getZrateByFlightNumber(scity, ecity, sdate, flightnumber, cabin, url);
            jinriZrates = getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
            //				jinriZrates = SupplyMethod.getZrateByFlightNumberbyDBusewhere(scity, ecity, sdate, flightnumber, cabin, url);
        }
        t = System.currentTimeMillis() - t;
        WriteLog.write("FindZrateByFlightNumber_time", this.getClass().getSimpleName(), tempRandom + ":" + t + ":"
                + url);
        url = null;
        t = null;
        return jinriZrates;
    }
}
