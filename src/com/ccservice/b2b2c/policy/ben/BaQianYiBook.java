package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class BaQianYiBook {
	// 以下是8000翼接口需要参数
	private String username8000 = "";
	private String password8000 = "";
	private String payAccount = "";
	private String staus="0";// 接口是否启用 1,启用 0,禁用

	public BaQianYiBook() {
		List<Eaccount> listEaccount8000yi = new ArrayList<Eaccount>();
		Eaccount e = Server.getInstance().getSystemService().findEaccount(3);
		if (e!=null&&e.getName() != null) {
			listEaccount8000yi.add(e);
		}
		if (listEaccount8000yi.size() > 0) {
			username8000 = listEaccount8000yi.get(0).getUsername();
			password8000 = listEaccount8000yi.get(0).getPassword();
			payAccount = listEaccount8000yi.get(0).getIspay();
			staus = listEaccount8000yi.get(0).getState();
		}else{
			staus="0";
			System.out.println("NO-8000yi");
		}
	}

	public String getUsername8000() {
		return username8000;
	}

	public void setUsername8000(String username8000) {
		this.username8000 = username8000;
	}

	public String getPassword8000() {
		return password8000;
	}

	public void setPassword8000(String password8000) {
		this.password8000 = password8000;
	}

	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

}
