package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author 贾建磊
 * 
 */
public class ZongHengTianDiBook {

    // 以下是纵横天地接口需要参数
    private String zhtdUsername = ""; // 纵横天地的用户名

    private String zhtdPassword = ""; // 纵横天地的密码

    private String zhtdClientid = ""; // 纵横天地的客户id

    private String zhtdStaus = "0"; // 纵横天地账号状态 0 禁用 1启用

    private String zhtdLinkName = ""; // 联系人姓名

    private String zhtdLinkMobile = "";// 联系人手机

    private String zhtdAUTHpnr = "";// pnr授权

    private String zhtdAccountOut = ""; // 支付宝付款账号

    // 政策获取接口账号信息
    private String zhtdGetPolicyUsername = "";// 纵横天地政策获取用户名

    private String zhtdGetPolicyPassword = "";// 纵横天地政策获取密码

    private String operatorID = "";//支付宝操作员id

    public ZongHengTianDiBook() {
        List<Eaccount> listEaccountZhtd = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(8);
        if (e != null && e.getName() != null) {
            listEaccountZhtd.add(e);
        }
        if (listEaccountZhtd.size() > 0) {
            zhtdUsername = listEaccountZhtd.get(0).getUsername();
            zhtdPassword = listEaccountZhtd.get(0).getPassword();
            zhtdClientid = listEaccountZhtd.get(0).getKeystr();
            zhtdStaus = listEaccountZhtd.get(0).getState();
            zhtdLinkName = listEaccountZhtd.get(0).getCreateuser();
            zhtdLinkMobile = listEaccountZhtd.get(0).getUrl();
            zhtdAUTHpnr = listEaccountZhtd.get(0).getEdesc();
            zhtdAccountOut = listEaccountZhtd.get(0).getPwd();
            zhtdGetPolicyUsername = listEaccountZhtd.get(0).getXiausername();
            zhtdGetPolicyPassword = listEaccountZhtd.get(0).getNourl();
            operatorID = listEaccountZhtd.get(0).getPayurl();
        }
        else {
            zhtdStaus = "0";
            System.out.println("NO-ZHTD");
        }
    }

    public String getZhtdUsername() {
        return zhtdUsername;
    }

    public void setZhtdUsername(String zhtdUsername) {
        this.zhtdUsername = zhtdUsername;
    }

    public String getZhtdPassword() {
        return zhtdPassword;
    }

    public void setZhtdPassword(String zhtdPassword) {
        this.zhtdPassword = zhtdPassword;
    }

    public String getZhtdClientid() {
        return zhtdClientid;
    }

    public void setZhtdClientid(String zhtdClientid) {
        this.zhtdClientid = zhtdClientid;
    }

    public String getZhtdStaus() {
        return zhtdStaus;
    }

    public void setZhtdStaus(String zhtdStaus) {
        this.zhtdStaus = zhtdStaus;
    }

    public String getZhtdLinkName() {
        return zhtdLinkName;
    }

    public void setZhtdLinkName(String zhtdLinkName) {
        this.zhtdLinkName = zhtdLinkName;
    }

    public String getZhtdLinkMobile() {
        return zhtdLinkMobile;
    }

    public void setZhtdLinkMobile(String zhtdLinkMobile) {
        this.zhtdLinkMobile = zhtdLinkMobile;
    }

    public String getZhtdAUTHpnr() {
        return zhtdAUTHpnr;
    }

    public void setZhtdAUTHpnr(String zhtdAUTHpnr) {
        this.zhtdAUTHpnr = zhtdAUTHpnr;
    }

    public String getZhtdAccountOut() {
        return zhtdAccountOut;
    }

    public void setZhtdAccountOut(String zhtdAccountOut) {
        this.zhtdAccountOut = zhtdAccountOut;
    }

    public String getZhtdGetPolicyUsername() {
        return zhtdGetPolicyUsername;
    }

    public void setZhtdGetPolicyUsername(String zhtdGetPolicyUsername) {
        this.zhtdGetPolicyUsername = zhtdGetPolicyUsername;
    }

    public String getZhtdGetPolicyPassword() {
        return zhtdGetPolicyPassword;
    }

    public void setZhtdGetPolicyPassword(String zhtdGetPolicyPassword) {
        this.zhtdGetPolicyPassword = zhtdGetPolicyPassword;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }
}
