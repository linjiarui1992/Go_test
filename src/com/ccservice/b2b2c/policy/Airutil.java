package com.ccservice.b2b2c.policy;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub;
import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub.CreateOrderNewByCHD;
import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub.OrderRGIInfo;
import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub.ReturnTicekNo;
import net._8000yi.websvr.newply.webinterface.plyintefaceservice_asmx.PlyIntefaceServiceStub;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 8000yi接口helper类，用于创建外部订单，查询最优政，获取实时政策等一些列工作...
 * 
 * @author hanmh
 * 
 */
public class Airutil extends SupplyMethod {
    final private static BaQianYiBook baQianYiBook = new BaQianYiBook();

    final private static String name = baQianYiBook.getUsername8000();

    final private static String pwd = baQianYiBook.getPassword8000();

    final private static DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");

    final private static DateFormat fromat2 = new SimpleDateFormat("yyyy/MM/dd");

    final private Log log = LogFactory.getLog(Airutil.class);

    /**
     * 根据8000翼订单号查询票号
     * 
     * @param OrderNo
     * @param
     * @return
     * @throws Exception
     */
    public String GetTicktNoByOrderID(String OrderNo) {
        String result = "-1";
        String ticktNO = "";
        String passname = "";
        try {
            OrderServiceStub orderServiceStub = new OrderServiceStub();
            ReturnTicekNo returnTicekNo = new ReturnTicekNo();
            returnTicekNo.setName(name);
            returnTicekNo.setPwd(pwd);
            returnTicekNo.setOrderGuid(OrderNo);
            OrderServiceStub.ReturnTicekNoResponse ticekNoResponse = orderServiceStub.returnTicekNo(returnTicekNo);

            OMElement element = ticekNoResponse.getReturnTicekNoResult().getExtraElement();
            Iterator<OMElement> oneiterator = element.getChildElements();

            while (oneiterator.hasNext()) {
                OMElement twoome = oneiterator.next();
                Iterator<OMElement> twoiterator = twoome.getChildElements();
                while (twoiterator.hasNext()) {
                    OMElement threeome = twoiterator.next();
                    Iterator<OMElement> threeiterator = threeome.getChildElements();
                    while (threeiterator.hasNext()) {
                        OMElement rt = threeiterator.next();
                        String name = rt.getLocalName().toUpperCase();
                        if (name.equals("ERRINFO")) {
                            System.out.println(rt.getText());
                            return "-1";
                        }
                        if (name.equals("PSGERTICKETNO")) {
                            String ticktvalue = rt.getText();
                            ticktNO += ticktvalue + "|";

                        }
                        if (name.equals("PSGERNAME")) {
                            String passnamevalue = rt.getText();
                            passname += passnamevalue + "|";
                        }
                    }
                }
            }
            result = passname + "^" + ticktNO;
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 创建80000翼儿童订单
     * 
     * @param
     * @param PNR
     * @return
     * @throws RemoteException
     */
    public String CreateOrderNewByCHD(String PNR, String AdultOrderGuid) throws RemoteException {
        WriteLog.write("8000yi", "创建儿童订单:PNR:" + PNR);
        String value = null;
        OrderServiceStub orderServiceStub = new OrderServiceStub();

        CreateOrderNewByCHD createOrderNewByCHD = new CreateOrderNewByCHD();
        createOrderNewByCHD.setName(name);
        createOrderNewByCHD.setPwd(pwd);
        createOrderNewByCHD.setPnr(PNR.trim());
        if (AdultOrderGuid != null && AdultOrderGuid.length() > 0) {
            createOrderNewByCHD.setAdultOrderGuid(AdultOrderGuid);
        }
        OrderServiceStub.CreateOrderNewByCHDResponse response = orderServiceStub
                .createOrderNewByCHD(createOrderNewByCHD);
        OMElement element = response.getCreateOrderNewByCHDResult().getExtraElement();
        Iterator<OMElement> oneiterator = element.getChildElements();
        while (oneiterator.hasNext()) {
            OMElement twoome = oneiterator.next();
            Iterator<OMElement> twoiterator = twoome.getChildElements();
            while (twoiterator.hasNext()) {
                OMElement threeome = twoiterator.next();
                Iterator<OMElement> threeiterator = threeome.getChildElements();
                while (threeiterator.hasNext()) {
                    OMElement rt = threeiterator.next();
                    String name = rt.getLocalName().toUpperCase();
                    if (name.equals("ERRINFO")) {
                        System.out.println();
                        System.out.println(rt.getText());

                        WriteLog.write("8000yi", "创建儿童票ERROR:PNR:" + PNR + ",rt.getText():" + rt.getText());

                        break;
                    }

                    value = rt.getText();

                    if ("ORDERID".equals(name)) {

                        WriteLog.write("8000yi", "创建儿童票OK:ordernumber:" + value);

                        return value;

                    }
                    return value;

                }
            }

        }
        return value;
    }

    /**
     * 创建80000翼订单
     * 
     * @param pid
     * @param PNR
     * @return
     * @throws RemoteException
     */
    public String createEeightOrder(long zid, String PNR, String ty) throws RemoteException {
        // System.out.println("orderid:"+orderid);
        WriteLog.write("8000yi", "createOrder:zid:" + zid + "--PNE=" + PNR);
        System.out.println("zid:" + zid);
        System.out.println("PNR:" + PNR);
        // <NewDataSet xmlns="">
        // <Table diffgr:id="Table1" msdata:rowOrder="0">
        // <OrderID>I634528149178593750</OrderID>
        // <FromCode>PEK</FromCode>
        // <ToCode>TAO</ToCode>
        // <Passengers>VIC/WONG</Passengers>
        // <Flight>MU525,</Flight>
        // <DepartureTime>2011-10-07 09:55</DepartureTime>
        // <Voyage>北京-青岛,</Voyage>
        // <CGYongJin>35</CGYongJin>
        // <FanDian>6.3</FanDian>
        // <OrderDate>2011-09-28T13:55:00+08:00</OrderDate>
        // <PNR>HTSQDB</PNR>
        // <CGShiFu>570</CGShiFu>
        // <CWZongJia>665</CWZongJia>
        // <Pid>1756028</Pid>
        // <OStateName>新订单，等待支付</OStateName>
        // <OState>1</OState>
        // <OTime>2011-09-28T13:55:30.84+08:00</OTime>
        // <PassengersNum>1</PassengersNum>
        // <PType>B2B</PType>
        // <cabin>H</cabin>
        // <Notes>
        // undefined【警告�?本代理除了南航B2B政策可以改签之外，其他一律不作改签，如不接受，请不要选择�?
        // </Notes>
        // </Table>
        // </NewDataSet>
        String value = null;
        Zrate zrate = Server.getInstance().getAirService().findZrate(zid);
        // Orderinfo orderinfo=new Orderinfo();
        // orderinfo.setId(orderid);
        OrderServiceStub stub = new OrderServiceStub();
        OrderServiceStub.CreatOrderNew create = new OrderServiceStub.CreatOrderNew();
        create.setName(name);
        create.setPwd(pwd);
        create.setPid(zrate.getOutid());
        create.setPnr(PNR.trim());
        OrderServiceStub.CreatOrderNewResponse response = stub.creatOrderNew(create);
        OMElement element = response.getCreatOrderNewResult().getExtraElement();
        Iterator<OMElement> oneiterator = element.getChildElements();
        while (oneiterator.hasNext()) {
            OMElement twoome = oneiterator.next();
            Iterator<OMElement> twoiterator = twoome.getChildElements();
            while (twoiterator.hasNext()) {
                OMElement threeome = twoiterator.next();
                Iterator<OMElement> threeiterator = threeome.getChildElements();
                while (threeiterator.hasNext()) {
                    OMElement rt = threeiterator.next();
                    String name = rt.getLocalName().toUpperCase();
                    if (name.equals("ERRINFO")) {
                        System.out.println();
                        System.out.println(rt.getText());
                        if (ty.equals("1")) {

                            WriteLog.write("8000yi", "根据PNR匹配政策createOrderERROR:PNR:" + PNR + ",zid:" + zid + ",outid:"
                                    + zrate.getOutid() + ",rt.getText():" + rt.getText());
                        }
                        else {
                            WriteLog.write("8000yi", "没根据PNR匹配政策createOrderERROR:PNR:" + PNR + ",zid:" + zid
                                    + ",outid:" + zrate.getOutid() + ",rt.getText():" + rt.getText());
                        }
                        log.debug(rt.getText());
                        break;
                    }

                    value = rt.getText();

                    if ("ORDERID".equals(name)) {
                        if (ty.equals("1")) {
                            WriteLog.write("8000yi", "根据PNR匹配政策createOrderOK:ordernumber:" + value);
                        }
                        else {
                            WriteLog.write("8000yi", "没根据PNR匹配政策createOrderOK:ordernumber:" + value);
                        }
                        log.debug(value);
                        // orderinfo.setExtorderid(value);
                        // orderinfo.setExtorderstatus(0);
                        return value;

                    }

                }
            }

        }
        /*
         * try{ if(orderinfo.getExtorderid()==null){
         * orderinfo.setExtorderid("-1"); orderinfo.setExtorderstatus(-1); }
         * System.out.println("生成外部订单号为:"+orderinfo.getExtorderid());
         * orderinfo.setExtordercreatetime(new Timestamp(System
         * .currentTimeMillis()));
         * Server.getInstance().getAirService().updateOrderinfoIgnoreNull(orderinfo);
         * 
         * 
         * 
         * 
         * }catch(Exception e){ orderinfo.setExtorderid("-1");
         * orderinfo.setExtorderstatus(-1); orderinfo.setExtordercreatetime(new
         * Timestamp(System .currentTimeMillis()));
         * Server.getInstance().getAirService().updateOrderinfoIgnoreNull(orderinfo);
         * e.printStackTrace(); }
         */
        return value;
    }

    /**
     * 根据PNR获取最优政策
     * 
     * @param pnr
     * @param special
     *            0:普通政策：1特殊政策 *
     * @return
     * @throws RemoteException
     * @throws ParseException
     * @throws SQLException
     */
    public static List<Zrate> getBestZratebyPnr_Note(Orderinfo order, int special) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.SPbyPNR_Note service = new PlyIntefaceServiceStub.SPbyPNR_Note();
            service.setName(name);
            service.setPwd(pwd);
            service.setIsSpecial(special);
            service.setPnr(order.getPnr().trim());
            service.setStrPnrInfo(order.getUserRtInfo() == null ? "" : order.getUserRtInfo().replaceAll("", "").trim());
            WriteLog.write("getzratebypnr_8000yi", "根据PNR匹配政策请求的信息");
            WriteLog.write("getzratebypnr_8000yi", name + ":" + pwd + ":" + special + ":" + service.getPnr());
            WriteLog.write("getzratebypnr_8000yi", service.getStrPnrInfo());
            PlyIntefaceServiceStub.SPbyPNR_NoteResponse response = stub.sPbyPNR_Note(service);
            OMElement ontome = response.getSPbyPNR_NoteResult().getExtraElement();
            Iterator<OMElement> oneiterator = ontome.getChildElements();
            while (oneiterator.hasNext()) {
                OMElement twoome = oneiterator.next();
                Iterator<OMElement> twoiterator = twoome.getChildElements();
                while (twoiterator.hasNext()) {
                    OMElement threeome = twoiterator.next();
                    WriteLog.write("getzratebypnr_8000yi", "根据PNR匹配政策返回的信息:" + threeome.toString());
                    Iterator<OMElement> threeiterator = threeome.getChildElements();
                    Zrate newrate = new Zrate();
                    newrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    newrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    newrate.setAgentid(3l);
                    newrate.setIsenable(1);
                    newrate.setCreateuser("job");
                    while (threeiterator.hasNext()) {
                        OMElement rt = threeiterator.next();
                        String name1 = rt.getLocalName().toUpperCase();
                        if (name1.equals("ERRINFO")) {
                            break;
                        }
                        String value = rt.getText();
                        if (name1.equals("A1")) {//
                            newrate.setOutid(value);
                        }
                        else if (name1.equals("A2")) {// 出发城市三字码,多个城市用“/”分隔,当返回值为’All’时是代表所有出发城市

                            newrate.setDepartureport(value);
                        }
                        else if (name1.equals("A3")) { // 出发城市三字码，多个城市用“/”分隔,当返回值为’All’时是代表所有出发城市
                            newrate.setArrivalport(value);
                        }
                        else if (name1.equals("A4")) {// 航空公司二字码代码,如CA(表示国航)
                            newrate.setAircompanycode(value);
                        }
                        else if (name1.equals("A5")) { // 由航空公司根据航线所规定的航班号如：CA4307，多个航班用“/”分隔
                            if (value != null && value.length() > 0) {

                                newrate.setFlightnumber(value);
                            }
                            newrate.setType(1);
                        }
                        else if (name1.equals("A6")) {// 表示政策不包括的航班号，多个航班用“/”分隔
                            if (value != null && value.length() > 0) {
                                newrate.setWeeknum(value);
                            }
                            newrate.setType(2);
                        }
                        else if ("A7".equals(name1)) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
                            if (value != null && value.length() > 0) {

                                newrate.setVoyagetype(value);
                            }
                            else {
                                newrate.setVoyagetype("1");
                            }
                        }
                        else if ("A8".equals(name1)) {// 该条政策的返点,正确的返点是大于等于2.5小于50
                            newrate.setRatevalue(Float.parseFloat(value));
                        }
                        else if (name1.equals("A9")) {// 
                            newrate.setCabincode(value);
                        }
                        else if (name1.equals("A10")) {// 生效日期 A10
                            value = value.split(" ")[0];
                            try {
                                newrate.setBegindate(new Timestamp(fromat2.parse(value).getTime()));
                            }
                            catch (Exception e) {
                                // TODO: handle exception
                            }
                            //                            newrate.setBegindate(new Timestamp(fromat.parse(value.substring(0, 10)).getTime()));
                        }
                        else if (name1.equals("A11")) {// 终止日期 A11
                            value = value.split(" ")[0];
                            try {
                                // Datetime
                                newrate.setEnddate(new Timestamp(fromat2.parse(value).getTime()));
                            }
                            catch (Exception e) {
                                // TODO: handle exception
                            }
                            // Datetime
                            //                            newrate.setEnddate(new Timestamp(fromat.parse(value.substring(0, 10)).getTime()));
                        }
                        else if ("A12".equals(name1)) {// 供应商平时上下班时间
                            try {
                                // 08:00|23:59
                                String[] times = value.split("[|]");
                                newrate.setWorktime(times[0].replaceAll(":", ""));
                                newrate.setAfterworktime(times[1].replaceAll(":", ""));
                            }
                            catch (Exception ex) {
                            }
                        }
                        else if (name1.equals("A14")) {// "0"表示不是VIP政策,其余表示为VIP政策
                        }
                        else if (name1.equals("A16")) {// 政策类型 A16
                            if (value.equals("B2B")) {
                                newrate.setTickettype(2);
                            }
                            else {
                                newrate.setTickettype(1);
                            }
                        }
                        else if (name1.equals("A17")) {// 政策备注
                            newrate.setRemark(value);
                        }
                        else if (name1.equals("A18")) {// 供应商标识符

                        }
                        else if (name1.equals("A19")) {//在工作日内可以提交废票的时间段
                            newrate.setOnetofivewastetime(value.replaceAll("[|]", "-").replaceAll(":", ""));
                        }
                        else if (name1.equals("A20")) {//在周末休息日内可以提交废票的时间段
                            newrate.setWeekendwastetime(value.replaceAll("[|]", "-").replaceAll(":", ""));
                        }
                        else if (name1.equals("A21")) {// 政策的适用班期
                            newrate.setFlightnumber(value);
                        }
                        else if (name1.equals("A22")) {// 政策的适用班期
                            if (value.equals("1")) {
                                newrate.setGeneral(2L);
                                newrate.setIstype(2L);
                            }
                            else {
                                newrate.setGeneral(1L);
                                newrate.setIstype(1L);
                            }
                        }
                        else if (name1.equals("A23")) {// 效率
                            int speed = Integer.parseInt(value) / 60;
                            newrate.setSpeed(speed + "分钟");
                        }
                        //                        else if (name1.equals("A24")) {// 舱位
                        //                            newrate.setAgentcode(value);
                        //                        }
                        else if (name1.equals("A26")) {// 授权Office
                            newrate.setAgentcode(value);
                        }
                    }
                    newrate.setIsenable(1);
                    newrate.setUsertype("1");
                    newrate.setZtype("1");
                    if (newrate.getRatevalue() != null && newrate.getRatevalue() > 0 && newrate.getGeneral() != null) {
                        zrates.add(newrate);
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrates;
    }

    /**
     * 根据航班号查询政策
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @author chendong 2013-6-26 19:42:01
     * @return
     */
    public static List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin) {
        Long templ = System.currentTimeMillis();
        WriteLog.write("8000yi", "getZrateByFlightNumber:0:" + templ + ":" + scity + ":" + ecity + ":" + flightnumber
                + ":" + cabin);
        List<Zrate> zrates = new ArrayList<Zrate>();
        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.SPExactitude sPExactitude = new PlyIntefaceServiceStub.SPExactitude();
            sPExactitude.setName(name);
            sPExactitude.setPwd(pwd);
            sPExactitude.setFromCity(scity);
            sPExactitude.setTocity(ecity);
            sPExactitude.setAirco(flightnumber.substring(0, 2));
            sPExactitude.setAirNo(flightnumber.substring(2, flightnumber.length()));
            sPExactitude.setAirbunk(cabin);
            sPExactitude.setAirTime(sdate + " 00:00:00");
            PlyIntefaceServiceStub.SPExactitudeResponse response = stub.sPExactitude(sPExactitude);
            OMElement ontome = response.getSPExactitudeResult().getExtraElement();
            Iterator<OMElement> oneiterator = ontome.getChildElements();
            DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat fromat2 = new SimpleDateFormat("yyyy/MM/dd");
            while (oneiterator.hasNext()) {
                OMElement twoome = oneiterator.next();
                Iterator<OMElement> twoiterator = twoome.getChildElements();
                while (twoiterator.hasNext()) {
                    OMElement threeome = twoiterator.next();
                    //					WriteLog.write("8000yi", "getZrateByFlightNumber:1:"+templ+":"+ threeome.toString());
                    Iterator<OMElement> threeiterator = threeome.getChildElements();
                    Zrate newrate = new Zrate();
                    newrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
                    newrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    newrate.setAgentid(3l);
                    newrate.setTickettype(1);
                    newrate.setIsenable(1);
                    newrate.setCreateuser("job");
                    String A5 = "";
                    String A6 = "";
                    while (threeiterator.hasNext()) {
                        OMElement rt = threeiterator.next();
                        String name1 = rt.getLocalName().toUpperCase();
                        if (name1.equals("ERRINFO")) {
                            break;
                        }
                        String value = rt.getText();
                        if (name1.equals("A1")) {//
                            newrate.setOutid(value);
                        }
                        else if (name1.equals("A2")) {// 出发城市三字码,多个城市用“/”分隔,当返回值为’All’时是代表所有出发城市

                            newrate.setDepartureport(value);
                        }
                        else if (name1.equals("A3")) { // 出发城市三字码，多个城市用“/”分隔,当返回值为’All’时是代表所有出发城市
                            newrate.setArrivalport(value);
                        }
                        else if (name1.equals("A4")) {// 航空公司二字码代码,如CA(表示国航)
                            newrate.setAircompanycode(value);
                        }
                        else if (name1.equals("A5")) { // 由航空公司根据航线所规定的航班号如：CA4307，多个航班用“/”分隔
                            if (value != null && value.length() > 0) {
                                A5 = value;
                                newrate.setFlightnumber(value);
                            }
                            newrate.setType(1);
                        }
                        else if (name1.equals("A6")) {// 表示政策不包括的航班号，多个航班用“/”分隔
                            if (value != null && value.length() > 0) {
                                A6 = value;
                                newrate.setWeeknum(value);
                            }
                            newrate.setType(2);
                        }
                        else if ("A7".equals(name1)) {// 返回1表示单程，返回2表示往返，返回3表示单程往返
                            if (value != null && value.length() > 0) {

                                newrate.setVoyagetype(value);
                            }
                            else {
                                newrate.setVoyagetype("1");
                            }
                        }
                        else if ("A8".equals(name1)) {// 该条政策的返点,正确的返点是大于等于2.5小于50
                            newrate.setRatevalue(Float.parseFloat(value));
                        }
                        else if (name1.equals("A9")) {// 
                            newrate.setCabincode(value);
                        }
                        else if (name1.equals("A10")) {// 生效日期 A10
                            value = value.split(" ")[0];
                            try {
                                newrate.setBegindate(new Timestamp(fromat2.parse(value).getTime()));
                            }
                            catch (Exception e) {
                                // TODO: handle exception
                            }
                        }
                        else if (name1.equals("A11")) {// 终止日期 A11
                            value = value.split(" ")[0];
                            try {
                                // Datetime
                                newrate.setEnddate(new Timestamp(fromat2.parse(value).getTime()));
                            }
                            catch (Exception e) {
                                // TODO: handle exception
                            }
                        }
                        else if ("A12".equals(name1)) {// 供应商平时上下班时间
                            try {
                                // 08:00|23:59
                                String[] times = value.split("[|]");
                                newrate.setWorktime(times[0].replaceAll(":", ""));
                                newrate.setAfterworktime(times[1].replaceAll(":", ""));
                            }
                            catch (Exception ex) {
                            }
                        }
                        else if (name1.equals("A14")) {// "0"表示不是VIP政策,其余表示为VIP政策
                        }
                        else if (name1.equals("A16")) {// 政策类型 A16
                            // String
                            // 政策类型包括B2B或BSP
                            // 0b2b 1bsp
                            // 2b2c 3 all
                            if (value.toLowerCase().equals("bsp")) {
                                newrate.setTickettype(1);
                            }
                            else {
                                newrate.setTickettype(0);
                            }
                        }
                        else if (name1.equals("A17")) {// 政策备注
                            newrate.setRemark(value);
                        }
                        else if (name1.equals("A18")) {// 供应商标识符

                        }
                        else if (name1.equals("A19")) {//在工作日内可以提交废票的时间段
                            newrate.setOnetofivewastetime(value.replaceAll("[|]", "-").replaceAll(":", ""));
                        }
                        else if (name1.equals("A20")) {//在周末休息日内可以提交废票的时间段
                            newrate.setWeekendwastetime(value.replaceAll("[|]", "-").replaceAll(":", ""));
                        }
                        else if (name1.equals("A21")) {// 政策的适用班期
                            newrate.setFlightnumber(value);
                        }
                        else if (name1.equals("A22")) {// 政策的适用班期
                            if (value.equals("1")) {
                                newrate.setGeneral(2L);
                                newrate.setIstype(2L);
                            }
                            else {
                                newrate.setGeneral(1L);
                                newrate.setIstype(1L);
                            }
                        }
                        else if (name1.equals("A23")) {// 效率
                            int speed = Integer.parseInt(value) / 60;
                            newrate.setSpeed(speed + "");
                        }
                    }
                    newrate.setIsenable(1);
                    newrate.setUsertype("1");
                    newrate.setZtype("1");
                    if (newrate.getRatevalue() != null && newrate.getRatevalue() > 0 && newrate.getGeneral() != null
                            && checkFlightNo(flightnumber, A5, A6)) {
                        zrates.add(newrate);
                    }
                }
            }
        }
        catch (AxisFault e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("8000yi", "getZrateByFlightNumber:1:size:" + templ + ":" + zrates.size());
        return zrates;
    }

    /**
     * 判断是否有使用航班号和不适用航班号
     * @param flightnumber
     * @param A5	适用航班号
     * @param A6	不适用航班号
     * @return	如果政策适用返回true
     * @author chen
     */
    public static boolean checkFlightNo(String flightnumber, String A5, String A6) {
        flightnumber = flightnumber.substring(2, flightnumber.length());
        //如果A5不为空且长度大于0
        if (A5 != null && A5.trim().length() > 0) {
            //如果适用航班在这里面包含返回true否则返回false
            if (A5.indexOf(flightnumber) >= 0) {
                return true;
            }
            else {
                return false;
            }
        }
        //如果A6不为空且长度大于0
        if (A6 != null && A6.trim().length() > 0) {
            //如果不适用航班在这里面包含返回false否则返回true
            if (A5.indexOf(flightnumber) >= 0) {
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("0830|2300".replaceAll("[|]", "-"));

        DateFormat fromat2 = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date date = fromat2.parse("2014/4/3");
            System.out.println(fromat2.format(date));
        }
        catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //		Airutil util = new Airutil();
        //		String s = util.GetTicktNoByOrderID("I635223695022794790");
        //		util.getZrateByFlightNumber("PEK", "TNA", "2014-04-28", "SC1158", "Y");
    }

    /**
     * 获取政策实时信息
     * 
     * @param zid
     * @param outid
     * @param fromcity
     * @param endcity
     * @param aircode
     * @return
     * @throws RemoteException
     * @throws ParseException
     */
    public Zrate getCurrentZrate(long zid, String outid, String fromcity, String endcity, String aircode)
            throws RemoteException, ParseException {
        Zrate zrate = new Zrate();
        zrate.setId(zid);
        PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
        PlyIntefaceServiceStub.SPOne service = new PlyIntefaceServiceStub.SPOne();
        service.setName(name);
        service.setPwd(pwd);
        service.setPolicyId(Integer.valueOf(outid));
        service.setFromCity(fromcity);
        service.setToCity(endcity);
        service.setAirCo(aircode);
        PlyIntefaceServiceStub.SPOneResponse response = stub.sPOne(service);
        OMElement omelement = response.getSPOneResult().getExtraElement();
        Iterator<OMElement> oneiter = omelement.getChildElements();
        DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+08:00");
        while (oneiter.hasNext()) {
            OMElement twoome = oneiter.next();
            System.out.println(twoome.getQName());
            System.out.println(twoome.getLocalName());
            Iterator<OMElement> twoiter = twoome.getChildElements();
            while (twoiter.hasNext()) {
                OMElement threeome = twoiter.next();

                boolean enable = true;
                Iterator<OMElement> threeitr = threeome.getChildElements();
                while (threeitr.hasNext()) {
                    OMElement ome = threeitr.next();
                    String name = ome.getLocalName().toUpperCase();
                    if (name.equals("ERRINFO")) {
                        System.out.println("8000yi政策核对提取异常");
                        System.out.println(ome.getText());
                        break;
                    }
                    String value = ome.getText();
                    zrate.setModifytime(new Timestamp(System.currentTimeMillis()));
                    if (name.equals("A5")) {
                        if (value != null && value.length() > 0) {
                            zrate.setType(1);
                            zrate.setFlightnumber(value);
                        }
                    }
                    else if (name.equals("A6")) { // 
                        if (value != null && value.length() > 0) {
                            zrate.setType(2);
                            zrate.setWeeknum(value);
                        }
                    }
                    else if ("A7".equals(name)) {// 返回1表示单程，返�?表示�?��，返�?表示单程�?��
                        if (value != null && value.length() > 0) {

                            zrate.setVoyagetype(value);
                        }
                        else {
                            zrate.setVoyagetype("1");
                        }
                    }
                    else if (name.equals("A14")) {// 
                        if ("0".equals(value))
                            zrate.setGeneral(1l);
                        try {
                            zrate.setIstype(Long.parseLong(value));
                        }
                        catch (Exception exx) {

                        }
                    }
                    else if (name.equals("A16")) {//
                        if (value.toLowerCase().equals("bsp")) {
                            zrate.setTickettype(1);
                        }
                        else {
                            zrate.setTickettype(0);
                        }
                    }
                    else if (name.equals("A8")) {// 
                        zrate.setRatevalue(Float.parseFloat(value));
                    }
                    else if (name.equals("A9")) {// 
                        zrate.setCabincode(value);
                    }
                    else if (name.equals("A10")) {//
                        zrate.setBegindate(new Timestamp(fromat.parse(value).getTime()));
                    }
                    else if (name.equals("A11")) {// 
                        // 
                        zrate.setEnddate(new Timestamp(fromat.parse(value).getTime()));

                    }
                    else if (name.equals("A17")) {// 
                        zrate.setRemark(value);
                    }
                    else if ("A22".equals(name)) {
                        /*
                         * if (value != null) {
                         * System.out.println("8000翼政策状态=="+value); if
                         * ("可用".equals(value) || value == null || value == "") {
                         * zrate.setIsenable(1); enable = true; } else {
                         * zrate.setIsenable(0); enable = false; } }
                         */
                        zrate.setIsenable(1);
                    }
                    zrate.setUsertype("1");
                    zrate.setZtype("1");

                }
                // Server.getInstance().getAirService().updateZrateIgnoreNull(zrate);
            }

        }

        return Server.getInstance().getAirService().findZrate(zrate.getId());
    }

    public String orderRGIInfo(String Orderguid, String TicketNo, String type, String whyid) {
        WriteLog.write("8000yi", "申请退费票:Orderguid:" + Orderguid + ",TicketNo:" + TicketNo + ",type:" + type + ",whyid:"
                + whyid);
        if (Orderguid == null) {
            Orderguid = "I634605973358906250";
        }

        String result = "-1";

        try {
            OrderServiceStub orderServiceStub = new OrderServiceStub();
            OrderRGIInfo orderRGIInfo = new OrderRGIInfo();
            orderRGIInfo.setName(name);
            orderRGIInfo.setPwd(pwd);//
            orderRGIInfo.setOrderguid(Orderguid);// 外部订单号
            // //I634605973358906250
            orderRGIInfo.setTicketNo(TicketNo);// 票号
            orderRGIInfo.setWhyId(whyid);// 理由的ID
            orderRGIInfo.setType(type);// 只能是‘退票’或‘废票’
            OrderServiceStub.OrderRGIInfoResponse orderRGIInfoResponse = orderServiceStub.orderRGIInfo(orderRGIInfo);
            result = orderRGIInfoResponse.getOrderRGIInfoResult();
            WriteLog.write("8000yi", "申请退费票返回结果:Orderguid:" + Orderguid + ",result:" + result.toString());
            // 解析返回的结果
            // result="trueI11111111111";
            if (result != null && !result.equals("")) {
                // 如果返回正确信息可以通过|来划分字符串
                if (result.indexOf("true") != -1) {

                    String[] returnstr = result.split("ue");
                    if (returnstr[0].equals("tr")) {// 证明返回成功

                        // 获得新的订单号
                        String orderNo = returnstr[1];
                        System.out.println("orderNo=" + orderNo);
                        WriteLog.write("8000yi", "申请退费票OK:Orderguid:" + Orderguid + ",TicketNo:" + TicketNo + ",type:"
                                + type + ",whyid:" + whyid + ",result:" + result + ",newOrderNo:" + orderNo);

                        return orderNo;

                    }
                    else {// 调用失败
                        String message = result;
                        System.out.println(message);
                        WriteLog.write("8000yi", "申请退费票EREER:Orderguid:" + Orderguid + ",TicketNo:" + TicketNo
                                + ",type:" + type + ",whyid:" + whyid + ",message:" + message);

                        return "-1@" + message;
                    }

                }

            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("得到的结果是：" + result);
        return "-1@" + result;

    }

    public Orderinfo getOrder(long orderid) {
        return Server.getInstance().getAirService().findOrderinfo(orderid);
    }

}
