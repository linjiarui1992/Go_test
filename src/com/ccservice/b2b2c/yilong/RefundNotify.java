package com.ccservice.b2b2c.yilong;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
//退款成功通知
public class RefundNotify {
	public static void main1(String[] args) {
		String xml="<?xml version=\"1.0\" encoding=\"utf-8\"?><OrderRequest xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Authentication><ServiceName>order.refundNotify</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2015-07-28 10:52:44</TimeStamp><MessageIdentity>56c2d8c244c01d6b3b3e856532c5cb7d</MessageIdentity></Authentication><TrainOrderService><OrderNumber>JingyanT20150728100004</OrderNumber><OrderTid>24151539</OrderTid><RefundFee>14.50</RefundFee><RefundTime>2015-07-28 10:52:44</RefundTime><OrderTradeNo>24151539</OrderTradeNo></TrainOrderService></OrderRequest>";
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element TrainOrderService=root.element("TrainOrderService");
			String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
			String orderTid=TrainOrderService.elementText("OrderTid");//退款流水ID
			String refundFee=TrainOrderService.elementText("RefundFee");//退款金额
			String refundTime=TrainOrderService.elementText("RefundTime");//退款时间
			String orderTradeNo=TrainOrderService.elementText("OrderTradeNo");//退款流水ID
			System.out.println(orderNumber+"--"+orderTid+"--"+refundFee+"--"+refundTime+"--"+orderTradeNo);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		//退款成功Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.refundNotify");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText("JingyanT20150825100014");
		Element discription=orderResponse.addElement("Discription");
		discription.setText("信息,退款成功..");
		System.out.println(document.asXML());
	}
}
