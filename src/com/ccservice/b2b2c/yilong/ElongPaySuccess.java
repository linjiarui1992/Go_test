package com.ccservice.b2b2c.yilong;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.callback.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;


public class ElongPaySuccess {
	SimpleDateFormat sdf=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
	
	public static void main(String[] args) {
		String xml = Util.loadFile("D://abc.txt");
		System.out.println(xml);
		new ElongPaySuccess().paySuccess(xml);
	}
	public String paySuccess(String xml){
		String ordernum="";
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element TrainOrderService=root.element("TrainOrderService");
			String orderNumber=TrainOrderService.elementText("OrderNumber");//订单号
			ordernum=orderNumber;
			String payedPrice=TrainOrderService.elementText("PayedPrice");//支付价格
			String payTime=TrainOrderService.elementText("PayTime");//支付时间
			String payType=TrainOrderService.elementText("PayType");//支付类型
			String tradeNumber=TrainOrderService.elementText("tradeNumber");//交易号
			//response
			String sql="SELECT M.ADDRESS,O.Id,O.OrderNumberOnline,O.OrderPrice FROM TrainOrderOffline O,mailaddress M WHERE O.Id=M.ORDERID AND O.OrderNumberOnline='"+orderNumber+"'";
			List list=Server.getInstance().getSystemService().findMapResultBySql(sql, null);
			if(list.size()>0){
				Map map=(Map)list.get(0);
				double payPrice=Double.parseDouble(payedPrice);
				double orderprice=Double.parseDouble(map.get("OrderPrice").toString());
				if(payPrice>=orderprice){
					//锁订单
//					String result=new HthyLockOrder().lockOrder(orderNumber);
//					if("SUCCESS".equals(result)){
//						String agentid=distribution1(map.get("ADDRESS").toString());
						String agentid=distribution2(map.get("ADDRESS").toString());
						String delivers=getDelieveStr(agentid,map.get("ADDRESS").toString());
						String updatesql="UPDATE TrainOrderOffline SET CreateTime='"+sdf.format(new Date())+"',AgentId="+agentid+",paystatus=1, expressDeliver='"+delivers+"' where id="+map.get("Id").toString();
						WriteLog.write("ELong线下火车票艺龙支付成功", "订单号:"+orderNumber+",updatesql:"+updatesql);
						Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
						String addsql="INSERT into TrainElongTradeRecord(OrderId,payPrice,payTime,payStatus,tradeNo) VALUES("+map.get("Id").toString()+","+payedPrice+",'"+payTime+"','"+payType+"','"+tradeNumber+"')";
						WriteLog.write("ELong线下火车票支付成功添加交易记录","订单号:"+map.get("Id").toString()+",sql:"+addsql);
						Server.getInstance().getSystemService().excuteAdvertisementBySql(addsql);
						
						  String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + map.get("Id").toString()
					                + ",@ProviderAgentid=" + agentid + ",@DistributionTime='" + sdf.format(new Date())
					                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
					        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
					        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
//					}
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		String result=paySuccessresult(ordernum);
		return result;
	}
	
	public static String getDelieveStr(String agengId,String address){
    	String results="";
    	String fromcode="010";
    	String tocode=getExpressCodes(address);
    	String time1="10:00:00";
    	String time2="18:00:00";
    	
    	SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        String dates=sdf.format(new Date());
    	String sql1="SELECT fromcode,time1,time2 from TrainOrderAgentTimes where agentId="+agengId;
    	List list=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
    	if(list.size()>0){
    		Map map=(Map)list.get(0);
    		fromcode=map.get("fromcode").toString();
    		time1=map.get("time1").toString();
    		time2=map.get("time2").toString();
    	}
    	String realTime=getRealTimes(dates,time1,time2);
    	String urlString=PropertyUtil.getValue("expressDeliverUrl", "train.properties");
    	String param="times="+realTime+"&fromcode="+fromcode+"&tocode="+tocode;
    	 WriteLog.write("TrainOrderOfflineOffline_保存快递时效信息", "agengId="+agengId+"------->"+"address="+address+"---------->"+urlString+"?"+param);
    	String result=SendPostandGet.submitPost(urlString, param,"UTF-8").toString();
		try {
			Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if("OK".equals(root.elementText("Head")) && result.contains("deliver_time")){
            	Element deliverTmResponse = body.element("DeliverTmResponse");
            	Element deliverTm = deliverTmResponse.element("DeliverTm");
            	String business_type_desc=deliverTm.attributeValue("business_type_desc");
            	String deliver_time=deliverTm.attributeValue("deliver_time");
            	String business_type=deliverTm.attributeValue("business_type");
            	results="如果"+realTime+"正常发件。快递类型为:"+business_type_desc+"。快递预计到达时间:"+deliver_time+"。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
            }else{
                results="获取快递时间失败！请上官网核验快递送达时间。";
            }
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          
    	return results;
    }
	
	/**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    public static String getRealTimes(String dates,String time1,String time2){
        String result="";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates=sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if(date0.before(date1)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date1));
            }else if(date0.after(date1) && date0.before(date2)){
                result=(realDates.substring(0, 10)+" "+sdf.format(date2));
            }else if(date0.after(date2)){
                Date ds=getDate(new Date());
                String nextd=sdf1.format(ds);
                result=(nextd.substring(0, 10)+" "+sdf.format(date1));
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        } 
        return result;
    }
    public static Date getDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }
	
	/**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    public static String getExpressCodes(String address){
    	String procedure="sp_TrainOfflineExpress_getCode @address='"+address+"'";
    	List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
    	String cityCode="010";
    	if(list.size()>0){
    		Map map=(Map)list.get(0);
    		cityCode=map.get("CityCode").toString();
    	}
    	return cityCode;
    }
    /**
     * 根据DB匹配出票点
     * @param address1
     * @return
     */
    public static String distribution2(String address1) {
    	boolean flag=false;
		//默认出票点
    	String sql1="SELECT agentId FROM TrainOfflineMatchAgent WHERE status=2 AND createUid=53";
    	List list1=Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
    	String agentId="378";
    	if(list1.size()>0){
    		Map map=(Map)list1.get(0);
    		agentId=map.get("agentId").toString();
    	}
    	//程序自动分配出票点
    	String sql2="SELECT * FROM TrainOfflineMatchAgent WHERE status=1 AND createUid=53";
    	List list2=Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
    	for(int i=0;i<list2.size();i++){
    		Map mapp=(Map)list2.get(i);
    		String provinces=mapp.get("provinces").toString();
    		String agentid=mapp.get("agentId").toString();
    		String[] add=provinces.split(",");
    		for (int j = 0; j < add.length; j++) {
				if (address1.startsWith(add[j])) {
					agentId=agentid;
					flag=true;
				}
				if(flag){
					break;
				}
			}
			if(flag){
				break;
			}
    	}
    	WriteLog.write("ELong新版分配订单", "agentId="+agentId+";address1="+address1);
    	return agentId;
	}
    
	public static String distribution1(String address1) {
		String resultp="";
		String agentp="378";
		int num=Integer.parseInt(PropertyUtil.getValue("allAgentNum", "train.properties"));
		boolean flag=false;
		for (int i = 1; i <= num; i++) {
			String agents=PropertyUtil.getValue("AgentId"+i, "train.properties");
			String provinces=PropertyUtil.getValue("MailProvince"+i, "train.properties");
			String resultNames="";
			try {
				resultNames = new String(provinces.getBytes("ISO-8859-1"),"utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String[] add=resultNames.split(",");
			for (int j = 0; j < add.length; j++) {
				if (address1.startsWith(add[j])) {
					resultp=add[j];
					agentp=agents;
					flag=true;
				}
				if(flag){
					break;
				}
			}
			if(flag){
				break;
			}
		}
		return agentp;
    }
	public String paySuccessresult(String ordernum){
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.pay");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText(ordernum);
		Element discription=orderResponse.addElement("Discription");
		discription.setText("信息,支付成功..");
		return document.asXML();
	
	}
}
