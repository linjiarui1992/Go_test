package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;

public class HthyStartDelivery {
	public static void main(String[] args) {
		String ss=new HthyStartDelivery().startDelivery("29241");
		System.out.println("回调结果：："+ss);
	}

	public String startDelivery(String orderid) {
		String returnorder = "";
		String result = "";
		try {
			JaxRpcCallbackServiceStub Stub = new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			String xmlparam = getXml(orderid);
			WriteLog.write("Elong_发件请求艺龙request", "订单id:" + orderid+ ",请求艺龙信息:" + xmlparam);
			param.setString(xmlparam);
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub
					.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss = ctripCallbackResponse
					.getCtripCallbackReturn();
			result = ss + "";
			System.out.println(result);
			WriteLog.write("Elong_发件请求艺龙response","订单id:"+orderid+",艺龙返回信息:"+result);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		// 锁单返回信息解析
		try {
			Document document = DocumentHelper.parseText(result);
			Element orderResponse = document.getRootElement();
			String serviceName = orderResponse.elementText("ServiceName");// 接口服务名
			String operationDateTime = orderResponse
					.elementText("OperationDateTime");// 操作日期
			String orderNumber = orderResponse.elementText("OrderNumber");// 订单号
			String status = orderResponse.elementText("Status");// 状态
			returnorder = status;
			Element errorResponse = orderResponse.element("ErrorResponse");
			String errorMessage = errorResponse.elementText("ErrorMessage");// 信息
			if ("FAIL".equals(status)) {
				String errorCode = errorResponse.elementText("ErrorCode");// ErrorCode
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return returnorder;
	}

	public static String getXml(String orderid) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 下单请求接口调用Response
		String sql = "SELECT O.ContactUser,O.ContactTel,M.ADDRESS,M.ExpressNum,O.OrderNumberOnline,M.MAIL,M.NOTE,M.BUSSTYPE,M.ExpressAgent FROM mailaddress M,TrainOrderOffline O WHERE O.Id=M.ORDERID AND O.Id="
				+ orderid;
		DataTable myDt = DBHelper.GetDataTable(sql);
		List<DataRow> list = myDt.GetRow();
		Document document = DocumentHelper.createDocument();
		if (list.size() > 0) {
			DataRow datarow = list.get(0);
			Element orderProcessRequest = document
					.addElement("OrderProcessRequest");
			orderProcessRequest.addAttribute("xsi",
					"http://www.w3.org/2001/XMLSchema-instance");
			Element authentication = orderProcessRequest
					.addElement("Authentication");
			Element timeStamp = authentication.addElement("TimeStamp");
			String times=sdf.format(new Date());
			timeStamp.setText(times);
			Element serviceName = authentication.addElement("ServiceName");
			serviceName.setText("web.order.startDelivery");
			Element messageIdentity = authentication
					.addElement("MessageIdentity");
			
			String message=getMessage(times, "web.order.startDelivery", "hangtian111");
			messageIdentity.setText(message);
			Element partnerName = authentication.addElement("PartnerName");
			partnerName.setText("hangtian111");
			Element trainOrderService = orderProcessRequest
					.addElement("TrainOrderService");
			Element deliveryInfo = trainOrderService.addElement("DeliveryInfo");
			Element contacterName = deliveryInfo.addElement("ContacterName");
			contacterName.setText(datarow.GetColumnString("ContactUser"));// 联系人姓名
			Element contacterAddressID = deliveryInfo
					.addElement("ContacterAddressID");
			contacterAddressID.setText(datarow.GetColumnString("BUSSTYPE"));// 地区编号----------
			Element contacterAddressDetail = deliveryInfo
					.addElement("ContacterAddressDetail");
			contacterAddressDetail.setText(datarow.GetColumnString("ADDRESS"));// 地址
			Element contacterMobile = deliveryInfo
					.addElement("ContacterMobile");
			contacterMobile.setText(datarow.GetColumnString("ContactTel"));// 联系人手机
			Element contacterTel = deliveryInfo.addElement("ContacterTel");
			contacterTel.setText(datarow.GetColumnString("ContactTel"));// 联系人电话
			Element orderDeliveryTypeID = deliveryInfo
					.addElement("OrderDeliveryTypeID");
			orderDeliveryTypeID.setText("1");// 物流类型 1当天件； 2次日件； 3隔日件； 4限时件
			Element orderDeliverFee = deliveryInfo
					.addElement("OrderDeliverFee");
			orderDeliverFee.setText(datarow.GetColumnString("Mail"));// 物流费用
			Element shipmentCompany = deliveryInfo
					.addElement("ShipmentCompany");
			String exepressagent=datarow.GetColumnString("ExpressAgent");
			if("0".equals(exepressagent)){
				shipmentCompany.setText("顺丰");// 物流公司
			}else if("1".equals(exepressagent)){
				shipmentCompany.setText("EMS");// 物流公司
			}else{
				shipmentCompany.setText("顺丰");// 物流公司
			}
			Element shipmentNo = deliveryInfo.addElement("ShipmentNo");
			shipmentNo.setText(datarow.GetColumnString("ExpressNum"));// 物流单号
			Element operationDateTime = deliveryInfo
					.addElement("OperationDateTime");
			operationDateTime.setText(sdf.format(new Date()));// 操作时间
			Element orderInfo = trainOrderService.addElement("OrderInfo");
			Element orderNumber = orderInfo.addElement("OrderNumber");
			orderNumber.setText(datarow.GetColumnString("OrderNumberOnline"));
		}
		return document.asXML();
	}
	public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
		System.out.println(times+serviceName+partName+"<-->"+result);
		return result.toUpperCase();
	}

}
