package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;


public class HthySetSuccess {
	public static void main(String[] args) {
		System.out.println(new HthySetSuccess().setSuccess("520"));
	}

    public String setSuccess(String orderid) {
        String returnorder="";
        String result="";
        try {
            JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
            CtripCallback ctripCallback = new CtripCallback();
            org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
            String xmlparam=getXml(orderid);
            WriteLog.write("HthyCancelOrder已签收log","订单id:"+orderid+",请求艺龙信息:"+xmlparam);
            param.setString(xmlparam);
            ctripCallback.setXml(param);
            CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
            org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
            System.out.println(ss+"");
            result=ss+"";
            WriteLog.write("HthyCancelOrder已签收log","订单id:"+orderid+",艺龙返回信息:"+result);
        } catch (AxisFault e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //锁单返回信息解析
        try {
            Document document = DocumentHelper.parseText(result);
            Element orderResponse = document.getRootElement();
            String serviceName=orderResponse.elementText("ServiceName");//接口服务名
            String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
            String orderNumber=orderResponse.elementText("OrderNumber");//订单号
            String status=orderResponse.elementText("Status");//状态
            returnorder=status;
            Element errorResponse=orderResponse.element("ErrorResponse");
            String errorMessage=errorResponse.elementText("ErrorMessage");//信息
            if("FAIL".equals(status)){
                String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    return returnorder;
    
    }
    public static String getXml(String orderid) {
    	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String sql="SELECT OrderNumberOnline from TrainOrderOffline where Id="+orderid;
    	DataTable myDt = DBHelper.GetDataTable(sql);
        List<DataRow> list = myDt.GetRow();
        String ordernum="";
        if(list.size()>0){
        	DataRow datarow = list.get(0);
        	ordernum=datarow.GetColumnString("OrderNumberOnline");
        }
        //下单请求接口调用Response
        Document document=DocumentHelper.createDocument();
        Element orderProcessRequest=document.addElement("OrderProcessRequest");
        orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
        Element authentication=orderProcessRequest.addElement("Authentication");
        Element timeStamp=authentication.addElement("TimeStamp");
        String times=sdf.format(new Date());
        timeStamp.setText(times);
        Element serviceName=authentication.addElement("ServiceName");
        serviceName.setText("web.order.setSuccess");
        Element messageIdentity=authentication.addElement("MessageIdentity");
        String message1=getMessage(times, "web.order.setSuccess", "hangtian111");
        messageIdentity.setText(message1);
        Element partnerName=authentication.addElement("PartnerName");
        partnerName.setText("hangtian111");
        Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
        Element orderInfo=trainOrderService.addElement("OrderInfo");
        Element orderNumber=orderInfo.addElement("OrderNumber");
        orderNumber.setText(ordernum);//订单号
        Element deliverFee=orderInfo.addElement("DeliverFee");
        deliverFee.setText("20");//物流费
        Element result=orderInfo.addElement("Result");
        result.setText("已签收");//结果
        Element message=orderInfo.addElement("ContacterMobile");
        message.setText("");//失败原因
        Element deliverType=orderInfo.addElement("DeliverType");
        deliverType.setText("1");//物流类型   1当天件；   2次日件；  3隔日件；  4限时件
        return document.asXML();
    }
	public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
		System.out.println(times+serviceName+partName+"<-->"+result);
		return result.toUpperCase();
	}


}
