package com.ccservice.b2b2c.yilonghthy;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccervice.util.db.DBHelper;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.hotel.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;

/**
 * 供应商取消订单
 * 
 * @author guozhengju
 *
 */
public class HthyCancelOrder {
	public static void main(String[] args) {
		System.out.println(new HthyCancelOrder().cancelOrder("518"));
	}

	public String cancelOrder(String orderid) {
		String returnorder = "";
		String result = "";
		try {
			JaxRpcCallbackServiceStub Stub = new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			String xmlparam = getXml(orderid);
			WriteLog.write("HthyCancelOrder取消订单请求信息log","订单id:"+orderid+",请求艺龙信息:"+xmlparam);
//			System.out.println("xmlparam<><><><>---" + xmlparam);
			param.setString(xmlparam);
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub
					.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss = ctripCallbackResponse
					.getCtripCallbackReturn();
			System.out.println("ElongReturn:" + ss + "");
			result = ss + "";
			WriteLog.write("HthyCancelOrder取消订单log","订单id:"+orderid+",艺龙返回信息:"+result);
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		// 锁单返回信息解析
		try {
			Document document = DocumentHelper.parseText(result);
			Element orderResponse = document.getRootElement();
			String serviceName = orderResponse.elementText("ServiceName");// 接口服务名
			String operationDateTime = orderResponse
					.elementText("OperationDateTime");// 操作日期
			String orderNumber = orderResponse.elementText("OrderNumber");// 订单号
			String status = orderResponse.elementText("Status");// 状态
			 returnorder=status;
			Element errorResponse = orderResponse.element("ErrorResponse");
			String errorMessage = errorResponse.elementText("ErrorMessage");// 信息
			if ("FAIL".equals(status)) {
				String errorCode = errorResponse.elementText("ErrorCode");// ErrorCode
				System.out.println(serviceName + "--" + operationDateTime
						+ "--" + orderNumber + "--" + status + "--"
						+ errorMessage + "--" + errorCode);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return returnorder;
	}

	public String getXml(String orderid) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
		// 下单请求接口调用Response
		String sql = "SELECT OrderNumberOnline FROM TrainOrderOffline WHERE ID="
				+ orderid;
		DataTable myDt = DBHelper.GetDataTable(sql);
		List<DataRow> list = myDt.GetRow();
		String ordernum = "";
		if (list.size() > 0) {
			DataRow datarow = list.get(0);
			ordernum = datarow.GetColumnString("OrderNumberOnline");
		}
		Document document = DocumentHelper.createDocument();
		Element orderProcessRequest = document
				.addElement("OrderProcessRequest");
		orderProcessRequest.addAttribute("xmlns:xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		Element authentication = orderProcessRequest
				.addElement("Authentication");
		Element serviceName = authentication.addElement("ServiceName");
		serviceName.setText("web.order.cancelOrder");
		Element partnerName = authentication.addElement("PartnerName");
		partnerName.setText("hangtian111");
		Element timeStamp = authentication.addElement("TimeStamp");
		String times=sdf.format(new Date());
		timeStamp.setText(times);
		Element messageIdentity = authentication.addElement("MessageIdentity");
		String message=getMessage(times,"web.order.cancelOrder","hangtian111").toUpperCase();
		messageIdentity.setText(message);
		Element trainOrderService = orderProcessRequest
				.addElement("TrainOrderService");
		Element orderInfo = trainOrderService.addElement("OrderInfo");
		Element orderNumber = orderInfo.addElement("OrderNumber");
		orderNumber.setText(ordernum);// 订单号
		return document.asXML();
	}
	public static String getMessage(String times,String serviceName,String partName){
		String result=MD5Util.MD5(times+serviceName+partName);
//		System.out.println(times+serviceName+partName+"<-->"+result);
		return result;
	}

}
