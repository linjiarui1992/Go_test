package com.ccservice.b2b2c.nfd;

import java.util.ArrayList;
import java.util.List;

public class NfdData {
	
	//task_id	office_code	carrier	citiespair	query_date	line	行号
	
	private String office;
	private String carrier; //承运人
	private String cityespair;//城市对
	private String querytime;
	
	private String allString;

	private List<NfdLine> lines = new ArrayList<NfdLine>();
	
	public NfdLine FindLine(String ln){
		
		for(NfdLine line:lines){
			
			if(line.getLN().equals(ln)){
				return line;
			}
		}
		return null;
		
	}
	
	
	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getCityespair() {
		return cityespair;
	}

	public void setCityespair(String cityespair) {
		this.cityespair = cityespair;
	}

	public String getQuerytime() {
		return querytime;
	}

	public void setQuerytime(String querytime) {
		this.querytime = querytime;
	}

	public List<NfdLine> getLines() {
		return lines;
	}

	public void setLines(List<NfdLine> lines) {
		this.lines = lines;
	}


	public String getAllString() {
		return allString;
	}


	public void setAllString(String allString) {
		this.allString = allString;
	}
	
	
}
