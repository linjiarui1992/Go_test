package com.ccservice.test.rabbitmq;

import java.io.IOException;

import com.ccservice.inter.job.WriteLog;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class MyThreadTest extends Thread {
    Channel channel;

    private final String QUEUE_NAME = "hello";

    int i;

    public MyThreadTest(Channel channel, int i) {
        this.channel = channel;
        this.i = i;
    }

    @Override
    public void run() {
        try {
            WriteLog.write("test12", " [" + i + "] client start");
            QueueingConsumer consumer = new QueueingConsumer(channel);
            //这个是设置消费者同时获取到的消息数量
            channel.basicQos(1);
            channel.basicConsume(QUEUE_NAME, true, consumer);
            while (true) {
                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                String message = new String(delivery.getBody());
                WriteLog.write("test1", " [" + i + "] Received '" + message + "'");
                try {
                    //                    Thread.sleep((int) (100 * Math.random()));
                    Thread.sleep(1000);
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        catch (ShutdownSignalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (ConsumerCancelledException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
