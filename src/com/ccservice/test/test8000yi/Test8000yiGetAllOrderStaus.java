package com.ccservice.test.test8000yi;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub;
import com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.PayState;
import com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.PayURL;
import com.eightyi.newply.webinterface.orderservice_asmx.OrderServiceStub.ReturnOrderState;





public class Test8000yiGetAllOrderStaus {

	public static void main(String[] args) throws NoSuchAlgorithmException, RemoteException {
		// TODO Auto-generated method stub
	
		OrderServiceStub orderServiceStub = new OrderServiceStub();
		
		ReturnOrderState returnOrderState = new ReturnOrderState();
		
		returnOrderState.setName("yptchen");
		returnOrderState.setPwd("yptchen");
		
		OrderServiceStub.ReturnOrderStateResponse orderStateResponse= orderServiceStub.returnOrderState(returnOrderState);
		
		System.out.println(orderStateResponse.getReturnOrderStateResult());
		
		OMElement e = orderStateResponse.getReturnOrderStateResult().getSchema();
		Iterator<OMElement> it = e.getChildElements();
		System.out.println("it="+it);
		while (it.hasNext()) {// dataset
			OMElement o = it.next();
			Iterator<OMElement> oit = o.getChildElements();
			while (oit.hasNext()) {// table
				OMElement ot = oit.next(); 
				Iterator<OMElement> rit = ot.getChildElements();
				while (rit.hasNext()) {// A
					OMElement rt = rit.next();
					String name = rt.getLocalName().toUpperCase();
					
					if (name.equals("ERRINFO")) {
						System.out.println("8000翼字典提取异常：");
						System.out.println(rt.getText());
						break;
					}
					
					String value = rt.getText();
						if (name.equals("a1")) {
							System.out.println("a1=="+value);
							
						}
					}
				
				
					}
				
			}
			
		
		}
	
	public String Get800YiOrderStaus(String staus){
		
		if(staus.equals("1")){
			
			return "新订单，等待支付";
		}
		if(staus.equals("2")){
			
			return "已经付款，等待出票";
		}
		if(staus.equals("3")){
			
			return "已经出票，交易结束";
		}
		
		if(staus.equals("4")){
			
			return "退票订单，等待审核";
		}
		if(staus.equals("5")){
			
			return "废票订单，等待审核";
		}
		if(staus.equals("6")){
			
			return "改签订单，等待审核";
		}
		
		if(staus.equals("7")){
			
			return "已经退票，退款中";
		}
		if(staus.equals("8")){
			
			return "已经退票，交易结束";
		}
		if(staus.equals("9")){
			
			return "审核未通过，拒绝退票";
		}
		
		if(staus.equals("10")){
			
			return "已经废票，退款中";
		}
		if(staus.equals("11")){
			
			return "已经废票，交易结束";
		}
		if(staus.equals("12")){
			
			return "审核未通过，拒绝废票";
		}
		
		
		if(staus.equals("13")){
			
			return "采购已付款，等待支付";
		}
		if(staus.equals("14")){
			
			return "审核通过，订单挂起，等待补差";
		}
		if(staus.equals("15")){
			
			return "补差完毕，等待解挂";
		}
		
		if(staus.equals("16")){
			
			return "已经解挂，交易结束";
		}
		if(staus.equals("17")){
			
			return "审核未通过，拒绝改签";
		}
		if(staus.equals("18")){
			
			return "取消订单，交易结束";
		}
		
		
		if(staus.equals("19")){
			
			return "平台未支付，已退款";
		}
		if(staus.equals("20")){
			
			return "手工订单，等待支付";
		}
		if(staus.equals("21")){
			
			return "订单超时，取消PNR";
		}
		
		if(staus.equals("22")){
			
			return "未支付，已取消，交易结束";
		}
		if(staus.equals("23")){
			
			return "拒绝补差，改签取消";
		}
		if(staus.equals("24")){
			
			return "同意退票，等待退款";
		}
		
		if(staus.equals("25")){
			
			return "同意废票，等待退款";
		}
		if(staus.equals("26")){
			
			return "暂停出票，待处理";
		}
		if(staus.equals("28")){
			
			return "取消出票，已退款";
		}
	   if(staus.equals("29")){
			
			return "换供应商出票，已退款";
		}
		
		
		if(staus.equals("30")){
			
			return "K位特价订单，等待审核";
		}
		if(staus.equals("31")){
			
			return "K位特价订单，审核未通过";
		}
		if(staus.equals("33")){
			
			return "退票订单，平台代退";
		}
		if(staus.equals("34")){
			
			return "废票订单，平台代退";
		}
		if(staus.equals("35")){
			
			return "退票订单，上级代退";
		}
		if(staus.equals("36")){
			
			return "废票订单，上级代退";
		}
			
		if(staus.equals("89")){
			
			return "已申请,等待审核";
		}
		if(staus.equals("90")){
			
			return "审核通过,等待支付";
		}
		
		if(staus.equals("91")){
			
			return "审核未通过";
		}
		if(staus.equals("101")){
			
			return "用户取消订单，交易结束";
		}
		
		
		if(staus.equals("111")){
			
			return "支付成功，等待上级代付";
		}
		if(staus.equals("112")){
			
			return "拒绝代付，已退款，交易结束";
		}
		
		
		return "";
	}
		
		
		
	}
	
	

