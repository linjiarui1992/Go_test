package com.ccservice.test;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.service.RateService;


public class HttpClient {
	
	public static int START=0;
	
	public static String getString(String str,String start,String end,int st){
		
		int s = str.indexOf(start,st);
		if(s<0){
			START =s;
			return null;
		}
		int e = str.indexOf(end,s+start.length());	
		START =e;
		return  str.substring(s+start.length(),e);
		
	}
	
	public static byte[] httpget(String url){
		try {
				
				URL Url = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
				
				conn.setDoInput(true);
				
				conn.connect();
				
				InputStream in = conn.getInputStream();
				
				byte[] buf = new byte[2046];
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				int len=0;
				int size=0;
				while((len=in.read(buf))>0){
					bout.write(buf,0,len);
					size+=len;
				}
				
	//			System.out.println(new String(content,0,size));
				in.close();
				conn.disconnect();
				
				return bout.toByteArray();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		
		
		return null;
	}
	
	public static void httpgetfile(String url,File file){
		
		try {
			
			URL Url = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
			
			conn.setDoInput(true);
			
			conn.connect();
			
			InputStream in = conn.getInputStream();
			
			byte[] buf = new byte[2046];
			FileOutputStream bout = new FileOutputStream(file);
			int len=0;
			int size=0;
			while((len=in.read(buf))>0){
				bout.write(buf,0,len);
				size+=len;
			}
			
//			System.out.println(new String(content,0,size));
			in.close();
			conn.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
		
		
	}
	
	public static String httpget(String url,String encode){
		
		try {
			return new String(httpget(url),encode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

	
	public static void BytetoFile(String name,byte[] content){
		if(content==null){
			new File(name).mkdir();
			return;
		}
		
		FileOutputStream out=null;
		try {
			out = new FileOutputStream(new File(name));
			out.write(content);
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
	}
	
	
	public static void StringtoFile(String name,String content,String encode){
		if(content==null){
			new File(name).mkdir();
			return;
		}
		
		FileOutputStream out=null;
		try {
			out = new FileOutputStream(new File(name));
			out.write(content.getBytes(encode));
			out.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
	}
	
	
public static byte[] filetoString(File f){
	try {
		FileInputStream fin= new FileInputStream(f);
	
		byte[] bs = new byte[fin.available()];
		fin.read(bs);
		fin.close();
		
		return bs;
	} catch (Exception e) {

		e.printStackTrace();
		return null;
	
	}
}
public static String MD5(String input) throws NoSuchAlgorithmException{
	if(input==null || input.length()==0)
		return "";
	
	String out="";
	byte[] output;
	MessageDigest m = MessageDigest.getInstance("MD5");
	output = m.digest(input.getBytes());
	int len = output.length;

	for(int i=0; i< len; i++){
		
		String t =Integer.toHexString(output[i]& 0x00FF);
	
		out+= (t.length()==1)?("0"+t):t ;
	
	}
	
	return out.toLowerCase();
}	






public static String GetSign(String[] rs) throws NoSuchAlgorithmException{
	 
	 Arrays.sort(rs);
		
	 StringBuffer prestr = new StringBuffer();

       for (int i = 0; i < rs.length; i++)
       {
           String str = (rs[i].split("="))[1];
           if (str!=null && str.length()!=0)
           {
               prestr.append(rs[i] + "&");
           }

       }
       String re = prestr.toString();
       re= re.substring(0, re.length()-1);
       
       JinRiBook jinRiBook = new JinRiBook();
       return HttpClient.MD5(re+jinRiBook.getJRKey());
}

public static String GetSign(String[] rs,String key) throws NoSuchAlgorithmException{
	 
	 Arrays.sort(rs);
		
	 StringBuffer prestr = new StringBuffer();

      for (int i = 0; i < rs.length; i++)
      {
          String str = (rs[i].split("="))[1];
          if (str!=null && str.length()!=0)
          {
              prestr.append(rs[i] + "&");
          }

      }
      String re = prestr.toString();
      re= re.substring(0, re.length()-1);
      return HttpClient.MD5(re+key);
}




public static void main(String[] args) {
	
	System.out.println(Float.parseFloat("9.7"));
	
	//String jrurl ="http://113.140.79.12/ticket_inter/testjr.jsp";
	
	//JobJRRatePackage.getRate(jrurl,"2011-05-16");
	new RateService().FindExtOrder("B10517095738296914");
	/*
	System.out.println("09:00-18:00".substring(0,5));
	System.out.println("09:00-18:00".substring(6));
	
	StringBuffer  worktimefrom = new StringBuffer( "100");
 worktimefrom =new StringBuffer( "2400");
	
	if(worktimefrom.length()>3){
		worktimefrom.insert(2,":");
	}else{
		worktimefrom.insert(0,"0");
		worktimefrom.insert(2,":");
	}
	System.out.println(worktimefrom);
	
	
	ScriptEngineManager sem = new ScriptEngineManager();
	ScriptEngine engine =sem.getEngineByExtension("js");
	try {
		System.out.println(engine.eval("function add(a,b){return a+b;}"));
		System.out.println(engine.eval("function sub(a,b){return (a-b);}"));
		System.out.println(engine.eval("add(1,2)"));
		System.out.println(engine.eval("sub(1,2)"));
	} catch (ScriptException e) {

		e.printStackTrace();
	}
	
	
	*/
}

}
