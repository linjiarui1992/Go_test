package com.ccservice.test.thread;

import java.net.URLEncoder;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.service.ITrainService;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.util.ActiveMQUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class Rehuidiaozhanzuoshibai_thread extends Thread {
    Map map;

    String url;

    String id;

    int type;//type 1回调出票失败2,扔队列里重新下单

    public Rehuidiaozhanzuoshibai_thread(Map map, String url, int type, String cn_servierUrl) {
        super();
        this.map = map;
        this.url = url;
        this.type = type;
        //        this.cn_servierUrl = cn_servierUrl;
        iSystemService = Server.getInstance().getSystemService(cn_servierUrl);
        iTrainService = Server.getInstance().getTrainService(cn_servierUrl);
    }

    public Rehuidiaozhanzuoshibai_thread(String id, String url, int type) {
        super();
        this.url = url;
        this.type = type;
        this.id = id;
    }

    ISystemService iSystemService;

    ITrainService iTrainService;

    @Override
    public void run() {
        if (this.type == 2) {//2,扔队列里重新下单
            System.out.println("QueueMQ_trainorder_waitorder_orderid:" + id + ":" + this.url);
            ActiveMQUtil.sendMessage(this.url, "QueueMQ_trainorder_waitorder_orderid", id);
        }
        if (this.type == 1) {//1回调占座失败
            JSONObject jso = new JSONObject();
            String id = map.get("ID").toString();
            System.out.println(id);
            jso.put("method", "train_order_callback");
            jso.put("trainorderid", id);
            String returnmsg = "已无余票";
            returnmsg = URLEncoder.encode(returnmsg);
            jso.put("returnmsg", returnmsg);
            int count = this.iSystemService.excuteEaccountBySql("UPDATE T_TRAINORDER SET C_ORDERSTATUS=8 WHERE ID="
                    + id);
            String result = "false";
            try {
                System.out.println(id + ":订单号:修改行数" + count + ":" + jso.toString());
                WriteLog.write("TongCheng手工回调_rehuidiaozhanzuoshibai",
                        id + ":订单号:地址:" + url + "-请求参数:" + jso.toString());
                result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
                WriteLog.write("TongCheng手工回调_rehuidiaozhanzuoshibai", id + ":订单号:-回调结果:" + result);
                System.out.println(id + ":订单号:-回调结果:" + result + "---------" + url + "?" + jso.toString());
                if (count > 0 && "success".equals(result)) {
                    createTrainorderrc(Long.parseLong(id), "下单失败-回调成功", "cccdddd1", 8);
                }
                else {
                    createTrainorderrc(Long.parseLong(id), "下单失败-回调失败", "cccdddd1", 8);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (this.type == 3) {
            //1回调出票失败
            JSONObject jso = new JSONObject();
            String id = map.get("ID").toString();
            System.out.println(id);
            jso.put("method", "train_order_callback");
            jso.put("trainorderid", id);
            jso.put("returnmsg", "");
            String result = "false";
            try {
                String sql = "UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=0 WHERE ID=" + id;
                System.out.println(sql);
                iSystemService.excuteEaccountBySql(sql);
                result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
                System.out.println(id + ":订单号:-回调结果:" + result);
                if ("success".equals(result)) {
                    createTrainorderrc(Long.parseLong(id), "下单成功-回调成功", "chendong", 8);
                }
                else {
                    sql = "UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=3 WHERE ID=" + id;
                    System.out.println(sql);
                    Server.getInstance().getSystemService().excuteEaccountBySql(sql);
                    createTrainorderrc(Long.parseLong(id), "下单成功-回调失败", "chendong", 8);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (this.type == 4) {//支付失败回调出票失败
            String id = map.get("ID").toString();
            String agentid = map.get("C_AGENTID").toString();
            System.out.println(id);
            long trainorderid = Long.valueOf(id);
            String result = "false";
            String qunarOrdernumber = gettrainorderinfodatabyMapkey(map, "C_QUNARORDERNUMBER");
            String ordernumber = gettrainorderinfodatabyMapkey(map, "C_ORDERNUMBER");
            JSONObject jso = new JSONObject();
            jso.put("orderid", qunarOrdernumber);
            jso.put("transactionid", ordernumber);
            jso.put("method", "train_pay_callback");
            jso.put("agentid", agentid);
            jso.put("iskefu", "1");
            jso.put("isSuccess", "N");
            try {
                result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if ("success".equalsIgnoreCase(result)) {
                String sql = "UPDATE T_TRAINORDER SET C_STATE12306=3,C_ORDERSTATUS=8 WHERE ID =" + trainorderid;
                int resultcount = iSystemService.excuteGiftBySql(sql);
                createTrainorderrc(trainorderid, "出票失败-回调成功:" + resultcount, "cdpay1", 8);
                System.out.println(trainorderid + "出票失败-回调成功:" + resultcount + ":" + result);
            }
            else {
                createTrainorderrc(trainorderid, "出票失败-回调失败", "cdpay1", 8);
            }
        }
    }

    /**
     * 根据查到的map信息获取value
     * 
     * @param key
     * @time 2015年1月22日 下午1:08:54
     * @author chendong
     */
    private String gettrainorderinfodatabyMapkey(Map map, String key) {
        String value = "";
        if (map.get(key) != null) {
            try {
                value = map.get(key).toString();
            }
            catch (Exception e) {
            }
        }
        return value;
    }

    /**
     * 创建火车票操作记录
     * 
     * @param trainorderId 火车票订单id
     * @param content 内容
     * @param createuser 用户
     * @param status 状态
     * @time 2015年1月18日 下午5:02:43
     * @author chendong
     */
    private void createTrainorderrc(Long trainorderId, String content, String createuser, int status) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderId);
        rc.setContent(content);
        rc.setStatus(status);
        rc.setCreateuser(createuser);
        rc.setYwtype(1);
        this.iTrainService.createTrainorderrc(rc);
    }
}
