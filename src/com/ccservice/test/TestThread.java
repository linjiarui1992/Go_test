/**
 * 
 */
package com.ccservice.test;

import com.ccservice.b2b2c.policy.SendPostandGet;

/**
 * 
 * @time 2015年9月25日 上午8:22:24
 * @author chendong
 */
public class TestThread extends Thread {
    int i;

    public TestThread(int i) {
        this.i = i;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        long l1 = System.currentTimeMillis();
        String s = SendPostandGet.submitGet("http://repdata_search_12306.hangtian123.net/Reptile/repTrainSearch");
        System.out.println(this.i + ":" + s + ":" + (System.currentTimeMillis() - l1));
    }
}