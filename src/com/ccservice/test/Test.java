package com.ccservice.test;

import java.io.IOException;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net._8000yi.websvr.newply.webinterface.plyintefaceservice_asmx.PlyIntefaceServiceStub;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.cityairport.Cityairport;
import com.ccservice.b2b2c.base.fflight.AllRouteInfo;
import com.ccservice.b2b2c.base.fflight.Route;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.SendPostandGet;

public class Test {
    public static void main(String[] args) {
        for (int i = 0; i < 500; i++) {
            new TestThread(i).start();
        }
    }

    public static void main11(String[] args) {
        //		String pnrRT="1.冯志祥 2.abc JDHM34 2. CZ3278 K TH16JAN PEKNNG HK1 1235 1600 E T2-- 3.PEK/T BJS/T-64540699/BEIJING TIAN QU AIR SERVICE LTD.,CO/CHENYIMIN ABCDEFG 4.REMARK 0115 0003 GUEST 5.TL/1035/16JAN/PEK242 6.SSR FOID CZ HK1 NIC9CC3V4PP/P1 7.OSI CZ CTCT13439311111 8.RMK CA/MGLMM3 9.PEK242";
        //		String pnr = "JDHM34";
        //		System.out.println(checkPNR(pnr, pnrRT));
        //		System.out.println(cheakNameisAllEN(pnr));

        Timestamp b1 = new Timestamp(0L);

        Long agentid = 5L;
        List<Zrate> zrates = new ArrayList<Zrate>();
        Zrate zrate1 = new Zrate();
        zrate1.setAgentid(3L);
        zrate1.setRatevalue(10F);

        Zrate zrate2 = new Zrate();
        zrate2.setAgentid(5L);
        zrate2.setRatevalue(10F);

        zrates.add(zrate1);
        zrates.add(zrate2);
        System.out.println(zrates.get(0).getAgentid());

        if (zrates.size() > 1) {
            if (zrates.get(0).getRatevalue().equals(zrates.get(1).getRatevalue())
                    && zrates.get(1).getAgentid() == agentid && zrates.get(0).getAgentid() != agentid) {
                Collections.swap(zrates, 1, 0);
            }
        }

        System.out.println(zrates.get(0).getAgentid());
    }

    public static String checkPNR(String pnr, String pnrRT) {
        String nameString = "";
        // 根据PNR截取pnr的RT
        String[] patas = pnrRT.split(pnr);
        // 名字信息
        String[] names = patas[0].split("\\s");
        boolean flag = true;
        for (int i = 0; i < names.length; i++) {
            if (names[i].length() > 2 && !cheakNameisAllEN(names[i].trim())) {
                // 当名字的第一个字符是字母的时候进入里面
                char temp = names[i].substring(2, names[i].length()).charAt(0);
                // 名字的第一个字符
                if (regx(temp)) {
                    // 循环每个乘客的姓名这里从第三个开始
                    for (int j = 2; j < names[i].length(); j++) {
                        // 如果第一个是字母但是后面有不是字母的时候匹配
                        if (!regx(names[i].charAt(j))) {
                            nameString += "<br>" + names[i].substring(2, names[i].length());
                            flag = false;
                            break;
                        }
                    }
                }
            }
        }
        return nameString;
    }

    public static boolean regx(char c) {
        String reg = "^[a-zA-Z]$";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(c + "");
        if (m.matches()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean cheakNameisAllEN(String name) {
        Pattern pattern = Pattern.compile("[0-9].([a-z]{1,100})/([a-z]{1,100})", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public static void main1(String[] args) {
        String cabinString = "JS RS YA BA M1A HS KS LS QS GS SS NS VS US TS ES";
        String[] cabintypes = cabinString.split("J");
        String toudeng = cabintypes[0].trim();
        String shangwu = "J" + cabintypes[1].split("Y")[0].trim();
        String jingji = "Y" + cabintypes[1].split("Y")[1].trim();
        System.out.println(jingji);
        System.out.println(shangwu);
        System.out.println(toudeng);
        String[] jingjicabins = jingji.split(" ");
        for (int i = jingjicabins.length - 1; i >= 0; i--) {
            char numString = jingjicabins[i].charAt(jingjicabins[i].length() - 1);
            if (numString == 'A' || (numString > 48 && numString < 58)) {
                System.out.println(jingjicabins[i].substring(0, jingjicabins[i].length() - 1) + ":" + numString);
            }
        }
        String[] shangwucabins = shangwu.split(" ");
        for (int i = shangwucabins.length - 1; i >= 0; i--) {
            char numString = shangwucabins[i].charAt(shangwucabins[i].length() - 1);
            if (numString == 'A' || (numString > 48 && numString < 58)) {
                System.out.println(shangwucabins[i].substring(0, shangwucabins[i].length() - 1) + ":" + numString);
            }
        }
        String[] toudengcabins = toudeng.split(" ");
        if (toudeng.length() > 0) {
            for (int i = toudengcabins.length - 1; i >= 0; i--) {
                char numString = toudengcabins[i].charAt(toudengcabins[i].length() - 1);
                if (numString == 'A' || (numString > 48 && numString < 58)) {
                    System.out.println(toudengcabins[i].substring(0, toudengcabins[i].length() - 1) + ":" + numString);
                }
            }
        }
    }

    public void internationaltest() {

        AllRouteInfo allRouteInfo = new AllRouteInfo();
        try {
            String result = "";
            org.dom4j.Document document = DocumentHelper.parseText(result);
            org.dom4j.Element root = document.getRootElement();
            String Is_Success = root.elementText("Is_Success");
            if (Is_Success.equals("1")) {
                List<Route> listRoutes = new ArrayList<Route>();
                List<org.dom4j.Element> AvhInfos = root.elements("AvhInfo");
                if (AvhInfos.size() > 0) {
                    for (int i = 0; i < AvhInfos.size(); i++) {
                        // 航班线路类-Routes
                        Route routeinfo = new Route();// 实例化航班线路
                        org.dom4j.Element avhInfoElement = AvhInfos.get(i);
                        String FromCity = avhInfoElement.elementText("FromCity");
                        String ToCity = avhInfoElement.elementText("ToCity");
                        String TotalFlightTime = avhInfoElement.elementText("TotalFlightTime");
                        List<org.dom4j.Element> flightInfoElements = avhInfoElement.elements("FlightInfo");
                        for (int j = 0; j < flightInfoElements.size(); j++) {
                            org.dom4j.Element FlightInfo = flightInfoElements.get(j);
                            String Carrier = FlightInfo.elementText("Carrier");
                            String FlightNum = FlightInfo.elementText("FlightNum");
                            String IsShared = FlightInfo.elementText("IsShared");
                            String SharedFlightNum = FlightInfo.elementText("SharedFlightNum");
                            String FromCityCode = FlightInfo.elementText("FromCityCode");
                            String ToCityCode = FlightInfo.elementText("ToCityCode");
                            String TakeoffDate = FlightInfo.elementText("TakeoffDate");
                            String TakeoffTime = FlightInfo.elementText("TakeoffTime");
                            String ArriveDate = FlightInfo.elementText("ArriveDate");
                            String ArriveTime = FlightInfo.elementText("ArriveTime");
                            String SeatClass = FlightInfo.elementText("SeatClass");
                            String ChildSeatClass = FlightInfo.elementText("ChildSeatClass");
                            String ConnProtocol = FlightInfo.elementText("ConnProtocol");
                            String AirplaneType = FlightInfo.elementText("AirplaneType");
                            String IsStop = FlightInfo.elementText("IsStop");
                            String StopCity = FlightInfo.elementText("StopCity");
                            String FoodMark = FlightInfo.elementText("FoodMark");
                            String Terminal = FlightInfo.elementText("Terminal");
                            String FlightTime = FlightInfo.elementText("FlightTime");
                            String StopTime = FlightInfo.elementText("StopTime");
                        }
                        listRoutes.add(routeinfo);
                    }
                }
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    // 将 s 进行 BASE64 编码
    public static String getBASE64(String s) {
        if (s == null)
            return null;
        String re = (new sun.misc.BASE64Encoder()).encode(s.getBytes());
        return URLEncoder.encode(re);
    }

    // 将 s 进行 BASE64 编码
    public static String getUNBASE64(String s) {
        if (s == null)
            return null;

        byte[] re = null;
        try {
            re = (new sun.misc.BASE64Decoder()).decodeBuffer(s);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return new String(re);
    }

    /**
     * 8000yi获取航空公司
     * 
     * @return
     */
    public static String getAir() {

        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.GetAirCo getAirCo22 = new PlyIntefaceServiceStub.GetAirCo();
            getAirCo22.setName("bjhthy");
            getAirCo22.setPwd("bjhthy");
            PlyIntefaceServiceStub.GetAirCoResponse response = stub.getAirCo(getAirCo22);
            OMElement e = response.getGetAirCoResult().getExtraElement();
            Iterator<OMElement> it = e.getChildElements();
            while (it.hasNext()) {
                OMElement o = it.next();
                Iterator<OMElement> oit = o.getChildElements();
                String data = "";
                int num = 0;
                while (oit.hasNext()) {
                    OMElement ot = oit.next();
                    Iterator<OMElement> rit = ot.getChildElements();
                    while (rit.hasNext()) {
                        OMElement rt = rit.next();
                        String value = rt.getText();
                        String name = rt.getLocalName();
                        System.out.println(name + ":" + value);
                        if (name.equals("AirCode") || name.equals("AirName") || name.equals("A1")) {
                            data += value + ",";
                        }
                    }
                    num++;
                    data += ";";
                }
                System.out.println(data);
                System.out.println(num);
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";

    }

    /**
     * 8000yi获取城市
     * 
     * @return
     */
    public static String getCity() {

        try {
            PlyIntefaceServiceStub stub = new PlyIntefaceServiceStub();
            PlyIntefaceServiceStub.GetCity getCity4 = new PlyIntefaceServiceStub.GetCity();
            getCity4.setName("bjhthy");
            getCity4.setPwd("bjhthy");
            PlyIntefaceServiceStub.GetCityResponse response = stub.getCity(getCity4);
            OMElement e = response.getGetCityResult().getExtraElement();
            Iterator<OMElement> it = e.getChildElements();
            while (it.hasNext()) {
                OMElement o = it.next();
                Iterator<OMElement> oit = o.getChildElements();
                String data = "";
                int num = 0;
                while (oit.hasNext()) {
                    OMElement ot = oit.next();
                    Iterator<OMElement> rit = ot.getChildElements();
                    String cityName = "";
                    String CityCode = "";
                    String CitySpell = "";
                    String CityShortSpell = "";
                    while (rit.hasNext()) {
                        OMElement rt = rit.next();
                        String value = rt.getText();
                        String name = rt.getLocalName();
                        // System.out.println(name + ":" + value);
                        if (name.equals("CityName")) {
                            cityName = value;
                        }
                        if (name.equals("CityCode")) {
                            CityCode = value;
                        }
                        if (name.equals("CitySpell")) {
                            CitySpell = value;
                        }
                        if (name.equals("CityShortSpell")) {
                            CityShortSpell = value;
                        }
                    }
                    // List list =
                    // Server.getInstance().getAirService().findAllCityairport("where c_airportcode='"+CityCode+"'",
                    // "", -1, 0);
                    // if (list.size()==0) {
                    // Cityairport cityairport = new Cityairport();
                    // cityairport.setCityname(cityName);
                    // cityairport.setPinyin(CitySpell);
                    // cityairport.setShortpinyin(CityShortSpell);
                    // cityairport.setAirportcode(CityCode);
                    // Server.getInstance().getAirService().createCityairport(cityairport);
                    // }
                    num++;
                    data += cityName + "," + CityCode + "," + CitySpell + "," + CityShortSpell + ";";
                }
                System.out.println(data);
                System.out.println(num);
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return "";

    }

    /**
     * 生成JS城市列表需要的城市数据
     */
    private static void buildcity() {
        List<Cityairport> list = Server.getInstance().getAirService()
                .findAllCityairport("where id<300", "order by id", -1, 0);
        for (int i = 0; i < list.size(); i++) {
            Cityairport cityairport = list.get(i);
            System.out.println("citysFlight[" + cityairport.getId() + "]=new Array('" + cityairport.getAirportcode()
                    + "','" + cityairport.getCityname() + "','" + cityairport.getPinyin() + "','"
                    + cityairport.getShortpinyin() + "');");
        }
    }
}
