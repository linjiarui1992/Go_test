package com.ccservice.bussinessman;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateHotelPriceBus implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("生意人酒店价格开始……");
		new BusinessmanUpdateJob().updateHotelPrice();
		System.out.println("生意人酒店价格结束……");
	}

}
