package com.ccservice.crack.ctrippffsts;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.offline.dao.TrainOfflineConfigDao;
import com.ccservice.offline.domain.TrainOfflineConfig;
import com.ccservice.offline.util.PropertyUtil;
import com.ccservice.offlineExpress.util.CommonUtil;

/**
 * @className: com.ccservice.ctripoffsts.crack.util.TrainCtripOfflineUtil
 * @description: TODO - 携程官网下单的相关破解
 * 
后台登陆地址：www.tiexiaoer.com/ticketagent/loginv2
 * 
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月21日 上午9:15:02 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainCtripOfflineUtil {

    // 批量发件URL
    private static final String BATCHDELIVERYSEND_URL = "http://www.tiexiaoer.com/TicketAgent/SendExpress/DeliverAllOrdersExceptAddressChanged";

    //加载订单的界面
    private static final String SETTICKETV2_URL = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2";

    // 监控订单URL
    private static final String FINDTOTALUNPULLEDORDERS_URL = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2/FindTotalUnPulledOrders";

    // 携程拒单URL
    private static final String SETNOTICKET_URL = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2/SetNoTicket";

    // 发送验证码URL
    private static final String SENDMSG_URL = "http://www.tiexiaoer.com/TicketAgent/LoginV2/SendMsg";

    // 登录
    private static final String VAILMSG_URL = "http://www.tiexiaoer.com/TicketAgent/LoginV2/VailMsg";

    // 获取订单数量
    private static final String FETCHORDER_URL = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2/FetchOrder";

    private static final String HOST1 = "www.tiexiaoer.com";

    private static final String ORIGIN = "http://www.tiexiaoer.com";

    private static final String REFERER1 = "http://www.tiexiaoer.com/ticketagent/loginv2";

    private static final String REFERER2 = "http://www.tiexiaoer.com/TicketAgent/SetTicketV2";

    private TrainOfflineConfigDao trainOfflineConfigDao = new TrainOfflineConfigDao();

    // get方式设置 请求头
    public HttpGet setGetRequestHeader(String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        get.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        get.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        get.addHeader("Accept-Encoding", "gzip, deflate");
        get.addHeader("Connection", "keep-alive");
        get.addHeader("Upgrade-Insecure-Requests", "1");
        return get;
    }

    // post方式设置请求头，主要是为了表单的提交
    public HttpPost setPostRequestHeader(String url) {
        HttpPost post = new HttpPost(url);
        post.addHeader("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");
        post.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        post.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        post.addHeader("Accept-Encoding", "gzip, deflate");
        post.addHeader("Connection", "keep-alive");
        post.addHeader("Upgrade-Insecure-Requests", "1");
        return post;
    }

    /**
     * 获取可提取订单数量
     * 
     * @time 2017年11月28日 上午10:23:06
     * @author liujun
     */
    public String trainCtripOfflineFindTotalUnPulledOrders(String logName, int random,
            CloseableHttpClient defaultClient, String username) throws Exception {
        String result = "";
        // 判断用户是否登录
        boolean checkIsLogin = checkIsLogin(defaultClient, username);
        WriteLog.write(logName, random + "---用户登录结果---" + checkIsLogin);
        // 没有登录进行登录操作
        if (!checkIsLogin) {
            login(defaultClient, username);
        }
        // 获取登录cookie
        TrainOfflineConfig trainOfflineConfig = getTrainOfflineConfigCtripLoginCookie(username);
        String loginCookie = trainOfflineConfig.getValue();
        HttpGet get = setGetRequestHeader(FINDTOTALUNPULLEDORDERS_URL);
        get.setHeader("Cookie", loginCookie);
        get.setHeader("Host", HOST1);
        get.setHeader("Referer", REFERER2);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        CloseableHttpResponse res = defaultClient.execute(get);
        HttpEntity entity = res.getEntity();
        result = EntityUtils.toString(entity);
        WriteLog.write(logName, random + "---携程平台返回结果---" + result);
        return result;
    }

    /**
     * 判断是否登录
     * 
     * @time 2017年11月28日 上午10:18:00
     * @author liujun
     */
    public boolean checkIsLogin(CloseableHttpClient defaultClient, String username) throws Exception {
        boolean checkIsLoginResult = true;

        HttpPost post = setPostRequestHeader(REFERER2);

        String loginCookie = "ASP.NET_SessionSvc=MTAuOC4xODkuNjh8OTA5MHxqaW5xaWFvfGRlZmF1bHR8MTUxMTIzMjY1MzU5Ng; ASP.NET_SessionId=cjme4oh0dvsal0h12yysile0; OutletsUserID=2567284; OutletsLoginName=TJZ01; OutletsName=tjz01; OutletsGsdm=000000665; OutletsOutletName=%e5%a4%a9%e6%b4%a5%e7%ab%99%e6%9f%9c%e5%8f%b0%ef%bc%88%e8%88%aa%e5%a4%a9%e5%8d%8e%e6%9c%89%ef%bc%89; AuthorizationKey=ae84ff4f-2da4-4e16-9bc9-4c544bf257a7; _bfa=1.1512552136339.3xhyrp.1.1512552136339.1512552136339.1.2; _bfs=1.2; page_time=1512552136674%2C1512552149700; _RF1=171.11.0.75; _RSG=raqnWQTFhs0Psrw1qD6k7B; _RDG=2862eb8803540223102192f86a180c93be; _RGUID=0dab3dd6-a0c7-4935-b1ee-18dd01eb5d99; _bfi=p1%3D0%26p2%3D0%26v1%3D2%26v2%3D1";
        post.setHeader("Cookie", loginCookie);

        post.setHeader("Host", HOST1);
        post.setHeader("Origin", ORIGIN);
        post.setHeader("Referer", REFERER1);

        List<NameValuePair> getInitOrderGetFirstPageList = new ArrayList<NameValuePair>();
        getInitOrderGetFirstPageList.add(new BasicNameValuePair("loginName", username));

        String pwd = "";
        getInitOrderGetFirstPageList.add(new BasicNameValuePair("password", pwd));

        UrlEncodedFormEntity userIdentifyEn = new UrlEncodedFormEntity(getInitOrderGetFirstPageList, "UTF-8");
        post.setEntity(userIdentifyEn);

        CloseableHttpResponse res = defaultClient.execute(post);
        HttpEntity entity = res.getEntity();
        String initOrderGetFirstPage = EntityUtils.toString(entity);

        /**
         * 如果是未登录的话 - 会反馈 - http://www.tiexiaoer.com/TicketAgent/LoginV2/Index?returnUrl=http://www.tiexiaoer.com/TicketAgent/SetTicketV2
         * 
        <html><head><title>Object moved</title></head><body>
        <h2>Object moved to <a href="%2fTicketAgent%2fLoginV2%2fIndex%3freturnUrl%3dhttp%3a%2f%2fwww.tiexiaoer.com%2fTicketAgent%2fSetTicketV2">here</a>.</h2>
        </body></html>
         * 
         */
        if (initOrderGetFirstPage.contains("<title>Object moved</title>")) {
            checkIsLoginResult = false;
        }

        return checkIsLoginResult;
    }

    /**
     * 获取cookie
     * 
     * @time 2017年11月28日 上午10:18:42
     * @author liujun
     */
    public TrainOfflineConfig getTrainOfflineConfigCtripLoginCookie(String username) throws Exception {
        String keyName = "CtripOfflineCookieStr" + username;//以账号名进行标准识别判定
        TrainOfflineConfig trainOfflineConfig = trainOfflineConfigDao.findTrainOfflineConfigByKeyName(keyName);
        return trainOfflineConfig;
    }

    /**
     * 发送验证码
     * 
     * @throws Exception
     * @time 2017年11月29日 下午2:35:31
     * @author liujun
     */
    private boolean sendMsg(CloseableHttpClient defaultClient, String username) throws Exception {
        boolean result = false;
        HttpGet get = setGetRequestHeader(SENDMSG_URL + "?loginName=" + username);
        get.setHeader("Host", HOST1);
        get.setHeader("Referer", REFERER1);
        CloseableHttpResponse res = defaultClient.execute(get);
        HttpEntity entity = res.getEntity();
        String reqResult = EntityUtils.toString(entity);
        result = "success".equals(reqResult);
        return result;
    }

    /**
     * 携程破解登录
     * 
     * @time 2017年11月28日 上午10:19:09
     * @author liujun
     */
    public boolean login(CloseableHttpClient defaultClient, String username) throws Exception {
        boolean result = false;
        int random = CommonUtil.randomNum();
        String logName = "线下火车票_携程登录破解_ticket_inter";
        WriteLog.write(logName, random + "---携程登录用户名---" + username);
        boolean sendMsgResult = sendMsg(defaultClient, username);
        WriteLog.write(logName, random + "---携程发送验证码结果---" + sendMsgResult);
        // 发送验证码成功 - 开始获取读取短信内的验证码，由于数据库的入库时间和服务器的时间会出现不一致的情况,所以不考虑时间的问题，只要差别不是很大，全部在等待30s之后，取第一条数据
        if (sendMsgResult) {
            // 获取验证码
            String password = getSmsMessage(logName, random, username);
            WriteLog.write(logName, random + "---携程验证码---" + password);
            if (!"".equals(password)) {
                // 输入验证码开始登录
                HttpPost post = setPostRequestHeader(VAILMSG_URL);
                post.setHeader("Host", HOST1);
                post.setHeader("Origin", ORIGIN);
                post.setHeader("Referer", REFERER1);
                //重试5次就会封账号
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("loginName", username));
                param.add(new BasicNameValuePair("password", password));
                UrlEncodedFormEntity loginEntity = new UrlEncodedFormEntity(param, "UTF-8");
                post.setEntity(loginEntity);
                CloseableHttpResponse res = defaultClient.execute(post);
                HttpEntity entity = res.getEntity();
                String reqResult = EntityUtils.toString(entity);
                WriteLog.write(logName, random + "---携程登录结果---" + reqResult);
                //loginFail-验证码错误 - SuccessiveFail-连续出错-账号已被封 -success登录成功
                if ("success".equals(reqResult)) {
                    //取出登录之后后期需要使用的cookie - 登录成功 - 更新入库
                    Header[] allHeaders = res.getAllHeaders();

                    String loginCookie = "";
                    for (Header header : allHeaders) {
                        /**
                         * 
                        Set-Cookie: OutletsUserID=2567284; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                        Set-Cookie: OutletsLoginName=tjz01; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                        Set-Cookie: OutletsName=tjz01; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                        Set-Cookie: OutletsGsdm=000000665; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                        Set-Cookie: OutletsOutletName=%e5%a4%a9%e6%b4%a5%e7%ab%99%e6%9f%9c%e5%8f%b0%ef%bc%88%e8%88%aa%e5%a4%a9%e5%8d%8e%e6%9c%89%ef%bc%89; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                        Set-Cookie: AuthorizationKey=95c091ff-497a-48c4-86a8-af84e3dacdd4; expires=Thu, 21-Sep-2017 10:39:53 GMT; path=/
                         * 
                         */
                        if ("Set-Cookie".equals(header.getName())) {
                            String cookieTemp = header.getValue();
                            loginCookie += cookieTemp.substring(0, cookieTemp.indexOf(";") + 2);
                        }
                    }

                    String keyName = "CtripOfflineCookieStr" + username;
                    //更新操作
                    int flag = trainOfflineConfigDao.updateValueByKeyName(keyName, loginCookie, password);
                    if (flag > 0) {
                        result = true;
                        WriteLog.write(logName, random + "---登录成功---");
                    }
                }
            }
        }
        return result;
    }

    /**
     * 根据手机号获取验证码
     * 
     * @time 2017年11月29日 下午4:15:53
     * @author liujun
     * @throws Exception 
     */
    private String getSmsMessage(String logName, int random, String username) throws Exception {
        String result = "";
        String keyName = "CtripOfflineCookieStr" + username;
        int RETRYGETSMSCOUNT = Integer.parseInt(PropertyUtil.getValue("retry_getsms_count", "train.properties"));
        for (int i = 0; i < RETRYGETSMSCOUNT; i++) {//间隔20次，重试获取
            // 火车票线下新订单
            String sql = "SELECT remark1,codeState FROM TrainOfflineConfig WITH(NOLOCK) WHERE keyName = '" + keyName
                    + "'";
            DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
            WriteLog.write(logName, random + "查询结果：" + dataTable);
            if (dataTable != null && dataTable.GetRow().size() > 0) {
                String remark1 = String.valueOf(dataTable.GetRow().get(0).GetColumn("remark1").GetValue());
                String codeState = String.valueOf(dataTable.GetRow().get(0).GetColumn("codeState").GetValue());
                if ("1".equals(codeState)) {
                    result = remark1;
                    String uptSql = "UPDATE TrainOfflineConfig SET codeState = 0 WHERE keyName='" + keyName + "'";
                    int res = DBHelperOffline.UpdateData(uptSql);
                    WriteLog.write(logName, random + "----验证码更新结果----" + res);
                    if (res > 0) {
                        break;
                    }
                    else {
                        WriteLog.write(logName, random + "----等待----" + i + 1 + "次数");
                    }
                }
            }
            Thread.sleep(2000);// 等待2秒
        }
        return result;
    }

    /**
     * 点击获取订单
     * 
     * @time 2017年11月22日 下午3:55:16
     * @author liujun
     */
    public String fetchOrder(String logName, int random, CloseableHttpClient defaultClient, String username)
            throws Exception {
        String result = "";
        // 判断用户是否登录
        boolean checkIsLogin = checkIsLogin(defaultClient, username);
        WriteLog.write(logName, random + "---用户登录结果---" + checkIsLogin);
        // 没有登录进行登录操作
        if (!checkIsLogin) {
            login(defaultClient, username);
        }
        // 获取登录cookie
        TrainOfflineConfig trainOfflineConfig = getTrainOfflineConfigCtripLoginCookie(username);
        String loginCookie = trainOfflineConfig.getValue();
        HttpGet get = setGetRequestHeader(FETCHORDER_URL);
        get.setHeader("Cookie", loginCookie);
        get.setHeader("Host", HOST1);
        get.setHeader("Referer", REFERER2);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        CloseableHttpResponse res = defaultClient.execute(get);
        HttpEntity entity = res.getEntity();
        result = EntityUtils.toString(entity);
        WriteLog.write(logName, random + "---携程平台返回结果---" + result);
        return result;
    }

    /**
     * 获取订单列表
     * 
     * @time 2017年11月22日 下午3:56:32
     * @author liujun
     */
    public JSONArray fetchOrderList(String logName, int random, CloseableHttpClient defaultClient, String username)
            throws Exception {
        JSONArray result = new JSONArray();
        // 判断用户是否登录
        boolean checkIsLogin = checkIsLogin(defaultClient, username);
        WriteLog.write(logName, random + "---用户登录结果---" + checkIsLogin);
        // 没有登录进行登录操作
        if (!checkIsLogin) {
            login(defaultClient, username);
        }
        TrainOfflineConfig trainOfflineConfig = getTrainOfflineConfigCtripLoginCookie(username);
        String loginCookie = trainOfflineConfig.getValue();
        HttpPost post = setPostRequestHeader(SETTICKETV2_URL);
        post.setHeader("Cookie", loginCookie);
        post.setHeader("Host", HOST1);
        post.setHeader("Origin", ORIGIN);
        post.setHeader("Referer", REFERER2);
        List<NameValuePair> getInitOrderGetFirstPageList = new ArrayList<NameValuePair>();
        getInitOrderGetFirstPageList.add(new BasicNameValuePair("ticketDate", ""));
        getInitOrderGetFirstPageList.add(new BasicNameValuePair("orderNumber", ""));
        getInitOrderGetFirstPageList.add(new BasicNameValuePair("selectName", ""));
        UrlEncodedFormEntity userIdentifyEn = new UrlEncodedFormEntity(getInitOrderGetFirstPageList, "UTF-8");
        post.setEntity(userIdentifyEn);
        CloseableHttpResponse reqResult = defaultClient.execute(post);
        HttpEntity entity = reqResult.getEntity();
        String res = EntityUtils.toString(entity);
        Document document = Jsoup.parse(res);
        Elements divsElements = document.select("#table1");
        Elements trs = divsElements.select("tr");
        for (int i = 0; i < trs.size(); i++) {
            // 去掉表头
            if (i != 0) {
                Elements tds = trs.get(i).select("td");
                // 如果订单信息不为空
                if (tds.text().isEmpty() || tds.text().contains("暂无订单信息")) {
                    continue;
                }
                else {
                    // 订单信息获取
                    String orderInfo = tds.get(3).select("input").get(0).attr("onclick")
                            .replace("SetHasTicketV2_onclick('", "").split("',")[0];
                    result.add(JSONObject.parse(orderInfo));
                }
            }
        }
        WriteLog.write(logName, random + "---携程平台获取订单列表结果---" + result.size());
        return result;
    }

    /**
     * 设置无票
     *     <select class="textClass" style="width: 200px; height: 20px;" onchange="ChangeNoTicketReason(this.value)">
        <option value="default">--</option>
        <option value="NoTicket">车次已无票</option>
        <option value="NoTicketCanMeetSpecificNeeds">坐席无法满足乘客要求</option>
        <option value="RestrictionsOnPassengerRealName">乘客实名制限制</option>
        <option value="HighConsumptionLimit">乘客高消费限制</option>
        <option value="OtherReason">其他</option>
        </select>
     * @time 2017年11月28日 下午5:02:12
     * @author liujun
     */
    public String setNoTicket(String logName, int random, String orderNumber, String noticketReason,
            CloseableHttpClient defaultClient, String username) throws Exception {
        String result = "";
        // 判断用户是否登录
        boolean checkIsLogin = checkIsLogin(defaultClient, username);
        WriteLog.write(logName, random + "---用户登录结果---" + checkIsLogin);
        // 没有登录进行登录操作
        if (!checkIsLogin) {
            login(defaultClient, username);
        }
        // 获取登录cookie
        TrainOfflineConfig trainOfflineConfig = getTrainOfflineConfigCtripLoginCookie(username);
        String loginCookie = trainOfflineConfig.getValue();
        String reqUrl = SETNOTICKET_URL + "?" + "orderNumber=" + orderNumber + "&" + "noticketReason=" + noticketReason;
        HttpGet get = setGetRequestHeader(reqUrl);
        get.setHeader("Cookie", loginCookie);
        get.setHeader("Host", HOST1);
        get.setHeader("Referer", REFERER2);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        CloseableHttpResponse res = defaultClient.execute(get);
        HttpEntity entity = res.getEntity();
        result = EntityUtils.toString(entity);
        WriteLog.write(logName, random + "---携程平台返回结果---" + result);
        return result;
    }

    /**
     * 批量发件
     * @time 2017年11月28日 下午5:02:12
     * @author liujun
     */
    public String batchDeliverySend(String logName, int random, CloseableHttpClient defaultClient, String username)
            throws Exception {
        String result = "";
        // 判断用户是否登录
        boolean checkIsLogin = checkIsLogin(defaultClient, username);
        WriteLog.write(logName, random + "---用户登录结果---" + checkIsLogin);
        // 没有登录进行登录操作
        if (!checkIsLogin) {
            login(defaultClient, username);
        }
        // 获取登录cookie
        TrainOfflineConfig trainOfflineConfig = getTrainOfflineConfigCtripLoginCookie(username);
        String loginCookie = trainOfflineConfig.getValue();
        HttpGet get = setGetRequestHeader(BATCHDELIVERYSEND_URL);
        get.setHeader("Cookie", loginCookie);
        get.setHeader("Host", HOST1);
        get.setHeader("Referer", REFERER2);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        CloseableHttpResponse res = defaultClient.execute(get);
        HttpEntity entity = res.getEntity();
        result = EntityUtils.toString(entity);
        WriteLog.write(logName, random + "---携程平台返回结果---" + result);
        return result;
    }
}
