package com.ccservice.taobao.util;

import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.sql.Timestamp;
import net.sf.json.JSONObject;
import java.text.SimpleDateFormat;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 淘宝更新工具类
 * @author WH
 */

public class TaoBaoUpdateUtil {

    //淘宝卖家
    @SuppressWarnings("rawtypes")
    public static Map<String, JSONObject> loadbuyer() {
        Map<String, JSONObject> map = new HashMap<String, JSONObject>();
        //代理未禁用、有效期内、有流量
        String sql = "select c.ID agentId, c.C_AGENTCOMPANYNAME agentName, t.C_SESSIONKEY sessionKey,"
                + "t.C_SESSIONKEYMODIFYTIME modifyTime, t.C_SESSIONKEYACTIVETIME activeTime "
                + "from T_TAOBAOAGENT t join T_CUSTOMERAGENT c on c.ID = t.C_AGENTID "
                + "where (c.C_AGENTISENABLE is null or c.C_AGENTISENABLE = 1) "
                + "and (C_AGENTVEDATE is null or C_AGENTVEDATE > CONVERT(varchar(100), GETDATE(), 20)) "
                + "and t.C_TAOBAOUSERNAME !='' and t.C_SESSIONKEY !='' and (t.C_TOTALLIULIANG = -1 or t.C_TOTALLIULIANG > t.C_LIULIANG)";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map m = (Map) list.get(i);
                if (m.get("agentId") == null || m.get("sessionKey") == null || m.get("modifyTime") == null
                        || m.get("activeTime") == null) {
                    continue;
                }
                String agentId = m.get("agentId").toString();
                String agentName = m.get("agentName") == null ? "" : m.get("agentName").toString();
                String sessionKey = m.get("sessionKey").toString();
                String modifyTime = m.get("modifyTime").toString();
                String activeTime = m.get("activeTime").toString();
                if (ElongHotelInterfaceUtil.StringIsNull(agentId) || ElongHotelInterfaceUtil.StringIsNull(sessionKey)
                        || ElongHotelInterfaceUtil.StringIsNull(modifyTime)
                        || ElongHotelInterfaceUtil.StringIsNull(activeTime)) {
                    continue;
                }
                try {
                    Timestamp sessionKeyModifyTime = Timestamp.valueOf(modifyTime);
                    long start = sessionKeyModifyTime.getTime();
                    long end = System.currentTimeMillis();
                    long result = (end - start) / 1000;
                    if (result > Long.valueOf(activeTime).longValue()) {
                        throw new Exception("SessionKey过期!");
                    }
                }
                catch (Exception e) {
                    continue;
                }
                JSONObject obj = new JSONObject();
                obj.put("agentId", agentId);
                obj.put("agentName", agentName);
                obj.put("sessionKey", sessionKey);
                map.put(agentId, obj);
            }
        }
        return map;
    }

    /**
     * 淘宝商品
     * @param hotelId 捷旅酒店ID，可为空
     * @param roomId 捷旅房型ID，不可为空
     * @param agentIds 代理ID，可为空，多个英文逗号相隔
     */
    @SuppressWarnings("rawtypes")
    public static Map<String, JSONObject> loadgoods(String hotelId, String roomId, String agentIds) {
        Map<String, JSONObject> map = new HashMap<String, JSONObject>();
        String sql = "select th.C_AGENTID agentId, lh.ID hotelId, lh.C_NAME hotelName, lr.ID roomId, lr.C_NAME roomName, lr.C_BED bed, tr.C_LOCALPROD prod,"
                + "tr.C_LOCALBF bf, tr.ID trId, tr.C_GID gid, tr.C_GOODSSTATUS status, tr.C_ADDPRICE addprice,tr.C_GOODSMODIFYTIME modifytime "
                + "from T_HOTEL lh with(nolock) "
                + "join T_ROOMTYPE lr with(nolock) on lr.C_HOTELID = lh.ID "
                + "join T_TAOBAOHOTEL th with(nolock) on th.C_NATIVEHOTELID = lh.ID "
                + "join T_TAOBAOROOMTYPE tr with(nolock) on tr.C_TAOBAOTABLEID = th.ID and tr.C_NATIVEROOMTYPEID = lr.ID "
                + "where lh.C_SOURCETYPE = 6 and tr.C_GID > 0 and tr.C_GOODSSTATUS in (1,2) and lr.C_ROOMCODE = '"
                + roomId + "'";
        if (!ElongHotelInterfaceUtil.StringIsNull(hotelId)) {
            sql += " and lh.C_HOTELCODE = '" + hotelId + "'";
        }
        if (!ElongHotelInterfaceUtil.StringIsNull(agentIds)) {
            sql += " and th.C_AGENTID in (" + agentIds + ")";
        }
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map m = (Map) list.get(i);
                String agentId = m.get("agentId").toString();
                String localHotelId = m.get("hotelId").toString();
                String hotelName = m.get("hotelName").toString();
                String localRoomId = m.get("roomId").toString();
                String roomName = m.get("roomName").toString();
                String bed = m.get("bed") == null ? "0" : m.get("bed").toString();
                String prod = m.get("prod").toString();
                String bf = m.get("bf").toString();
                String trId = m.get("trId").toString();
                String gid = m.get("gid").toString();
                String status = m.get("status").toString();
                String addprice = m.get("addprice").toString();
                String modifytime = m.get("modifytime").toString();
                //put
                JSONObject obj = new JSONObject();
                obj.put("agentId", agentId);
                obj.put("hotelId", localHotelId);
                obj.put("hotelName", hotelName);
                obj.put("roomId", localRoomId);
                obj.put("roomName", roomName);
                obj.put("bed", bed);
                obj.put("prod", prod);
                obj.put("bf", bf);
                obj.put("trId", trId);
                obj.put("gid", gid);
                obj.put("status", status);
                obj.put("addprice", addprice);
                obj.put("modifytime", modifytime);
                map.put(agentId, obj);
            }
        }
        return map;
    }

    //下架操作
    public static void taobaoDown(String trid, String gid, String sessionkey, String agentName, String hotelName,
            String roomName, SimpleDateFormat timeFormat) {
        try {
            String json = Server.getInstance().getTaoBaoHotelSerice()
                    .productUpdate(Long.parseLong(gid), null, null, 2l, sessionkey);//2：下架
            JSONObject obj = JSONObject.fromObject(json);
            if (obj.containsKey("hotel_room_update_response")) {
                obj = obj.getJSONObject("hotel_room_update_response");
                obj = obj.getJSONObject("room");
                long status = obj.getLong("status");
                long iid = obj.getLong("iid");
                if (status == 2) {
                    String sql = "update T_TAOBAOROOMTYPE set C_GOODSSTATUS = 2 where ID = " + trid + " and C_IID = "
                            + iid;
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    System.out.println(timeFormat.format(new Date()) + "  " + agentName + "，" + hotelName + "，"
                            + roomName + "，无价格，商品下架成功。");
                }
                else {
                    String statusStr = "";
                    if (status == 1) {
                        statusStr = "上架";
                    }
                    else if (status == 3) {
                        statusStr = "删除";
                    }
                    else {
                        statusStr = String.valueOf(status);
                    }
                    System.out.println(timeFormat.format(new Date()) + "  " + agentName + "，" + hotelName + "，"
                            + roomName + "，无价格，商品下架失败，返回状态为：" + statusStr);
                }
            }
        }
        catch (Exception e) {
        }
    }

    //深捷旅早餐转本地
    public static long bfToLocal(String bf) {
        long ret = 0;
        if (ElongHotelInterfaceUtil.StringIsNull(bf) || bf.equals("10")) {
            ret = 0;
        }
        else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
            ret = 1;
        }
        else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
            ret = 2;
        }
        else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
            ret = 3;
        }
        else if (bf.equals("34")) {
            ret = 6;
        }
        return ret;
    }

    //复制价格
    public static Hmhotelprice copyprice(Hmhotelprice old) {
        Hmhotelprice temp = new Hmhotelprice();
        temp.setAbleornot(old.getAbleornot());
        temp.setAdvancedday(old.getAdvancedday());
        temp.setAllot(old.getAllot());
        temp.setAllotmenttype(old.getAllotmenttype());
        temp.setBf(old.getBf());
        temp.setBreakfasttype(old.getBreakfasttype());
        temp.setCanceladvance(old.getCanceladvance());
        temp.setCanceldesc(old.getCanceldesc());
        temp.setCanceltime(old.getCanceltime());
        temp.setCanceltype(old.getCanceltype());
        temp.setCheckflag(old.getCheckflag());
        temp.setCityid(old.getCityid());
        temp.setContractid(old.getContractid());
        temp.setContractver(old.getContractver());
        temp.setCountryid(old.getCountryid());
        temp.setCountryname(old.getCountryname());
        temp.setCur(old.getCur());
        temp.setDeadline(old.getDeadline());
        temp.setGuamoneytype(old.getGuamoneytype());
        temp.setHotelid(old.getHotelid());
        temp.setId(old.getId());
        temp.setIsallot(old.getIsallot());
        temp.setJlkeyid(old.getJlkeyid());
        temp.setJltime(old.getJltime());
        temp.setMaxday(old.getMaxday());
        temp.setMinday(old.getMinday());
        temp.setNoeditcancel(old.getNoeditcancel());
        temp.setNoedittype(old.getNoedittype());
        temp.setPaytype(old.getPaytype());
        temp.setPrice(old.getPrice());
        temp.setPriceoffer(old.getPriceoffer());
        temp.setProd(old.getProd());
        temp.setQunarprice(old.getQunarprice());
        temp.setRatetype(old.getRatetype());
        temp.setRoomstatus(old.getRoomstatus());
        temp.setRoomtypeid(old.getRoomtypeid());
        temp.setServ(old.getServ());
        temp.setSourcetype(old.getSourcetype());
        temp.setStatedate(old.getStatedate());
        temp.setSupplyid(old.getSupplyid());
        temp.setTicket(old.getTicket());
        temp.setType(old.getType());
        temp.setUpdatetime(old.getUpdatetime());
        temp.setYuliuNum(old.getYuliuNum());
        temp.setZshotelid(old.getZshotelid());
        temp.setZsroomtypeid(old.getZsroomtypeid());
        return temp;
    }
}