package com.ccservice.accountSwitchJob;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.AccountSwitchInitServlet;
import com.ccservice.Util.db.DBHelper;
import com.ccservice.Util.db.DataRow;
import com.ccservice.Util.db.DataTable;
import com.ccservice.Util.file.ExceptionUtil;

public class Testa {

	public static void main(String[] args) {
		accountSwitchInit();
	}
	private static void accountSwitchInit() {
		//获取数据库中的开关状态
    	boolean accountSwitch  =false;
    	String sql = "GetAccountSwitch";
    	try {
			DataTable dataTable = DBHelper.GetDataTable(sql);
			DataRow dataRow = dataTable.GetRow().get(0);
			String accountSwitchStr = dataRow.GetColumnString("C_VALUE");
			System.out.println(accountSwitchStr);
			if (!"".equals(accountSwitchStr) && accountSwitchStr != null) {
				 accountSwitch = Integer.valueOf(accountSwitchStr) == 1?true:false;
			}
			AccountSwitch.setAccountSwitch(accountSwitch);
		} catch (Exception e) {
			ExceptionUtil.writelogByException("获取账号开关状态", e, sql);
		}
    	System.out.println(AccountSwitch.isAccountSwitch());
    	String time = "0/30 * * * * ? ";
		JobDetail jobDetail = new JobDetail("AccountSwitchJob", "AccountSwitchJobGroup",
				AccountSwitchJob.class);
		try {
			CronTrigger trigger = new CronTrigger("AccountSwitchJob", "AccountSwitchJobGroup", time);
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.scheduleJob(jobDetail, trigger);
			scheduler.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
