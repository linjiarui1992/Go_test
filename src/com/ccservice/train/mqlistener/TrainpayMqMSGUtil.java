package com.ccservice.train.mqlistener;


import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.ActiveMQUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.pay.access.ISaveMessage;
import com.ccservice.pay.access.TrainEqualPayDB;
import com.ccservice.train.qunar.QunarOrderMethod;

public class TrainpayMqMSGUtil extends TrainSupplyMethod {
    private String url;

    private String QUEUE_NAME = "";

    public TrainpayMqMSGUtil(String QUEUE_NAME) {
        this.QUEUE_NAME = QUEUE_NAME;
    }

    /**
     * 
     * @param order 订单
     * @param msgtype 发送信息类型
     */
    public void sendGetUrlMQmsg(Trainorder order) {
        this.url = getSysconfigString("activeMQ_url");
        JSONObject jsoseng = new JSONObject();
        try {
            jsoseng.put("type", MQMethod.ORDERGETURL);
            jsoseng.put("orderid", order.getId());
            WriteLog.write("12306_TrainpayMqMSGUtil_GetURL_MQ", jsoseng.toString()); 
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, jsoseng.toString());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 改签获取支付链接
     * @param order 订单
     * @param msgtype 发送信息类型
     */
    public void sendGetGQUrlMQmsg(Trainorder order, Trainorderchange orderchange) {
        this.url = getActiveMQUrl();
        JSONObject jsoseng = new JSONObject();
        try {
            jsoseng.put("type", MQMethod.GQORDERURL);   
            jsoseng.put("orderid", order.getId());
            jsoseng.put("changeorderid", orderchange.getId());
            WriteLog.write("12306_TrainpayMqMSGUtil_GQGetURL_MQ", jsoseng.toString());
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, jsoseng.toString());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            //            JSONObject jsoseng = new JSONObject();
            //            jsoseng.put("type", MQMethod.ORDERGETURL);
            //            jsoseng.put("orderid", 5124);
            //            WriteLog.write("12306_MQ_GetUrl", jsoseng.toString());
            //            ActiveMQUtil.sendMessage("tcp://192.168.0.5:61616", "PayMQ_TrainGetURL", jsoseng.toString());
            //            String[] arg = new String[] { "5219", "5220", "5221", "5222", "5223", "5224", "5225", "5226" };
            //            for (int i = 0; i < arg.length; i++) {
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("type", MQMethod.ORDERPAY);
            jsoseng.put("orderid", 5733);
            jsoseng.put("paymethod", 1);
            jsoseng.put("paytimes", 1);
            WriteLog.write("12306_MQ_PAY_TrainpayMqMSGUtil_MQ", jsoseng.toString());
            ActiveMQUtil.sendMessage("tcp://localhost:61616", "PayMQ_TrainPay", jsoseng.toString());
            //                        ActiveMQUtil
            //                        .sendMessage("tcp://tongchengtrain.hangtian123.com:61616", "PayMQ_TrainPay", jsoseng.toString());

            //            jsoseng.put("type", MQMethod.ORDERGETURL);
            //            jsoseng.put("orderid", "5722");
            //            WriteLog.write("12306_TrainpayMqMSGUtil_GetURL_MQ", jsoseng.toString());
            //            ActiveMQUtil.sendMessage("tcp://localhost:61616", "PayMQ_TrainGetURL", jsoseng.toString());
            System.out.println("王弼");
        }

        //        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 支付mq信息
     */
    public void sendPayMQmsg(Trainorder order, int paytype, int paytimes) {
        this.url = getActiveMQUrl();
        String quenename = QUEUE_NAME;
       
        if (order.getAgentid() == Long.valueOf(getSysconfigString("TaobaoAgentID")).longValue()) {
            url = PropertyUtil.getValue("TaoBaoactiveMQ_url", "train.properties");
            quenename = MQMethod.OrderPay_TaoBaoName;
        }
        long Agentid  = order.getAgentid();
        JSONObject jsoseng = new JSONObject();
        try {
            long orderid = order.getId();
            jsoseng.put("type", MQMethod.ORDERPAY);
            jsoseng.put("orderid", orderid); 
            jsoseng.put("paymethod", paytype); 
            jsoseng.put("AgentId", Agentid);
            jsoseng.put("paytimes", ++paytimes);
            new TrainEqualPayDB().getEqualPayMessage(orderid, jsoseng, new ISaveMessage() {
                @Override
                public Map executeSql(String sql) {
                    List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
                    Map map = null;
                    if (list != null && list.size() > 0) {
                        map = (Map) list.get(0);
                    }
                    return map;
                } 
            },passengerListConvertedToJSON(order.getPassengers()));
            WriteLog.write("12306_MQ_PAY_TrainpayMqMSGUtil_MQ", jsoseng.toString());
            ActiveMQUtil.sendMessage(url, quenename, jsoseng.toString());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 乘客信息的list集合转换成jsonArray
     * @param passengers
     * @return jsonArray
     */
    private JSONArray passengerListConvertedToJSON(List<Trainpassenger> passengers) {
    	JSONArray jsonArray = new JSONArray();
        if (passengers != null) {
            for (Trainpassenger trainpassenger : passengers) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("passengersename", trainpassenger.getName());
                jsonObject.put("passportseidnumber", trainpassenger.getIdnumber());
                jsonArray.add(jsonObject);
            }
        }
        return jsonArray;
	}
    

    /**
     * 支付mq信息
     */
    public void sendPayMQmsgPublic(String mqname, String jsonstring) {
        this.url = PropertyUtil.getValue("payMQ_url", "train.properties");
        String quenename = mqname;
        try {
            WriteLog.write("12306_MQ_PAY_TrainpayMqMSGUtil_MQ", jsonstring);
            ActiveMQUtil.sendMessage(url, quenename, jsonstring);
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     *  改签支付mq信息
     * @param paytype 1 支付宝  2网银
     */
    public void sendGQPayMQmsg(Trainorder order, int paytype, int paytimes, Trainorderchange orderchange) {
        this.url = getActiveMQUrl();
        JSONObject jsoseng = new JSONObject();
        try {
            jsoseng.put("type", MQMethod.GQPayNumberUPDATE_NAME);
            jsoseng.put("orderid", order.getId());
            jsoseng.put("paymethod", paytype);
            jsoseng.put("paytimes", ++paytimes);
            jsoseng.put("changeorderid", orderchange.getId());
            WriteLog.write("12306_GQ_TrainpayMqMSGUtil_MQ", jsoseng.toString());
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, jsoseng.toString());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @Title:       sendQunarOrderMQmsg 
     * @Description: TODO(将去哪儿的订单放入队列) 
     * @param：       @param order
     * @param：       @param paytype
     * @param：       @param paytimes
     * @param：       @param orderchange 
     * @return：              void    返回类型 
     * @author       wangwei
     * @throws
     */
    public void sendQunarOrderMQmsg(QunarOrderMethod qunarOrderMethod, long qunar_agentid, String qunarPayurl) {
        this.url = getSysconfigString("activeMQ_url");
        JSONObject jsoseng = new JSONObject();
        try {
            jsoseng.put("id", qunarOrderMethod.getId());
            jsoseng.put("InterfaceType", qunarOrderMethod.getInterfaceType());
            jsoseng.put("Createtime", qunarOrderMethod.getCreatetime());
            jsoseng.put("Isquestionorder", qunarOrderMethod.getIsquestionorder());
            jsoseng.put("Orderjson", qunarOrderMethod.getOrderjson());
            jsoseng.put("Orderstatus", qunarOrderMethod.getOrderstatus());
            jsoseng.put("Qunarordernumber", qunarOrderMethod.getQunarordernumber());
            jsoseng.put("State12306", qunarOrderMethod.getState12306());
            jsoseng.put("qunar_agentid", qunar_agentid);
            jsoseng.put("qunarPayurl", qunarPayurl);
            WriteLog.write("Qunar_Order", jsoseng.toString());
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, jsoseng.toString());
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void sendQunarWaitOrderMQmsg(long orderId) {
        this.url = getSysconfigString("activeMQ_url");
        try {
            WriteLog.write("Qunar_OrderId", orderId + "");
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, orderId + "");
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQUEUE_NAME() {
        return QUEUE_NAME;
    }

    public void setQUEUE_NAME(String qUEUE_NAME) {
        QUEUE_NAME = qUEUE_NAME;
    }

}
