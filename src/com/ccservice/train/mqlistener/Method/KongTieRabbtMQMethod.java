package com.ccservice.train.mqlistener.Method;

import java.io.IOException;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ActiveMQUtil;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.mq.producer.exception.MQProducerException;
import com.ccservice.mq.producer.method.MQSendMessageMethod;
import com.ccservice.rabbitmq.util.RabbitMQUtil;

/**
 * 
 * MQ迁移        阿里 ——————>到美团MQ
 * @author RRRRRR
 * @time 2016年11月16日 上午11:01:34
 */
public class KongTieRabbtMQMethod extends TrainSupplyMethod {

    /**
     * 
     * @author 之至
     * @time 2016年11月19日 下午5:57:26
     * @Description 发mq  循环取消订单
     */
    public void cannelOrderMQ(String activeMQ_url, String QUEUE_NAME, String json) {
        int isMeiTuan = getIsUserRabbitMQ();
        String queueName = QUEUE_NAME;
        if (isMeiTuan == 1) {//美团云MQ
            try {
                int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
                MQSendMessageMethod.sendOnemessageUnException(json, queueName, pingTaiType);
                //RabbitMQUtil.sendOnemessageV2(json, queueName);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_TrainDeduction-e", e);
            }
        }
        else {
            ActiveMQUtil.sendMessage(activeMQ_url, QUEUE_NAME, json);
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年11月23日 下午3:37:19
     * @Description 改签扣款
     */
    public void rabbitGQDeduction(String url, String MQ_NAME, String json) {
        int isMeiTuan = getIsUserRabbitMQ();
        String queueName = MQ_NAME;
        if (isMeiTuan == 1) {//美团云MQ
            try {
                RabbitMQUtil.sendOnemessageV2(json, queueName);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("Rabbit_QueueMQ_TrainDeduction-e", e);
            }
        }
        else {
            ActiveMQUtil.sendMessage(url, queueName, json);
        }
    }

    /**
     * 
     * @author zlx
     * @time 2016年11月17日 下午4:49:40
     * @Description TODO
     * @param 发送扣款MQ
     */
    public void activeMQDeduction(String json) {
        int isMeiTuan = getIsUserRabbitMQ();
        String queueName = PropertyUtil.getValue("Rabbit_QueueMQ_Deduction", "rabbitMQ.properties");
        int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
        if (isMeiTuan == 1) {//美团云MQ
            try {
                //RabbitMQUtil.sendOnemessageV2(json, queueName);
                MQSendMessageMethod.sendOnemessage(json, queueName, pingTaiType);
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQDeduction_IOException", e, queueName
                        + "--->" + pingTaiType + "--->" + json);
            }
            catch (NullPointerException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQDeduction_NullPointerException", e,
                        queueName + "--->" + pingTaiType + "--->" + json);
            }
            catch (MQProducerException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQDeduction_MQProducerException", e,
                        queueName + "--->" + pingTaiType + "--->" + json);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQDeduction_Exception", e, queueName
                        + "--->" + pingTaiType + "--->" + json);
            }
        }
        else {//阿里云MQ
            try {
                String url = getActiveMQUrl();
                String MQ_NAME = "QueueMQ_TrainDeduction";
                ActiveMQUtil.sendMessage(url, MQ_NAME, json.toString());
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("QueueMQ_TrainDeduction-e", e);
            }
        }
    }

    /**
     * 
     * @author zlx
     * @time 2016年11月17日 下午4:49:12
     * @Description TODO
     * @param 发送审核MQ
     */
    public void activeMQquery(String json) {
        int isMeiTuan = getIsUserRabbitMQ();
        String queueName = PropertyUtil.getValue("Rabbit_QueueMQ_QueryOrder", "rabbitMQ.properties");
        int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
        if (isMeiTuan == 1) {//美团云MQ
            try {
                //RabbitMQUtil.sendOnemessageV2(json, queueName);
                MQSendMessageMethod.sendOnemessage(json, queueName, pingTaiType);
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQquery_IOException", e, queueName
                        + "--->" + pingTaiType + "--->" + json);
            }
            catch (NullPointerException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQquery_NullPointerException", e,
                        queueName + "--->" + pingTaiType + "--->" + json);
            }
            catch (MQProducerException e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQquery_MQProducerException", e,
                        queueName + "--->" + pingTaiType + "--->" + json);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("KongTieRabbtMQMethod_activeMQquery_Exception", e, queueName + "--->"
                        + pingTaiType + "--->" + json);
            }
        }
        else {//阿里云
            String MQ_URL = getActiveMQUrl();
            String QUEUE_NAME = getSysconfigString("QUEUE_NAME");
            try {
                ActiveMQUtil.sendMessage(MQ_URL, QUEUE_NAME, json);
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException(QUEUE_NAME, e);
            }
        }
    }

    /**
     * 
     * @author zlx
     * @time 2016年11月16日 上午11:09:25
     * @Description TODO
     *  发送下单MQ
     */
    public void activeMQroordering(long id) {
        String QUEUE_NAME = PropertyUtil.getValue("QueueMQ_trainorder_waitorder_orderid", "Train.properties");
        String Rabbit_QUEUE_NAME = PropertyUtil.getValue("Rabbit_QueueMQ_trainorder_waitorder_orderid",
                "rabbitMQ.properties");
        String activeMQ_url = PropertyUtil.getValue("activeMQ_url", "train.TrainCreateOrder.properties");
        //        int isMeiTuanMQ = Integer.valueOf(PropertyUtil.getValue("isMeiTuanMQ","rabbitMQ.properties"));
        int isMeiTuanMQ = getIsUserRabbitMQ();
        try {
            if (isMeiTuanMQ == 1) {//美团云MQ 1:使用  2:不使用
                WriteLog.write("Rabbit_下单测试", isMeiTuanMQ + "");
                //RabbitMQUtil.sendOnemessage(id, Rabbit_QUEUE_NAME);
                int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
                MQSendMessageMethod.sendOnemessage(id + "", Rabbit_QUEUE_NAME, pingTaiType);
            }
            else {
                ActiveMQUtil.sendMessage(activeMQ_url, QUEUE_NAME, id + "");
            }
        }
        catch (Exception e) {
            WriteLog.write("Rabbit_TongchengSupplyMethod_activeMQroordering", e.getMessage());
            try {
                if (isMeiTuanMQ == 1) {
                    //RabbitMQUtil.sendOnemessage(id, Rabbit_QUEUE_NAME);
                    int pingTaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));
                    MQSendMessageMethod.sendOnemessage(id + "", Rabbit_QUEUE_NAME, pingTaiType);
                }
                else {
                    ActiveMQUtil.sendMessage(activeMQ_url, QUEUE_NAME, id + "");
                }
            }
            catch (Exception e1) {
                WriteLog.write("Rabbit_tongchengsupplyMethod_activeMQroordering_e1", e1.getMessage());
            }
        }
    }

    /**
     * 
     * @author 之至
     * @time 2016年11月25日 下午2:22:57
     * @Description 排队队列 发送 rabbit 或者   active   
     * @param url
     * @param queueName
     * @param orderid
     */
    public void rabbitSendOrderPaiDui(String url, String queueName, long orderid) {
        int isMeiTuan = getIsUserRabbitMQ();
        if (isMeiTuan == 1) {//美团云
            RabbitMQUtil.sendOnemessage(orderid, queueName);
        }
        else {
            ActiveMQUtil.sendMessage(url, queueName, orderid + "");
        }
    }

    /**
     * 
     * @author zlx
     * @time 2016年11月16日 上午11:29:22
     * @Description TODO
     *获取DB配置文件开关
     */
    public int getIsMeiTuanMQ() {
        int result = 0;
        try {
            result = Integer.parseInt(getSysconfigStringbydb("isMeiTuanMQ"));
        }
        catch (Exception e) {
        }
        return result;
    }

    /**
     * 
     * @author 之至
     * @time 2016年11月23日 下午3:34:51
     * @Description 读取配置文件
     * @return
     */
    public int getIsUserRabbitMQ() {
        int num = 0;
        try {
            num = Integer.valueOf(PropertyUtil.getValue("isMeiTuanMQ", "rabbitMQ.properties"));
        }
        catch (Exception e) {
        }
        return num;
    }

    /**
     * 获取MQ的地址
     * 
     * @return
     * @time 2015年3月30日 下午5:30:38
     * @author chendong
     */
    protected String getActiveMQUrl() {
        String url = getSysconfigString("activeMQ_url");
        return url;
    }

}
