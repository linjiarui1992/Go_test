package com.ccservice.train.mqlistener;

import java.util.Random;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.MessageListener;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.inter.job.train.TrainSupplyMethod;

/**
 * 异步改签确认 
 * @author WH
 */

public class TrainConfirmChangeMessageListener extends TrainSupplyMethod implements MessageListener {

    private long changeId;

    private String changeNoticeResult;

    public void onMessage(Message message) {
        try {
            changeNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("开始异步改签确认", System.currentTimeMillis() + ":changeNoticeResult:" + changeNoticeResult);
            try {
                changeId = Long.parseLong(changeNoticeResult);
            }
            catch (Exception e) {
                changeId = 0;
            }
            //改签占座操作
            if (changeId > 0) {
                RepOperate();
            }
        }
        catch (Exception e) {
        }
    }

    private void RepOperate() {
        int random = new Random().nextInt(1000000);
        //改签信息
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        //异步改签
        int isAsync = trainOrderChange == null || trainOrderChange.getConfirmIsAsync() == null ? 0 : trainOrderChange
                .getConfirmIsAsync();
        //改签订单不存在、非异步
        if (isAsync != 1 || trainOrderChange.getOrderid() <= 0) {
            return;
        }
        //状态判断
        int tcstatus = trainOrderChange.getTcstatus();
        int isQuestionChange = trainOrderChange.getIsQuestionChange() == null ? 0 : trainOrderChange
                .getIsQuestionChange();
        float changePrice = trainOrderChange.getTcprice();
        //非等待下单
        if (tcstatus != Trainorderchange.CHANGEWAITPAY || isQuestionChange != 0 || changePrice <= 0) {
            WriteLog.write("重复进入改签确认队列", random + ":改签ID:" + changeId);
            return;
        }
        //更新改签
        int C_TCSTATUS = Trainorderchange.CHANGEPAYING;
        int C_STATUS12306 = Trainorderchange.ORDEREDPAYING;
        String updateSql = "update T_TRAINORDERCHANGE set C_TCSTATUS = " + C_TCSTATUS + ", C_STATUS12306 = "
                + C_STATUS12306 + " where ID = " + changeId + " and C_TCSTATUS = " + Trainorderchange.CHANGEWAITPAY;
        //更新结果
        int updateResult = Server.getInstance().getSystemService().excuteAdvertisementBySql(updateSql);
        //更新失败
        if (updateResult != 1) {
            WriteLog.write("改签占座队列异常", random + ":改签ID:" + changeId + ":更新改签:" + updateResult);
            return;
        }
        //异步确认
        Server.getInstance().getTrain12306Service().AsyncChangeConfirm(trainOrderChange);
    }

}
