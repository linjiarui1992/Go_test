package com.ccservice.train.mqlistener;

import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.rule.TaobaoTrainRule;

/**
 * 支付处理消费者
 * @author wzc
 *
 */
public class TrainorderPayMessageListenerNew extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private long orderid;

    private String alipayurlset;

    private long order_timeout_time = 10 * 60 * 1000l;//淘宝支付前给后续操作的预留时间

    public TrainorderPayMessageListenerNew(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

    @Override
    public void onMessage(Message message) {
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            int paycount = Integer.valueOf(getSysconfigString("paycount")) == -1 ? 3 : Integer
                    .valueOf(getSysconfigString("paycount"));//总支付次数
            int t = new Random().nextInt(10000);
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ", t + ":" + orderNoticeResult);
            int paytype = Integer.parseInt(getSysconfigString("getpayurltype"));
            Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Long.valueOf(orderid));
            //TODO 淘宝超时 不再支付
            if (taobaoTimeout(trainorder)) {
                return;
            }
            String callbackurl = PropertyUtil.getValue("pay_callback", "train.properties");
            if (trainorder.getOrderstatus() == Trainorder.WAITISSUE
                    && trainorder.getState12306() == Trainorder.ORDEREDWAITPAY) {//等待出票，12306等待支付
                if (paytype == 2) {//支付宝支付
                    System.out.println("支付宝支付:" + this.orderid);
                    autoalipayPay(trainorder, t, paycount, callbackurl);
                }
                else if (paytype == 1) {//网银支付
                    System.out.println("网银支付:" + this.orderid);
                    autoUnionPay(trainorder, t, paycount, callbackurl);
                }
            }
            else {
                WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ", t + "订单id:" + trainorder.getId()
                        + ":不符合支付条件,订单状态：" + trainorder.getOrderstatus() + ",12306状态：" + trainorder.getState12306());
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

    /**
     * 银联自动支付
     * @param trainorder 订单
     * @param t 随机数
     * @param 访问地址url
     * @param times 支付次数
     * @return
     */
    public boolean autoUnionPay(Trainorder trainorder, int t, int times, String callbackurl) {
        boolean flag = false;
        String payurl = trainorder.getAutounionpayurl();
        if (payurl != null && !"".equals(payurl)) {
            JSONObject jso = new JSONObject();
            jso.put("orderid", trainorder.getId());
            jso.put("paytimes", times);
            jso.put("callbackurl", callbackurl);//回调地址
            jso.put("postdata", payurl);
            jso.put("paytype", "1");
            jso.put("ordernumber", trainorder.getOrdernumber());
            trainRC(trainorder.getId(), "发送支付请求");
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_wangyin", t + ":" + trainorder.getOrdernumber()
                    + ":" + jso.toJSONString());
        }
        else {
            new TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_wangyin", t + ":" + trainorder.getOrdernumber()
                    + ":发送支付链接队列。");
        }
        return flag;
    }

    /**
     * 支付宝支付方法
     * @param trainorder 订单
     * @param i 支付索引
     * @param url 支付ur
     * @param t 标识
     * @param times 支付此时
     * @return
     */
    public boolean autoalipayPay(Trainorder trainorder, int t, int times, String callbackurl) {
        boolean flag = false;
        String payurl = trainorder.getChangesupplytradeno();
        if ((payurl != null) && (!("".equals(payurl)))) {
            JSONObject jso = new JSONObject();
            jso.put("orderid", trainorder.getId());
            jso.put("paytimes", times);
            jso.put("callbackurl", callbackurl);//回调地址
            jso.put("postdata", payurl);
            jso.put("paytype", "2");
            jso.put("ordernumber", trainorder.getOrdernumber());
            trainRC(trainorder.getId(), "发送支付请求");
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zhifubao", t + ":" + trainorder.getOrdernumber()
                    + ":" + jso.toJSONString());
        }
        else {
            new TrainpayMqMSGUtil(MQMethod.ORDERGETURL_NAME).sendGetUrlMQmsg(trainorder);
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zhifubao", t + ":" + trainorder.getOrdernumber()
                    + ":发送支付链接队列。");
        }
        return flag;
    }

    /**
     * 支付预警
     */
    public void payclock(String AliPayUserName, Float balance, int t) {
        try {
            WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zijinzhanghu", t + ":" + AliPayUserName + ",余额："
                    + balance);
            if (balance >= 0) {
                String sqlmoney = "SELECT C_CLOCKMONEY,C_BANLANCE FROM T_PAYACCOUNTINFO WHERE C_ACCOUNT='"
                        + AliPayUserName + "'";
                List yuetixinglistt = Server.getInstance().getSystemService().findMapResultBySql(sqlmoney, null);
                float C_CLOCKMONEY = 20000;
                float C_BANLANCE = 0;
                if (yuetixinglistt.size() > 0) {
                    Map map = (Map) yuetixinglistt.get(0);
                    C_CLOCKMONEY = Float.valueOf(map.get("C_CLOCKMONEY").toString());
                    C_BANLANCE = Float.valueOf(map.get("C_BANLANCE").toString());
                    WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zijinzhanghu", t + ":支付前：" + C_BANLANCE
                            + ",支付后：" + balance + ",提醒金额：" + C_CLOCKMONEY);
                    if (C_BANLANCE > C_CLOCKMONEY && balance < C_CLOCKMONEY) {
                        String content = AliPayUserName + "账户余额" + balance + ",请尽快充值。";
                        WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zijinzhanghu", t + ":" + content);
                        String tetctelphonenum = getSysconfigString("payremandtel");
                        sendSmspublic(content, tetctelphonenum, 46);
                    }
                }
                else {
                    String content = "账户数据库没找到支付账号" + AliPayUserName;
                    WriteLog.write("pay_TrainorderPayMessageListenerNew_MQ_zijinzhanghu", t + ":" + content);
                    sendSmspublic(content, "15811073432", 46);
                }
                String sql = "update T_PAYACCOUNTINFO set C_BANLANCE=" + balance + ",C_LASTUPDATETIME='"
                        + TimeUtil.gettodaydate(4) + "' where C_ACCOUNT='" + AliPayUserName + "'";
                Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            }
        }
        catch (Exception e) {
        }
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 淘宝是否超时,超时回调淘宝出票失败,返回true
     * @param trainorder
     * @return
     * @time 2015年3月31日 上午5:35:39
     * @author fiend
     */
    public boolean taobaoTimeout(Trainorder trainorder) {
        try {
            int interfacetype = trainorder.getInterfacetype() != null && trainorder.getInterfacetype() > 0 ? trainorder
                    .getInterfacetype() : Server.getInstance().getInterfaceTypeService()
                    .getTrainInterfaceType(trainorder.getId());
            if (TrainInterfaceMethod.TAOBAO == interfacetype) {
                try {
                    String pay_order_timeout_time_str = getSysconfigString("pay_order_timeout_time");
                    this.order_timeout_time = Long.valueOf(pay_order_timeout_time_str) * 60 * 1000;
                }
                catch (Exception e) {
                    this.order_timeout_time = 10 * 60 * 1000L;
                }
            }
            if (TaobaoTrainRule.isTimeout(trainorder, interfacetype, this.order_timeout_time)) {
                refundTaobao(trainorder, "支付超时", "下单时间已超过规定时间");
                return true;
            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getAlipayurlset() {
        return alipayurlset;
    }

    public void setAlipayurlset(String alipayurlset) {
        this.alipayurlset = alipayurlset;
    }

}
