package com.ccservice.train.tbbespeakpay;

import java.io.IOException;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.train.mqlistener.TrainpayMqMSGUtilNew;

/**
 * 淘宝抢票支付
 * @author guozhengju
 * 2016-09-23
 */
public class TrainTaoBaoBespeakPay extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TrainTaoBaoBespeakPay() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain; charset=utf-8");
        String orderid=request.getParameter("orderId");
        String urltype=request.getParameter("urltype");
        WriteLog.write("TB抢票支付测试", "==orderid"+orderid+";==urltype:"+urltype);
        try {
            new TrainpayMqMSGUtilNew().sendPayMQmsgByUrltype(Long.parseLong(orderid), Long.parseLong(urltype));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
	}

}
