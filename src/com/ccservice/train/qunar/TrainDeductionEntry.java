package com.ccservice.train.qunar;

public class TrainDeductionEntry {
    private long TimeBefore = 0;

    private String TempValue;

    public long getTimeBefore() {
        return TimeBefore;
    }

    public void setTimeBefore(long timeBefore) {
        TimeBefore = timeBefore;
    }

    public String getTempValue() {
        return TempValue;
    }

    public void setTempValue(String tempValue) {
        TempValue = tempValue;
    }

}
