package com.ccservice.train.rule;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.train.TrainInterfaceMethod;

/**
 * 美团火车票规则类
 * @author fiend
 *
 */
public class MeituanTrainRule {

    /**
     * 美团模式下单成功后是否可以支付规则
     * @param trainorder
     * @param interfacetype
     * @return
     * @author fiend
     */
    public static boolean isCanGenerate(Trainorder trainorder, int interfacetype) {
        //如果是美团(Agentid()==67)并且interface是==7||3 就核验价格
        //如果是美团模式，或者是美团并且是先占座模式就比对。
        if (TrainInterfaceMethod.MEITUAN == interfacetype || (67 == trainorder.getAgentid() && 3 == interfacetype)) {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                    if (trainticket.getPrice() > trainticket.getPayprice()) {
                        return false;
                    }
                }
            }
        }
        else {
            return true;
        }
        return true;
    }

}
