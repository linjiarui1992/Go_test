package com.ccservice.train.rule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.inter.job.WriteLog;

/**
 * 去哪儿火车票规则类
 * @author fiend
 *
 */
public class QunarTrainRule {
    public static void main(String[] args) {
        try {
            List list = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(" sp_QunarTrainRule_SELECT @AgentId=80");
            System.out.println(list);
            //            if (list.size() > 0) {
            //                payRule = Integer.valueOf(((Map) list.get(0)).get("PayRule").toString());
            //            }
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    //QUNAR支付失败自己支付
    public static final int QUNAR_PAY_FAIL_SYS_PAY = 1;

    //QUNAR支付失败拒单
    public static final int QUNAR_PAY_FAIL_REFUSE = 2;

    //自己支付
    public static final int SYS_PAY = 3;

    /**
     * 说明：获取订单的JSONObject对象
     * @param url
     * @return  jsonObject
     * @time 2014年8月30日 上午11:05:53
     * @author yinshubin
     */
    public static JSONObject getJson(URL url) {
        JSONObject jsonObject = null;
        URLConnection con = null;
        try {
            con = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String result = "";
            jsonObject = null;
            while (true) {
                result = br.readLine();
                if (result == null) {
                    break;
                }
                else {
                    //                    jsonObject = JSONObject.fromObject(result);
                    jsonObject = JSONObject.parseObject(result);
                    WriteLog.write("qunartrainorder", jsonObject + "");
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                con.getInputStream().close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    /**
     * 是否是
     * QUNAR支付失败自己支付
     * @param payrual
     * @return
     * @time 2015年11月26日 上午11:43:41
     * @author fiend
     */
    public static boolean isQunarPayFailSysPay(int payrual) {
        return payrual == QUNAR_PAY_FAIL_SYS_PAY;
    }

    /**
     * 是否是
     * QUNAR支付失败拒单
     * @param payrual
     * @return
     * @time 2015年11月26日 上午11:43:41
     * @author fiend
     */
    public static boolean isQunarPayFailRufuse(int payrual) {
        return payrual == QUNAR_PAY_FAIL_REFUSE;
    }

    /**
     * 是否是
     * 直接自己支付
     * @param payrual
     * @return
     * @time 2015年11月26日 上午11:43:41
     * @author fiend
     */
    public static boolean isSysPay(int payrual) {
        return payrual == SYS_PAY;
    }

    /**
     * qunar坐席转换成DB坐席
     * @param seat
     * @return
     */
    public static String changeSeatQunar2DB(String seat) {
        if (seat.equals("0")) {
            return "无座";
        }
        if (seat.equals("1")) {
            return "硬座";
        }
        if (seat.equals("2")) {
            return "软座";
        }
        if (seat.equals("3")) {
            return "一等软座";
        }
        if (seat.equals("4")) {
            return "二等软座";
        }
        if (seat.equals("7")) {
            return "硬卧";
        }
        if (seat.equals("9")) {
            return "软卧";
        }
        if (seat.equals("11")) {
            return "高级软卧";
        }
        if (seat.equals("12")) {
            return "特等座";
        }
        if (seat.equals("13")) {
            return "商务座";
        }
        return "硬座";
    }

}
