package com.ccservice.train.order;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * @author wzc
 */
public class PayCallBackMethod extends TrainSupplyMethod {
    /**
     * main测试
     * @param args
     */
    public static void main(String[] args) {
        JSONObject parjson = new JSONObject();
        parjson.put("orderid", "16226");
        parjson.put("paymethod", "alipayurl2");
        System.out.println(new PayCallBackMethod().parseOrderMethod(parjson.toJSONString(), 50));
    }

    /**
     * 支付服务器分配处理支付方式
     * @param parjson
     * @return
     */
    public String parseOrderMethod(String str, int t) {
        JSONObject parjson = JSONObject.parseObject(str);
        String orderid = parjson.getString("orderid");
        String paymethod = parjson.getString("paymethod");
        trainRC(Long.valueOf(orderid), "开始支付");
        JSONObject obj = new JSONObject();
        WriteLog.write("pay_PayCallBackMethod", t + ":支付方式请求:" + str);
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(Long.valueOf(orderid));
        if (trainorder.getOrderstatus() == Trainorder.WAITISSUE
                && trainorder.getState12306() == Trainorder.ORDEREDWAITPAY) {//等待出票，12306等待支付
            obj.put("payflag", "true");
            if (trainorder.getSupplyaccount() != null && trainorder.getSupplyaccount().contains("alipayurl")) {
                String paymode = trainorder.getSupplyaccount().split("/")[1];
                String payaddress = getSysconfigString(paymode);
                obj.put("payaddress", payaddress);
                obj.put("paymode", paymode);
            }
            else {
                trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/" + paymethod);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                obj.put("paymode", "");
                obj.put("payaddress", "");
            }
        }
        else {
            obj.put("payflag", "false");
            obj.put("paymode", "");
            obj.put("payaddress", "");
        }
        WriteLog.write("pay_PayCallBackMethod", t + ":支付方式响应:" + obj.toJSONString());
        return obj.toJSONString();
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }
}
