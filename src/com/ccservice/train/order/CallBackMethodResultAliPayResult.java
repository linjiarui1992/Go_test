package com.ccservice.train.order;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.train.mqlistener.MQMethod;
import com.ccservice.train.mqlistener.TrainActiveMQ;

/**
 * 支付结果处理
 * @author wzc
 *
 */
public class CallBackMethodResultAliPayResult extends TrainSupplyMethod {
    /**
     * 支付结果处理
     * @param obj
     * @return
     */
    public String parsePayResult(String result, long orderid, int t, String payset, String paymethodtype, String payMethodNew) {
        WriteLog.write("pay_CallBackMethodResultPayResult", t + ":请求返回:" + result + ",订单id:" + orderid + ",payset:"
                + payset + ":payMethodNew=" + payMethodNew);
        boolean flag = false;
        String haddleresult = "false";
        String AliPayUserNameTemp = "";
        String AliPayWnumberTemp = "";
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        try {
            trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/" + payset);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if ("".equals(result)) {
            changeQuestionOrder(trainorder, "支付超时");
            return haddleresult;
        }
        else if (!(result.contains("status"))) {
            changeQuestionOrder(trainorder, "支付异常");
            return haddleresult;
        }
        //支付成功
        //{"status":true,"info":"Success","OrderID":"T150204070050667635522",
        //"AliTradeNo":"2015020421001004270098590118","AliOrderID":"0204b93360d1ce49d45aa86015463277",
        //"AliPayUserName":"hangtianhy002@126.com","out_trade_no":"","Amount":29.5,"Banlance":42462.4,
        //"RefundAmount":0.0,"orderstatus":"支付成功","PayRetUrl":"","PayRetParam":""}
        //支付失败
        //{"status":false,"info":"订单状态不对暂时不能支付","OrderID":"T150204101104367736741"}
        if (result.contains("status")) {
            JSONObject obj = JSONObject.parseObject(result);
            boolean status = obj.getBoolean("status");
            //查询失败成功信息
            String msg = "";
            if (obj.containsKey("info")) {
                msg = obj.getString("info");
            }
            if (obj.containsKey("AliPayUserName")) {
                AliPayUserNameTemp = obj.getString("AliPayUserName");//当前支付宝账号
            }
            if (obj.containsKey("WNumber")) {
                AliPayWnumberTemp = obj.getString("WNumber");//当前支付宝账号
            }
            if (status) {
                String liushuihao = obj.getString("AliTradeNo");
                String state = "";
                if (obj.containsKey("orderstatus")) {
                    state = obj.getString("orderstatus");
                }
                String AliPayUserName = "";
                if (obj.containsKey("AliPayUserName")) {
                    AliPayUserName = obj.getString("AliPayUserName");//当前支付宝账号
                }

                //解析银联参数成功  已经有交易号  必须返回RunOK才能确认已经支付
                if (liushuihao != null && !"".equals(liushuihao) && liushuihao.length() > 0 && "支付成功".equals(state)) {
                    flag = true;

                    try {
                    	String interfaceOrderNumber = trainorder.getQunarOrdernumber();
                    	Integer PayMethod = Integer.valueOf(payMethodNew);
                    	// 更新正式的支付方式
    					String updatePayMethodSql = "[dbo].[sp_TrainOrderInfo_insert_PayMethod]@interfaceOrderNumber = '"
    							+ interfaceOrderNumber + "',@PayMethod = " + PayMethod;
    					Server.getInstance().getSystemService().findMapResultByProcedure(updatePayMethodSql);
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
                    
                    String sqlpaytime = "delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                            + ";insert T_TRAINREFINFO(C_ORDERID,C_PAYSUCCESSTIME) values(" + trainorder.getId() + ",'"
                            + TimeUtil.gettodaydate(4) + "')";
                    List list = Server.getInstance().getSystemService().findMapResultBySql(sqlpaytime, null);
                    WriteLog.write("pay_CallBackMethodResultPayResult", t + ":" + sqlpaytime + ":" + liushuihao);
                    trainorder.setSupplytradeno(liushuihao);
                    trainorder.setAutounionpayurlsecond(AliPayUserName);//使用的银行卡号
                    trainorder.setState12306(Trainorder.ORDEREDPAYED);//更新12306订单为  已支付订单
                    trainorder.setPaysupplystatus(0);
                    trainRC(trainorder.getId(), "支付成功");
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    WriteLog.write("pay_CallBackMethodResultPayResult", t + ":" + trainorder.getOrdernumber() + ":"
                            + msg);
                    new TrainActiveMQ(MQMethod.QUERY, trainorder.getId(), System.currentTimeMillis(),
                            System.currentTimeMillis(), 0).sendqueryMQ();
                    WriteLog.write("pay_CallBackMethodResultPayResult", t + ":更新结束");
                    haddleresult = "true";
                }
                else if (state != null && "支付失败".equals(state)) {
                    changeQuestionOrder(trainorder, "支付返回失败");
                }
                else {
                    changeQuestionOrder(trainorder, "支付需审核");
                }
            }
            else {
                WriteLog.write("pay_CallBackMethodResultPayResult", t + ":" + trainorder.getOrdernumber() + ":" + msg);
                changeQuestionOrder(trainorder, "支付返回失败");
            }
        }
        else {
            changeQuestionOrder(trainorder, "支付需审核");
        }
        if (!flag) {
            String sql = "UPDATE dbo.T_TRAINORDER SET C_AUTOUNIONPAYURLSECOND='" + AliPayUserNameTemp
                    + "',C_SUPPLYTRADENO='" + AliPayWnumberTemp + "' WHERE ID=" + orderid;
            Server.getInstance().getSystemService().excuteGiftBySql(sql);
        }
        return haddleresult;
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }

    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

}
