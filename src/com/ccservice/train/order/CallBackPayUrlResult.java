package com.ccservice.train.order;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainInterfaceMethod;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 处理获取支付链接
 */
public class CallBackPayUrlResult extends TrainSupplyMethod {

    /**
     *订单超时处理 
     */
    public String payTimeOut(String result, long orderid, int t) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        WriteLog.write("CallBackPayUrlResult", trainorder.getOrdernumber() + ":timeout");
        changeQuestionOrder(trainorder, "订单支付超时");
        int interfacetype = trainorder.getInterfacetype() != null && trainorder.getInterfacetype() > 0
                ? trainorder.getInterfacetype()
                : Server.getInstance().getInterfaceTypeService().getTrainInterfaceType(trainorder.getId());
        if (TrainInterfaceMethod.TAOBAO == interfacetype) {
            refundTaobao(trainorder, "支付超时", "下单时间已超过规定时间");
        }
        return "";
    }

    /**
     * 支付结果处理
     * @param obj
     * @return
     */
    public String parsePayUrlResult(String result, long orderid, int t) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        if (result != null && "success".equals(result)) {
            trainRC(orderid, "获取链接成功");
        }
        else {
            Trainorder o = new Trainorder();
            o.setId(orderid);
            changeQuestionOrder(o, "获取链接失败[" + result + "]");
            if (trainorder.getOrderstatus() == 2) {
            }
            else {
                WriteLog.write("CallBackPayUrlResult",
                        trainorder.getOrdernumber() + ":stateexception:" + trainorder.getOrderstatus());
            }
        }
        return "success";
    }

    /**
     * 记录日志
     * @param trainorderid
     * @param msg
     */
    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 变成问题订单
     * @param trainorderid
     * @param msg
     */
    public void changeQuestionOrder(Trainorder trainorder, String msg) {
        trainorder.setState12306(Integer.valueOf(7));
        trainorder.setIsquestionorder(Integer.valueOf(2));
        trainorder.setPaysupplystatus(Integer.valueOf(2));
        try {
            trainRC(trainorder.getId(), msg);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
        }
    }
}
