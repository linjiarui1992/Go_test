package com.ccservice.elong.hoteldb;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.elong.base.NorthBoundAPIServiceStub;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.ElongRequestHead;

/**
 * 酒店价格查询接口
 * 
 * @author 师卫林
 * 
 */
public class GetHotelPrice {

	public static void main(String[] args) throws RemoteException, SQLException {
		List<Hotel> hotels=Server.getInstance().getHotelService().findAllHotel("where c_sourcetype=1 and c_state=3", "", -1, 0);
		for (int i = 0; i < hotels.size(); i++) {
			getHotelPrice("", hotels.get(i).getHotelcode(), "");
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void getHotelPrice(String HotelName, String HotelId, String CityId) throws RemoteException, SQLException {
		NorthBoundAPIServiceStub stub = new NorthBoundAPIServiceStub();
		NorthBoundAPIServiceStub.GetHotelConditionForGetHotelList conditionForGetHotelList = new NorthBoundAPIServiceStub.GetHotelConditionForGetHotelList();
		NorthBoundAPIServiceStub.GetHotelListRequest request = new NorthBoundAPIServiceStub.GetHotelListRequest();
		NorthBoundAPIServiceStub.GetHotelListResponseE response = new NorthBoundAPIServiceStub.GetHotelListResponseE();
		NorthBoundAPIServiceStub.GetHotelList getList = new NorthBoundAPIServiceStub.GetHotelList();

		// 入住时间
		Calendar CheckInDate = DateSwitch.SwitchCalendar2(DateSwitch.CatchDay());
		// 离开时间
		Calendar CheckOutDate = DateSwitch.SwitchCalendar2(DateSwitch.CatchLaterDay());

		conditionForGetHotelList.setCheckInDate(CheckInDate);
		conditionForGetHotelList.setCheckOutDate(CheckOutDate);
		conditionForGetHotelList.setStartLongitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setStartLatitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setEndLatitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setEndLongitude(BigDecimal.valueOf(0));
		conditionForGetHotelList.setOpeningDate(DateSwitch.SwitchCalendar("0001-01-01 00:00:00"));
		conditionForGetHotelList.setDecorationDate(DateSwitch.SwitchCalendar("0001-01-01 00:00:00"));
		conditionForGetHotelList.setCityId(CityId);
		conditionForGetHotelList.setHotelId(HotelId);
		conditionForGetHotelList.setHotelName(HotelName);
		conditionForGetHotelList.setRatePlanID(0);
		conditionForGetHotelList.setRoomTypeID("");
		conditionForGetHotelList.setStarCode("");
		conditionForGetHotelList.setPositionModeCode("");
		conditionForGetHotelList.setDistrictId("");
		conditionForGetHotelList.setRadius(0);
		conditionForGetHotelList.setCommercialLocationId("");
		conditionForGetHotelList.setLandmarkLocationID("");
		conditionForGetHotelList.setRoomAmount(0);
		conditionForGetHotelList.setOrderByCode("");
		conditionForGetHotelList.setOrderTypeCode("");
		conditionForGetHotelList.setPageIndex(0);
		conditionForGetHotelList.setMaxRows(0);
		conditionForGetHotelList.setCurrencyCode("");

		request.setRequestHead(ElongRequestHead.getRequestHead(""));
		request.setGetHotelCondition(conditionForGetHotelList);

		getList.setGetHotelListRequest(request);

		response = stub.getHotelList(getList);
		// System.out.println("结果代码:"+response.getGetHotelListResult().getResponseHead().getResultCode());
		// System.out.println("结果信息:"+response.getGetHotelListResult().getResponseHead().getResultMessage());
		NorthBoundAPIServiceStub.HotelForGetHotelList[] hotelLists = response.getGetHotelListResult().getHotels().getHotel();
		if (hotelLists != null && hotelLists.length > 0) {
			System.out.println(":::::::" + hotelLists.length);
			System.out.println("getHotelPrice执行中.....");
			if (hotelLists != null && hotelLists.length > 0) {
				System.out.println("getHotelPrice执行中!!!!");
				for (int i = 0; i < hotelLists.length; i++) {
					NorthBoundAPIServiceStub.HotelForGetHotelList hotelList = hotelLists[i];
					System.out.println("开始创建Hotel对象....");
					Hotel hotel = new Hotel();
					// 酒店名称
					System.out.println("酒店名称:" + hotelList.getHotelName());
					hotel.setName(hotelList.getHotelName());
					// 酒店ID
					hotel.setHotelcode(hotelList.getHotelId());
					System.out.println("酒店ID:" + hotel.getHotelcode());
					// 酒店最低价
					hotel.setStartprice(hotelList.getLowestPrice().doubleValue());
					System.out.println("酒店最低价:" + hotel.getStartprice());
					NorthBoundAPIServiceStub.RoomForGetHotelList[] roomLists = hotelList.getRooms().getRoom();
					if (roomLists != null&& roomLists.length > 0) {
						
					}else{
						hotel.setState(2);
						hotel.setStatedesc("酒店可用,但是艺龙没有提供给我们房型!");
					}
					List<Hotel> listHotel = Server.getInstance().getHotelService().findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotel.getHotelcode() + "'", "", -1, 0);
					if (listHotel.size() > 0) {
						hotel.setId(listHotel.get(0).getId());
						Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
					}
				}
			}
		}
	}
}
