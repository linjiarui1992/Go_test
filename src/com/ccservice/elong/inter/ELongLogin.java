package com.ccservice.elong.inter;

import java.rmi.RemoteException;

import com.ccservice.elong.base.NorthBoundAPIServiceStub;

public class ELongLogin {
	public static void main(String[] args) throws RemoteException {
		getLoginToken();
		System.out.println("LoginToken=" + getLoginToken());
	}

	public static String getLoginToken() throws RemoteException {
		String loginToken = "";
		NorthBoundAPIServiceStub stub = new NorthBoundAPIServiceStub();
		NorthBoundAPIServiceStub.Login login = new NorthBoundAPIServiceStub.Login();
		NorthBoundAPIServiceStub.LoginRequest loginRequest = new NorthBoundAPIServiceStub.LoginRequest();
		NorthBoundAPIServiceStub.LoginResponseE loginResponse = new NorthBoundAPIServiceStub.LoginResponseE();

		// 用户名
		String UserName = "AP0024620";
		// 密码
		String Password = "AP0024620";

		loginRequest.setUserName(UserName);
		loginRequest.setPassword(Password);

		login.setLoginRequest(loginRequest);

		loginResponse = stub.login(login);

		loginToken = loginResponse.getLoginResult().getLoginToken().toString();

		System.out.println(loginToken);
		// System.out.println(loginResponse.getLoginResult().getResponseHead().getResultCode());
		// System.out.println(loginResponse.getLoginResult().getResponseHead().getResultMessage());
		// System.out.println(loginResponse.getLoginResult().getLoginTokenExpiredTime());
		return loginToken;
	}
}
