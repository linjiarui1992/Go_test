package com.ccservice.elong.inter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.service.IHotelService;
/**
 * 下载并修改图片路径
 * @author 师卫林
 * 2012-6-19下午06:38:09
 */
public class DownImage {
	public static void main(String[] args) {
		String url = "http://localhost:8080/cn_service/service/";

		HessianProxyFactory factory = new HessianProxyFactory();
		IHotelService servier = null;
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url + IHotelService.class.getSimpleName());
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		List<Hotelimage> hotelImagefromtable = servier.findAllHotelimage("WHERE C_PATH like '%http%'", "", -1, 0);

		for (Hotelimage hotelimage : hotelImagefromtable) {
			List<Hotel> hotelfromtable = servier.findAllHotel(" WHERE " + Hotel.COL_id + "=" + hotelimage.getHotelid(), "", -1, 0);
			File file = new File("D:\\hotelimage\\2012-6-20\\");
			if (!file.exists()) {
				file.mkdirs();
			}
			try {
				download(hotelimage.getPath(), "D:\\hotelimage\\2012-6-20\\"
						+ hotelimage.getPath().substring(hotelimage.getPath().lastIndexOf("/"), hotelimage.getPath().lastIndexOf(".")) + ".jpg");
				hotelimage.setPath("/hotelimage/2012-6-20"
						+ hotelimage.getPath().substring(hotelimage.getPath().lastIndexOf("/"), hotelimage.getPath().lastIndexOf(".")) + ".jpg");
				servier.updateHotelimageIgnoreNull(hotelimage);
				System.out.println("更改了一条图片记录!!!");
				List<Hotel> huaminhotelfromtable = servier.findAllHotel(" WHERE " + Hotel.COL_name + "='"+hotelfromtable.get(0).getName()+ "' AND C_SOURCETYPE=3", "", -1, 0);
				if(huaminhotelfromtable.size()>0){
					Hotelimage huaminimage=new Hotelimage();
					huaminimage.setPath(hotelimage.getPath());
					huaminimage.setDescription(hotelimage.getDescription());
					huaminimage.setHotelid(huaminhotelfromtable.get(0).getId());
					huaminimage.setLanguage(hotelimage.getLanguage());
					huaminimage.setType(hotelimage.getType());
					servier.createHotelimage(huaminimage);
					System.out.println("创建了一条新的图片记录~~~");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void download(String urlString, String filename) throws Exception {
		System.out.println("download xml start.......");
		URL url = new URL(urlString);
		URLConnection con = url.openConnection();
		InputStream is = con.getInputStream();

		byte[] bs = new byte[1024];
		int len;
		OutputStream os = new FileOutputStream(filename);
		while ((len = is.read(bs)) != -1) {
			os.write(bs, 0, len);
		}
		os.close();
		is.close();
		System.out.println("download xml over.......");
	}
}
