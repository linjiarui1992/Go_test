/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.tongcheng.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOfflineTomAgentAddDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.DelieveUtils;
import com.ccservice.offline.util.DistributionUtil;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.NewDelieveUtils;
import com.ccservice.offline.util.TrainOrderOfflineUtil;

/**
 * @className: com.ccservice.tongcheng.service.TrainTongChengOfflineAddOrderThread
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月27日 上午11:26:07 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineAddOrderThread implements Runnable {
    private static final String LOGNAME = "同程线下票出票请求线程队列";

    private int r1 = new Random().nextInt(10000000);

    private JSONObject reqData = new JSONObject();

    public TrainTongChengOfflineAddOrderThread(JSONObject reqData) {
        this.reqData = reqData;
    }

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    private TrainOfflineTomAgentAddDao trainOfflineTomAgentAddDao = new TrainOfflineTomAgentAddDao();

    @Override
    public void run() {
        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->reqData:" + reqData);

        JSONObject resResult = new JSONObject();

        Boolean isSuccess = false;//程序是否执行成功
        Integer msgCode = 231099;//错误代码 true用231000,false用231099

        String orderNo = reqData.getString("orderNo");//同程订单号 - 例:FT599U57UF210D174002304643
        resResult.put("hsOrderNo", orderNo);

        //创建火车票线下订单TrainOrderOffline
        //处理相关的数据进行入库操作

        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();

        String contactName = reqData.getString("contactName");//联系人姓名
        String contactPhone = reqData.getString("contactPhone");//联系人手机

        //其它多余无用信息的补充
        trainOrderOffline.setContactUser(contactName);
        trainOrderOffline.setContactTel(contactPhone);

        Integer ticketType = reqData.getInteger("ticketType");//0: 送票到家,1:送票到站
        Integer isDelivery = 0;
        if (ticketType == 1) {
            isDelivery = 1;
        }
        trainOrderOffline.setIsDelivery(isDelivery);//送票到站标识 ??? 0-不送票到站 1-送票到站 - 默认值是 0 

        Integer orderType = reqData.getInteger("orderType");//0: 普通订单,1: 抢票订单

        trainOrderOffline.setIsGrab(orderType);//同程抢票订单标识  0: 普通订单,1: 抢票订单 - 默认值是 0 

        JSONObject deliveryInfo = reqData.getJSONObject("mailInfo");//配送信息 - 否 邮寄地址信息（送票上门用）

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-deliveryInfo-->" + deliveryInfo);

        MailAddress mailAddress = null;

        String province = "";//收件省
        String city = "";//收件市 - 后续需要单独使用
        String district = "";//收件区
        String address = "";//收件地址

        String totalAddress = "";

        if (ticketType == 0 && deliveryInfo != null) {//送票到家【闪送和普通】
            //创建火车票线下订单MailAddress
            //处理相关的数据进行入库操作
            mailAddress = new MailAddress();

            String recipientName = deliveryInfo.getString("name");//收件人
            mailAddress.setMAILNAME(recipientName);

            //String zipCode = deliveryInfo.getString("zipCode");//邮编
            mailAddress.setPOSTCODE("");

            String recipientPhone = deliveryInfo.getString("mobile");//收件人联系电话
            mailAddress.setMAILTEL(recipientPhone);

            /**
             * 获取到省份信息之后,进行插入 - 但是后期无用,又不能为null - 可以出传入空字符串
             * 
             * ExpressNum - MAILNAME - MAILTEL - 【PROVINCENAME】 - 【CITYNAME】 - 【REGIONNAME】 - ADDRESS - ExpressAgent
             * 
             * 【PROVINCENAME】/【CITYNAME】/【REGIONNAME】 - ""
             */
            province = deliveryInfo.getString("province");//收件省
            city = deliveryInfo.getString("city");//收件市
            district = deliveryInfo.getString("district");//收件区
            address = deliveryInfo.getString("address");//收件地址

            mailAddress.setPROVINCENAME(province);
            mailAddress.setCITYNAME(city);
            mailAddress.setREGIONNAME(district);

            //北京-北京市-顺义区-仁和 石门市场主楼336

            if (province != null && !province.equals("")) {
                totalAddress = totalAddress + province + "-";
            }
            if (city != null && !city.equals("")) {
                totalAddress = totalAddress + city + "-";
            }
            if (district != null && !district.equals("")) {
                totalAddress = totalAddress + district + "-";
            }
            if (address != null && !address.equals("")) {
                totalAddress = totalAddress + address;
            }

            mailAddress.setADDRESS(totalAddress);//收件地址

            //0-顺丰 - 1-EMS 2-宅急送
            Integer expressType = deliveryInfo.getInteger("expressType");//否 - 快递类型 0: 顺丰,1:EMS（检验接口返回的结果）
            if (expressType != null) {
                mailAddress.setExpressAgent(expressType);
            }
            else {
                mailAddress.setExpressAgent(-1);
            }
        }

        JSONObject dataResult = new JSONObject();
        dataResult.put("orderNo", orderNo);
        resResult.put("data", dataResult);

        //生成自己系统平台的订单号
        String OrderNumber = "TC" + TrainOrderOfflineUtil.timeMinID();
        trainOrderOffline.setOrderNumber(OrderNumber);
        trainOrderOffline.setOrderNumberOnline(orderNo);

        //不能为空的字段的值 - 分单逻辑

        trainOrderOffline.setOrderStatus(1);//订单状态

        //trainOrderOffline.setSupplyPayWay(0);
        //trainOrderOffline.setRefundReason(0);
        //trainOrderOffline.setChuPiaoAgentid(0);
        trainOrderOffline.setLockedStatus(0);

        //获取包含乘客信息和保单信息的 passengers - JSONArray - 至少有一个
        JSONArray passengers = reqData.getJSONArray("passengers");//List<PassengerInfo> 是   乘客信息

        JSONObject bookInfo = reqData.getJSONObject("bookInfo");//BookInfo  是   预定信息 - 车次信息

        String fromStationName = bookInfo.getString("startStation");//是  出发站
        String fromStationNameTemp = "";//此处需要后期做配送到站的分单逻辑的兼容传递站点内容的处理

        //送票到站,根据始发站点名称进行订单拦截 - 送票到站的城市里面的信息是我们提供给同程的,应该不存在相关的错误的情况,此处仅仅是健壮性的拦截
        if (ticketType == 1) {//抢票订单的代售点和送票到站的代售点之间的逻辑，目前是没有统一在一起的，所以暂不考虑
            List<String> trainStationList = new ArrayList<String>();
            try {
                trainStationList = trainOfflineTomAgentAddDao.findTrainStationList();
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
                return;
            }

            if (trainStationList == null || trainStationList.size() <= 0) {
                resResult.put("isSuccess", isSuccess);
                resResult.put("msgCode", msgCode);
                resResult.put("msgInfo", "无法获取送票到站的配置信息");

                //幂等的设计和实现
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
                
                return resResult;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
                return;
            }

            //有的 fromStationName - 包含了站的名字 - 有的没有包含 - 此处需要做替换的转换 - 数据库中也是 - 需要转换处理

            String fromStationNameTemp1 = "";//替换处理 - 含"站"
            Boolean fromStationNameBoo1 = false;
            String fromStationNameTemp2 = "";//替换处理 - 不含"站"
            Boolean fromStationNameBoo2 = false;

            if (fromStationName.contains("站")) {
                fromStationNameTemp1 = fromStationName;
                fromStationNameTemp2 = fromStationName.replace("站", "");
            }
            else {
                fromStationNameTemp1 = fromStationName + "站";
                fromStationNameTemp2 = fromStationName;
            }

            //此处需要后期做配送到站的分单逻辑的兼容传递站点内容的处理 - 下述二者只会有其一
            if (trainStationList.contains(fromStationNameTemp1)) {
                fromStationNameBoo1 = true;
                fromStationNameTemp = fromStationNameTemp1;
            }
            if (trainStationList.contains(fromStationNameTemp2)) {
                fromStationNameBoo2 = true;
                fromStationNameTemp = fromStationNameTemp2;
            }

            if (!(fromStationNameBoo1 || fromStationNameBoo2)) {
                resResult.put("isSuccess", isSuccess);
                resResult.put("msgCode", msgCode);
                resResult.put("msgInfo", "送票到站不支持的站点,请联系技术或产品解决");

                //幂等的设计和实现
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
                
                return resResult;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
                return;
            }
        }

        String cheCi = bookInfo.getString("trainNo");//是    车次号

        //String fromStationCode = reqData.getString("fromStationCode");//出发站简码 - 【无用字段,无需处理】

        //String fromStationName = bookInfo.getString("startStation");//是  出发站

        //String toStationCode = reqData.getString("toStationCode");//到达站简码 - 【无用字段,无需处理】

        String toStationName = bookInfo.getString("endStation");//是  到达站

        //是 出发时间,格式:2017-08-24 13:15:00
        String startTime = bookInfo.getString("startTime");//是  到达站

        //是 到达时间,格式:2017-08-24 15:15:00
        String endTime = bookInfo.getString("endTime");//是  到达站

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-passengers-->" + passengers);

        List<TrainTicketOffline> trainTicketOfflineList = new ArrayList<TrainTicketOffline>();
        List<TrainPassengerOffline> trainPassengerOfflineList = new ArrayList<TrainPassengerOffline>();

        Double orderPrice = 0.0D;

        int ticketCount = passengers.size();
        trainOrderOffline.setTicketCount(ticketCount);

        //判断儿童票的张数和成人票的张数
        int adultNum = 0;
        int kidNum = 0;

        Boolean isHaveStu = false;
        Boolean isHaveArmy = false;

        String passengerName = "";//便于后期做屏蔽操作
        String distributionDepartTime = "";

        for (int i = 0; i < ticketCount; i++) {
            JSONObject passenger = passengers.getJSONObject(i);

            //创建火车票线下订单TrainPassengerOffline
            //处理相关的数据进行入库操作
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();

            Long passengerId = passenger.getLong("id");//同程乘客Id
            trainPassengerOffline.setPassengerId(String.valueOf(passengerId));

            passengerName = passenger.getString("name");//乘客姓名
            trainPassengerOffline.setName(passengerName);

            String passportNo = passenger.getString("idNo");//乘客证件号码
            trainPassengerOffline.setIdNumber(passportNo);

            //证件类型:1: 身份证,2:护照,3:台胞证,4:港澳通行证
            Integer passportTypeId = passenger.getInteger("idType");
            trainPassengerOffline.setIdType(passportTypeId);

            //无用字段 - gender int 是   性别,0:未知,1:男性,2:女性

            trainPassengerOfflineList.add(trainPassengerOffline);

            //创建火车票线下订单TrainOrderOffline
            //处理相关的数据进行入库操作
            TrainTicketOffline trainTicketOffline = new TrainTicketOffline();

            String trainDate = startTime.substring(0, 10);//乘车日期 - 2015-08-09
            String departTime = startTime.substring(11, 16);//出发时间 - 07:02

            String arriveDate = endTime.substring(0, 10);//到达日期 - 2015-08-09
            String arriveTime = endTime.substring(11, 16);//到达时间 - 07:02

            //计算 历时
            String CostTime = "0";
            try {
                CostTime = TrainOrderOfflineUtil.getCostTime(startTime, endTime);
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/
            }

            //来自外围数据
            trainTicketOffline.setTrainNo(cheCi);
            trainTicketOffline.setDeparture(fromStationName);
            trainTicketOffline.setArrival(toStationName);

            distributionDepartTime = trainDate + " " + departTime + ":00.000";
            trainTicketOffline.setDepartTime(distributionDepartTime);// - 2016-06-28 18:10:00.000
            //trainTicketOffline.setDepartTime(trainDate);// - 2016-06-28 18:10:00.000

            trainTicketOffline.setStartTime(departTime);// - 2016-06-28 18:10:00.000
            trainTicketOffline.setArrivalTime(arriveTime);
            trainTicketOffline.setCostTime(CostTime);

            //票种ID。 - 与票种名称对应关系: - 1:成人票,2:儿童票 - 是  乘客类型:1:成人,2:儿童
            Integer piaoType = passenger.getInteger("type");
            trainTicketOffline.setTicketType(piaoType);

            if (piaoType == 1) {
                adultNum += 1;
            }
            else if (piaoType == 2) {
                kidNum += 1;
            }
            else if (piaoType == 3) {
                isHaveStu = true;
            }
            else if (piaoType == 4) {
                isHaveArmy = true;
            }

            String piaoTypeName = passenger.getString("piaoTypeName");//票种名称 - 【无用字段,无需处理 - 后期在程序内部做了处理和显示】

            /**
             * seatClass    int 是   - 席别类别
             */
            Integer seatClass = passenger.getInteger("seatClass");//通过席别标识,获取席别名称
            if (seatClass == null) {
                seatClass = 0;
            }
            String SeatType = TrainOrderOfflineUtil.getSeatTypeByTCSeatClass(seatClass);//座位名称 - 【非必须】
            trainTicketOffline.setSeatType(SeatType);

            Double price = passenger.getDouble("ticketPrice");//票价

            //无用字段？
            Double issueFee = passenger.getDouble("issueFee");//出票手续费 - 后期如有需要,可以传入

            //票价 = 车票价 + 出票手续费
            Double totalTicketPrice = price + issueFee;//订单价 - 票价+服务费+快递费

            //订单总价
            orderPrice += totalTicketPrice;//订单价 - 票价+服务费+快递费

            trainTicketOffline.setPrice(price);//票面价

            trainTicketOfflineList.add(trainTicketOffline);

            //orderPrice += Double.valueOf(totalTicketPrice);//订单价 - 票价+服务费+快递费
        }

        if (isHaveStu) {//如果包含学生票,不允许
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "线下票请求出票接口,不支持学生票,需要学生证原件才能购买");

            //幂等的设计和实现
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;*/

            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
            return;
        }

        if (isHaveArmy) {//如果包含残军票,不允许
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "线下票请求出票接口,不支持的售票类型-残军票");

            //幂等的设计和实现
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;*/

            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
            return;
        }

        //判断儿童票的张数和成人票的张数 - 不能单独乘车
        if (kidNum > 0 && kidNum == ticketCount) {
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "线下票请求出票接口,儿童不能单独乘车");

            //幂等的设计和实现
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;*/

            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
            return;
        }

        //通过票的信息进行单独的订单的金额的总的计算 - 
        trainOrderOffline.setOrderPrice(orderPrice);

        //送票到家的分单逻辑

        //送票到站的分单逻辑

        //通过快递信息设计分单逻辑 - 分配代售点

        //同程线下订单分流 ---线下票分流--同程----81 - 北京北京海淀#西三旗街道永泰东里x号楼x单元x室
        String createUid = "81";
        String createUser = "同程";

        trainOrderOffline.setCreateUId(Integer.valueOf(createUid));
        trainOrderOffline.setCreateUser(createUser);

        Integer agentid = 0;

        Integer isRapidSend = 0;//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

        Double rapidSendPrice = 0D;//闪送订单的价格 - 在下单的时候做反馈

        String agentidtemp = "";

        JSONObject isRapidSendResult = new JSONObject();//闪送的判定和分单逻辑的相关内容并入到了一起，此处作为一个总的全局变量，方便后续的再次使用

        if (ticketType == 1) {//送票到站点的分单逻辑
            //抢票订单的代售点和送票到站的代售点之间的逻辑，目前是没有统一在一起的，所以暂不考虑
            //通过 - fromStationName 获取 AgentId
            agentid = TrainOrderOfflineUtil.distributionStation(fromStationNameTemp);

            //闪送单具备起始价【最便宜8元，有些地方是10、12】，大部分的时候的成本都比代售点自送【平均每单3元】要高 - 所以送票到站的单子不可能走入闪送的逻辑 - 

            WriteLog.write("TrainTuNiuOfflineOrderServlet", r1 + ":addTongChengOffline:"
                    + "线下火车票分单log记录-送票到站的分单的记录信息-OrderNumber:" + OrderNumber + "----->agentId:" + agentid);

        }
        else {//配送票的分单逻辑 - 此处不允许不传快递信息
                  //抢票订单的代售点和送票到家的代售点之间的逻辑，目前是统一在一起的，需要兼容考虑
            if (mailAddress == null) {
                resResult.put("isSuccess", isSuccess);
                resResult.put("msgCode", msgCode);
                resResult.put("msgInfo", "线下票配送到家,未传快递信息");

                //幂等的设计和实现
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
                
                return resResult;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
                return;
            }

            String deliveryAddress = mailAddress.getADDRESS();//传递的完整的地址

            //春节期间的快递的拦截 - 今年的春节的时间
            String star = "2017-01-26 00:00:00";//开始时间
            String end = "2017-02-03 00:00:00";//结束时间
            SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date sdate = localTime.parse(star);
                Date edate = localTime.parse(end);
                long time = System.currentTimeMillis();
                if (time >= sdate.getTime() && time < edate.getTime()) {
                    if (deliveryAddress.contains("县") || deliveryAddress.contains("镇")
                            || deliveryAddress.contains("乡")) {
                        if (orderType == 0) {//0: 普通订单,1: 抢票订单
                            agentidtemp = "414";//分到了测试账号 - 送票到家
                            WriteLog.write("春节同程部分地区拦截", r1 + ":addTongChengOffline:" + "线下火车票分单log记录:地址-送票到家:"
                                    + deliveryAddress + "----->agentId:" + agentidtemp);
                        }
                        else {
                            agentidtemp = "414";//分到了测试账号 - 抢票订单
                            WriteLog.write("春节同程部分地区拦截", r1 + ":addTongChengOffline:" + "线下火车票分单log记录:地址-抢票订单:"
                                    + deliveryAddress + "----->agentId:" + agentidtemp);
                        }
                    }
                }
                else {
                    if (orderType == 0) {//0: 普通订单,1: 抢票订单
                        /**
                         * 此处增加了闪送单的分单逻辑
                         * 后期的闪送单 有一部分会走分给代售点自送的分单逻辑 - 目前走的是UU跑腿的相关逻辑
                         * 
                         * 同程的单子可以根据邮寄地址的城市名称和发车时间获取该单是否是闪送订单
                         * 
                         * 后期的逻辑变动 - 
                         * 
                         * result.put("isRapidSend", true);
                        result.put("need_paymoney", need_paymoney);
                        result.put("agentId", agentId);
                         */
                        isRapidSendResult = TrainOrderOfflineUtil.isRapidSend(OrderNumber, totalAddress, city,
                                startTime);

                        Boolean isRapidSendB = isRapidSendResult.getBooleanValue("isRapidSend");
                        if (isRapidSendB) {// 0: 普通订单,1: 闪送订单
                            isRapidSend = 1;
                        }

                        if (isRapidSend == 0) {
                            //                            agentidtemp = TrainOrderOfflineUtil.distribution(deliveryAddress,
                            //                                    Integer.valueOf(createUid));
                            DistributionUtil du = new DistributionUtil();
                            agentidtemp = du.distribution(r1, deliveryAddress, distributionDepartTime, 1);
                        }
                        else {
                            //此处的方法的调用需要处理的是一个是传城市地址的，一个是不传城市地址的 - 整个的地址是一定必须传输的，但是城市名称根据需要去传输，有的话就传，没有的话，传递null
                            /*JSONObject jsonObject = TrainOrderOfflineUtilCS.distributionRapidSendCS(OrderNumber, deliveryAddress, city);
                                                        String agentId =  "414";//正式环境的测试账号
                            Double need_paymoney = 0.0;// 实际需要支付金额
                            
                            agentidtemp = jsonObject.getString("agentId");
                            rapidSendPrice = jsonObject.getDouble("need_paymoney");*///闪送订单的价格 - 在下单的时候做反馈

                            /**
                             * result.put("isRapidSend", true);
                            result.put("need_paymoney", need_paymoney);
                            result.put("agentId", agentId);
                             * 
                             */
                            agentidtemp = isRapidSendResult.getString("agentId");
                            rapidSendPrice = isRapidSendResult.getDouble("need_paymoney");
                        }
                    }
                    else {//目前的抢票订单暂不考虑闪送的逻辑
                        agentidtemp = String.valueOf(TrainOrderOfflineUtil.distributionGrabTicket());
                    }
                }
            }
            catch (Exception e) {
                ExceptionUtil.writelogByException("同程从数据库中获取分单的代售点失败,或者,同程春节顺丰无法邮寄的地区拦截异常", e);

                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
                return;
            }

            WriteLog.write(LOGNAME,
                    r1 + ":addTongChengOffline:" + "线下火车票分单log记录:订单类型-orderType:" + orderType + ",是否是闪送订单-isRapidSend:"
                            + isRapidSend + "----->deliveryAddress:" + deliveryAddress + "----->agentId:"
                            + agentidtemp);

            //此处写在后面只是衔接上述的日志记录的情况 - 闪送的分单逻辑和抢票的分单逻辑里面并不存在下述的判定内容

            //对于逻辑中可能存在的转换异常抛出的情况需要做相关的处理 - 并解除幂等的等待限制

            if (agentidtemp.contains("途牛的默认出票点的信息不存在")) {//
                resResult.put("isSuccess", isSuccess);
                resResult.put("msgCode", msgCode);
                resResult.put("msgInfo", "同程的默认出票点的信息不存在");

                //幂等的设计和实现
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
                
                return resResult;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
                return;
            }
            else {
                agentid = Integer.valueOf(agentidtemp);//分配逻辑中分配的代售点的相关的信息
            }

            //agentid = Integer.valueOf(agentidtemp);
        }

        trainOrderOffline.setAgentId(agentid);//分配逻辑中分配的代售点的相关的信息

        //待补充 - 暂时设定为0
        trainOrderOffline.setAgentProfit(0.0);

        //车次的定制信息和备选车次信息的处理

        /**
         * 非必须字段 - 【非必须】
         * 
         * 个人定制信息,可以指定几个上下铺等信息。如果指定了订单信息,并不接受其他情况,当出不了指定的铺位或坐席时,会出票失败处理。
         * 
         * 定制信息 - 录入线下订单表的部分 - 待补充
         * 
         */
        String extSeat = "无";

        String backupSeatClass = bookInfo.getString("backupSeatClass");//备选坐席 格式:1|2|3 含义:上铺|中铺|下铺

        if (backupSeatClass != null) {
            if (backupSeatClass.equals("1")) {
                extSeat = "上铺";
            }
            else if (backupSeatClass.equals("2")) {
                extSeat = "中铺";
            }
            else if (backupSeatClass.equals("3")) {
                extSeat = "下铺";
            }
            else {
                if (!"".equals(backupSeatClass)) {
                    extSeat = backupSeatClass;
                }
            }
        }
        trainOrderOffline.setExtSeat(extSeat);

        /**
         * 
        customizeType    int 是   订制类型
        0:默认,3:下铺  4: 指定靠窗
        5: 高铁动车选座票  7: 卧铺 
        8: 免核验购票
         * 
        customizeContent    string  否   订制内容
        customizeType为3时,该字段为数量,2表示2张下铺
        customizeType为4时,该字段为数量,2表示2张靠窗票
        customizeType为5时,该字段格式为1/A|1/B|1/C, 表示1张A座,1张B座, 1张C座
        customizeType为7时,该字段 格式为3/2|2/2|1/1, 表示
        下铺/2张|中铺/2张|上铺/1张
        customizeType为0或8时,为空
         * 
         * customizeContent - 不是必传字段
         * 
         */
        Integer customizeType = bookInfo.getInteger("customizeType");//是  到达站

        String customizeContent = bookInfo.getString("customizeContent");//

        JSONArray backupTrainInfo = reqData.getJSONArray("backupTrainInfo");//List<BackupTrain> 否   备选车次 属于定制服务

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-bookInfo-->" + bookInfo + "-backupTrainInfo-->" + backupTrainInfo);

        String TradeNo = "无";

        StringBuilder TradeNoSb = new StringBuilder("");

        if (customizeType == 0 || customizeType == 8) {//默认 - 这个直接写进去就可以,线下票出票主要针对,身份证待核验,身份证被冒用等,不需要身份核验的情况,因为代售点出票只需要提供身份证号就可以,所以就属于免核验购票
            TradeNoSb.append("");
        }
        else if (customizeType == 3) {
            if (customizeContent != null && !customizeContent.equals("")) {
                Integer lowerNum = Integer.valueOf(customizeContent);
                if (lowerNum != null && lowerNum > 0) {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("指定下铺票,至少需要" + lowerNum + "张下铺票");
                    }
                    else {
                        TradeNoSb.append(",指定下铺票,至少需要" + lowerNum + "张下铺票");
                    }
                }
            }
        }
        else if (customizeType == 4) {
            if (customizeContent != null && !customizeContent.equals("")) {
                Integer nextWindowNumber = Integer.valueOf(customizeContent);
                if (nextWindowNumber != null && nextWindowNumber > 0) {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("指定靠窗票,至少需要" + nextWindowNumber + "张靠窗票");
                    }
                    else {
                        TradeNoSb.append(",指定靠窗票,至少需要" + nextWindowNumber + "张靠窗票");
                    }
                }
            }
        }
        else if (customizeType == 5) {
            if (customizeContent != null && !customizeContent.equals("")) {//1/A|1/B|1/C, 表示1张A座,1张B座, 1张C座
                String[] strings = customizeContent.split("\\|");

                StringBuilder sBuilder = new StringBuilder("");
                for (int i = 0; i < strings.length; i++) {
                    sBuilder.append(strings[i].replace("/", "张") + "座");
                    if (i < (strings.length - 1)) {
                        sBuilder.append(",");
                    }
                }

                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("指定座位:" + sBuilder.toString());
                }
                else {
                    TradeNoSb.append(",指定座位:" + sBuilder.toString());
                }
            }
        }
        else if (customizeType == 7) {//3/2|2/2|1/1, 表示下铺/2张|中铺/2张|上铺/1张
            if (customizeContent != null && !customizeContent.equals("")) {//"3/2|2/2|1/1";//, 表示下铺/2张|中铺/2张|上铺/1张
                String[] strings = customizeContent.split("\\|");

                StringBuilder sBuilder = new StringBuilder("");
                for (int i = 0; i < strings.length; i++) {
                    String[] stringsn = strings[i].split("/");
                    sBuilder.append(stringsn[1] + "张"
                            + TrainOrderOfflineUtil.getTCSeatNameBySeatCode(Integer.valueOf(stringsn[0])));
                    if (i < (strings.length - 1)) {
                        sBuilder.append(",");
                    }
                }

                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("指定铺位:" + sBuilder.toString());
                }
                else {
                    TradeNoSb.append(",指定铺位:" + sBuilder.toString());
                }
            }
        }
        else if (customizeType == 8) {
            if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                TradeNoSb.append("免核验购票");
            }
            else {
                TradeNoSb.append(",免核验购票");
            }
        }

        //备选车次信息 - List<BackupTrain> 否   备选车次
        if (backupTrainInfo != null) {
            StringBuilder sBuilder = new StringBuilder("");
            for (int i = 0; i < backupTrainInfo.size(); i++) {
                JSONObject backupJSONObject = backupTrainInfo.getJSONObject(i);
                sBuilder.append("车次:" + backupJSONObject.getString("trainNo"));
                sBuilder.append(",出发站:" + backupJSONObject.getString("startStation"));
                sBuilder.append(",到达站:" + backupJSONObject.getString("endStation"));
                sBuilder.append(",出发时间:" + backupJSONObject.getString("startTime"));
                sBuilder.append(",到达时间:" + backupJSONObject.getString("endTime"));
                if (i < (backupTrainInfo.size() - 1)) {
                    sBuilder.append(".");
                }
            }

            if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                TradeNoSb.append("备选车次:" + sBuilder.toString());
            }
            else {
                TradeNoSb.append(",备选车次:" + sBuilder.toString());
            }
        }

        Integer acceptOtherSeat = bookInfo.getInteger("acceptOtherSeat");//是 是否接受其它坐席 , 0不接受, 1: 接受

        if (acceptOtherSeat == 0) {
            if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                TradeNoSb.append("(无法满足定制服务时:是否接受其它席别:不接受)");
            }
            else {
                TradeNoSb.append("。(无法满足定制服务时:是否接受其它席别:不接受)");
            }
        }
        else {
            if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                //TradeNoSb.append("(无法满足定制服务时:是否接受其它席别:接受)");
            }
            else {
                TradeNoSb.append("。(无法满足定制服务时:是否接受其它席别:接受)");
            }
        }

        /*if (TradeNoSb != null && !TradeNo.equals("")) {
            trainOrderOffline.setTradeNo(TradeNoSb.toString());
        } else {
            trainOrderOffline.setTradeNo("无");
        }*/

        TradeNo = TradeNoSb.toString();
        trainOrderOffline.setTradeNo(TradeNo);

        int PaperBackup = 1;//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
        int paperLowSeatCount = 0;//至少接受下铺/靠窗/连坐数量

        //这两个即使没有,也必须传入
        trainOrderOffline.setPaperBackup(PaperBackup);
        trainOrderOffline.setPaperLowSeatCount(paperLowSeatCount);

        trainOrderOffline.setPaperType(0);

        /**
         * 
         * 考虑抢票的定制信息
         * 
        grabEndTime string  否   抢票截止日期 例:”2017-08-20 13:30:00”
        grabSeatClass   string  否   抢票坐席,可为多坐席,逗号隔开
        事例:硬座,硬卧,无座
         * 
        你们传递的抢票单，里面，是不是也是会传很多这些定制信息？
        
        抢票没有这些定制 内容的，
        都是默认值
        
        抢票这个，如果有多坐席的话，坐席之间应该有优先出票级别之类的限定吧？
        是不是不在抢票坐席之内的，就不考虑？
        
        嗯，排在前面的优先考虑，没有的，不考虑
         * 
         */

        if (orderType != null && orderType == 1) {
            /*resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "此接口只处理普通订单,抢票订单的提交请联系技术解决");
            
            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;*/

            //处理抢票订单 - 先还原默认的定制信息 - 不做考虑
            TradeNoSb = new StringBuilder("");

            String grabEndTime = bookInfo.getString("grabEndTime");//2017-08-20 13:30:00
            String grabSeatClass = bookInfo.getString("grabSeatClass");//可为多坐席,逗号隔开     事例:硬座,硬卧,无座 - 排在前面的优先考虑，没有的，不考虑

            if (grabEndTime != null) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("抢票截止日期:" + grabEndTime);
                }
                else {
                    TradeNoSb.append(",抢票截止日期:" + grabEndTime);
                }
            }

            if (grabSeatClass != null) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("抢票坐席(靠前者优先出票):" + grabSeatClass);
                }
                else {
                    TradeNoSb.append(",抢票坐席(靠前者优先出票):" + grabSeatClass);
                }
            }

            if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                //TradeNoSb.append("(无法满足定制服务时:是否接受其它席别:不接受)");
            }
            else {
                TradeNoSb.append("。(无法满足定制服务时:是否接受其它席别:不接受)");
            }
        }

        //处理抢票订单 - 先还原默认的定制信息 - 不做考虑
        TradeNo = "";
        TradeNo = TradeNoSb.toString();

        //做特殊提示和转换动作 -
        if (TradeNo.equals("(无法满足定制服务时:是否接受其它席别:不接受)")) {//(无法满足定制服务时:是否接受其它席别:不接受)
            TradeNo = "无(此席位无座时，不接受其他席位)";
        }
        else if (TradeNo.equals("")) {//空字符串
            TradeNo = "无";
        }

        trainOrderOffline.setTradeNo(TradeNo);

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-trainOrderOffline-->" + trainOrderOffline);

        //健壮性入库判定
        TrainOrderOffline trainOrderOfflineTemp = null;
        try {
            trainOrderOfflineTemp = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderNo);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
            return jsonTemp;*/

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
            return;
        }

        if (trainOrderOfflineTemp != null) {
            //插入数据之前的幂等的二次判断逻辑
            isSuccess = true;
            msgCode = 231000;//isSuccess true用231000,false用231099
            resResult.put("isSuccess", isSuccess);
            resResult.put("msgCode", msgCode);
            resResult.put("msgInfo", "请求已经接收");

            //此处另开线程处理出票请求超时的逻辑

            //幂等的设计和实现
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;*/

            WriteLog.write(LOGNAME,
                    r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resResult);
            return;
        }

        /**
         * 闪送订单新增存储过程处理方式 -
         * 
         * 快递类型和闪送单的价格的处理
         * 
         */
        trainOrderOffline.setIsRapidSend(isRapidSend);

        /**
         * 正式环境的订单中的测试订单的可能性的拦截 - 
         * 
         * 乘客姓名
         * 订单联系人
         * 邮寄收件人
         * 
         * 邮寄地址
         * 
         * .equals("测试")
         * 的都可能是测试单，需要做相关的分配账号的拦截
         * 
         * 此处的操作并未更新相关的 agentid 的值，只是在插入的时候做了拦截动作而已
         * 
         * 此处是最终的限定拦截条件
         */
        if (mailAddress == null) {
            if (contactName.equals("测试") || passengerName.equals("测试")) {
                trainOrderOffline = TrainOrderOfflineUtil.interceptTestZS(trainOrderOffline);
            }
        }
        else {
            if (contactName.equals("测试") || passengerName.equals("测试") || mailAddress.getMAILNAME().equals("测试")) {
                trainOrderOffline = TrainOrderOfflineUtil.interceptTestZS(trainOrderOffline);
            }
        }

        //插入线下订单表 - 
        Long offlineOrderId = 0L;
        try {
            offlineOrderId = Long.valueOf(trainOrderOfflineDao.addTrainOrderOfflineTCRapidSend(trainOrderOffline));

            //闪送之前的入单逻辑
            //offlineOrderId = Long.valueOf(trainOrderOfflineDao.addTrainOrderOfflineTC(trainOrderOffline));
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
            return jsonTemp;*/

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
            return;
        }

        /**
         * 送票到站 - 闪送 - 送票到家【抢票与非抢票-抢票的截止日期】
         * 
         *  送票到站没有快递时效
         * 
         */

        //更新快递时效信息
        String delieveStr = "暂无快递信息!";

        Integer delieveType = -1;//获取除了UU跑腿之外的快递类型 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】

        if (mailAddress != null) {//送票上门的都有
            if (isRapidSend == 1) {//是闪送单
                delieveType = 10;//闪送
                delieveStr = DelieveUtils.getUUptDelieveStr(String.valueOf(agentid), mailAddress.getADDRESS());//
            }
            else {//不是闪送单，需要再次区分是顺丰还是EMS？ - 调用获取不同的快递时效 - 0-顺丰 - 1-EMS

                //暂且不送EMS - 系统内容尚有问题待处理
                // 快递类型判断
                delieveType = TrainOrderOfflineUtil.getDelieveType(province, city, district, address, startTime,
                        Integer.valueOf(agentidtemp));//
                WriteLog.write(LOGNAME, r1 + ":同程线下票出票快递类型请求结果:" + delieveType);
                if (delieveType == 0) {//顺丰
                    //利用顺丰的接口,获取快递时效信息 - 46 - 系统
                    try {
                        //                        delieveStr = TrainOrderOfflineUtil.getDelieveStr(offlineOrderId, agentid, mailAddress.getADDRESS());//
                        //                        delieveStr = DelieveUtils.getDelieveStr(String.valueOf(agentid), mailAddress.getADDRESS());//
                        NewDelieveUtils util = new NewDelieveUtils();
                        delieveStr = util.getDelieveStr("0", String.valueOf(agentid), mailAddress.getADDRESS(), 81);
                    }
                    catch (Exception e) {
                        //return ExceptionTCUtil.handleTCException(e);

                        JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);

                        WriteLog.write(LOGNAME,
                                r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + resultJSON);
                        return;
                    }
                }
                else if (delieveType == 1) {//EMS
                    //EMS的快递时效的获取的方法和内容执行
                    //                    delieveStr = DelieveUtils.getEmsDelieveStr(String.valueOf(agentid), mailAddress.getADDRESS());//
                    NewDelieveUtils util = new NewDelieveUtils();
                    delieveStr = util.getDelieveStr("1", String.valueOf(agentid), mailAddress.getADDRESS(), 81);
                }

                //利用顺丰的接口,获取快递时效信息 - 46 - 系统
                /*try {
                    //delieveStr = TrainOrderOfflineUtil.getDelieveStr(offlineOrderId, agentid, mailAddress.getADDRESS());//
                    delieveStr = DelieveUtils.getDelieveStr(String.valueOf(agentid), mailAddress.getADDRESS());//
                }
                catch (Exception e) {
                    //return ExceptionTCUtil.handleTCException(e);
                    
                    JSONObject resultJSON = ExceptionTCUtil.handleTCException(e);
                
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:"+resultJSON);
                    return;
                }*/
            }
        }
        else {
            delieveStr = "送票到站";
        }

        WriteLog.write(LOGNAME,
                r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread,获取快递时效的结果--->delieveType:" + delieveType);

        //更新快递时效信息到数据库中
        trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(offlineOrderId, delieveStr);

        //Integer ExpressAgent = 0;

        if (mailAddress != null) {//插入邮寄信息表
            mailAddress.setORDERID(offlineOrderId.intValue());

            /*WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-mailAddress-->"+mailAddress);
            
            try {
                mailAddressDao.addMailAddressTC(mailAddress);
            }
            catch (Exception e1) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e1);
                TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }*/

        }
        else {//为了兼容之前的逻辑，也需要在此处进行配送到站的某些快递信息的添加
            mailAddress = new MailAddress();
            mailAddress.setORDERID(offlineOrderId.intValue());

            mailAddress.setMAILNAME(contactName);
            mailAddress.setPOSTCODE("");//邮编
            mailAddress.setMAILTEL(contactPhone);

            mailAddress.setPROVINCENAME("");//收件省
            mailAddress.setCITYNAME("");//收件市
            mailAddress.setREGIONNAME("");//收件区

            String deliveryAddress = "";
            try {
                deliveryAddress = trainOfflineTomAgentAddDao.findDeliveryAddressByTrainStation(fromStationNameTemp);
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
                return;
            }
            mailAddress.setADDRESS(deliveryAddress);//收件地址

            delieveType = 99;//配送到站
        }

        //此处需要做其它逻辑的处理和兼容设计 - //0-顺丰 - 1-EMS 2-宅急送 3-京东 - 99配送到站 10-闪送【UU跑腿】
        mailAddress.setExpressAgent(delieveType);//设置快递类型

        WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-mailAddress-->" + mailAddress);

        try {
            if (delieveType == 10) {//设置一下闪送单的快递的价格
                mailAddress.setRapidSendPrice(rapidSendPrice);
                mailAddressDao.addMailAddressTCRapidSend(mailAddress);
            }
            else {//非闪送单，无需考虑多余的逻辑
                mailAddressDao.addMailAddressTC(mailAddress);
            }
        }
        catch (Exception e1) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e1);
            /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
            return jsonTemp;*/

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
            return;
        }

        //插入乘客表 - 插入火车票表
        for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
            //插入乘客表
            TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
            trainPassengerOffline.setOrderId(offlineOrderId);//关联系统平台的 - Id
            Long offlinePassengerId = 0L;
            try {
                offlinePassengerId = Long
                        .valueOf(trainPassengerOfflineDao.addTrainPassengerOffline(trainPassengerOffline));
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
                return;
            }

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-trainPassengerOffline-->" + trainPassengerOffline);

            //插入火车票表
            TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(i);
            trainTicketOffline.setTrainPid(offlinePassengerId);
            trainTicketOffline.setOrderId(offlineOrderId);
            try {
                trainTicketOfflineDao.addTrainTicketOffline(trainTicketOffline);
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTCUtil.handleTCException(e);
                /*TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, jsonTemp.toJSONString(), r1);
                return jsonTemp;*/

                WriteLog.write(LOGNAME,
                        r1 + ":同程线下票出票请求-进入到线程类-TrainTongChengOfflineAddOrderThread--->result:" + jsonTemp);
                return;
            }

            WriteLog.write(LOGNAME, r1 + ":同程线下票出票请求-trainTicketOffline-->" + trainTicketOffline);
        }

        //记录日志
        //TrainOrderOfflineUtil.CreateBusLogAdminNoUserIdOtherDealResult(offlineOrderId, "订单接收成功", 0);//0 - 订单已发放 - 刚接受订单
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(offlineOrderId, agentid, "订单接收成功", 0);//0 - 订单已发放 - 刚接受订单

        //上述获取和插入的全部都是对 TrainTicket 和 TrainInsure 的 集合 的相关操作 - 

        //订单信息入库

        //TicketCount - [需要订单提交的时候,即进行计算] - 来自集合的长度

        //记录请求信息日志

        //不同的支付方式,涉及不同的处理方式

    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        /*String data = "{\"orderNo\":\"FT59B63596210D354010952950\",\"contactName\":\"邮寄测试单2\",\"contactPhone\":\"15370283333\","
                //+ "\"ticketType\":0,\"orderType\":0,"//正常订单
                
                + "\"ticketType\":0,\"orderType\":1,"//抢票订单不考虑定制信息 - //0: 普通订单,1: 抢票订单
                
                + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南省\",\"city\":\"新乡市\",\"district\":\"卫滨区\",\"address\":\"河南省新乡市牧野区东干道360号\",\"expressType\":0},"
                
                //无定制
                + "\"bookInfo\":{\"trainNo\":\"K7804\",\"startStation\":\"太原\",\"endStation\":\"太原东\",\"startTime\":\"2017-10-27 08:55:00\",\"endTime\":\"2017-09-30 09:01:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\",\"grabEndTime\":\"2017-09-26 20:20:00\",\"grabSeatClass\":\"硬卧,硬座,无座\"},"
                
                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";
        
                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":1,\"idNo\":\"111222190001013333\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";*/

        //闪送【送票到家】的测试订单的信息
        /*String data = "{\"orderNo\":\"FT59B63596210D354010952983\",\"contactName\":\"邮寄测试单1\",\"contactPhone\":\"15370283333\","
                + "\"ticketType\":0,\"orderType\":0,"//正常订单 - //0: 普通订单,1: 抢票订单
        
                //+ "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"高新区\",\"address\":\"中江路388号\",\"expressType\":0},"
        
                // + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"二七纪念塔388号\",\"expressType\":0},"
                + "\"mailInfo\":{\"name\":\"邮寄测试单2\",\"mobile\":\"15370283333\",\"province\":\"河南\",\"city\":\"郑州市\",\"district\":\"二七区\",\"address\":\"庆丰街与淮河路北20米路东\",\"expressType\":0},"
                
                //无定制
                //+ "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-09-22 13:35:00\",\"endTime\":\"2017-09-22 14:09:00\",\"customizeType\":0,\"customizeContent\":\"\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"
                
                //有定制
                + "\"bookInfo\":{\"trainNo\":\"G7148\",\"startStation\":\"上海虹桥\",\"endStation\":\"苏州\",\"startTime\":\"2017-11-01 13:35:00\",\"endTime\":\"2017-11-02 14:09:00\",\"customizeType\":7,\"customizeContent\":\"3/1|1/1\",\"acceptOtherSeat\":0,\"backupSeatClass\":\"\"},"
                
                //一个乘客
                //+ "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}]}";
        
                //多个乘客
                + "\"passengers\":[{\"id\":898379388,\"type\":1,\"name\":\"邮寄测试单3-1\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":1,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00},"
                + "{\"id\":898379388,\"type\":2,\"name\":\"邮寄测试单3-2\",\"idType\":3,\"idNo\":\"2727388383\",\"gender\":2,\"seatClass\":4,\"ticketPrice\":39.50,\"issueFee\":5.00}"
                + "]}";*/

        //问题订单处理
        String data = "{\"passengers\":[{\"idNo\":\"51102519790810736X\",\"id\":899740682,\"idType\":1,\"name\":\"聂海鸥\",\"ticketPrice\":30.00,\"gender\":2,\"issueFee\":5.00,\"seatClass\":4,\"type\":1}],\"contactPhone\":\"15370283556\",\"orderNo\":\"FT5A001FC22109414007448490\",\"orderType\":0,\"ticketType\":1,\"contactName\":\"15370283556\",\"bookInfo\":{\"startTime\":\"2017-11-09 15:48:00\",\"backupSeatClass\":\"\",\"customizeType\":5,\"customizeContent\":\"1/A\",\"startStation\":\"门源\",\"endStation\":\"西宁\",\"acceptOtherSeat\":0,\"endTime\":\"2017-11-09 16:25:00\",\"trainNo\":\"D2674\"}}";

        String reqBody = data;
        JSONObject reqData = JSONObject.parseObject(reqBody);

        TrainTongChengOfflineAddOrderThread trainTongChengOfflineAddOrderThread = new TrainTongChengOfflineAddOrderThread(
                reqData);
        trainTongChengOfflineAddOrderThread.run();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
