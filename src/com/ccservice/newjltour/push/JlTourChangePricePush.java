package com.ccservice.newjltour.push;

import java.io.PrintWriter;
import java.io.IOException;
import net.sf.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.newjltour.hoteldb.JlHotelDB;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 深捷旅变价通知
 * @author WH
 */

@SuppressWarnings("serial")
public class JlTourChangePricePush extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //用户编号
        String usercd = "";
        //授权码
        String authno = "";
        //房型ID
        String roomIds = "";
        //RET
        PrintWriter out = null;
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        try {
            //POST请求数据
            out = response.getWriter();
            BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
            String line = "";
            StringBuffer buf = new StringBuffer(1024);
            while ((line = br.readLine()) != null) {
                buf.append(line);
            }
            String param = buf.toString();
            JSONObject req = JSONObject.fromObject(param);
            //用户编号
            usercd = req.getString("usercd");
            if (ElongHotelInterfaceUtil.StringIsNull(usercd)) {
                usercd = req.getString("Usercd");
            }
            //授权码
            authno = req.getString("authno");
            if (ElongHotelInterfaceUtil.StringIsNull(authno)) {
                authno = req.getString("Authno");
            }
            //房型ID
            roomIds = req.getString("roomtypeids");
            //本地配置
            String jlUserCd = PropertyUtil.getValue("jlUserCd");
            //String jlAuthNo = PropertyUtil.getValue("jlAuthNo");//暂无用
            if (!jlUserCd.equals(usercd) || ElongHotelInterfaceUtil.StringIsNull(roomIds)) {//|| !jlAuthNo.equals(authno)
                throw new Exception("ERROR：Request Data Error.");
            }
            //变价或房态变化
            if (!ElongHotelInterfaceUtil.StringIsNull(roomIds)) {
                ThreadUpdate update = new ThreadUpdate(roomIds);
                update.start();
            }
            JSONObject ret = new JSONObject();
            ret.put("usercd", usercd);
            ret.put("authno", authno);
            ret.put("success", 1);
            ret.put("msg", "成功!");
            out.print(ret.toString());
        }
        catch (Exception e) {
            String msg = e.getMessage() == null ? "" : e.getMessage();
            //记录日志
            com.ccservice.huamin.WriteLog.write("深捷旅JSON通知接口", msg);
            //返回深捷旅
            JSONObject ret = new JSONObject();
            ret.put("usercd", usercd);
            ret.put("authno", authno);
            ret.put("success", 8);
            ret.put("msg", msg.startsWith("ERROR：") ? msg.replace("ERROR：", "") : "失败!");
            out.print(ret.toString());
        }
        finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }
}

class ThreadUpdate extends Thread {
    private String roomIds;

    public ThreadUpdate(String roomIds) {
        this.roomIds = roomIds;
    }

    public void run() {
        JlHotelDB.updateChange(roomIds);
    }
}