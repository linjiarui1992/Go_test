package com.ccservice.service;

import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.b2b2c.policy.SupplyMethod;

public class QTrainService extends SupplyMethod implements IQTrainService {

    public boolean trainIssueOrRefuse(long orderid, int state) {
        return QunarTrainMethod.trainIssueOrRefuse(orderid, state);
    }

    public boolean trainRefundresult(String qunarordernum, int state, int reason) {
        return QunarTrainMethod.trainRefundresult(qunarordernum, state, reason);
    }

    public boolean trainRefundPrice(String qunarordernum, int reason, float refundCash) {
        return QunarTrainMethod.trainRefundPrice(qunarordernum, reason, refundCash);
    }
}
