package com.ccservice.service;

import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.interflight.SearchFligehtBean;
import com.ccservice.b2b2c.base.interticket.AllRouteBean;
import com.ccservice.b2b2c.base.zrate.Zrate;

/**
 * 国际机票
 * 作者：邹远超
 * 日期：2014-4-21
 */
public interface IInterRateService {
    /**
     * 
     * 作者：邹远超
     * 日期：2014-4-10
     * 说明：查询国际航班0
     * @param fromCtiy
     * @param toCity
     * @param fromDate
     * @param returnDate
     * @param airCo
     * @param seatType
     * @return
     */
    public AllRouteBean SeachInterFlight(SearchFligehtBean searchFligehtBean);

    /**
     * 
     * 作者：邹远超
     * 日期：2014年5月17日
     * 说明：获取国际机票政策
     * @param searchFligehtBean
     * @return
     */
    public List<Zrate> searchZrate();

    /**
     * 作者：邹远超
     * 日期：2014年5月26日
     * 说明：获取政策
     * @param zrates
     * @param count
     * @return
     */
    public List<Zrate> getsortZrate(List<Zrate> zrates, int count);

    /**
     * 作者：邹远超
     * 日期：2014年5月27日
     * 说明：参考改退签
     * @param str
     * @return
     */
    public List<Map<String, String>> getrefundAndapplychange(String str);
}
