
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PolicyCoordination complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyCoordination">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tempuri.org/}EntityOfDecimal">
 *       &lt;sequence>
 *         &lt;element name="AirlinCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArriveCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfinePolicyType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NotVipAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VipAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PeerRebate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LowerRebate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EfficiencyDateRangeLower" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EfficiencyDateRangeUpper" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PolicyCoordinationValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Remark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="LimitCityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyCoordination", propOrder = {
    "airlinCode",
    "departureCity",
    "arriveCity",
    "confinePolicyType",
    "notVipAccount",
    "vipAccount",
    "peerRebate",
    "lowerRebate",
    "efficiencyDateRangeLower",
    "efficiencyDateRangeUpper",
    "policyCoordinationValue",
    "remark",
    "addAccount",
    "addDate",
    "limitCityName"
})
public class PolicyCoordination
    extends EntityOfDecimal
{

    @XmlElement(name = "AirlinCode")
    protected String airlinCode;
    @XmlElement(name = "DepartureCity")
    protected String departureCity;
    @XmlElement(name = "ArriveCity")
    protected String arriveCity;
    @XmlElement(name = "ConfinePolicyType")
    protected int confinePolicyType;
    @XmlElement(name = "NotVipAccount")
    protected String notVipAccount;
    @XmlElement(name = "VipAccount")
    protected String vipAccount;
    @XmlElement(name = "PeerRebate")
    protected String peerRebate;
    @XmlElement(name = "LowerRebate")
    protected String lowerRebate;
    @XmlElement(name = "EfficiencyDateRangeLower", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar efficiencyDateRangeLower;
    @XmlElement(name = "EfficiencyDateRangeUpper", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar efficiencyDateRangeUpper;
    @XmlElement(name = "PolicyCoordinationValue", required = true)
    protected BigDecimal policyCoordinationValue;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "AddAccount")
    protected String addAccount;
    @XmlElement(name = "AddDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar addDate;
    @XmlElement(name = "LimitCityName")
    protected String limitCityName;

    /**
     * Gets the value of the airlinCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlinCode() {
        return airlinCode;
    }

    /**
     * Sets the value of the airlinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlinCode(String value) {
        this.airlinCode = value;
    }

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCity(String value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the arriveCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArriveCity() {
        return arriveCity;
    }

    /**
     * Sets the value of the arriveCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArriveCity(String value) {
        this.arriveCity = value;
    }

    /**
     * Gets the value of the confinePolicyType property.
     * 
     */
    public int getConfinePolicyType() {
        return confinePolicyType;
    }

    /**
     * Sets the value of the confinePolicyType property.
     * 
     */
    public void setConfinePolicyType(int value) {
        this.confinePolicyType = value;
    }

    /**
     * Gets the value of the notVipAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotVipAccount() {
        return notVipAccount;
    }

    /**
     * Sets the value of the notVipAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotVipAccount(String value) {
        this.notVipAccount = value;
    }

    /**
     * Gets the value of the vipAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVipAccount() {
        return vipAccount;
    }

    /**
     * Sets the value of the vipAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVipAccount(String value) {
        this.vipAccount = value;
    }

    /**
     * Gets the value of the peerRebate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeerRebate() {
        return peerRebate;
    }

    /**
     * Sets the value of the peerRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeerRebate(String value) {
        this.peerRebate = value;
    }

    /**
     * Gets the value of the lowerRebate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowerRebate() {
        return lowerRebate;
    }

    /**
     * Sets the value of the lowerRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowerRebate(String value) {
        this.lowerRebate = value;
    }

    /**
     * Gets the value of the efficiencyDateRangeLower property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEfficiencyDateRangeLower() {
        return efficiencyDateRangeLower;
    }

    /**
     * Sets the value of the efficiencyDateRangeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEfficiencyDateRangeLower(XMLGregorianCalendar value) {
        this.efficiencyDateRangeLower = value;
    }

    /**
     * Gets the value of the efficiencyDateRangeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEfficiencyDateRangeUpper() {
        return efficiencyDateRangeUpper;
    }

    /**
     * Sets the value of the efficiencyDateRangeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEfficiencyDateRangeUpper(XMLGregorianCalendar value) {
        this.efficiencyDateRangeUpper = value;
    }

    /**
     * Gets the value of the policyCoordinationValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyCoordinationValue() {
        return policyCoordinationValue;
    }

    /**
     * Sets the value of the policyCoordinationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyCoordinationValue(BigDecimal value) {
        this.policyCoordinationValue = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * Sets the value of the remark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the addAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddAccount() {
        return addAccount;
    }

    /**
     * Sets the value of the addAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddAccount(String value) {
        this.addAccount = value;
    }

    /**
     * Gets the value of the addDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAddDate() {
        return addDate;
    }

    /**
     * Sets the value of the addDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAddDate(XMLGregorianCalendar value) {
        this.addDate = value;
    }

    /**
     * Gets the value of the limitCityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCityName() {
        return limitCityName;
    }

    /**
     * Sets the value of the limitCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCityName(String value) {
        this.limitCityName = value;
    }

}
