
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestrictFlightType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RestrictFlightType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ApplyAll"/>
 *     &lt;enumeration value="ApplyOnly"/>
 *     &lt;enumeration value="Excluded"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RestrictFlightType")
@XmlEnum
public enum RestrictFlightType {

    @XmlEnumValue("ApplyAll")
    APPLY_ALL("ApplyAll"),
    @XmlEnumValue("ApplyOnly")
    APPLY_ONLY("ApplyOnly"),
    @XmlEnumValue("Excluded")
    EXCLUDED("Excluded");
    private final String value;

    RestrictFlightType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RestrictFlightType fromValue(String v) {
        for (RestrictFlightType c: RestrictFlightType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
