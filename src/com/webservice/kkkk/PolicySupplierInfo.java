
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicySupplierInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicySupplierInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SupplierID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="WorkTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefundTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefundTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsValidAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CityNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFree" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SupplierTicketEfficiency" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="SupplierRefundEfficiency" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsCanChild" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ChildRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsCanInfant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="InfantDeduction" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsVipRight" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDynamicSpecialRight" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFixecSpecialRight" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsPeerRight" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsSetBasicRebate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="BasicRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PulishGeneralPolicyAirline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PulishSpecialPolicyAirLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayWorkTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayWorkTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendWorkTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendWorkTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayRefundTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayRefundTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendRefundTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendRefundTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayInvalidatedTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkdayInvalidatedTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendInvalidatedTimeLower" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeekendInvalidatedTimeUpper" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LazyRebate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ValidBackWithHolding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsCanPulishHighRebatePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicySupplierInfo", propOrder = {
    "supplierID",
    "workTimeLower",
    "workTimeUpper",
    "refundTimeLower",
    "refundTimeUpper",
    "isValidAccount",
    "cityNumber",
    "isFree",
    "supplierTicketEfficiency",
    "supplierRefundEfficiency",
    "isCanChild",
    "childRebate",
    "isCanInfant",
    "infantDeduction",
    "isVipRight",
    "isDynamicSpecialRight",
    "isFixecSpecialRight",
    "isPeerRight",
    "isSetBasicRebate",
    "basicRebate",
    "pulishGeneralPolicyAirline",
    "pulishSpecialPolicyAirLine",
    "workdayWorkTimeLower",
    "workdayWorkTimeUpper",
    "weekendWorkTimeLower",
    "weekendWorkTimeUpper",
    "workdayRefundTimeLower",
    "workdayRefundTimeUpper",
    "weekendRefundTimeLower",
    "weekendRefundTimeUpper",
    "workdayInvalidatedTimeLower",
    "workdayInvalidatedTimeUpper",
    "weekendInvalidatedTimeLower",
    "weekendInvalidatedTimeUpper",
    "lazyRebate",
    "validBackWithHolding",
    "isCanPulishHighRebatePolicy"
})
public class PolicySupplierInfo {

    @XmlElement(name = "SupplierID", required = true)
    protected BigDecimal supplierID;
    @XmlElement(name = "WorkTimeLower")
    protected String workTimeLower;
    @XmlElement(name = "WorkTimeUpper")
    protected String workTimeUpper;
    @XmlElement(name = "RefundTimeLower")
    protected String refundTimeLower;
    @XmlElement(name = "RefundTimeUpper")
    protected String refundTimeUpper;
    @XmlElement(name = "IsValidAccount")
    protected boolean isValidAccount;
    @XmlElement(name = "CityNumber")
    protected String cityNumber;
    @XmlElement(name = "IsFree")
    protected boolean isFree;
    @XmlElement(name = "SupplierTicketEfficiency")
    protected int supplierTicketEfficiency;
    @XmlElement(name = "SupplierRefundEfficiency")
    protected int supplierRefundEfficiency;
    @XmlElement(name = "IsCanChild")
    protected boolean isCanChild;
    @XmlElement(name = "ChildRebate", required = true)
    protected BigDecimal childRebate;
    @XmlElement(name = "IsCanInfant")
    protected boolean isCanInfant;
    @XmlElement(name = "InfantDeduction", required = true)
    protected BigDecimal infantDeduction;
    @XmlElement(name = "IsVipRight")
    protected boolean isVipRight;
    @XmlElement(name = "IsDynamicSpecialRight")
    protected boolean isDynamicSpecialRight;
    @XmlElement(name = "IsFixecSpecialRight")
    protected boolean isFixecSpecialRight;
    @XmlElement(name = "IsPeerRight")
    protected boolean isPeerRight;
    @XmlElement(name = "IsSetBasicRebate")
    protected boolean isSetBasicRebate;
    @XmlElement(name = "BasicRebate", required = true)
    protected BigDecimal basicRebate;
    @XmlElement(name = "PulishGeneralPolicyAirline")
    protected String pulishGeneralPolicyAirline;
    @XmlElement(name = "PulishSpecialPolicyAirLine")
    protected String pulishSpecialPolicyAirLine;
    @XmlElement(name = "WorkdayWorkTimeLower")
    protected String workdayWorkTimeLower;
    @XmlElement(name = "WorkdayWorkTimeUpper")
    protected String workdayWorkTimeUpper;
    @XmlElement(name = "WeekendWorkTimeLower")
    protected String weekendWorkTimeLower;
    @XmlElement(name = "WeekendWorkTimeUpper")
    protected String weekendWorkTimeUpper;
    @XmlElement(name = "WorkdayRefundTimeLower")
    protected String workdayRefundTimeLower;
    @XmlElement(name = "WorkdayRefundTimeUpper")
    protected String workdayRefundTimeUpper;
    @XmlElement(name = "WeekendRefundTimeLower")
    protected String weekendRefundTimeLower;
    @XmlElement(name = "WeekendRefundTimeUpper")
    protected String weekendRefundTimeUpper;
    @XmlElement(name = "WorkdayInvalidatedTimeLower")
    protected String workdayInvalidatedTimeLower;
    @XmlElement(name = "WorkdayInvalidatedTimeUpper")
    protected String workdayInvalidatedTimeUpper;
    @XmlElement(name = "WeekendInvalidatedTimeLower")
    protected String weekendInvalidatedTimeLower;
    @XmlElement(name = "WeekendInvalidatedTimeUpper")
    protected String weekendInvalidatedTimeUpper;
    @XmlElement(name = "LazyRebate")
    protected boolean lazyRebate;
    @XmlElement(name = "ValidBackWithHolding")
    protected boolean validBackWithHolding;
    @XmlElement(name = "IsCanPulishHighRebatePolicy")
    protected boolean isCanPulishHighRebatePolicy;

    /**
     * Gets the value of the supplierID property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSupplierID() {
        return supplierID;
    }

    /**
     * Sets the value of the supplierID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSupplierID(BigDecimal value) {
        this.supplierID = value;
    }

    /**
     * Gets the value of the workTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkTimeLower() {
        return workTimeLower;
    }

    /**
     * Sets the value of the workTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkTimeLower(String value) {
        this.workTimeLower = value;
    }

    /**
     * Gets the value of the workTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkTimeUpper() {
        return workTimeUpper;
    }

    /**
     * Sets the value of the workTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkTimeUpper(String value) {
        this.workTimeUpper = value;
    }

    /**
     * Gets the value of the refundTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundTimeLower() {
        return refundTimeLower;
    }

    /**
     * Sets the value of the refundTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundTimeLower(String value) {
        this.refundTimeLower = value;
    }

    /**
     * Gets the value of the refundTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundTimeUpper() {
        return refundTimeUpper;
    }

    /**
     * Sets the value of the refundTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundTimeUpper(String value) {
        this.refundTimeUpper = value;
    }

    /**
     * Gets the value of the isValidAccount property.
     * 
     */
    public boolean isIsValidAccount() {
        return isValidAccount;
    }

    /**
     * Sets the value of the isValidAccount property.
     * 
     */
    public void setIsValidAccount(boolean value) {
        this.isValidAccount = value;
    }

    /**
     * Gets the value of the cityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityNumber() {
        return cityNumber;
    }

    /**
     * Sets the value of the cityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityNumber(String value) {
        this.cityNumber = value;
    }

    /**
     * Gets the value of the isFree property.
     * 
     */
    public boolean isIsFree() {
        return isFree;
    }

    /**
     * Sets the value of the isFree property.
     * 
     */
    public void setIsFree(boolean value) {
        this.isFree = value;
    }

    /**
     * Gets the value of the supplierTicketEfficiency property.
     * 
     */
    public int getSupplierTicketEfficiency() {
        return supplierTicketEfficiency;
    }

    /**
     * Sets the value of the supplierTicketEfficiency property.
     * 
     */
    public void setSupplierTicketEfficiency(int value) {
        this.supplierTicketEfficiency = value;
    }

    /**
     * Gets the value of the supplierRefundEfficiency property.
     * 
     */
    public int getSupplierRefundEfficiency() {
        return supplierRefundEfficiency;
    }

    /**
     * Sets the value of the supplierRefundEfficiency property.
     * 
     */
    public void setSupplierRefundEfficiency(int value) {
        this.supplierRefundEfficiency = value;
    }

    /**
     * Gets the value of the isCanChild property.
     * 
     */
    public boolean isIsCanChild() {
        return isCanChild;
    }

    /**
     * Sets the value of the isCanChild property.
     * 
     */
    public void setIsCanChild(boolean value) {
        this.isCanChild = value;
    }

    /**
     * Gets the value of the childRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChildRebate() {
        return childRebate;
    }

    /**
     * Sets the value of the childRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChildRebate(BigDecimal value) {
        this.childRebate = value;
    }

    /**
     * Gets the value of the isCanInfant property.
     * 
     */
    public boolean isIsCanInfant() {
        return isCanInfant;
    }

    /**
     * Sets the value of the isCanInfant property.
     * 
     */
    public void setIsCanInfant(boolean value) {
        this.isCanInfant = value;
    }

    /**
     * Gets the value of the infantDeduction property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInfantDeduction() {
        return infantDeduction;
    }

    /**
     * Sets the value of the infantDeduction property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInfantDeduction(BigDecimal value) {
        this.infantDeduction = value;
    }

    /**
     * Gets the value of the isVipRight property.
     * 
     */
    public boolean isIsVipRight() {
        return isVipRight;
    }

    /**
     * Sets the value of the isVipRight property.
     * 
     */
    public void setIsVipRight(boolean value) {
        this.isVipRight = value;
    }

    /**
     * Gets the value of the isDynamicSpecialRight property.
     * 
     */
    public boolean isIsDynamicSpecialRight() {
        return isDynamicSpecialRight;
    }

    /**
     * Sets the value of the isDynamicSpecialRight property.
     * 
     */
    public void setIsDynamicSpecialRight(boolean value) {
        this.isDynamicSpecialRight = value;
    }

    /**
     * Gets the value of the isFixecSpecialRight property.
     * 
     */
    public boolean isIsFixecSpecialRight() {
        return isFixecSpecialRight;
    }

    /**
     * Sets the value of the isFixecSpecialRight property.
     * 
     */
    public void setIsFixecSpecialRight(boolean value) {
        this.isFixecSpecialRight = value;
    }

    /**
     * Gets the value of the isPeerRight property.
     * 
     */
    public boolean isIsPeerRight() {
        return isPeerRight;
    }

    /**
     * Sets the value of the isPeerRight property.
     * 
     */
    public void setIsPeerRight(boolean value) {
        this.isPeerRight = value;
    }

    /**
     * Gets the value of the isSetBasicRebate property.
     * 
     */
    public boolean isIsSetBasicRebate() {
        return isSetBasicRebate;
    }

    /**
     * Sets the value of the isSetBasicRebate property.
     * 
     */
    public void setIsSetBasicRebate(boolean value) {
        this.isSetBasicRebate = value;
    }

    /**
     * Gets the value of the basicRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBasicRebate() {
        return basicRebate;
    }

    /**
     * Sets the value of the basicRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBasicRebate(BigDecimal value) {
        this.basicRebate = value;
    }

    /**
     * Gets the value of the pulishGeneralPolicyAirline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPulishGeneralPolicyAirline() {
        return pulishGeneralPolicyAirline;
    }

    /**
     * Sets the value of the pulishGeneralPolicyAirline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPulishGeneralPolicyAirline(String value) {
        this.pulishGeneralPolicyAirline = value;
    }

    /**
     * Gets the value of the pulishSpecialPolicyAirLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPulishSpecialPolicyAirLine() {
        return pulishSpecialPolicyAirLine;
    }

    /**
     * Sets the value of the pulishSpecialPolicyAirLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPulishSpecialPolicyAirLine(String value) {
        this.pulishSpecialPolicyAirLine = value;
    }

    /**
     * Gets the value of the workdayWorkTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayWorkTimeLower() {
        return workdayWorkTimeLower;
    }

    /**
     * Sets the value of the workdayWorkTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayWorkTimeLower(String value) {
        this.workdayWorkTimeLower = value;
    }

    /**
     * Gets the value of the workdayWorkTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayWorkTimeUpper() {
        return workdayWorkTimeUpper;
    }

    /**
     * Sets the value of the workdayWorkTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayWorkTimeUpper(String value) {
        this.workdayWorkTimeUpper = value;
    }

    /**
     * Gets the value of the weekendWorkTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendWorkTimeLower() {
        return weekendWorkTimeLower;
    }

    /**
     * Sets the value of the weekendWorkTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendWorkTimeLower(String value) {
        this.weekendWorkTimeLower = value;
    }

    /**
     * Gets the value of the weekendWorkTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendWorkTimeUpper() {
        return weekendWorkTimeUpper;
    }

    /**
     * Sets the value of the weekendWorkTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendWorkTimeUpper(String value) {
        this.weekendWorkTimeUpper = value;
    }

    /**
     * Gets the value of the workdayRefundTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayRefundTimeLower() {
        return workdayRefundTimeLower;
    }

    /**
     * Sets the value of the workdayRefundTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayRefundTimeLower(String value) {
        this.workdayRefundTimeLower = value;
    }

    /**
     * Gets the value of the workdayRefundTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayRefundTimeUpper() {
        return workdayRefundTimeUpper;
    }

    /**
     * Sets the value of the workdayRefundTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayRefundTimeUpper(String value) {
        this.workdayRefundTimeUpper = value;
    }

    /**
     * Gets the value of the weekendRefundTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendRefundTimeLower() {
        return weekendRefundTimeLower;
    }

    /**
     * Sets the value of the weekendRefundTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendRefundTimeLower(String value) {
        this.weekendRefundTimeLower = value;
    }

    /**
     * Gets the value of the weekendRefundTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendRefundTimeUpper() {
        return weekendRefundTimeUpper;
    }

    /**
     * Sets the value of the weekendRefundTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendRefundTimeUpper(String value) {
        this.weekendRefundTimeUpper = value;
    }

    /**
     * Gets the value of the workdayInvalidatedTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayInvalidatedTimeLower() {
        return workdayInvalidatedTimeLower;
    }

    /**
     * Sets the value of the workdayInvalidatedTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayInvalidatedTimeLower(String value) {
        this.workdayInvalidatedTimeLower = value;
    }

    /**
     * Gets the value of the workdayInvalidatedTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkdayInvalidatedTimeUpper() {
        return workdayInvalidatedTimeUpper;
    }

    /**
     * Sets the value of the workdayInvalidatedTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkdayInvalidatedTimeUpper(String value) {
        this.workdayInvalidatedTimeUpper = value;
    }

    /**
     * Gets the value of the weekendInvalidatedTimeLower property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendInvalidatedTimeLower() {
        return weekendInvalidatedTimeLower;
    }

    /**
     * Sets the value of the weekendInvalidatedTimeLower property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendInvalidatedTimeLower(String value) {
        this.weekendInvalidatedTimeLower = value;
    }

    /**
     * Gets the value of the weekendInvalidatedTimeUpper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeekendInvalidatedTimeUpper() {
        return weekendInvalidatedTimeUpper;
    }

    /**
     * Sets the value of the weekendInvalidatedTimeUpper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeekendInvalidatedTimeUpper(String value) {
        this.weekendInvalidatedTimeUpper = value;
    }

    /**
     * Gets the value of the lazyRebate property.
     * 
     */
    public boolean isLazyRebate() {
        return lazyRebate;
    }

    /**
     * Sets the value of the lazyRebate property.
     * 
     */
    public void setLazyRebate(boolean value) {
        this.lazyRebate = value;
    }

    /**
     * Gets the value of the validBackWithHolding property.
     * 
     */
    public boolean isValidBackWithHolding() {
        return validBackWithHolding;
    }

    /**
     * Sets the value of the validBackWithHolding property.
     * 
     */
    public void setValidBackWithHolding(boolean value) {
        this.validBackWithHolding = value;
    }

    /**
     * Gets the value of the isCanPulishHighRebatePolicy property.
     * 
     */
    public boolean isIsCanPulishHighRebatePolicy() {
        return isCanPulishHighRebatePolicy;
    }

    /**
     * Sets the value of the isCanPulishHighRebatePolicy property.
     * 
     */
    public void setIsCanPulishHighRebatePolicy(boolean value) {
        this.isCanPulishHighRebatePolicy = value;
    }

}
