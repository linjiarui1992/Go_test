
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ApplyFormView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplyFormView">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FoundationOrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Pnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BPnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequireCancelSeat" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Type" type="{http://tempuri.org/}ApplyType"/>
 *         &lt;element name="Status" type="{http://tempuri.org/}ApplyStatus"/>
 *         &lt;element name="Applier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplySubType" type="{http://tempuri.org/}ApplySubType"/>
 *         &lt;element name="ApplyRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Items" type="{http://tempuri.org/}ArrayOfApplyItemView" minOccurs="0"/>
 *         &lt;element name="PostponeFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="HandleInfos" type="{http://tempuri.org/}ArrayOfApplyHandleInfo" minOccurs="0"/>
 *         &lt;element name="CoordinationInfos" type="{http://tempuri.org/}ArrayOfCoordinationInfo" minOccurs="0"/>
 *         &lt;element name="IsAgainRefund" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ActualRefundAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsRefundForSpecialReason" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyFormView", propOrder = {
    "id",
    "orderId",
    "foundationOrderId",
    "pnr",
    "bPnr",
    "requireCancelSeat",
    "type",
    "status",
    "applier",
    "applierName",
    "applySubType",
    "applyRemark",
    "applyTime",
    "items",
    "postponeFee",
    "handleInfos",
    "coordinationInfos",
    "isAgainRefund",
    "actualRefundAmount",
    "isRefundForSpecialReason"
})
public class ApplyFormView {

    @XmlElement(name = "Id", required = true)
    protected BigDecimal id;
    @XmlElement(name = "OrderId", required = true)
    protected BigDecimal orderId;
    @XmlElement(name = "FoundationOrderId", required = true)
    protected BigDecimal foundationOrderId;
    @XmlElement(name = "Pnr")
    protected String pnr;
    @XmlElement(name = "BPnr")
    protected String bPnr;
    @XmlElement(name = "RequireCancelSeat")
    protected boolean requireCancelSeat;
    @XmlElement(name = "Type", required = true)
    protected ApplyType type;
    @XmlElement(name = "Status", required = true)
    protected ApplyStatus status;
    @XmlElement(name = "Applier")
    protected String applier;
    @XmlElement(name = "ApplierName")
    protected String applierName;
    @XmlElement(name = "ApplySubType", required = true, nillable = true)
    protected ApplySubType applySubType;
    @XmlElement(name = "ApplyRemark")
    protected String applyRemark;
    @XmlElement(name = "ApplyTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar applyTime;
    @XmlElement(name = "Items")
    protected ArrayOfApplyItemView items;
    @XmlElement(name = "PostponeFee", required = true, nillable = true)
    protected BigDecimal postponeFee;
    @XmlElement(name = "HandleInfos")
    protected ArrayOfApplyHandleInfo handleInfos;
    @XmlElement(name = "CoordinationInfos")
    protected ArrayOfCoordinationInfo coordinationInfos;
    @XmlElement(name = "IsAgainRefund")
    protected boolean isAgainRefund;
    @XmlElement(name = "ActualRefundAmount", required = true)
    protected BigDecimal actualRefundAmount;
    @XmlElement(name = "IsRefundForSpecialReason")
    protected boolean isRefundForSpecialReason;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOrderId(BigDecimal value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the foundationOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFoundationOrderId() {
        return foundationOrderId;
    }

    /**
     * Sets the value of the foundationOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFoundationOrderId(BigDecimal value) {
        this.foundationOrderId = value;
    }

    /**
     * Gets the value of the pnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnr() {
        return pnr;
    }

    /**
     * Sets the value of the pnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnr(String value) {
        this.pnr = value;
    }

    /**
     * Gets the value of the bPnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBPnr() {
        return bPnr;
    }

    /**
     * Sets the value of the bPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBPnr(String value) {
        this.bPnr = value;
    }

    /**
     * Gets the value of the requireCancelSeat property.
     * 
     */
    public boolean isRequireCancelSeat() {
        return requireCancelSeat;
    }

    /**
     * Sets the value of the requireCancelSeat property.
     * 
     */
    public void setRequireCancelSeat(boolean value) {
        this.requireCancelSeat = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ApplyType }
     *     
     */
    public ApplyType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyType }
     *     
     */
    public void setType(ApplyType value) {
        this.type = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ApplyStatus }
     *     
     */
    public ApplyStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyStatus }
     *     
     */
    public void setStatus(ApplyStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the applier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplier() {
        return applier;
    }

    /**
     * Sets the value of the applier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplier(String value) {
        this.applier = value;
    }

    /**
     * Gets the value of the applierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplierName() {
        return applierName;
    }

    /**
     * Sets the value of the applierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplierName(String value) {
        this.applierName = value;
    }

    /**
     * Gets the value of the applySubType property.
     * 
     * @return
     *     possible object is
     *     {@link ApplySubType }
     *     
     */
    public ApplySubType getApplySubType() {
        return applySubType;
    }

    /**
     * Sets the value of the applySubType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplySubType }
     *     
     */
    public void setApplySubType(ApplySubType value) {
        this.applySubType = value;
    }

    /**
     * Gets the value of the applyRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyRemark() {
        return applyRemark;
    }

    /**
     * Sets the value of the applyRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyRemark(String value) {
        this.applyRemark = value;
    }

    /**
     * Gets the value of the applyTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApplyTime() {
        return applyTime;
    }

    /**
     * Sets the value of the applyTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApplyTime(XMLGregorianCalendar value) {
        this.applyTime = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfApplyItemView }
     *     
     */
    public ArrayOfApplyItemView getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfApplyItemView }
     *     
     */
    public void setItems(ArrayOfApplyItemView value) {
        this.items = value;
    }

    /**
     * Gets the value of the postponeFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPostponeFee() {
        return postponeFee;
    }

    /**
     * Sets the value of the postponeFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPostponeFee(BigDecimal value) {
        this.postponeFee = value;
    }

    /**
     * Gets the value of the handleInfos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfApplyHandleInfo }
     *     
     */
    public ArrayOfApplyHandleInfo getHandleInfos() {
        return handleInfos;
    }

    /**
     * Sets the value of the handleInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfApplyHandleInfo }
     *     
     */
    public void setHandleInfos(ArrayOfApplyHandleInfo value) {
        this.handleInfos = value;
    }

    /**
     * Gets the value of the coordinationInfos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCoordinationInfo }
     *     
     */
    public ArrayOfCoordinationInfo getCoordinationInfos() {
        return coordinationInfos;
    }

    /**
     * Sets the value of the coordinationInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCoordinationInfo }
     *     
     */
    public void setCoordinationInfos(ArrayOfCoordinationInfo value) {
        this.coordinationInfos = value;
    }

    /**
     * Gets the value of the isAgainRefund property.
     * 
     */
    public boolean isIsAgainRefund() {
        return isAgainRefund;
    }

    /**
     * Sets the value of the isAgainRefund property.
     * 
     */
    public void setIsAgainRefund(boolean value) {
        this.isAgainRefund = value;
    }

    /**
     * Gets the value of the actualRefundAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActualRefundAmount() {
        return actualRefundAmount;
    }

    /**
     * Sets the value of the actualRefundAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActualRefundAmount(BigDecimal value) {
        this.actualRefundAmount = value;
    }

    /**
     * Gets the value of the isRefundForSpecialReason property.
     * 
     */
    public boolean isIsRefundForSpecialReason() {
        return isRefundForSpecialReason;
    }

    /**
     * Sets the value of the isRefundForSpecialReason property.
     * 
     */
    public void setIsRefundForSpecialReason(boolean value) {
        this.isRefundForSpecialReason = value;
    }

}
