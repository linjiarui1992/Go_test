
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SpecialFixedAddType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SpecialFixedAddType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Price"/>
 *     &lt;enumeration value="Discount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SpecialFixedAddType")
@XmlEnum
public enum SpecialFixedAddType {

    @XmlEnumValue("Price")
    PRICE("Price"),
    @XmlEnumValue("Discount")
    DISCOUNT("Discount");
    private final String value;

    SpecialFixedAddType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SpecialFixedAddType fromValue(String v) {
        for (SpecialFixedAddType c: SpecialFixedAddType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
