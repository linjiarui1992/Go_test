
package com.speed.iesales.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for syncRateRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="syncRateRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aircomp2c" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="minDiscount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="psgType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strategyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "syncRateRequest", propOrder = {
    "aircomp2C",
    "appcode",
    "minDiscount",
    "pageSize",
    "psgType",
    "rateType",
    "sign",
    "strategyId",
    "updateTime",
    "username"
})
public class SyncRateRequest {

    @XmlElement(name = "aircomp2c")
    protected String aircomp2C;
    protected String appcode;
    protected String minDiscount;
    protected String pageSize;
    protected String psgType;
    protected String rateType;
    protected String sign;
    protected String strategyId;
    protected String updateTime;
    protected String username;

    /**
     * Gets the value of the aircomp2C property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAircomp2C() {
        return aircomp2C;
    }

    /**
     * Sets the value of the aircomp2C property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAircomp2C(String value) {
        this.aircomp2C = value;
    }

    /**
     * Gets the value of the appcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppcode() {
        return appcode;
    }

    /**
     * Sets the value of the appcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppcode(String value) {
        this.appcode = value;
    }

    /**
     * Gets the value of the minDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinDiscount() {
        return minDiscount;
    }

    /**
     * Sets the value of the minDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinDiscount(String value) {
        this.minDiscount = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageSize(String value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the psgType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPsgType() {
        return psgType;
    }

    /**
     * Sets the value of the psgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPsgType(String value) {
        this.psgType = value;
    }

    /**
     * Gets the value of the rateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateType() {
        return rateType;
    }

    /**
     * Sets the value of the rateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateType(String value) {
        this.rateType = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

    /**
     * Gets the value of the strategyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrategyId() {
        return strategyId;
    }

    /**
     * Sets the value of the strategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrategyId(String value) {
        this.strategyId = value;
    }

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateTime(String value) {
        this.updateTime = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

}
