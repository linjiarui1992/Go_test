
package com.speed.iesales.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for syncRateDelResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="syncRateDelResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://service.iesales.speed.com/}statusResponse">
 *       &lt;sequence>
 *         &lt;element name="deleteStrategyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rateList" type="{http://service.iesales.speed.com/}rateDel" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="strategyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "syncRateDelResponse", propOrder = {
    "deleteStrategyId",
    "rateList",
    "strategyId",
    "updateTime"
})
public class SyncRateDelResponse
    extends StatusResponse
{

    protected String deleteStrategyId;
    @XmlElement(nillable = true)
    protected List<RateDel> rateList;
    protected String strategyId;
    protected String updateTime;

    /**
     * Gets the value of the deleteStrategyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteStrategyId() {
        return deleteStrategyId;
    }

    /**
     * Sets the value of the deleteStrategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteStrategyId(String value) {
        this.deleteStrategyId = value;
    }

    /**
     * Gets the value of the rateList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateDel }
     * 
     * 
     */
    public List<RateDel> getRateList() {
        if (rateList == null) {
            rateList = new ArrayList<RateDel>();
        }
        return this.rateList;
    }

    /**
     * Gets the value of the strategyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrategyId() {
        return strategyId;
    }

    /**
     * Sets the value of the strategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrategyId(String value) {
        this.strategyId = value;
    }

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateTime(String value) {
        this.updateTime = value;
    }

}
