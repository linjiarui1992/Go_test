
package com.liantuo.webservice.version2_0.createpolicyorderbypnr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0.createpolicyorderbypnr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreatePolicyOrderByPNRReplyParam1_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "param1");
    private final static QName _CreatePolicyOrderByPNRReplyOrder_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "order");
    private final static QName _CreatePolicyOrderByPNRReplyParam2_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "param2");
    private final static QName _CreatePolicyOrderByPNRReplyReturnCode_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "returnCode");
    private final static QName _CreatePolicyOrderByPNRReplyReturnMessage_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "returnMessage");
    private final static QName _WSPassengerParam3_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "param3");
    private final static QName _CreatePolicyOrderByPNRRequestIsPay_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "isPay");
    private final static QName _CreatePolicyOrderByPNRRequestB2CCreatorCn_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "b2cCreatorCn");
    private final static QName _WSPolicyOrderIncreaseSystemCharge_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "increaseSystemCharge");
    private final static QName _PaymentInfoTradeNo_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "tradeNo");
    private final static QName _PaymentInfoSettlePrice_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "settlePrice");
    private final static QName _PaymentInfoPayerAccount_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "payerAccount");
    private final static QName _PaymentInfoPaymentUrl_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "paymentUrl");
    private final static QName _FlightInfoPlaneModel_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "planeModel");
    private final static QName _FlightInfoDepCode_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "depCode");
    private final static QName _FlightInfoArrDate_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "arrDate");
    private final static QName _FlightInfoFlightNo_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "flightNo");
    private final static QName _FlightInfoDepDate_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "depDate");
    private final static QName _FlightInfoArrCode_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "arrCode");
    private final static QName _FlightInfoSeatClass_QNAME = new QName("http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", "seatClass");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0.createpolicyorderbypnr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRReply }
     * 
     */
    public CreatePolicyOrderByPNRReply createCreatePolicyOrderByPNRReply() {
        return new CreatePolicyOrderByPNRReply();
    }

    /**
     * Create an instance of {@link WSPassenger }
     * 
     */
    public WSPassenger createWSPassenger() {
        return new WSPassenger();
    }

    /**
     * Create an instance of {@link ArrayOfWSPassenger }
     * 
     */
    public ArrayOfWSPassenger createArrayOfWSPassenger() {
        return new ArrayOfWSPassenger();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRRequest }
     * 
     */
    public CreatePolicyOrderByPNRRequest createCreatePolicyOrderByPNRRequest() {
        return new CreatePolicyOrderByPNRRequest();
    }

    /**
     * Create an instance of {@link WSPolicyOrder }
     * 
     */
    public WSPolicyOrder createWSPolicyOrder() {
        return new WSPolicyOrder();
    }

    /**
     * Create an instance of {@link PaymentInfo }
     * 
     */
    public PaymentInfo createPaymentInfo() {
        return new PaymentInfo();
    }

    /**
     * Create an instance of {@link FlightInfo }
     * 
     */
    public FlightInfo createFlightInfo() {
        return new FlightInfo();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNR }
     * 
     */
    public CreatePolicyOrderByPNR createCreatePolicyOrderByPNR() {
        return new CreatePolicyOrderByPNR();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRResponse }
     * 
     */
    public CreatePolicyOrderByPNRResponse createCreatePolicyOrderByPNRResponse() {
        return new CreatePolicyOrderByPNRResponse();
    }

    /**
     * Create an instance of {@link ArrayOfFlightInfo }
     * 
     */
    public ArrayOfFlightInfo createArrayOfFlightInfo() {
        return new ArrayOfFlightInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRReplyParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, CreatePolicyOrderByPNRReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSPolicyOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "order", scope = CreatePolicyOrderByPNRReply.class)
    public JAXBElement<WSPolicyOrder> createCreatePolicyOrderByPNRReplyOrder(WSPolicyOrder value) {
        return new JAXBElement<WSPolicyOrder>(_CreatePolicyOrderByPNRReplyOrder_QNAME, WSPolicyOrder.class, CreatePolicyOrderByPNRReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRReplyParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam2_QNAME, String.class, CreatePolicyOrderByPNRReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "returnCode", scope = CreatePolicyOrderByPNRReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRReplyReturnCode(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyReturnCode_QNAME, String.class, CreatePolicyOrderByPNRReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "returnMessage", scope = CreatePolicyOrderByPNRReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRReplyReturnMessage(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyReturnMessage_QNAME, String.class, CreatePolicyOrderByPNRReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param2", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam2_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param3", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRRequestParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, CreatePolicyOrderByPNRRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRRequestParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam2_QNAME, String.class, CreatePolicyOrderByPNRRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param3", scope = CreatePolicyOrderByPNRRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRRequestParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, CreatePolicyOrderByPNRRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "isPay", scope = CreatePolicyOrderByPNRRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRRequestIsPay(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRRequestIsPay_QNAME, String.class, CreatePolicyOrderByPNRRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "b2cCreatorCn", scope = CreatePolicyOrderByPNRRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRRequestB2CCreatorCn(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRRequestB2CCreatorCn_QNAME, String.class, CreatePolicyOrderByPNRRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param2", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam2_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param3", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "increaseSystemCharge", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderIncreaseSystemCharge(String value) {
        return new JAXBElement<String>(_WSPolicyOrderIncreaseSystemCharge_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "tradeNo", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoTradeNo(String value) {
        return new JAXBElement<String>(_PaymentInfoTradeNo_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "settlePrice", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoSettlePrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoSettlePrice_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "payerAccount", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPayerAccount(String value) {
        return new JAXBElement<String>(_PaymentInfoPayerAccount_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "paymentUrl", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPaymentUrl(String value) {
        return new JAXBElement<String>(_PaymentInfoPaymentUrl_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "param1", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRReplyParam1_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "planeModel", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoPlaneModel(String value) {
        return new JAXBElement<String>(_FlightInfoPlaneModel_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "depCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepCode(String value) {
        return new JAXBElement<String>(_FlightInfoDepCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "arrDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrDate(String value) {
        return new JAXBElement<String>(_FlightInfoArrDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "flightNo", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoFlightNo(String value) {
        return new JAXBElement<String>(_FlightInfoFlightNo_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "depDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepDate(String value) {
        return new JAXBElement<String>(_FlightInfoDepDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "arrCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrCode(String value) {
        return new JAXBElement<String>(_FlightInfoArrCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", name = "seatClass", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoSeatClass(String value) {
        return new JAXBElement<String>(_FlightInfoSeatClass_QNAME, String.class, FlightInfo.class, value);
    }

}
