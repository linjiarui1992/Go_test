
package com.liantuo.webservice.version2_0.createpolicyorderbypnr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSPolicyOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSPolicyOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commisionInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="flightInfoList" type="{http://createpolicyorderbypnr.version2_0.webservice.liantuo.com}ArrayOfFlightInfo"/>
 *         &lt;element name="gmtCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gmtTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="increaseSystemCharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="passengerList" type="{http://createpolicyorderbypnr.version2_0.webservice.liantuo.com}ArrayOfWSPassenger"/>
 *         &lt;element name="paymentInfo" type="{http://createpolicyorderbypnr.version2_0.webservice.liantuo.com}PaymentInfo"/>
 *         &lt;element name="pnrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pnrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="policyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sequenceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="systemAlipayAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSPolicyOrder", propOrder = {
    "commisionInfo",
    "flightInfoList",
    "gmtCreated",
    "gmtTime",
    "increaseSystemCharge",
    "param1",
    "param2",
    "param3",
    "passengerList",
    "paymentInfo",
    "pnrNo",
    "pnrTxt",
    "policyId",
    "sequenceNo",
    "status",
    "systemAlipayAccount"
})
public class WSPolicyOrder {

    @XmlElement(required = true)
    protected String commisionInfo;
    @XmlElement(required = true)
    protected ArrayOfFlightInfo flightInfoList;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gmtCreated;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gmtTime;
    @XmlElementRef(name = "increaseSystemCharge", namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> increaseSystemCharge;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "param3", namespace = "http://createpolicyorderbypnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param3;
    @XmlElement(required = true)
    protected ArrayOfWSPassenger passengerList;
    @XmlElement(required = true)
    protected PaymentInfo paymentInfo;
    @XmlElement(required = true)
    protected String pnrNo;
    @XmlElement(required = true)
    protected String pnrTxt;
    protected int policyId;
    @XmlElement(required = true, nillable = true)
    protected String sequenceNo;
    protected int status;
    @XmlElement(required = true)
    protected String systemAlipayAccount;

    /**
     * Gets the value of the commisionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommisionInfo() {
        return commisionInfo;
    }

    /**
     * Sets the value of the commisionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommisionInfo(String value) {
        this.commisionInfo = value;
    }

    /**
     * Gets the value of the flightInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFlightInfo }
     *     
     */
    public ArrayOfFlightInfo getFlightInfoList() {
        return flightInfoList;
    }

    /**
     * Sets the value of the flightInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFlightInfo }
     *     
     */
    public void setFlightInfoList(ArrayOfFlightInfo value) {
        this.flightInfoList = value;
    }

    /**
     * Gets the value of the gmtCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGmtCreated() {
        return gmtCreated;
    }

    /**
     * Sets the value of the gmtCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGmtCreated(XMLGregorianCalendar value) {
        this.gmtCreated = value;
    }

    /**
     * Gets the value of the gmtTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGmtTime() {
        return gmtTime;
    }

    /**
     * Sets the value of the gmtTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGmtTime(XMLGregorianCalendar value) {
        this.gmtTime = value;
    }

    /**
     * Gets the value of the increaseSystemCharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIncreaseSystemCharge() {
        return increaseSystemCharge;
    }

    /**
     * Sets the value of the increaseSystemCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIncreaseSystemCharge(JAXBElement<String> value) {
        this.increaseSystemCharge = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam3() {
        return param3;
    }

    /**
     * Sets the value of the param3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam3(JAXBElement<String> value) {
        this.param3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the passengerList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfWSPassenger }
     *     
     */
    public ArrayOfWSPassenger getPassengerList() {
        return passengerList;
    }

    /**
     * Sets the value of the passengerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfWSPassenger }
     *     
     */
    public void setPassengerList(ArrayOfWSPassenger value) {
        this.passengerList = value;
    }

    /**
     * Gets the value of the paymentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentInfo }
     *     
     */
    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * Sets the value of the paymentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentInfo }
     *     
     */
    public void setPaymentInfo(PaymentInfo value) {
        this.paymentInfo = value;
    }

    /**
     * Gets the value of the pnrNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrNo() {
        return pnrNo;
    }

    /**
     * Sets the value of the pnrNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrNo(String value) {
        this.pnrNo = value;
    }

    /**
     * Gets the value of the pnrTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrTxt() {
        return pnrTxt;
    }

    /**
     * Sets the value of the pnrTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrTxt(String value) {
        this.pnrTxt = value;
    }

    /**
     * Gets the value of the policyId property.
     * 
     */
    public int getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     */
    public void setPolicyId(int value) {
        this.policyId = value;
    }

    /**
     * Gets the value of the sequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNo() {
        return sequenceNo;
    }

    /**
     * Sets the value of the sequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNo(String value) {
        this.sequenceNo = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the systemAlipayAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemAlipayAccount() {
        return systemAlipayAccount;
    }

    /**
     * Sets the value of the systemAlipayAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemAlipayAccount(String value) {
        this.systemAlipayAccount = value;
    }

}
