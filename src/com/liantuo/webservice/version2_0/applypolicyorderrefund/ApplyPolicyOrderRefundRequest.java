
package com.liantuo.webservice.version2_0.applypolicyorderrefund;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.liantuo.webservice.version2_0.SecurityCredential;


/**
 * <p>Java class for ApplyPolicyOrderRefundRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplyPolicyOrderRefundRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="credential" type="{http://version2_0.webservice.liantuo.com}SecurityCredential"/>
 *         &lt;element name="logMassage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderSequenceNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderSequenceNoType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundAnnexDirPath" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundNotifiedUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="segment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ticketNos" type="{http://applypolicyorderrefund.version2_0.webservice.liantuo.com}ArrayOfString"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyPolicyOrderRefundRequest", propOrder = {
    "actionType",
    "credential",
    "logMassage",
    "orderSequenceNo",
    "orderSequenceNoType",
    "param1",
    "param2",
    "param3",
    "refundAnnexDirPath",
    "refundNotifiedUrl",
    "segment",
    "ticketNos"
})
public class ApplyPolicyOrderRefundRequest {

    @XmlElement(required = true)
    protected String actionType;
    @XmlElement(required = true)
    protected SecurityCredential credential;
    @XmlElement(required = true)
    protected String logMassage;
    @XmlElement(required = true)
    protected String orderSequenceNo;
    @XmlElement(required = true)
    protected String orderSequenceNoType;
    @XmlElementRef(name = "param1", namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "param3", namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param3;
    @XmlElementRef(name = "refundAnnexDirPath", namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> refundAnnexDirPath;
    @XmlElement(required = true)
    protected String refundNotifiedUrl;
    @XmlElementRef(name = "segment", namespace = "http://applypolicyorderrefund.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> segment;
    @XmlElement(required = true)
    protected ArrayOfString ticketNos;

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionType(String value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the credential property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityCredential }
     *     
     */
    public SecurityCredential getCredential() {
        return credential;
    }

    /**
     * Sets the value of the credential property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityCredential }
     *     
     */
    public void setCredential(SecurityCredential value) {
        this.credential = value;
    }

    /**
     * Gets the value of the logMassage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogMassage() {
        return logMassage;
    }

    /**
     * Sets the value of the logMassage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogMassage(String value) {
        this.logMassage = value;
    }

    /**
     * Gets the value of the orderSequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderSequenceNo() {
        return orderSequenceNo;
    }

    /**
     * Sets the value of the orderSequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderSequenceNo(String value) {
        this.orderSequenceNo = value;
    }

    /**
     * Gets the value of the orderSequenceNoType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderSequenceNoType() {
        return orderSequenceNoType;
    }

    /**
     * Sets the value of the orderSequenceNoType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderSequenceNoType(String value) {
        this.orderSequenceNoType = value;
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam3() {
        return param3;
    }

    /**
     * Sets the value of the param3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam3(JAXBElement<String> value) {
        this.param3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the refundAnnexDirPath property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRefundAnnexDirPath() {
        return refundAnnexDirPath;
    }

    /**
     * Sets the value of the refundAnnexDirPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRefundAnnexDirPath(JAXBElement<String> value) {
        this.refundAnnexDirPath = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the refundNotifiedUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundNotifiedUrl() {
        return refundNotifiedUrl;
    }

    /**
     * Sets the value of the refundNotifiedUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundNotifiedUrl(String value) {
        this.refundNotifiedUrl = value;
    }

    /**
     * Gets the value of the segment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSegment() {
        return segment;
    }

    /**
     * Sets the value of the segment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSegment(JAXBElement<String> value) {
        this.segment = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ticketNos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getTicketNos() {
        return ticketNos;
    }

    /**
     * Sets the value of the ticketNos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setTicketNos(ArrayOfString value) {
        this.ticketNos = value;
    }

}
