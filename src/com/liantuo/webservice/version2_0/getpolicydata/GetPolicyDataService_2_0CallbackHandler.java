
/**
 * GetPolicyDataService_2_0CallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.2  Built on : Sep 06, 2010 (09:42:01 CEST)
 */

    package com.liantuo.webservice.version2_0.getpolicydata;

    /**
     *  GetPolicyDataService_2_0CallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class GetPolicyDataService_2_0CallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public GetPolicyDataService_2_0CallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public GetPolicyDataService_2_0CallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getPolicyData method
            * override this method for handling normal response from getPolicyData operation
            */
           public void receiveResultgetPolicyData(
                    com.liantuo.webservice.version2_0.getpolicydata.GetPolicyDataService_2_0Stub.GetPolicyDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPolicyData operation
           */
            public void receiveErrorgetPolicyData(java.lang.Exception e) {
            }
                


    }
    