
package com.liantuo.webservice.version2_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityCredentialParam2_QNAME = new QName("http://version2_0.webservice.liantuo.com", "param2");
    private final static QName _SecurityCredentialParam1_QNAME = new QName("http://version2_0.webservice.liantuo.com", "param1");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SecurityCredential }
     * 
     */
    public SecurityCredential createSecurityCredential() {
        return new SecurityCredential();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://version2_0.webservice.liantuo.com", name = "param2", scope = SecurityCredential.class)
    public JAXBElement<String> createSecurityCredentialParam2(String value) {
        return new JAXBElement<String>(_SecurityCredentialParam2_QNAME, String.class, SecurityCredential.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://version2_0.webservice.liantuo.com", name = "param1", scope = SecurityCredential.class)
    public JAXBElement<String> createSecurityCredentialParam1(String value) {
        return new JAXBElement<String>(_SecurityCredentialParam1_QNAME, String.class, SecurityCredential.class, value);
    }

}
