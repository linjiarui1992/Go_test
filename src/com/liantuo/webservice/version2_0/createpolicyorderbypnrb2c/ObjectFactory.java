
package com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreatePolicyOrderByPNRB2CReplyParam1_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "param1");
    private final static QName _CreatePolicyOrderByPNRB2CReplyOrder_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "order");
    private final static QName _CreatePolicyOrderByPNRB2CReplyParam2_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "param2");
    private final static QName _CreatePolicyOrderByPNRB2CReplyReturnCode_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "returnCode");
    private final static QName _CreatePolicyOrderByPNRB2CReplyReturnMessage_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "returnMessage");
    private final static QName _WSPassengerParam3_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "param3");
    private final static QName _PaymentInfoTradeNo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "tradeNo");
    private final static QName _PaymentInfoSettlePrice_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "settlePrice");
    private final static QName _PaymentInfoWebPayPrice_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "webPayPrice");
    private final static QName _PaymentInfoWebPayCharge_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "webPayCharge");
    private final static QName _PaymentInfoWebCharge_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "webCharge");
    private final static QName _PaymentInfoPaymentUrl_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "paymentUrl");
    private final static QName _PaymentInfoPayerAccount_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "payerAccount");
    private final static QName _FlightInfoPlaneModel_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "planeModel");
    private final static QName _FlightInfoDepCode_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "depCode");
    private final static QName _FlightInfoFlightNo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "flightNo");
    private final static QName _FlightInfoDepDate_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "depDate");
    private final static QName _FlightInfoDepTime_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "depTime");
    private final static QName _FlightInfoArrCode_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "arrCode");
    private final static QName _FlightInfoSeatClass_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "seatClass");
    private final static QName _FlightInfoArrTime_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "arrTime");
    private final static QName _CreatePolicyOrderByPNRB2CRequestIsPay_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "isPay");
    private final static QName _CreatePolicyOrderByPNRB2CRequestPayBank_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "payBank");
    private final static QName _CreatePolicyOrderByPNRB2CRequestB2CCreatorCn_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "b2cCreatorCn");
    private final static QName _CreatePolicyOrderByPNRB2CRequestPayType_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "payType");
    private final static QName _WSPolicyOrderPassengerList_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "passengerList");
    private final static QName _WSPolicyOrderPaymentInfo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "paymentInfo");
    private final static QName _WSPolicyOrderPnrNo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "pnrNo");
    private final static QName _WSPolicyOrderFlightInfoList_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "flightInfoList");
    private final static QName _WSPolicyOrderSequenceNo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "sequenceNo");
    private final static QName _WSPolicyOrderPnrTxt_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "pnrTxt");
    private final static QName _WSPolicyOrderCommisionInfo_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "commisionInfo");
    private final static QName _WSPolicyOrderIncreaseSystemCharge_QNAME = new QName("http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", "increaseSystemCharge");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRB2C }
     * 
     */
    public CreatePolicyOrderByPNRB2C createCreatePolicyOrderByPNRB2C() {
        return new CreatePolicyOrderByPNRB2C();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRB2CReply }
     * 
     */
    public CreatePolicyOrderByPNRB2CReply createCreatePolicyOrderByPNRB2CReply() {
        return new CreatePolicyOrderByPNRB2CReply();
    }

    /**
     * Create an instance of {@link PaymentInfo }
     * 
     */
    public PaymentInfo createPaymentInfo() {
        return new PaymentInfo();
    }

    /**
     * Create an instance of {@link ArrayOfWSPassenger }
     * 
     */
    public ArrayOfWSPassenger createArrayOfWSPassenger() {
        return new ArrayOfWSPassenger();
    }

    /**
     * Create an instance of {@link WSPassenger }
     * 
     */
    public WSPassenger createWSPassenger() {
        return new WSPassenger();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRB2CResponse }
     * 
     */
    public CreatePolicyOrderByPNRB2CResponse createCreatePolicyOrderByPNRB2CResponse() {
        return new CreatePolicyOrderByPNRB2CResponse();
    }

    /**
     * Create an instance of {@link FlightInfo }
     * 
     */
    public FlightInfo createFlightInfo() {
        return new FlightInfo();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRB2CRequest }
     * 
     */
    public CreatePolicyOrderByPNRB2CRequest createCreatePolicyOrderByPNRB2CRequest() {
        return new CreatePolicyOrderByPNRB2CRequest();
    }

    /**
     * Create an instance of {@link ArrayOfFlightInfo }
     * 
     */
    public ArrayOfFlightInfo createArrayOfFlightInfo() {
        return new ArrayOfFlightInfo();
    }

    /**
     * Create an instance of {@link WSPolicyOrder }
     * 
     */
    public WSPolicyOrder createWSPolicyOrder() {
        return new WSPolicyOrder();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRB2CReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CReplyParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, CreatePolicyOrderByPNRB2CReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSPolicyOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "order", scope = CreatePolicyOrderByPNRB2CReply.class)
    public JAXBElement<WSPolicyOrder> createCreatePolicyOrderByPNRB2CReplyOrder(WSPolicyOrder value) {
        return new JAXBElement<WSPolicyOrder>(_CreatePolicyOrderByPNRB2CReplyOrder_QNAME, WSPolicyOrder.class, CreatePolicyOrderByPNRB2CReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRB2CReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CReplyParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, CreatePolicyOrderByPNRB2CReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "returnCode", scope = CreatePolicyOrderByPNRB2CReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CReplyReturnCode(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyReturnCode_QNAME, String.class, CreatePolicyOrderByPNRB2CReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "returnMessage", scope = CreatePolicyOrderByPNRB2CReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CReplyReturnMessage(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyReturnMessage_QNAME, String.class, CreatePolicyOrderByPNRB2CReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param3", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "tradeNo", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoTradeNo(String value) {
        return new JAXBElement<String>(_PaymentInfoTradeNo_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "settlePrice", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoSettlePrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoSettlePrice_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "webPayPrice", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoWebPayPrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoWebPayPrice_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "webPayCharge", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoWebPayCharge(Double value) {
        return new JAXBElement<Double>(_PaymentInfoWebPayCharge_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "webCharge", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoWebCharge(Double value) {
        return new JAXBElement<Double>(_PaymentInfoWebCharge_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "paymentUrl", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPaymentUrl(String value) {
        return new JAXBElement<String>(_PaymentInfoPaymentUrl_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "payerAccount", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPayerAccount(String value) {
        return new JAXBElement<String>(_PaymentInfoPayerAccount_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param3", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "planeModel", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoPlaneModel(String value) {
        return new JAXBElement<String>(_FlightInfoPlaneModel_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "depCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepCode(String value) {
        return new JAXBElement<String>(_FlightInfoDepCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "flightNo", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoFlightNo(String value) {
        return new JAXBElement<String>(_FlightInfoFlightNo_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "depDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepDate(String value) {
        return new JAXBElement<String>(_FlightInfoDepDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "depTime", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepTime(String value) {
        return new JAXBElement<String>(_FlightInfoDepTime_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "arrCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrCode(String value) {
        return new JAXBElement<String>(_FlightInfoArrCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "seatClass", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoSeatClass(String value) {
        return new JAXBElement<String>(_FlightInfoSeatClass_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "arrTime", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrTime(String value) {
        return new JAXBElement<String>(_FlightInfoArrTime_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param3", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "isPay", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestIsPay(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CRequestIsPay_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "payBank", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestPayBank(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CRequestPayBank_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "b2cCreatorCn", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestB2CCreatorCn(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CRequestB2CCreatorCn_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "payType", scope = CreatePolicyOrderByPNRB2CRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRB2CRequestPayType(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CRequestPayType_QNAME, String.class, CreatePolicyOrderByPNRB2CRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param3", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfWSPassenger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "passengerList", scope = WSPolicyOrder.class)
    public JAXBElement<ArrayOfWSPassenger> createWSPolicyOrderPassengerList(ArrayOfWSPassenger value) {
        return new JAXBElement<ArrayOfWSPassenger>(_WSPolicyOrderPassengerList_QNAME, ArrayOfWSPassenger.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param1", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam1(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam1_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "param2", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam2(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRB2CReplyParam2_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "paymentInfo", scope = WSPolicyOrder.class)
    public JAXBElement<PaymentInfo> createWSPolicyOrderPaymentInfo(PaymentInfo value) {
        return new JAXBElement<PaymentInfo>(_WSPolicyOrderPaymentInfo_QNAME, PaymentInfo.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "pnrNo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderPnrNo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrNo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFlightInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "flightInfoList", scope = WSPolicyOrder.class)
    public JAXBElement<ArrayOfFlightInfo> createWSPolicyOrderFlightInfoList(ArrayOfFlightInfo value) {
        return new JAXBElement<ArrayOfFlightInfo>(_WSPolicyOrderFlightInfoList_QNAME, ArrayOfFlightInfo.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "sequenceNo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderSequenceNo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderSequenceNo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "pnrTxt", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderPnrTxt(String value) {
        return new JAXBElement<String>(_WSPolicyOrderPnrTxt_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "commisionInfo", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderCommisionInfo(String value) {
        return new JAXBElement<String>(_WSPolicyOrderCommisionInfo_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", name = "increaseSystemCharge", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderIncreaseSystemCharge(String value) {
        return new JAXBElement<String>(_WSPolicyOrderIncreaseSystemCharge_QNAME, String.class, WSPolicyOrder.class, value);
    }

}
