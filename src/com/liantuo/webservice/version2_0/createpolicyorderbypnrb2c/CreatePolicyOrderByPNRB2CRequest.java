
package com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.liantuo.webservice.version2_0.SecurityCredential;


/**
 * <p>Java class for CreatePolicyOrderByPNRB2CRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreatePolicyOrderByPNRB2CRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="b2cCreatorCn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="credential" type="{http://version2_0.webservice.liantuo.com}SecurityCredential"/>
 *         &lt;element name="isPay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notifiedUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payBank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentReturnUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pnrNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="policyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="webCharge" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="webPayPrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatePolicyOrderByPNRB2CRequest", propOrder = {
    "b2CCreatorCn",
    "credential",
    "isPay",
    "notifiedUrl",
    "param1",
    "param2",
    "param3",
    "payBank",
    "payType",
    "paymentReturnUrl",
    "pnrNo",
    "policyId",
    "webCharge",
    "webPayPrice"
})
public class CreatePolicyOrderByPNRB2CRequest {

    @XmlElementRef(name = "b2cCreatorCn", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> b2CCreatorCn;
    @XmlElement(required = true)
    protected SecurityCredential credential;
    @XmlElementRef(name = "isPay", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> isPay;
    @XmlElement(required = true)
    protected String notifiedUrl;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "param3", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param3;
    @XmlElementRef(name = "payBank", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> payBank;
    @XmlElementRef(name = "payType", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> payType;
    @XmlElement(required = true)
    protected String paymentReturnUrl;
    @XmlElement(required = true)
    protected String pnrNo;
    protected int policyId;
    @XmlElement(required = true)
    protected String webCharge;
    @XmlElement(required = true)
    protected String webPayPrice;

    /**
     * Gets the value of the b2CCreatorCn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getB2CCreatorCn() {
        return b2CCreatorCn;
    }

    /**
     * Sets the value of the b2CCreatorCn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setB2CCreatorCn(JAXBElement<String> value) {
        this.b2CCreatorCn = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the credential property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityCredential }
     *     
     */
    public SecurityCredential getCredential() {
        return credential;
    }

    /**
     * Sets the value of the credential property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityCredential }
     *     
     */
    public void setCredential(SecurityCredential value) {
        this.credential = value;
    }

    /**
     * Gets the value of the isPay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIsPay() {
        return isPay;
    }

    /**
     * Sets the value of the isPay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIsPay(JAXBElement<String> value) {
        this.isPay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the notifiedUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifiedUrl() {
        return notifiedUrl;
    }

    /**
     * Sets the value of the notifiedUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifiedUrl(String value) {
        this.notifiedUrl = value;
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam3() {
        return param3;
    }

    /**
     * Sets the value of the param3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam3(JAXBElement<String> value) {
        this.param3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the payBank property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayBank() {
        return payBank;
    }

    /**
     * Sets the value of the payBank property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayBank(JAXBElement<String> value) {
        this.payBank = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the payType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayType() {
        return payType;
    }

    /**
     * Sets the value of the payType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayType(JAXBElement<String> value) {
        this.payType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the paymentReturnUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReturnUrl() {
        return paymentReturnUrl;
    }

    /**
     * Sets the value of the paymentReturnUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReturnUrl(String value) {
        this.paymentReturnUrl = value;
    }

    /**
     * Gets the value of the pnrNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrNo() {
        return pnrNo;
    }

    /**
     * Sets the value of the pnrNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrNo(String value) {
        this.pnrNo = value;
    }

    /**
     * Gets the value of the policyId property.
     * 
     */
    public int getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     */
    public void setPolicyId(int value) {
        this.policyId = value;
    }

    /**
     * Gets the value of the webCharge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebCharge() {
        return webCharge;
    }

    /**
     * Sets the value of the webCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebCharge(String value) {
        this.webCharge = value;
    }

    /**
     * Gets the value of the webPayPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebPayPrice() {
        return webPayPrice;
    }

    /**
     * Sets the value of the webPayPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebPayPrice(String value) {
        this.webPayPrice = value;
    }

}
