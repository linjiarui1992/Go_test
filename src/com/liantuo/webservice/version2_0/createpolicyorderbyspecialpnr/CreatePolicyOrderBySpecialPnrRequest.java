
package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.liantuo.webservice.version2_0.SecurityCredential;


/**
 * <p>Java class for CreatePolicyOrderBySpecialPnrRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreatePolicyOrderBySpecialPnrRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BPnrNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BPnrTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="b2cCreatorCn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="buildPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="credential" type="{http://version2_0.webservice.liantuo.com}SecurityCredential"/>
 *         &lt;element name="isGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="notifiedUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oilPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="passengers" type="{http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com}ArrayOfPassengerInfo" minOccurs="0"/>
 *         &lt;element name="pataTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentReturnUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pnrNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pnrTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="routeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="segments" type="{http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com}ArrayOfSegmentInfo" minOccurs="0"/>
 *         &lt;element name="ticketPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatePolicyOrderBySpecialPnrRequest", propOrder = {
    "bPnrNo",
    "bPnrTxt",
    "b2CCreatorCn",
    "buildPrice",
    "credential",
    "isGroup",
    "notifiedUrl",
    "oilPrice",
    "param1",
    "param2",
    "param3",
    "passengers",
    "pataTxt",
    "paymentReturnUrl",
    "pnrNo",
    "pnrTxt",
    "policyId",
    "routeType",
    "segments",
    "ticketPrice"
})
public class CreatePolicyOrderBySpecialPnrRequest {

    @XmlElementRef(name = "BPnrNo", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> bPnrNo;
    @XmlElementRef(name = "BPnrTxt", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> bPnrTxt;
    @XmlElementRef(name = "b2cCreatorCn", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> b2CCreatorCn;
    @XmlElementRef(name = "buildPrice", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> buildPrice;
    @XmlElement(required = true)
    protected SecurityCredential credential;
    @XmlElementRef(name = "isGroup", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Boolean> isGroup;
    @XmlElementRef(name = "notifiedUrl", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> notifiedUrl;
    @XmlElementRef(name = "oilPrice", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> oilPrice;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "param3", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param3;
    @XmlElementRef(name = "passengers", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<ArrayOfPassengerInfo> passengers;
    @XmlElementRef(name = "pataTxt", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> pataTxt;
    @XmlElementRef(name = "paymentReturnUrl", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> paymentReturnUrl;
    @XmlElementRef(name = "pnrNo", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> pnrNo;
    @XmlElementRef(name = "pnrTxt", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> pnrTxt;
    @XmlElementRef(name = "policyId", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Integer> policyId;
    @XmlElementRef(name = "routeType", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> routeType;
    @XmlElementRef(name = "segments", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<ArrayOfSegmentInfo> segments;
    @XmlElementRef(name = "ticketPrice", namespace = "http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> ticketPrice;

    /**
     * Gets the value of the bPnrNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBPnrNo() {
        return bPnrNo;
    }

    /**
     * Sets the value of the bPnrNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBPnrNo(JAXBElement<String> value) {
        this.bPnrNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the bPnrTxt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBPnrTxt() {
        return bPnrTxt;
    }

    /**
     * Sets the value of the bPnrTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBPnrTxt(JAXBElement<String> value) {
        this.bPnrTxt = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the b2CCreatorCn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getB2CCreatorCn() {
        return b2CCreatorCn;
    }

    /**
     * Sets the value of the b2CCreatorCn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setB2CCreatorCn(JAXBElement<String> value) {
        this.b2CCreatorCn = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the buildPrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBuildPrice() {
        return buildPrice;
    }

    /**
     * Sets the value of the buildPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBuildPrice(JAXBElement<String> value) {
        this.buildPrice = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the credential property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityCredential }
     *     
     */
    public SecurityCredential getCredential() {
        return credential;
    }

    /**
     * Sets the value of the credential property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityCredential }
     *     
     */
    public void setCredential(SecurityCredential value) {
        this.credential = value;
    }

    /**
     * Gets the value of the isGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsGroup() {
        return isGroup;
    }

    /**
     * Sets the value of the isGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsGroup(JAXBElement<Boolean> value) {
        this.isGroup = ((JAXBElement<Boolean> ) value);
    }

    /**
     * Gets the value of the notifiedUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotifiedUrl() {
        return notifiedUrl;
    }

    /**
     * Sets the value of the notifiedUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotifiedUrl(JAXBElement<String> value) {
        this.notifiedUrl = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the oilPrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOilPrice() {
        return oilPrice;
    }

    /**
     * Sets the value of the oilPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOilPrice(JAXBElement<String> value) {
        this.oilPrice = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam3() {
        return param3;
    }

    /**
     * Sets the value of the param3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam3(JAXBElement<String> value) {
        this.param3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the passengers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPassengerInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPassengerInfo> getPassengers() {
        return passengers;
    }

    /**
     * Sets the value of the passengers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPassengerInfo }{@code >}
     *     
     */
    public void setPassengers(JAXBElement<ArrayOfPassengerInfo> value) {
        this.passengers = ((JAXBElement<ArrayOfPassengerInfo> ) value);
    }

    /**
     * Gets the value of the pataTxt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPataTxt() {
        return pataTxt;
    }

    /**
     * Sets the value of the pataTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPataTxt(JAXBElement<String> value) {
        this.pataTxt = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the paymentReturnUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentReturnUrl() {
        return paymentReturnUrl;
    }

    /**
     * Sets the value of the paymentReturnUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentReturnUrl(JAXBElement<String> value) {
        this.paymentReturnUrl = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the pnrNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPnrNo() {
        return pnrNo;
    }

    /**
     * Sets the value of the pnrNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPnrNo(JAXBElement<String> value) {
        this.pnrNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the pnrTxt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPnrTxt() {
        return pnrTxt;
    }

    /**
     * Sets the value of the pnrTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPnrTxt(JAXBElement<String> value) {
        this.pnrTxt = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the policyId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPolicyId(JAXBElement<Integer> value) {
        this.policyId = ((JAXBElement<Integer> ) value);
    }

    /**
     * Gets the value of the routeType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRouteType() {
        return routeType;
    }

    /**
     * Sets the value of the routeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRouteType(JAXBElement<String> value) {
        this.routeType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the segments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSegmentInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSegmentInfo> getSegments() {
        return segments;
    }

    /**
     * Sets the value of the segments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSegmentInfo }{@code >}
     *     
     */
    public void setSegments(JAXBElement<ArrayOfSegmentInfo> value) {
        this.segments = ((JAXBElement<ArrayOfSegmentInfo> ) value);
    }

    /**
     * Gets the value of the ticketPrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTicketPrice() {
        return ticketPrice;
    }

    /**
     * Sets the value of the ticketPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTicketPrice(JAXBElement<String> value) {
        this.ticketPrice = ((JAXBElement<String> ) value);
    }

}
