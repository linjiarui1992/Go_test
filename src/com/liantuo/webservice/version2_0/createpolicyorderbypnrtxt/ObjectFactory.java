
package com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FlightInfoArrCode_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "arrCode");
    private final static QName _FlightInfoSeatClass_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "seatClass");
    private final static QName _FlightInfoPlaneModel_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "planeModel");
    private final static QName _FlightInfoDepCode_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "depCode");
    private final static QName _FlightInfoParam1_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "param1");
    private final static QName _FlightInfoFlightNo_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "flightNo");
    private final static QName _FlightInfoDepDate_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "depDate");
    private final static QName _FlightInfoArrDate_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "arrDate");
    private final static QName _WSPassengerParam3_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "param3");
    private final static QName _WSPassengerParam2_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "param2");
    private final static QName _CreatePolicyOrderByPNRTxtRequestB2CCreatorCn_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "b2cCreatorCn");
    private final static QName _PaymentInfoPaymentUrl_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "paymentUrl");
    private final static QName _PaymentInfoPayerAccount_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "payerAccount");
    private final static QName _PaymentInfoTradeNo_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "tradeNo");
    private final static QName _PaymentInfoSettlePrice_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "settlePrice");
    private final static QName _CreatePolicyOrderByPNRTxtReplyReturnMessage_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "returnMessage");
    private final static QName _CreatePolicyOrderByPNRTxtReplyReturnCode_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "returnCode");
    private final static QName _WSPolicyOrderIncreaseSystemCharge_QNAME = new QName("http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", "increaseSystemCharge");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FlightInfo }
     * 
     */
    public FlightInfo createFlightInfo() {
        return new FlightInfo();
    }

    /**
     * Create an instance of {@link WSPassenger }
     * 
     */
    public WSPassenger createWSPassenger() {
        return new WSPassenger();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRTxtRequest }
     * 
     */
    public CreatePolicyOrderByPNRTxtRequest createCreatePolicyOrderByPNRTxtRequest() {
        return new CreatePolicyOrderByPNRTxtRequest();
    }

    /**
     * Create an instance of {@link ArrayOfWSPassenger }
     * 
     */
    public ArrayOfWSPassenger createArrayOfWSPassenger() {
        return new ArrayOfWSPassenger();
    }

    /**
     * Create an instance of {@link ArrayOfFlightInfo }
     * 
     */
    public ArrayOfFlightInfo createArrayOfFlightInfo() {
        return new ArrayOfFlightInfo();
    }

    /**
     * Create an instance of {@link PaymentInfo }
     * 
     */
    public PaymentInfo createPaymentInfo() {
        return new PaymentInfo();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRTxtResponse }
     * 
     */
    public CreatePolicyOrderByPNRTxtResponse createCreatePolicyOrderByPNRTxtResponse() {
        return new CreatePolicyOrderByPNRTxtResponse();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRTxt }
     * 
     */
    public CreatePolicyOrderByPNRTxt createCreatePolicyOrderByPNRTxt() {
        return new CreatePolicyOrderByPNRTxt();
    }

    /**
     * Create an instance of {@link CreatePolicyOrderByPNRTxtReply }
     * 
     */
    public CreatePolicyOrderByPNRTxtReply createCreatePolicyOrderByPNRTxtReply() {
        return new CreatePolicyOrderByPNRTxtReply();
    }

    /**
     * Create an instance of {@link WSPolicyOrder }
     * 
     */
    public WSPolicyOrder createWSPolicyOrder() {
        return new WSPolicyOrder();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "arrCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrCode(String value) {
        return new JAXBElement<String>(_FlightInfoArrCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "seatClass", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoSeatClass(String value) {
        return new JAXBElement<String>(_FlightInfoSeatClass_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "planeModel", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoPlaneModel(String value) {
        return new JAXBElement<String>(_FlightInfoPlaneModel_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "depCode", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepCode(String value) {
        return new JAXBElement<String>(_FlightInfoDepCode_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "flightNo", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoFlightNo(String value) {
        return new JAXBElement<String>(_FlightInfoFlightNo_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "depDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoDepDate(String value) {
        return new JAXBElement<String>(_FlightInfoDepDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "arrDate", scope = FlightInfo.class)
    public JAXBElement<String> createFlightInfoArrDate(String value) {
        return new JAXBElement<String>(_FlightInfoArrDate_QNAME, String.class, FlightInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param3", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param2", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam2(String value) {
        return new JAXBElement<String>(_WSPassengerParam2_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = WSPassenger.class)
    public JAXBElement<String> createWSPassengerParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, WSPassenger.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "b2cCreatorCn", scope = CreatePolicyOrderByPNRTxtRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtRequestB2CCreatorCn(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRTxtRequestB2CCreatorCn_QNAME, String.class, CreatePolicyOrderByPNRTxtRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRTxtRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtRequestParam2(String value) {
        return new JAXBElement<String>(_WSPassengerParam2_QNAME, String.class, CreatePolicyOrderByPNRTxtRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRTxtRequest.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtRequestParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, CreatePolicyOrderByPNRTxtRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "paymentUrl", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPaymentUrl(String value) {
        return new JAXBElement<String>(_PaymentInfoPaymentUrl_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "payerAccount", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoPayerAccount(String value) {
        return new JAXBElement<String>(_PaymentInfoPayerAccount_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "tradeNo", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoTradeNo(String value) {
        return new JAXBElement<String>(_PaymentInfoTradeNo_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = PaymentInfo.class)
    public JAXBElement<String> createPaymentInfoParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "settlePrice", scope = PaymentInfo.class)
    public JAXBElement<Double> createPaymentInfoSettlePrice(Double value) {
        return new JAXBElement<Double>(_PaymentInfoSettlePrice_QNAME, Double.class, PaymentInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "returnMessage", scope = CreatePolicyOrderByPNRTxtReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtReplyReturnMessage(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRTxtReplyReturnMessage_QNAME, String.class, CreatePolicyOrderByPNRTxtReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param2", scope = CreatePolicyOrderByPNRTxtReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtReplyParam2(String value) {
        return new JAXBElement<String>(_WSPassengerParam2_QNAME, String.class, CreatePolicyOrderByPNRTxtReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = CreatePolicyOrderByPNRTxtReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtReplyParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, CreatePolicyOrderByPNRTxtReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "returnCode", scope = CreatePolicyOrderByPNRTxtReply.class)
    public JAXBElement<String> createCreatePolicyOrderByPNRTxtReplyReturnCode(String value) {
        return new JAXBElement<String>(_CreatePolicyOrderByPNRTxtReplyReturnCode_QNAME, String.class, CreatePolicyOrderByPNRTxtReply.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "increaseSystemCharge", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderIncreaseSystemCharge(String value) {
        return new JAXBElement<String>(_WSPolicyOrderIncreaseSystemCharge_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param3", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam3(String value) {
        return new JAXBElement<String>(_WSPassengerParam3_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param2", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam2(String value) {
        return new JAXBElement<String>(_WSPassengerParam2_QNAME, String.class, WSPolicyOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", name = "param1", scope = WSPolicyOrder.class)
    public JAXBElement<String> createWSPolicyOrderParam1(String value) {
        return new JAXBElement<String>(_FlightInfoParam1_QNAME, String.class, WSPolicyOrder.class, value);
    }

}
