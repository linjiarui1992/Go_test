
package com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.liantuo.webservice.version2_0.SecurityCredential;


/**
 * <p>Java class for CreatePolicyOrderByPNRTxtRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreatePolicyOrderByPNRTxtRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="b2cCreatorCn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="credential" type="{http://version2_0.webservice.liantuo.com}SecurityCredential"/>
 *         &lt;element name="notifiedUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pataTxt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentReturnUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pnrTxt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="policyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreatePolicyOrderByPNRTxtRequest", propOrder = {
    "b2CCreatorCn",
    "credential",
    "notifiedUrl",
    "param1",
    "param2",
    "pataTxt",
    "paymentReturnUrl",
    "pnrTxt",
    "policyId"
})
public class CreatePolicyOrderByPNRTxtRequest {

    @XmlElementRef(name = "b2cCreatorCn", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> b2CCreatorCn;
    @XmlElement(required = true)
    protected SecurityCredential credential;
    @XmlElement(required = true)
    protected String notifiedUrl;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElement(required = true)
    protected String pataTxt;
    @XmlElement(required = true)
    protected String paymentReturnUrl;
    @XmlElement(required = true)
    protected String pnrTxt;
    protected int policyId;

    /**
     * Gets the value of the b2CCreatorCn property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getB2CCreatorCn() {
        return b2CCreatorCn;
    }

    /**
     * Sets the value of the b2CCreatorCn property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setB2CCreatorCn(JAXBElement<String> value) {
        this.b2CCreatorCn = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the credential property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityCredential }
     *     
     */
    public SecurityCredential getCredential() {
        return credential;
    }

    /**
     * Sets the value of the credential property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityCredential }
     *     
     */
    public void setCredential(SecurityCredential value) {
        this.credential = value;
    }

    /**
     * Gets the value of the notifiedUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotifiedUrl() {
        return notifiedUrl;
    }

    /**
     * Sets the value of the notifiedUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotifiedUrl(String value) {
        this.notifiedUrl = value;
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the pataTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPataTxt() {
        return pataTxt;
    }

    /**
     * Sets the value of the pataTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPataTxt(String value) {
        this.pataTxt = value;
    }

    /**
     * Gets the value of the paymentReturnUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReturnUrl() {
        return paymentReturnUrl;
    }

    /**
     * Sets the value of the paymentReturnUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReturnUrl(String value) {
        this.paymentReturnUrl = value;
    }

    /**
     * Gets the value of the pnrTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrTxt() {
        return pnrTxt;
    }

    /**
     * Sets the value of the pnrTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrTxt(String value) {
        this.pnrTxt = value;
    }

    /**
     * Gets the value of the policyId property.
     * 
     */
    public int getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     */
    public void setPolicyId(int value) {
        this.policyId = value;
    }

}
